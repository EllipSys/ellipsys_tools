c     $Rev:: 2           $: Revision of last commit
c     $Author:: frza     $: Author of last commit
c     $Date:: 2011-03-23#$: Date of last commit
      program Mesh2Fieldview2D
      implicit none
      real(kind=8),dimension(:,:,:),allocatable::x,y
      real(kind=8),dimension(:,:,:),allocatable::var
      integer,dimension(:,:,:),allocatable::attr
      integer::i,j,n,bsize,ni,nblock

      open(unit=20,file='grid.x2d')
      read(20,*) bsize,nblock
      ni=bsize+1
      allocate(x(ni,ni,nblock),y(ni,ni,nblock)
     &        ,attr(ni,ni,nblock),var(ni,ni,nblock))

      do n=1,nblock
      do j=1,bsize+1
      do i=1,bsize+1
      read(20,*)attr(i,j,n),x(i,j,n),y(i,j,n)
      enddo;enddo
      enddo
      close(20)

c----------------------------------------------------------------------
c     Place whichever variable you please in the variable "var"       c
c----------------------------------------------------------------------

     
      call plotfieldview(bsize,nblock,x,y,attr,var)

      end

      subroutine plotfieldview(bsize,nb,x,y,attr,var)
c----------------------------------------------------------------------
c     make plot3d grid and boundary file
c----------------------------------------------------------------------
      implicit none
      integer,parameter::idp4=4
      integer::bsize,nb,i,j,f,n,imin,imax,jmin,jmax,l,lmin,lmax,icc,jcc
     &        ,icca,jcca,k,nvar
      integer::attrmin,attrmax,nwallblock
      logical::bctrue=.false.
      real(kind=8),dimension(bsize+1,bsize+1,nb)::x,y,var
      integer,dimension(bsize+1,bsize+1,nb)::attr


c-----make FieldView plot3d unformatted grid file
      print*,'writing grid.xyz ...'
      open(unit=4,file='grid.xyz',form='unformatted')
      write(4)nb
      write(4)(bsize+1,bsize+1,2,n=1,nb)
      do n=1,nb
      write(4)(((real(x(i,j,n),idp4),i=1,bsize+1),j=1,bsize+1),k=1,2),
     &        (((real(y(i,j,n),idp4),i=1,bsize+1),j=1,bsize+1),k=1,2),
     &        (((real((k-1)*0.001,idp4),i=1,bsize+1),j=1,bsize+1),k=1,2)
      enddo
      close(4)
c---- write scalar to function file ------------------------------------
      print*,'writing grid.f ...'
      open(unit=5,file='grid.f',form='unformatted')
      nvar=1
      write(5)nb
      write(5)(bsize+1,bsize+1,2,nvar,n=1,nb)
      do n=1,nb
      write(5)(((real(attr(i,j,n),idp4),i=1,bsize+1),j=1,bsize+1),k=1,2)
      enddo
      close(5)
c-----------------------------------------------------------------------
      print*,'writing grid.nam ...'
      open(unit=6,file='grid.nam')
      write(6,*)'attr'
      close(6)
c-----------------------------------------------------------------------

c---- make FieldView plot3d fvbnd file ---------------------------------
      open(unit=4,file='grid.xyz.fvbnd',form='formatted')
      write(4,*)'FVBND 1 4'
      write(4,*)' wall'
      write(4,*)' inlet'
      write(4,*)' outlet'
      write(4,*)' cyclic'
      write(4,*)' symmetry'
c-----------------------------------------------------------------------
      write(4,*)'BOUNDARIES'

c--------------------------------------------- boundary regions -------
c---------------------------------------------walls --------------------
      nwallblock=0
      do i=1,5
      select case(i)
       case(1) 
         attrmin=101;attrmax=200
       case(2) 
         attrmin=201;attrmax=300
       case(3)  
         attrmin=401;attrmax=500
       case(4) 
         attrmin=501;attrmax=600
       case(5) 
         attrmin=601;attrmax=700
      end select 
      BLOCKS: do n=1,nb
        FACE: do f=1,4
c--------------------------------------------- loop over face ----------
        do l=1,bsize+1
C-----------------------------------------------------------------------
          select case(f)
          case(1)
            icc=1;jcc=l
          case(2)
            icc=bsize+1;jcc=l
          case(3)
            icc=l;jcc=1
          case(4)
            icc=l;jcc=bsize+1
          end select
          if(.not.bctrue.and.l.lt.bsize+1.and.
     &                      (attr(icc,jcc,n).ge.attrmin.and.
     &                       attr(icc,jcc,n).le.attrmax))then
             bctrue=.true.
             lmin=l
          endif
          if(bctrue.and.((attr(icc,jcc,n).lt.attrmin.or.
     &                   attr(icc,jcc,n).gt.attrmax).or
     &                   .l.gt.bsize))then
            bctrue=.false.
            lmax=l
c-----------eventual remove cell at upper end 
            if(attr(icc,jcc,n).gt.attrmax.or
     &        .attr(icc,jcc,n).lt.100)lmax=l-1
c-----------check lower end for additional cell
            if(lmin.gt.1)then
            select case(f)
            case(1)
              icca=1;jcca=lmin-1
            case(2)
              icca=bsize+1;jcca=lmin-1
            case(3)
              icca=lmin-1;jcca=1
            case(4)
              icca=lmin-1;jcca=bsize+1
            end select
            if(attr(icca,jcca,n).lt.attrmin)lmin=lmin-1
            endif
c------------------ write segment to file ------------------------------
             if(lmax.gt.lmin)then
             select case(f)
             case(1)
               write(4,'(8i5,1a4,i4)')i,n,1,1,lmin,lmax,1,2
     &                          ,'F',(-1)**f
             case(2)
                write(4,'(8i5,1a4,i4)')i,n,bsize+1,bsize+1,lmin,lmax,1,2
     &                         ,'F',(-1)**f
             case(3)
                write(4,'(8i5,1a4,i4)')i,n,lmin,lmax,1,1,1,2
     &                         ,'F',(-1)**f
             case(4)
               write(4,'(8i5,1a4,i4)')i,n,lmin,lmax,bsize+1,bsize+1,1,2
     &                         ,'F',(-1)**f
             end select
             endif
             lmin=bsize+1
           endif
         end do
      end do FACE
      end do BLOCKS
c-----------------------------------------------------------------------
      end do
      close(4)
c----------------------------------------------------------------------
      return
      end
