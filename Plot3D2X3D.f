      program Plot3D2X3D
      implicit none
      integer ni,nj,nk,bsize,nblock,nim,njm,nkm
      integer i,j,k,n,ii,jj,kk,nn
      integer nbi,nbj,nbk,istart,jstart,kstart,io,jo,ko
      integer dim,nbs
      integer,dimension(:),allocatable::nis,njs,nks
      real(kind=8),dimension(:,:,:,:),allocatable::x,y,z,xs,ys,zs
      integer,dimension(:,:,:,:),allocatable::attr

c-------Write Plot3D file
      open(unit=10,file='grid.plot3d')
      read(10,*)nbs
      allocate(nis(nbs),njs(nbs),nks(nbs))
      read(10,*)(nis(n),njs(n),nks(n),n=1,nbs)
      nim=maxval(nis(:))
      njm=maxval(njs(:))
      nkm=maxval(nks(:))
      allocate(xs(nim,njm,nkm,nbs)
     &        ,ys(nim,njm,nkm,nbs)
     &        ,zs(nim,njm,nkm,nbs))
      do n=1,nbs
        print*,' Reading block nr. : ',n
        print*,' Block size : ',nis(n),njs(n),nks(n)
        read(10,*)
     &  (((xs(i,j,k,n),i=1,nis(n)),j=1,njs(n)),k=1,nks(n)),
     &  (((ys(i,j,k,n),i=1,nis(n)),j=1,njs(n)),k=1,nks(n)),
     &  (((zs(i,j,k,n),i=1,nis(n)),j=1,njs(n)),k=1,nks(n))
      enddo
      close(10)

      print*,' Give block size '
      read*,bsize
      nblock=0
      do n=1,nbs
        nbi=(nis(n)-1)/real(bsize)
        nbj=(njs(n)-1)/real(bsize)
        nbk=(nks(n)-1)/real(bsize)
        nblock=nblock+nbi*nbj*nbk
      enddo
      print*,' Number of x3d blocks : ',nblock
      ni=bsize+3
      allocate(x(ni,ni,ni,nblock)
     &        ,y(ni,ni,ni,nblock)
     &        ,z(ni,ni,ni,nblock))
      allocate(attr(ni,ni,ni,nblock))
      attr=101

      nblock=0
      do n=1,nbs
        nbi=(nis(n)-1)/real(bsize)
        nbj=(njs(n)-1)/real(bsize)
        nbk=(nks(n)-1)/real(bsize)
        do kk=1,nbk;kstart=(kk-1)*bsize
        do jj=1,nbj;jstart=(jj-1)*bsize
        do ii=1,nbi;istart=(ii-1)*bsize
          nblock=nblock+1
          print*,' Block nr :  ',nblock
          do k=2,bsize+2;ko=kstart+k-1
          do j=2,bsize+2;jo=jstart+j-1
          do i=2,bsize+2;io=istart+i-1
            x(i,j,k,nblock)=xs(io,jo,ko,n)
            y(i,j,k,nblock)=ys(io,jo,ko,n)
            z(i,j,k,nblock)=zs(io,jo,ko,n)
          enddo;enddo;enddo
        enddo;enddo;enddo
      enddo

c-------write unformatted grid -----------------------------------------
      call UnfGridWrite(20,ni,nblock,x,y,z,attr)

c     open(unit=10,file='grid.x3d')
c     write(10,*)ni-1,nblock
c     do n=1,nblock
c     do k=2,bsize+2
c     do j=2,bsize+2
c     do i=2,bsize+2
c       write(10,*)attr(i,j,k,n),x(i,j,k,n),y(i,j,k,n),z(i,j,k,n)
c     enddo;enddo;enddo;enddo
c     close(10)


      stop
      end


      subroutine UnfGridWrite(unitnr,ni,nblock,x,y,z,attr)
C=======================================================================
C     routine for writing unformatted x3d file
C     remember that the grid must positions in the following region
C
C        x(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C        y(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C        z(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C     attr(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C
C     the remaining spaces are intended for ghost cells
C=======================================================================
      implicit none
      integer unitnr,ni,nblock
      real(kind=8),dimension(ni,ni,ni,nblock)::x,y,z
      integer,dimension(ni,ni,ni,nblock)::attr

      print*,' unitnr ',unitnr
      open(unit=unitnr,file='grid.X3DUNF',form='unformatted')
      write(unitnr)ni-3
      write(unitnr)nblock
      print*,' Writing Attributes '
      call WriteBlockInteger(unitnr,ni**3,nblock,attr)
      print*,' Writing x '
      call WriteBlockReal   (unitnr,ni**3,nblock,x   )
      print*,' Writing y '
      call WriteBlockReal   (unitnr,ni**3,nblock,y   )
      print*,' Writing z '
      call WriteBlockReal   (unitnr,ni**3,nblock,z   )
      print*,' Finished writing '
      close(unitnr)
      return
      end

      subroutine WriteBlockReal(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      real(kind=8),dimension(block,nblock)::x
      real(kind=8),dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine WriteBlockReal

      subroutine WriteBlockInteger(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      integer,dimension(block,nblock)::x
      integer,dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine WriteBlockInteger
