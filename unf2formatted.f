c     $Rev:: 8           $: Revision of last commit
c     $Author:: nsqr     $: Author of last commit
c     $Date:: 2011-08-02#$: Date of last commit
      module params 
c----------------------------------- common variables ------------------
      integer::bsize,nblock
      real(kind=4),dimension(:,:,:,:,:),allocatable::var
      integer,dimension(:,:,:,:),allocatable::attr
      integer,dimension(:,:,:,:),allocatable::attrcf
      end module


      program Mesh2FieldView
      use params
      implicit none
      integer::ni
      integer,parameter::idpo=4,ncomp=4
      real(kind=4)::dummy
      character*11 fnamd,fnamh
      character*128 fieldname(ncomp)
      real(kind=8),allocatable,dimension(:,:,:,:)::x
      integer::idummy
      integer::i,j,k,n,nc,nvar,fileform
      integer::b1,iblock
      logical::statex

      inquire(file='grid.x3dunf',exist=statex)
      if(statex)then
        print*,' READING UNFORMATTED GRID FILE'
        open(unit=20,file='grid.x3dunf',form='unformatted')
        read(20)bsize
        read(20)nblock
        print*,bsize,nblock
        ni=bsize+3
        allocate(x(ni,ni,ni,nblock))
        allocate(var(ncomp,ni,ni,ni,nblock),attr(ni,ni,ni,nblock))
        var=0.d0;x=0.d0
        print*,' Reading Attributs : '
        call ReadBlockInteger(20,ni**3,nblock,attr)
        print*,' Reading x-coord : '
        call ReadBlockReal   (20,ni**3,nblock,x)
        var(1,:,:,:,:)=x(:,:,:,:)
        print*,' Reading y-coord : '
        call ReadBlockReal   (20,ni**3,nblock,x)
        var(2,:,:,:,:)=x(:,:,:,:)
        print*,' Reading z-coord : '
        call ReadBlockReal   (20,ni**3,nblock,x)
        var(3,:,:,:,:)=x(:,:,:,:)
        print*,' Finished Reading : '
        deallocate(x)
        close(10)
      endif
        open(unit=20,file='grid.x3d')
        write(20,*)bsize,nblock
        ni=bsize+3
        do n=1,nblock
        do k=2,bsize+2
        do j=2,bsize+2
        do i=2,bsize+2
          write(20,'(i4,3(f35.28))')attr(i,j,k,n)  
     &            ,var(1,i,j,k,n)
     &            ,var(2,i,j,k,n)
     &            ,var(3,i,j,k,n)
        enddo;enddo;enddo;enddo

      stop
   30 format('')
   31 format('VARIABLES = ',15(A20,','))
   32 format('ZONE I=',i2,', J=',i2,', K=',i2,' F=BLOCK')
      end





      subroutine ExtractWallBoundaryFile(reduce)
c-----------------------------------------------------------------------      
      use params
c-----------------------------------------------------------------------      
      implicit none
      character(len=13)zonename
      character(len=256) filename1
      integer lmin,lmax,mmin,mmax,n,nn,l,m,i,j,k,reduce,b1,b2
      integer natr,imin,imax,jmin,jmax,kmin,kmax
      integer attrmin,attrmax
      integer nwallblock,f,i0
c-----------------------------------------------------------------------      
      i0=0
      b1=bsize+1;b2=bsize+2
c     print*,'allocating attrcf',bsize,nblock
c---- cell face attributes ---------------------------------------------
      allocate(attrcf(b2+1,b2+1,6,nblock))
      do n=1,nblock
c---- face 1-2 ---------------------------------------------------------
      do f=1,2;i=2+mod(f+1,2)*(bsize)
      do k=2,bsize+2; do j=2,bsize+2
         attrcf(j,k,f,n)=attr(i,j,k,n)
      enddo;enddo
      enddo
c---- face 3-4 ---------------------------------------------------------      
      do f=3,4;j=2+mod(f+1,2)*(bsize)
      do k=2,bsize+2; do i=2,bsize+2
         attrcf(k,i,f,n)=attr(i,j,k,n)
      enddo;enddo
      enddo
c---- face 5-6 ---------------------------------------------------------      
      do f=5,6;k=2+mod(f+1,2)*(bsize)
      do j=2,bsize+2; do i=2,bsize+2
         attrcf(i,j,f,n)=attr(i,j,k,n)
      enddo;enddo
      enddo
c-----------------------------------------------------------------------
      enddo

      open(unit=17, file='grid.xyz.fvbnd',FORM='formatted')
      write(17,*)'FVBND 1 4'
      write(17,*)' wall'
      write(17,*)' inlet'
      write(17,*)' outlet'
      write(17,*)' cyclic'
      write(17,*)' symmetry'
c-----------------------------------------------------------------------
      write(17,*)'BOUNDARIES'
c--------------------------------------------- boundary regions -------
      natr=5
      nwallblock=0
      do i=1,natr
      select case(i)
       case(1) 
         attrmin=101;attrmax=200
       case(2) 
         attrmin=201;attrmax=300
       case(3)  
         attrmin=401;attrmax=500
       case(4) 
         attrmin=501;attrmax=600
       case(5) 
         attrmin=601;attrmax=700
      end select 
      ACTIVE_BLOCKS: do n=1,nblock
          FACE: do f=1,6
c------------------ write segment to file ------------------------------
          SEGMENT_START: do mmin=2,bsize+2,reduce
                         m=max(3,min(mmin,bsize+1))
                         do lmin=2,bsize+2,reduce
                         l=max(3,min(lmin,bsize+1))
c            print*,'segment test',attrcf(l,m,f,n),attrmin,l,m,f,n
          if(attrcf(l,m,f,n).ge.attrmin.and.
     &       attrcf(l,m,f,n).le.attrmax     )then
c            print*,'segment start',attrmin,lmin,mmin,f,n
             exit SEGMENT_START
        endif
      end do; end do SEGMENT_START
c-----------------------------------------------------------------------
      if(lmin.gt.bsize+1.or.mmin.gt.bsize+1) cycle FACE
c-----------------------------------------------------------------------
      SEGMENT_END: do mmax=bsize+2,mmin,-reduce
                    m=max(3,min(mmax,bsize+1))
                   do lmax=bsize+2,lmin,-reduce
                    l=max(3,min(lmax,bsize+1))
        if(attrcf(l,m,f,n).ge.attrmin.and.
     o     attrcf(l,m,f,n).le.attrmax     )then
c            print*,'segment end',attrmin,lmax,mmax,f,n
           exit SEGMENT_END
        endif
      end do; end do SEGMENT_END
c-----------------------------------------------------------------------
      if(lmax.le.lmin.or.mmax.le.mmin) cycle FACE
c-----------------------------------------------------------------------
      lmax=(lmax-1)/reduce+i0
      mmin=(mmin-1)/reduce+i0
      mmax=(mmax-1)/reduce+i0
      if(lmin.gt.1)then
         if(attrcf(lmin-1,mmin,f,n).lt.attrmin)lmin=lmin-1
      endif
      if(mmin.gt.1)then
         if(attrcf(lmin,mmin-1,f,n).lt.attrmin)mmin=mmin-1
      endif
      select case(f)
      case(1)
       imin=1;imax=1
       jmin=lmin;jmax=lmax
       kmin=mmin;kmax=mmax
      case(2)
       imin=bsize/reduce+1;imax=bsize/reduce+1
       jmin=lmin;jmax=lmax
       kmin=mmin;kmax=mmax
      case(3)
       imin=mmin;imax=mmax
       jmin=1;jmax=1
       kmin=lmin;kmax=lmax
      case(4)
       imin=mmin;imax=mmax
       jmin=bsize/reduce+1;jmax=bsize/reduce+1
       kmin=lmin;kmax=lmax
      case(5)
       imin=lmin;imax=lmax
       jmin=mmin;jmax=mmax
       kmin=1;kmax=1
      case(6)
       imin=lmin;imax=lmax
       jmin=mmin;jmax=mmax
       kmin=bsize/reduce+1;kmax=bsize/reduce+1
      end select 
         nn=n
         write(17,'(8i5,1a4,i4)')i,n,
     &                     imin,imax,jmin,jmax,kmin,kmax
     &                         ,'F',(-1)**f
c-----------------------------------------------------------------------
      nwallblock=nwallblock+1
      end do FACE
      end do ACTIVE_BLOCKS
c-----------------------------------------------------------------------
      end do
      close(17)
c----------------------------------------------------------------------
      return
      end

      subroutine ReadBlockReal(unit,block,nblock,x)
      implicit none
      integer unit,nblock,block,i,n
      real(kind=8),dimension(block,nblock)::x
      real(kind=8),dimension(block)       ::xblock
        do n=1,nblock
          read(unit)xblock
          do i=1,block
            x(i,n)=xblock(i)
          end do
        end do
      end subroutine ReadBlockReal

      subroutine ReadBlockInteger(unit,block,nblock,x)
      implicit none
      integer unit,nblock,block,i,n
      integer,dimension(block,nblock)::x
      integer,dimension(block)       ::xblock
        do n=1,nblock
          read(unit)xblock
          do i=1,block
            x(i,n)=xblock(i)
          end do
        end do
      end subroutine ReadBlockInteger

