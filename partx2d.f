      program partgrid
c----------------------------------------------------------------------
c     repartitionate a x2d surface grid 
c----------------------------------------------------------------------
      implicit none
      integer ni,nb
      real(kind=8),dimension(:,:,:),allocatable::x,y,z
      integer,dimension(:,:,:),allocatable::attr
      integer i,j,n
      integer dim,nbi,nbm
      integer istart,jstart,io,jo,ii,jj
      integer yesno
      real*8 rest

c-----open singleblock grid file
      open(unit=10,file='org.x2d')
      open(unit=20,file='grid.x2d')

c-----read grid dimension
      read(10,*) ni,nb
      ni=ni+1
      allocate(x(ni,ni,nb),y(ni,ni,nb),z(ni,ni,nb))
      allocate(attr(ni,ni,nb))

c-----read vertice attributes and vertice coordinats
      do n=1,nb
      do j=1,ni
      do i=1,ni
        read(10,*)attr(i,j,n)
     &              ,x(i,j,n)
     &              ,y(i,j,n)
     &              ,z(i,j,n)
      enddo;enddo;enddo

      dim=ni-1
      print*,'*******************************************************'
      print*,'size of multidomaine blocks         : ',dim
      print*,'want to change this yes = 1, no = 2   ' 
      read(*,*) yesno
      if(yesno.eq.1)then
        print*,'give new dimension  '
        read(*,*)dim
      endif
      nbi=(ni-1)/dim
      nbm=nbi**2*nb
      print*,'*******************************************************'
      print*,'size of multidomaine blocks         : ',dim
      print*,'total number of blocks              : ',nbm 
      print*,'*******************************************************'

      write(20,*) dim,nbm
      do n=1,nb
      do jj=1,nbi;jstart=(jj-1)*dim
      do ii=1,nbi;istart=(ii-1)*dim
        do j=1,dim+1;jo=jstart+j
        do i=1,dim+1;io=istart+i
          write(20,*)attr(io,jo,n)
     &                 ,x(io,jo,n) 
     &                 ,y(io,jo,n) 
     &                 ,z(io,jo,n) 
        enddo;enddo
      enddo;enddo;enddo

      return
      end
