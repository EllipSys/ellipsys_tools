      program SetAttr2D
C----------------------------------------------------------------------
C     Program for specifying the attributes for an x2d multi-block grid
C     
C     The program first sets all attributs to wall=101, all internal 
C     points will be reset to internal by basis2d
C     
C     Next it attempts to read a file bc.dat with boundary condition 
C     specification of the form, where the attributs should be given
C     in decending order.
C
C     x gt limit attrval
C     y lt limit attrval
C     box xmin ymin xmax ymax attrval
C
C     Niels N. Sørensen
C     February, 2017
C----------------------------------------------------------------------
      implicit none
      integer::bsize,ni,nblock
      real(kind=8),dimension(:,:,:),allocatable::x,y
      integer,dimension(:,:,:),allocatable::attr
      integer i,j,n
      real(kind=8)::rlim,xmax,xmin,ymax,ymin
      integer::attrset
      
      integer,parameter::mword=40
      integer nrchar(mword),nrword
      character(len=256)InputLine
      character(len=40),dimension(mword)::words
      logical::lrecogn=.false.


      open(unit=10,file='grid.x2d')
      read(10,*)bsize,nblock
      ni=bsize+3
      allocate(attr(ni,ni,nblock),x(ni,ni,nblock),y(ni,ni,nblock))
      do n=1,nblock
      do j=2,bsize+2
      do i=2,bsize+2
        read(10,*)attr(i,j,n),x(i,j,n),y(i,j,n)
      enddo;enddo
      enddo
      close(10)


c-------specify boundary conditions
      attr=101
      print*,' Processing boundary conditions : '
      open(unit=10,file='bc.dat')
      do 
        read(10,'(a256)',end=100)InputLine
        print*,InputLine 
        call cmdpart(InputLine,mword,nrchar,nrword,words)
C------------------------------------- Skip blank lines and comments ---
        if(nrword.le.1) then
          cycle
C---------------------------------- Skip rest of file if stop appears --
        elseif(words(1)(1:4).eq.'stop'.or.words(1)(1:4).eq.'STOP') then
        exit
c----------------------------- command InputLine interpreter -----------
        else
          lrecogn=.false.
        select case(words(1)(1:nrchar(1)))
        case default
          lrecogn=.false.
c-----------------------------------------------------------------------
        case('x')
          read(words(3),*)rlim
          read(words(4),*)attrset
          select case(words(2)(1:nrchar(2)))
          case('gt')
            call AttrPlane('xgt',rlim,ni,nblock,x,y,attrset,attr)
          case('lt')
            call AttrPlane('xlt',rlim,ni,nblock,x,y,attrset,attr)
          end select
        case('y')
          read(words(3),*)rlim
          read(words(4),*)attrset
          select case(words(2)(1:nrchar(2)))
          case('gt')
            call AttrPlane('ygt',rlim,ni,nblock,x,y,attrset,attr)
          case('lt')
            call AttrPlane('ylt',rlim,ni,nblock,x,y,attrset,attr)
          end select
        case('box')
          read(words(2),*)xmin
          read(words(3),*)ymin
          read(words(4),*)xmax
          read(words(5),*)ymax
          read(words(6),*)attrset
          call AttrBox(xmin,ymin,xmax,ymax,ni,nblock,x,y,attrset,attr)
        end select
      endif
      enddo
     
c     call AttrPlane('ygt', 99.d0,ni,nblock,x,y,502,attr)
c     call AttrPlane('ylt',-99.d0,ni,nblock,x,y,502,attr)
c     call AttrPlane('xgt', 99.d0,ni,nblock,x,y,501,attr)
c     call AttrPlane('xlt',-99.d0,ni,nblock,x,y,501,attr)

 100  open(unit=10,file='grid.X2D')
      write(10,*)bsize,nblock
      do n=1,nblock
      do j=2,bsize+2
      do i=2,bsize+2
        write(10,*)attr(i,j,n),x(i,j,n),y(i,j,n)
      enddo;enddo
      enddo
      close(10)
      stop
      end
      


      subroutine AttrPlane(modus,value,ni,nblock,x,y,attrval,attr)
      implicit none
      integer::bsize,ni,nblock,i,j,n,l,f,ncc
      character(len=3)::modus
      real(kind=8),dimension(ni,ni,nblock)::x,y
      integer,dimension(ni,ni,nblock)::attr
      real(kind=8)::value
      integer::attrval
  
      bsize=ni-3

      do n=1,nblock
      do f=1,4
C-----------------------------------------------------------------------
       select case(f)
        case(1,3);ncc=      2
        case(2,4);ncc=bsize+2
       end select
c---------check cell face center
       select case(f)
         case(1:2);i=ncc;j=bsize/2+1
         case(3:4);i=bsize/2+1;j=ncc
       end select
c-------check the different planes
        if((modus.eq.'xgt'.and.x(i,j,n).gt.value).or.
     &     (modus.eq.'xlt'.and.x(i,j,n).lt.value).or.
     &     (modus.eq.'ygt'.and.y(i,j,n).gt.value).or.
     &     (modus.eq.'ylt'.and.y(i,j,n).lt.value))then
         do l=2,bsize+2
           select case(f)
             case(1:2);i=ncc;j=l  
             case(3:4);i=l  ;j=ncc
           end select
           attr(i,j,n)=attrval
         enddo
        endif
      enddo;enddo

      return
      end

      subroutine AttrBox(xmin,ymin,xmax,ymax,ni,nblock,x,y,attrval,attr)
      implicit none
      integer::bsize,ni,nblock,i,j,n,l,f,ncc
      real(kind=8),dimension(ni,ni,nblock)::x,y
      real(kind=8)::xmin,xmax,ymin,ymax
      integer,dimension(ni,ni,nblock)::attr
      real(kind=8)::value
      integer::attrval

      bsize=ni-3

      do n=1,nblock
      do f=1,4
C-----------------------------------------------------------------------
       select case(f)
        case(1,3);ncc=      2
        case(2,4);ncc=bsize+2
       end select
c---------check cell face center
       select case(f)
         case(1:2);i=ncc;j=bsize/2+1
         case(3:4);i=bsize/2+1;j=ncc
       end select
c-------check the different planes
        if((x(i,j,n).gt.xmin).and.(x(i,j,n).lt.xmax).and.
     &     (y(i,j,n).gt.ymin).and.(y(i,j,n).lt.ymax))then
         do l=2,bsize+2
           select case(f)
             case(1:2);i=ncc;j=l
             case(3:4);i=l  ;j=ncc
           end select
           attr(i,j,n)=attrval
         enddo
        endif
      enddo;enddo

      return
      end

      subroutine cmdpart(InputLine,mword,nrchar,nrword,words)
c-----------------------------------------------------------------------
c     routine parting command InputLine into words
c----------------------------------------------------------------------
c
c     Author: Jess A. Michelsen and Niels N. Soerensen
c     last revision :march 3, 1998
c-----------------------------------------------------------------------
      implicit none
      integer mword,nrchar(mword),nrword,i
      integer,parameter::mchar=256
      character(len=40),dimension(mword):: words
      character(len=40)::blank
      character(len=256) InputLine
      integer CurrentPosition,RelativeBlankPosition,BlankPosition
C--------------------------------------------- Reset word counter ------
      nrword=0
      CurrentPosition=1
C--------------------------------------------- Skip if a comment line --
      if(index(InputLine(1:mchar),'#').gt.0) return
C---------------------------------------- find distance to next blank --
   20 RelativeBlankPosition=index(InputLine(CurrentPosition:mchar),' ')
      if(CurrentPosition.eq.mchar.or.RelativeBlankPosition.eq.0) return
C---------------------------------------- found the end of a word ------
      if(RelativeBlankPosition.gt.1.and.RelativeBlankPosition.le.40)then
        BlankPosition=CurrentPosition-1+RelativeBlankPosition
        nrword=nrword+1
        if(nrword.gt.mword)return
        words(nrword)=InputLine(CurrentPosition:BlankPosition)
        nrchar(nrword)=RelativeBlankPosition-1
      endif
c-----------------------------------------------------------------------
      CurrentPosition=CurrentPosition+RelativeBlankPosition
      goto 20
c-----------------------------------------------------------------------
      end
