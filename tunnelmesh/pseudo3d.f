      program polar
      implicit none
      real(kind=8),dimension(:,:,:),allocatable::x2d,z2d
      integer,dimension(:,:,:),allocatable::attr2d
      real(kind=8),dimension(:,:,:,:),allocatable::x,y,z
      integer,dimension(:,:,:,:),allocatable::attr
      real(kind=8)::pi,alpha,len,s,dx
      integer ni,nj,nblock,i,j,k,n
      integer dim,nbi,nbj,nbk,nbm,io,jo,ko,ii,jj,kk
     &       ,istart,jstart,kstart,factor,nib,nn
      integer::nrdist,bctype,errchk
      real(kind=8),dimension(:,:),allocatable::dist
      real(kind=8),dimension(:),allocatable::fs
      real(kind=8),dimension(:,:,:,:),allocatable::xt,yt,zt
      integer,dimension(:,:,:,:),allocatable::attrt
      logical::lstatex

      pi=4.d0*atan(1.d0)

      print*,' Give spanwise length : '
      read*,len

c--------allocate 2d vertices
      open(unit=10,file='grid.x2d')
      read(10,*)ni,nblock
      ni=ni+1
      allocate(x2d(ni,ni,nblock),z2d(ni,ni,nblock))
      allocate(attr2d(ni,ni,nblock))
      do n=1,nblock
      do j=1,ni
      do i=1,ni
        read(10,*)attr2d(i,j,n),x2d(i,j,n),z2d(i,j,n)
      enddo;enddo
      enddo
      close(10)

c--------allocate 3d vertices
      print*,' Give number of vertices in j-direction : '
      read*,nj
      allocate(x(ni,nj,ni,nblock),y(ni,nj,ni,nblock),z(ni,nj,ni,nblock)
     &        ,attr(ni,nj,ni,nblock))


c--------spanwise distribution
      allocate(fs(nj))
      inquire(exist=lstatex,file='distfunc.dat')
      if(lstatex)then
      open(unit=10,file='distfunc.dat')
      read(10,*)nrdist
      allocate(dist(nrdist,3))
c ------curve length [0:1], actual cell length , vertec number
      do i=1,nrdist
        read(10,*)dist(i,1),dist(i,2),dist(i,3)
      enddo
      dist(:,1)=dist(:,1)*len
      close(10)
      fs=0
      if(nj.ne.dist(nrdist,3))then
        print*,' Number of points specified in distfunc is wrong '
        stop
      endif
      errchk=1
      call DistFunc(nrdist,dist,1,nj,errchk,fs)
      print*,' aft distfunc '
      else
        do j=1,nj
          fs(j)=real(j-1)/real(nj-1)
        enddo
      endif
      print*,' Finished reading distfunc : '

      open(unit=10,file='dist.out')
      do j=2,nj
        write(10,*)j,fs(j),(fs(j)-fs(j-1))*len
      enddo
      close(10)
c     stop


c--------compute 3d vertice location
      do n=1,nblock
      do k=1,ni
      do j=1,nj
      do i=1,ni
        x(i,j,k,n)=x2d(i,k,n)
        y(i,j,k,n)=fs(j)
        z(i,j,k,n)=z2d(i,k,n)
      enddo;enddo;enddo
      enddo

c--------boundary condition
      attr=1
      do n=1,nblock
      do k=1,ni
      do j=1,nj
      do i=1,ni
        attr(i,j,k,n)=attr2d(i,k,n)
      enddo;enddo;enddo
      enddo

      print*,' Use symmetry=1,periodic=2,wall/sym=3,wall/wall=4 bc '
      read*,bctype
      if(bctype.eq.1)then
      do n=1,nblock
      do k=1,ni
      do i=1,ni
        if(attr(i,1 ,k,n).eq.1)then
          attr(i,1 ,k,n)=601        
        endif
        if(attr(i,nj,k,n).eq.1)then
          attr(i,nj,k,n)=601        
        endif
      enddo;enddo
      enddo
      else if(bctype.eq.2)then
      do n=1,nblock
      do k=1,ni
      do i=1,ni
        if(attr(i,1 ,k,n).eq.1.or.attr(i,1 ,k,n).gt.501)
     &    attr(i,1 ,k,n)=501        
        if(attr(i,nj,k,n).eq.1.or.attr(i,nj,k,n).gt.501)
     &     attr(i,nj,k,n)=501        
      enddo;enddo
      enddo
      else if(bctype.eq.3)then
      do n=1,nblock
      do k=1,ni
      do i=1,ni
        attr(i,1 ,k,n)=102        
        if(attr(i,nj,k,n).eq.1)attr(i,nj,k,n)=601        
      enddo;enddo
      enddo
      else if(bctype.eq.4)then
      do n=1,nblock
      do k=1,ni
      do i=1,ni
        attr(i,1 ,k,n)=102        
        attr(i,nj,k,n)=102        
      enddo;enddo
      enddo
      endif

      print*,'*******************************************************'
      print*,' Mesh size ni,nj,ni : ',ni,nj,ni
      print*,' Give block size : '
      read*,dim
      print*,' Give reduction factor: '
      read*,factor
      nbi=ni/dim
      nbj=nj/dim
      nbk=ni/dim
      nbm=nbi*nbj*nbk*nblock
      print*,'*******************************************************'
      print*,'size of multidomaine blocks         : ',dim/factor
      print*,'number of block in xi direction     : ',nbi
      print*,'number of block in eta direction    : ',nbj
      print*,'number of block in zeta direction   : ',nbk
      print*,'total number of blocks              : ',nbm
      print*,'*******************************************************'

      allocate(attrt(dim+3,dim+3,dim+3,nbm))
      attrt=0
      do n=1,nblock
      do kk=1,nbk;kstart=(kk-1)*dim
      do jj=1,nbj;jstart=(jj-1)*dim
      do ii=1,nbi;istart=(ii-1)*dim
        nn=(n-1)*nbi*nbj*nbk+ii+(jj-1)*nbi+(kk-1)*nbi*nbj
        do k=1,dim+1,factor;ko=kstart+k
        do j=1,dim+1,factor;jo=jstart+j
        do i=1,dim+1,factor;io=istart+i
          attrt(i+1,j+1,k+1,nn)=attr(io,jo,ko,n)
        enddo;enddo;enddo
      enddo;enddo;enddo
      enddo
      deallocate(attr)
      print*,' attr '
      allocate(xt(dim+3,dim+3,dim+3,nbm))
      xt=0
      do n=1,nblock
      do kk=1,nbk;kstart=(kk-1)*dim
      do jj=1,nbj;jstart=(jj-1)*dim
      do ii=1,nbi;istart=(ii-1)*dim
        nn=(n-1)*nbi*nbj*nbk+ii+(jj-1)*nbi+(kk-1)*nbi*nbj
        do k=1,dim+1,factor;ko=kstart+k
        do j=1,dim+1,factor;jo=jstart+j
        do i=1,dim+1,factor;io=istart+i
             xt(i+1,j+1,k+1,nn)=x(io,jo,ko,n)
        enddo;enddo;enddo
      enddo;enddo;enddo
      enddo
      deallocate(x)
      print*,' x '
      allocate(yt(dim+3,dim+3,dim+3,nbm))
      yt=0
      do n=1,nblock
      do kk=1,nbk;kstart=(kk-1)*dim
      do jj=1,nbj;jstart=(jj-1)*dim
      do ii=1,nbi;istart=(ii-1)*dim
        nn=(n-1)*nbi*nbj*nbk+ii+(jj-1)*nbi+(kk-1)*nbi*nbj
        do k=1,dim+1,factor;ko=kstart+k
        do j=1,dim+1,factor;jo=jstart+j
        do i=1,dim+1,factor;io=istart+i
             yt(i+1,j+1,k+1,nn)=y(io,jo,ko,n)
        enddo;enddo;enddo
      enddo;enddo;enddo
      enddo
      deallocate(y)
      print*,' y '
      allocate(zt(dim+3,dim+3,dim+3,nbm))
      zt=0
      do n=1,nblock
      do kk=1,nbk;kstart=(kk-1)*dim
      do jj=1,nbj;jstart=(jj-1)*dim
      do ii=1,nbi;istart=(ii-1)*dim
        nn=(n-1)*nbi*nbj*nbk+ii+(jj-1)*nbi+(kk-1)*nbi*nbj
        do k=1,dim+1,factor;ko=kstart+k
        do j=1,dim+1,factor;jo=jstart+j
        do i=1,dim+1,factor;io=istart+i
             zt(i+1,j+1,k+1,nn)=z(io,jo,ko,n)
        enddo;enddo;enddo
      enddo;enddo;enddo
      enddo
      deallocate(z)


      nib=dim+3
      call UnfGridWrite(20,nib,nbm,xt,yt,zt,attrt)

      stop
   21 format(i6,3(f18.12))
  666 write(*,*) ' error: too few data in input file (grid.dat)'
      end



      subroutine distfunc(nn,dinp,ival,ndist,errchk,fdist)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      implicit none
      integer::nn,ndist
      real(kind=8),dimension(ndist)::fdist,dy
      real(kind=8),dimension(nn,3)::dinp
      real(kind=8) s0,s1,d0,d1
      real(kind=8) len,delta1,delta2,error
      integer n0,n1,ns,nf
      integer i,j,ival,n,errchk,err


      fdist(1)=0.d0
      s0=dinp(1,1);d0=dinp(1,2);n0=int(dinp(1,3))
      do i=2,nn
        s1=dinp(i,1);d1=dinp(i,2);n1=int(dinp(i,3))
        len=s1-s0
        delta1=d0
        delta2=d1
        err=1
        if(ival.eq.1)call tanhdist(delta1,delta2,len,n0,n1,err,dy(n0))
        if(ival.eq.2)call sinhdist(delta1,delta2,len,n0,n1,err,dy(n0))
          write(*,'(2x,a35,i5)')' Ok section ',i
        if(errchk.eq.1.and.err.eq.-1)then
          delta1=dy(n0+1)-dy(n0)
          delta2=dy(n1)-dy(n1-1)
          write(*,'(2x,a35)')' Convergence problem in distfunc : '
          write(*,'(2x,a10,i5,i5,i5)')'Section : ',i,n0,n1
          error=abs(d0-delta1)/d0*100;if(d0.lt.0.d0)error=0.d0
          write(*,'(2x,a24,f20.4)')'Start err in percent  : ',error
          error=abs(d1-delta2)/d1*100;if(d1.lt.0.d0)error=0.d0
          write(*,'(2x,a24,f20.4)')'End   err in percent  : ',error
        endif
        do j=n0+1,n1
        fdist(j)=fdist(n0)+dy(j)
        enddo
        s0=s1;d0=d1;n0=n1
      enddo
      return
   21 format('#',i6)
      end

      subroutine tanhdist(delta1,delta2,len,i1,i2,err,fdist)
************************************************************************
*     TANDIST
*     program for computing the hyperbolic tangent distribution
*     delta1 : grid spacing at i=i1 (first cell)
*     delta2 : grid spacing at i=i2 (last  cell)
*     len    : lenght of curve
*     i1     : indices of first vertex
*     i2     : indices of last vertex
*     fdist  : compute distribution function
*
*
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      integer i1,i2,ni,i,err
      real(kind=8)::delta1,delta2,len
      real(kind=8)::delta,b,a
      real(kind=8)::fdist(i2-i1+1)
      real(kind=8)::transsinh,transtanh,ftmp
      external transsinh,transtanh

      if(i2.eq.i1)return

c------- Normalize delta1 and delta2 with lenght
      delta1=delta1/len
      delta2=delta2/len
c------- Compute number of points
      ni=i2-i1


c------- Assure that B > 1
      if(delta1.le.0.d0.and.1/delta2.lt.ni)then
        delta1=1/(ni**2*delta2*1.02)
      elseif(delta2.le.0d0.and.1/delta1.lt.ni)then
        delta2=1/(ni**2*delta1*1.02)
      endif

c------- Spacing at both end specified
      if(delta1.gt.0.d0.and.delta2.gt.0.d0)then
        a=sqrt(delta2)/sqrt(delta1)
        b=1.d0/(ni*sqrt(delta1*delta2))
        if(b.ge.1.d0)then
          delta=transsinh(b,err)
          do i=1,ni+1
            ftmp=.5*(1+tanh(delta*(real(i-1)/real(ni)-0.5))
     &          /tanh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        else
          delta=transtanh(b,err)
          do i=1,ni+1
            ftmp=.5*(1+sinh(delta*(real(i-1)/real(ni)-0.5))
     &          /sinh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        endif
c------- Spacing only specified at xi=1
      else if(delta1.gt.0.d0)then
        b=1.d0/(ni*delta1)
        delta=transsinh(b,err)
        do i=1,ni+1
          fdist(i)=1.d0+tanh(.5d0*delta*(real(i-1)/real(ni)-1.d0))
     &            /tanh(.5d0*delta)
        enddo
c------- Spacing only specified at xi=ni
      else if(delta2.gt.0.d0)then
        b=1.d0/(ni*delta2)
        delta=transsinh(b,err)
        do i=1,ni+1
          fdist(i)=tanh(.5*delta*real(i-1)/real(ni))/tanh(.5*delta)
        enddo
c------- Error no spacing is given
      else
        print*,' Error from tandist, no cell hight is given '
      endif
c------- Compute len*fdist
      do i=1,ni+1
        fdist(i)=fdist(i)*len
      enddo
      return
      end

      subroutine sinhdist(delta1,delta2,len,i1,i2,err,fdist)
************************************************************************
*     SINHDIST
*     program for computing the hyperbolic sine distribution
*     delta1 : grid spacing at i=i1 (first cell)
*     delta2 : grid spacing at i=i2 (last  cell)
*     len    : lenght of curve
*     i1     : indices of first vertex
*     i2     : indices of last vertex
*     fdist  : compute distribution function
*
*
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      integer i1,i2,ni,i,err
      real(kind=8)::delta1,delta2,len
      real(kind=8)::delta,b,a
      real(kind=8)::fdist(*)
      real(kind=8)::transsinh,transtanh,ftmp
      external transsinh,transtanh

      if(i2.eq.i1)return

c------- Normalize delta1 and delta2 with lenght
      delta1=delta1/len
      delta2=delta2/len
c------- Compute number of points
      ni=i2-i1

c------- Assure that B > 1
      if(delta1.le.0.d0.and.1/delta2.lt.ni)then
        delta1=1/(ni**2*delta2*1.02)
      elseif(delta2.le.0d0.and.1/delta1.lt.ni)then
        delta2=1/(ni**2*delta1*1.02)
      endif

c------- Spacing at both end specified
      if(delta1.gt.0.d0.and.delta2.gt.0.d0)then
        a=sqrt(delta2)/sqrt(delta1)
        b=1.d0/(ni*sqrt(delta1*delta2))
        if(b.ge.1.d0)then
          delta=transsinh(b,err)
          do i=1,ni+1
            ftmp=.5*(1+tanh(delta*(real(i-1)/real(ni)-0.5))
     &          /tanh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        else
          delta=transtanh(b,err)
          do i=1,ni+1
            ftmp=.5*(1+sinh(delta*(real(i-1)/real(ni)-0.5))
     &          /sinh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        endif
c------- Spacing only specified at xi=1
      else if(delta1.gt.0.d0)then
        b=1.d0/(ni*delta1)
        delta=transsinh(b,err)
        do i=1,ni+1
          fdist(i)=sinh(delta*real(i-1)/real(ni))/sinh(delta)
        enddo
c------- Spacing only specified at xi=ni
      else if(delta2.gt.0.d0)then
        b=1.d0/(ni*delta2)
        delta=transsinh(b,err)
        do i=1,ni+1
          fdist(i)=1-sinh(delta*(1-real(i-1)/real(ni)))/sinh(delta)
        enddo
c------- Error no spacing is given
      else
        print*,' Error from tandist, no cell hight is given '
      endif
c------- Compute len*fdist
      do i=1,ni+1
        fdist(i)=fdist(i)*len
      enddo
      return
      end

      double precision function transsinh(b,err)
************************************************************************
*     routine for solving the transendental equation
*
*     b=sinh(delta)/delta
*
*     using NewtonRaphson
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      integer::err
      real(kind=8)::delta,b
      real(kind=8)::delta_old,f,df,rlim
      parameter(rlim=1.d-1)
      integer::n,nmax=250

c------- Preforme Newton iteration to obtain delta
      delta=1.d0
      delta_old=delta
      do n=1,nmax
c--------- Compute f(delta)
c        f =b-sinh(delta)/delta
c        f =b*delta-sinh(delta)
         f =delta/(sinh(delta)+1.d-60)-1.d0/b
c--------- Compute d/d(delta)(f(delta))
c        df=(sinh(delta)-delta*cosh(delta))/delta**2
c        df=b-cosh(delta)
         df=(sinh(delta)-delta*cosh(delta))/sinh(delta)**2
c--------- update delta
        delta=delta-.5*f/df
c       delta=delta-f/df
c       write(*,'(4e16.4)')f,df,f/df,delta
c--------- check convergence criterium
        if(n.eq.nmax.and.abs((delta-delta_old)/delta_old).gt.rlim)then
          err=-1
c         print*,' Convergence problem in transinh dist. function !!! '
c         print*,' residual ',(delta-delta_old)/delta_old
        endif
c--------- update old value
        delta_old=delta
      enddo
      transsinh=delta
      return
      end

      double precision function transtanh(b,err)
************************************************************************
*     routine for solving the transendental equation
*
*     b=tanh(delta/2)/(delta/2)
*
*     using NewtonRaphson
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      integer::err
      real(kind=8)::delta,b
      real(kind=8)::delta_old,f,df,rlim
      parameter(rlim=1.d-1)
      integer::n,nmax=250

      if(b.gt.1.0d0)then
        print*,' Error form sinhdist: Can not be gridded '
c       stop
      endif
c------- Preforme Newton iteration to obtain delta
      delta=1.d0
      delta_old=delta
      do n=1,nmax
c--------- Compute f(delta)
        f =delta/(tanh(.5*delta)+1.d-30)-2./B
c--------- Compute d/d(delta)(f(delta))
        df=(tanh(.5*delta)-delta*(1-tanh(.5*delta)**2))
     &    /(tanh(.5*delta)**2+1.d-30)
c--------- update delta
        delta=delta-.5*f/df
c       delta=delta-f/df
c       write(*,'(i3,1x,3e16.4)')n,delta,f,df
c--------- check convergence criterium
        if(n.eq.nmax.and.abs((delta-delta_old)/delta_old).gt.rlim)then
          err=-1
c         print*,' Convergence problem in trantanh dist. function !!! '
c         print*,' residual ',(delta-delta_old)/delta_old
        endif
c--------- update old value
        delta_old=delta
      enddo
      transtanh=delta
      return
      end

      subroutine UnfGridWrite(unitnr,ni,nblock,x,y,z,attr)
C=======================================================================
C     routine for writing unformatted x3d file
C     remember that the grid must positions in the following region 
C
C        x(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C        y(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C        z(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C     attr(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C
C     the remaining spaces are intended for ghost cells
C=======================================================================
      implicit none
      integer  unitnr,ni,nblock
      real(kind=8),dimension(ni,ni,ni,nblock)::x,y,z
      integer,dimension(ni,ni,ni,nblock)::attr

      open(unit=unitnr,file='grid.x3dunf',form='unformatted')
      write(unitnr)ni-3
      write(unitnr)nblock
      print*, ' bsize, nblock ',ni-3,nblock
      call WriteBlockIntegerHyp(unitnr,ni**3,nblock,attr)
      call WriteBlockRealHyp   (unitnr,ni**3,nblock,x   )
      call WriteBlockRealHyp   (unitnr,ni**3,nblock,y   )
      call WriteBlockRealHyp   (unitnr,ni**3,nblock,z   )

      close(unitnr)
      return
      end

      subroutine WriteBlockRealHyp(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      real(kind=8),dimension(block,nblock)::x
      real(kind=8),dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine WriteBlockRealHyp

      subroutine WriteBlockIntegerHyp(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      integer,dimension(block,nblock)::x
      integer,dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine WriteBlockIntegerHyp
