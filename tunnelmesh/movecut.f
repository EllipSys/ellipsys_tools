      program movecut
      implicit none
      integer ni,ncut,i
      character(len=1)::adum
      real(kind=8),allocatable,dimension(:)::x,y
      

      open(unit=10,file='sfout.dat')
      read(10,*)adum,ni
      allocate(x(ni),y(ni))
      do i=1,ni
        read(10,*)x(i),y(i)
      enddo
      close(10)

      print*,' Give number of points to move cut '
      read*,ncut


      open(unit=10,file='sfout.DAT')
      write(10,'(a1,1x,i6)')'#',ni
      do i=ncut,ni
        write(10,*)x(i),y(i)
      enddo
      do i=2,ncut
        write(10,*)x(i),y(i)
      enddo
      stop
      end
 
