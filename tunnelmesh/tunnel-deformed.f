      module params
      integer nx,ny,ni,nj,nk,niu,nid,njs
      real(kind=8) lxu,lxd,ly
      real(kind=8),allocatable,dimension(:)::fx,fy
      end module

      module vertices
      real(kind=8),allocatable,dimension(:,:,:)::x,y,z
      integer(kind=8),allocatable,dimension(:,:,:)::attr
      end module

      program tunnel
C-----------------------------------------------------------------------
C     program for generating the outer blocks for a 2D wind tunnel
C     configuration
C     sfout.dat  : the outer surface of the o-mesh to be embedded
C     niu,nid
C     njs
C-----------------------------------------------------------------------
      use params
      use vertices
      implicit none
      integer i,j,k
      integer nivg
      integer niout
      real(kind=8),allocatable,dimension(:)::xout,yout
      character(len=1) adum
      integer nrdist
      real(kind=8),allocatable,dimension(:,:)::distinp
      real(kind=8),allocatable,dimension(:)::fs
      integer errchk
      real(kind=8) dx,dy,pi,factor
      integer nbi,nbj,nbk,nblock,ii,jj,kk,bsize,tt
     &       ,istart,jstart,kstart,i0,j0,k0


      pi=4.d0*atan(1.d0)

c-------specification of grid
      nx=97;ny=65
      niu=33;lxu=8.d0
      nid=97;lxd=8.d0
      njs=33;ly=4
      bsize=32

      print*,' Give horizontal and vertical nr. of points '
      read*,nx,ny
      print*,' Give upstream length and nr. of points'
      read*,lxu,niu
      print*,' Give downstream length and nr. of points'
      read*,lxd,nid
      print*,' Give distance above/below and nr. of points'
      read*,ly,njs
      print*,' Give block size '
      read*,bsize

     
      open(unit=10,file='sfout.dat')
      read(10,*)adum,niout
c-------check size of vg grid
      print*,' niout : ',niout,2*(nx-1)+2*(ny-1)+1
      if(2*(nx-1)+2*(ny-1)+1.ne.niout)then
        print*,' Problem with out grid size '
        stop
      endif
      allocate(xout(niout),yout(niout))
      do i=1,niout
        read(10,*)xout(i),yout(i)
      enddo
      close(10)


c-----generate distributions
      ni=niu+nid+nx-2
      nj=2*njs+ny-2
c     nk=nku+nz-1
      nk=1
      allocate(fx(ni),fy(nj))

c-----x-distribution
c-----central
      fx=0.d0
      do i=1,nx
        fx(niu-1+i)=xout(ny-1+nx+1-i)
      enddo
c-----upstream
      dx=fx(niu+1)-fx(niu)
      nrdist=2
      allocate(distinp(nrdist,3),fs(niu))
      fs=0.d0
      distinp(1,1)=0.d0
      distinp(1,2)=-1
      distinp(1,3)=1
      distinp(2,1)=1.d0
      distinp(2,2)=dx/lxu
      distinp(2,3)=niu
      errchk=0
      call distfunc(nrdist,distinp,1,niu,errchk,fs)
      do i=1,niu
        fx(i)=fx(niu)-lxu+lxu*fs(i)
      enddo
      deallocate(distinp,fs)
c-----downstream
      dx=fx(niu-1+nx)-fx(niu-2+nx)
      nrdist=2
      allocate(distinp(nrdist,3),fs(nid))
      fs=0.d0
      distinp(1,1)=0.d0
      distinp(1,2)=dx/lxd
      distinp(1,3)=1
      distinp(2,1)=1.d0
      distinp(2,2)=.01*lxd
      distinp(2,3)=nid
      errchk=0
      call distfunc(nrdist,distinp,1,nid,errchk,fs)
      do i=1,nid
        fx(ni-nid+i)=fx(ni-nid+1)+lxd*fs(i)
      enddo
      deallocate(distinp,fs)

      open(unit=11,file='fx.dat')
      do i=1,ni
        write(11,*)i,fx(i),0
      enddo
      close(11)

c-----y-distribution
c-----central
      fy=0.d0
      do i=1,ny
        fy(njs-1+i)=yout(ny+1-i)
      enddo
c-----right side
      dx=fy(njs+1)-fy(njs)
      nrdist=2
      allocate(distinp(nrdist,3),fs(njs))
      fs=0.d0
      distinp(1,1)=0.d0
c     distinp(1,2)=-1
      distinp(1,2)=1d-6/ly
      distinp(1,3)=1
      distinp(2,1)=1.d0
      distinp(2,2)=dx/ly
      distinp(2,3)=njs
      errchk=0
      call distfunc(nrdist,distinp,1,njs,errchk,fs)
      do i=1,njs
        fy(i)=fy(njs)-ly+ly*fs(i)
      enddo
      deallocate(distinp,fs)
c-----left side
      dx=fy(njs-1+ny)-fy(njs-2+ny)
      nrdist=2
      allocate(distinp(nrdist,3),fs(njs))
      fs=0.d0
      distinp(1,1)=0.d0
      distinp(1,2)=dx/ly
      distinp(1,3)=1
      distinp(2,1)=1.d0
      distinp(2,2)=1d-6/ly
c     distinp(2,2)=-1
      distinp(2,3)=njs
      errchk=0
      call distfunc(nrdist,distinp,1,njs,errchk,fs)
      do i=1,njs
        fy(njs+ny-2+i)=fy(njs+ny-1)+ly*fs(i)
      enddo
      deallocate(distinp,fs)

      open(unit=11,file='fy.dat')
      do i=1,nj
        write(11,*)i,fy(i),0
      enddo
      close(11)

      allocate(x(ni,nj,nk),y(ni,nj,nk),z(ni,nj,nk),attr(ni,nj,nk))
      do k=1,nk
      do j=1,nj
      do i=1,ni
        x(i,j,k)=fx(i)
        y(i,j,k)=fy(j)
        z(i,j,k)=0.d0
      enddo;enddo;enddo

c-----deform top
      do k=1,nk
      do j=nj-njs+3,nj
        factor=(real(j-nj+njs-3)/real(njs-3))
        factor=(tanh((factor-.25)*10)+1)*.5
      do i=1,ni
        y(i,j,k)=y(i,j,k)
     &        -.5*exp(-2*x(i,j,k)**2)*.05*factor
      enddo;enddo
      enddo

c-----deform bottom
      do k=1,nk
      do j=1,njs-2
        factor=(real(njs-2-j)/real(njs-3))
        factor=(tanh((factor-.25)*10)+1)*.5
      do i=1,ni
        y(i,j,k)=y(i,j,k)
     &         -.5*exp(-2*(x(i,j,k)-1.5d0)**2)*.035*factor
      enddo;enddo
      enddo

      attr=1
      attr(ni,:,:)=401
      attr(1 ,:,:)=201

c     attr(:,1 ,:)=601
c     attr(:,nj,:)=601
      attr(:,1 ,:)=102
      attr(:,nj,:)=102

      do i=1,ni
        if(x(i,1,1).gt.-1.d0.and.x(i,1,1).lt.2.d0)then
c         attr(i,1 ,:)=700
c         attr(i,nj,:)=700
          attr(i,1 ,:)=200
          attr(i,nj,:)=200
        endif
      enddo


      open(unit=10,file='tunnel.x2d')
      nbi=ni/bsize;nbj=nj/bsize
      nblock=nbi*nbj
      write(10,*)bsize,nblock
      do ii=1,nbi;istart=(ii-1)*bsize
      do jj=1,nbj;jstart=(jj-1)*bsize
        do j=1,bsize+1;j0=jstart+j
        do i=1,bsize+1;i0=istart+i
          write(10,111)attr(i0,j0,1),x(i0,j0,1),y(i0,j0,1)
        enddo;enddo
      enddo;enddo
      close(10)

      stop
  111 format(1x,i6,3e30.20)
      end


  
c     $Rev::             $: Revision of last commit
c     $Author::          $: Author of last commit
c     $Date::            $: Date of last commit
      subroutine distfunc(nn,dinp,ival,ndist,errchk,fdist)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      implicit none
      integer::nn,ndist
      real(kind=8),dimension(ndist)::fdist,dy
      real(kind=8),dimension(nn,3)::dinp
      real(kind=8) s0,s1,d0,d1
      real(kind=8) len,delta1,delta2,error
      integer n0,n1,ns,nf
      integer i,j,ival,n,errchk,err


      fdist(1)=0.d0
      s0=dinp(1,1);d0=dinp(1,2);n0=int(dinp(1,3))
      do i=2,nn
        s1=dinp(i,1);d1=dinp(i,2);n1=int(dinp(i,3))
        len=s1-s0
        delta1=d0
        delta2=d1
        err=1
        print*,' inner ',ival
        print*,delta1,delta2,len
        print*,n0,n1,err
        if(ival.eq.1)call tanhdist(delta1,delta2,len,n0,n1,err,dy(n0))
        if(ival.eq.2)call sinhdist(delta1,delta2,len,n0,n1,err,dy(n0))
        if(errchk.eq.1.and.err.eq.-1)then
          delta1=dy(n0+1)-dy(n0)
          delta2=dy(n1)-dy(n1-1)
          write(*,'(2x,a35)')' Convergence problem in distfunc : '
          write(*,'(2x,a10,i5,i5,i5)')'Section : ',i,n0,n1
          error=abs(d0-delta1)/d0*100;if(d0.lt.0.d0)error=0.d0
          write(*,'(2x,a24,f20.4)')'Start err in percent  : ',error
          error=abs(d1-delta2)/d1*100;if(d1.lt.0.d0)error=0.d0
          write(*,'(2x,a24,f20.4)')'End   err in percent  : ',error
        endif
        do j=n0+1,n1
        fdist(j)=fdist(n0)+dy(j)
        enddo
        s0=s1;d0=d1;n0=n1
      enddo
      return
      do i=9,ndist,16
        fdist(i)=.5*(fdist(i-8)+fdist(i+8))
      enddo
      do i=5,ndist,8
        fdist(i)=.5*(fdist(i-4)+fdist(i+4))
      enddo
      do i=3,ndist,4
        fdist(i)=.5*(fdist(i-2)+fdist(i+2))
      enddo
      do i=2,ndist,2
        fdist(i)=.5*(fdist(i-1)+fdist(i+1))
      enddo
      do n=1,12
        do i=2,ndist-1
          fdist(i)=.25d0*(fdist(i-1)+2*fdist(i)+fdist(i+1))
        enddo
      enddo
      return
   21 format('#',i6)
      end

      subroutine tanhdist(delta1,delta2,len,i1,i2,err,fdist)
************************************************************************
*     TANDIST
*     program for computing the hyperbolic tangent distribution
*     delta1 : grid spacing at i=i1 (first cell)
*     delta2 : grid spacing at i=i2 (last  cell)
*     len    : lenght of curve
*     i1     : indices of first vertex
*     i2     : indices of last vertex
*     fdist  : compute distribution function
*
*
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      integer i1,i2,ni,i,err
      real(kind=8)::delta1,delta2,len
      real(kind=8)::delta,b,a
      real(kind=8)::fdist(i2-i1+1)
      real(kind=8)::transsinh,transtanh,ftmp
      external transsinh,transtanh

      if(i2.eq.i1)return

c------- Normalize delta1 and delta2 with lenght
      delta1=delta1/len
      delta2=delta2/len
c------- Compute number of points
      ni=i2-i1


c------- Assure that B > 1
      if(delta1.le.0.d0.and.1/delta2.lt.ni)then
        delta1=1/(ni**2*delta2*1.02)
      elseif(delta2.le.0d0.and.1/delta1.lt.ni)then
        delta2=1/(ni**2*delta1*1.02)
      endif

c------- Spacing at both end specified
      if(delta1.gt.0.d0.and.delta2.gt.0.d0)then
        a=sqrt(delta2)/sqrt(delta1)
        b=1.d0/(ni*sqrt(delta1*delta2))
        if(b.ge.1.d0)then
          delta=transsinh(b,err)
          do i=1,ni+1
            ftmp=.5*(1+tanh(delta*(real(i-1)/real(ni)-0.5))
     &          /tanh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        else
          delta=transtanh(b,err)
          do i=1,ni+1
            ftmp=.5*(1+sinh(delta*(real(i-1)/real(ni)-0.5))
     &          /sinh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        endif
c------- Spacing only specified at xi=1
      else if(delta1.gt.0.d0)then
        b=1.d0/(ni*delta1)
        delta=transsinh(b,err)
        do i=1,ni+1
          fdist(i)=1.d0+tanh(.5d0*delta*(real(i-1)/real(ni)-1.d0))
     &            /tanh(.5d0*delta)
        enddo
c------- Spacing only specified at xi=ni
      else if(delta2.gt.0.d0)then
        b=1.d0/(ni*delta2)
        delta=transsinh(b,err)
        do i=1,ni+1
          fdist(i)=tanh(.5*delta*real(i-1)/real(ni))/tanh(.5*delta)
        enddo
c------- Error no spacing is given
      else
        print*,' Error from tandist, no cell hight is given '
      endif
c------- Compute len*fdist
      do i=1,ni+1
        fdist(i)=fdist(i)*len
      enddo
      return
      end

      subroutine sinhdist(delta1,delta2,len,i1,i2,err,fdist)
************************************************************************
*     SINHDIST
*     program for computing the hyperbolic sine distribution
*     delta1 : grid spacing at i=i1 (first cell)
*     delta2 : grid spacing at i=i2 (last  cell)
*     len    : lenght of curve
*     i1     : indices of first vertex
*     i2     : indices of last vertex
*     fdist  : compute distribution function
*
*
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      integer i1,i2,ni,i,err
      real(kind=8)::delta1,delta2,len
      real(kind=8)::delta,b,a
      real(kind=8)::fdist(*)
      real(kind=8)::transsinh,transtanh,ftmp
      external transsinh,transtanh

      if(i2.eq.i1)return

c------- Normalize delta1 and delta2 with lenght
      delta1=delta1/len
      delta2=delta2/len
c------- Compute number of points
      ni=i2-i1

c------- Assure that B > 1
      if(delta1.le.0.d0.and.1/delta2.lt.ni)then
        delta1=1/(ni**2*delta2*1.02)
      elseif(delta2.le.0d0.and.1/delta1.lt.ni)then
        delta2=1/(ni**2*delta1*1.02)
      endif

c------- Spacing at both end specified
      if(delta1.gt.0.d0.and.delta2.gt.0.d0)then
        a=sqrt(delta2)/sqrt(delta1)
        b=1.d0/(ni*sqrt(delta1*delta2))
        if(b.ge.1.d0)then
          delta=transsinh(b,err)
          do i=1,ni+1
            ftmp=.5*(1+tanh(delta*(real(i-1)/real(ni)-0.5))
     &          /tanh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        else
          delta=transtanh(b,err)
          do i=1,ni+1
            ftmp=.5*(1+sinh(delta*(real(i-1)/real(ni)-0.5))
     &          /sinh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        endif
c------- Spacing only specified at xi=1
      else if(delta1.gt.0.d0)then
        b=1.d0/(ni*delta1)
        delta=transsinh(b,err)
        do i=1,ni+1
          fdist(i)=sinh(delta*real(i-1)/real(ni))/sinh(delta)
        enddo
c------- Spacing only specified at xi=ni
      else if(delta2.gt.0.d0)then
        b=1.d0/(ni*delta2)
        delta=transsinh(b,err)
        do i=1,ni+1
          fdist(i)=1-sinh(delta*(1-real(i-1)/real(ni)))/sinh(delta)
        enddo
c------- Error no spacing is given
      else
        print*,' Error from tandist, no cell hight is given '
      endif
c------- Compute len*fdist
      do i=1,ni+1
        fdist(i)=fdist(i)*len
      enddo
      return
      end

      double precision function transsinh(b,err)
************************************************************************
*     routine for solving the transendental equation
*
*     b=sinh(delta)/delta
*
*     using NewtonRaphson
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      integer::err
      real(kind=8)::delta,b
      real(kind=8)::delta_old,f,df,rlim
      parameter(rlim=1.d-1)
      integer::n,nmax=250

c------- Preforme Newton iteration to obtain delta
      delta=1.d0
      delta_old=delta
      do n=1,nmax
c--------- Compute f(delta)
c        f =b-sinh(delta)/delta
c        f =b*delta-sinh(delta)
         f =delta/(sinh(delta)+1.d-60)-1.d0/b
c--------- Compute d/d(delta)(f(delta))
c        df=(sinh(delta)-delta*cosh(delta))/delta**2
c        df=b-cosh(delta)
         df=(sinh(delta)-delta*cosh(delta))/sinh(delta)**2
c--------- update delta
        delta=delta-.5*f/df
c       delta=delta-f/df
c       write(*,'(4e16.4)')f,df,f/df,delta
c--------- check convergence criterium
        if(n.eq.nmax.and.abs((delta-delta_old)/delta_old).gt.rlim)then
          err=-1
c         print*,' Convergence problem in transinh dist. function !!! '
c         print*,' residual ',(delta-delta_old)/delta_old
        endif
c--------- update old value
        delta_old=delta
      enddo
      transsinh=delta
      return
      end

      double precision function transtanh(b,err)
************************************************************************
*     routine for solving the transendental equation
*
*     b=tanh(delta/2)/(delta/2)
*
*     using NewtonRaphson
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      integer::err
      real(kind=8)::delta,b
      real(kind=8)::delta_old,f,df,rlim
      parameter(rlim=1.d-1)
      integer::n,nmax=250

      if(b.gt.1.0d0)then
        print*,' Error form sinhdist: Can not be gridded '
c       stop
      endif
c------- Preforme Newton iteration to obtain delta
      delta=1.d0
      delta_old=delta
      do n=1,nmax
c--------- Compute f(delta)
        f =delta/(tanh(.5*delta)+1.d-30)-2./B
c--------- Compute d/d(delta)(f(delta))
        df=(tanh(.5*delta)-delta*(1-tanh(.5*delta)**2))
     &    /(tanh(.5*delta)**2+1.d-30)
c--------- update delta
        delta=delta-.5*f/df
c       delta=delta-f/df
c       write(*,'(i3,1x,3e16.4)')n,delta,f,df
c--------- check convergence criterium
        if(n.eq.nmax.and.abs((delta-delta_old)/delta_old).gt.rlim)then
          err=-1
c         print*,' Convergence problem in trantanh dist. function !!! '
c         print*,' residual ',(delta-delta_old)/delta_old
        endif
c--------- update old value
        delta_old=delta
      enddo
      transtanh=delta
      return
      end
