#######################################################################
# A small script for generating a 3D airfoil in wind tunnel mesh
# The script needs a airfoil surface mesh in 2D (airfoil.sf), based
# on this it first generates a 2D mesh (grid.x2d) and finally a
# 3D mesh grid.x3d
#
# Niels N. Soerensen
# 07-07-2012
#######################################################################
EPATH=/home/nsqr/git/ellipsys_tools/tunnelmesh/
rm -r tmpdir
mkdir tmpdir
cd tmpdir
cp ../airfoil.sf .

###   INPUT SECTION START

# AOA and rotation centre
AOA=3
#AOA=3 
#AOA=0 
XC=0.225
YC=0.0

# move outer boundary cut before generating hypgrid mesh
CMOVE=33 
# move airfoil cut to compensate for the airfoil pitch
# 20 ACMOVE=290
# 16 ACMOVE=315
# 25 ACMOVE=290 
#ACMOVE=315
ACMOVE=0

# 3D input
# Spanwise length and number of points
LSPAN=1.8
#NSPAN=193
#NSPAN=33
NSPAN=193
BCTYPE=4  # BCTYPE=1 symmetry, BCTYPE=2 periodic, BCTYPE=3 wall/symmetry

## If wall resolved flow is needed generate distribution function
#echo 2                    >3d.distfunc
#echo 0.0 5.d-7  1        >>3d.distfunc
#echo 1.0 0.015  65        >>3d.distfunc
#echo 4                    >3d.distfunc
#echo 0.0 1.d-6  1        >>3d.distfunc
#echo 0.05 0.015313d0 37  >>3d.distfunc
#echo 0.95 0.015313d0 157 >>3d.distfunc
#echo 1.0 1.d-6  193      >>3d.distfunc
 echo 4                    >3d.distfunc
 echo 0.0 1.d-6  1        >>3d.distfunc
 echo 0.03 0.0151d0 41  >>3d.distfunc
 echo 0.97 0.0151d0 153 >>3d.distfunc
 echo 1.0 1.d-6  193      >>3d.distfunc
#rm distfunc.dat

###   INPUT SECTION END

#======================================================================
# Generating the outer surface for the airfoil grid generation
#    sfout.dat
#======================================================================

## Generate file for surfgen
# Boundaries for the airfoil grid
XM=-0.50d0
XP=1.55d0
YM=-0.75d0
YP=0.75d0

echo '#SURFACE DESCRIPTION'      >geometry.dat
echo '#FACE1 4'                 >>geometry.dat
echo '#SEC1  2'                 >>geometry.dat
echo  $XP  $YP                 >>geometry.dat
echo  $XP  $YM                  >>geometry.dat
echo  '#SEC1  2'                >>geometry.dat
echo  $XP  $YM               >>geometry.dat
echo  $XM  $YM               >>geometry.dat
echo '#SEC3  2'                 >>geometry.dat
echo  $XM  $YM               >>geometry.dat
echo  $XM  $YP              >>geometry.dat
echo '#SEC4  2'                 >>geometry.dat
echo  $XM  $YP              >>geometry.dat
echo  $XP  $YP              >>geometry.dat
echo '#DISTRIBUTION FUNCTIONS'  >>geometry.dat
echo '#FACE1 4'                 >>geometry.dat
#echo '#SEC1 3  65'              >>geometry.dat
echo '#SEC1 2  65'              >>geometry.dat
echo '  0.0 0.03 1'             >>geometry.dat
#echo '  0.5 0.01 33'            >>geometry.dat
echo '  1.0 0.03 65'            >>geometry.dat
#echo '#SEC2 3  97'              >>geometry.dat
echo '#SEC2 2  97'              >>geometry.dat
echo '  0.0 0.03 1'             >>geometry.dat
#echo '  0.4 0.01  49'           >>geometry.dat
echo '  1.0 0.03 97'            >>geometry.dat
#echo '#SEC3 3  65'              >>geometry.dat
echo '#SEC3 2  65'              >>geometry.dat
echo '  0.0 0.03 1'             >>geometry.dat
#echo '  0.5 0.01 33'            >>geometry.dat
echo '  1.0 0.03 65'            >>geometry.dat
#echo '#SEC4 3  97'              >>geometry.dat          
echo '#SEC4 2  97'              >>geometry.dat          
echo '  0.0 0.03 1'             >>geometry.dat
#echo '  0.6 0.01  49'           >>geometry.dat
echo '  1.0 0.03 97'            >>geometry.dat

$EPATH/surfgen
mv sf.dat sfout.dat


#======================================================================
# Generate the outer wind tunnel geometry based on the sfout.dat
# file
#======================================================================

#$EPATH/tunnel <<END
#97 65
#25.0d0  65 
#15.d0 129
#1.5d0 33 
#32
#END
$EPATH/tunnel <<END
97 65
2.8d0 33
5.d0 97
0.256d0 33 
32
END



#======================================================================
# Generate the inner airfoil grid
#======================================================================

## reposition the outer boundary (sfout.dat)

$EPATH/movecut <<END
$CMOVE
END
mv sfout.DAT sfout.dat

## rotate the airfoil

$EPATH/moveairfoilcut <<END
$ACMOVE
$AOA
$XC $YC
END
mv airfoil.SF sf.dat
#cp sf.dat prof.dat
#cp prof.dat ../.


## construct input file gcf.dat

#meshtype to generate
echo    meshtype omesh                                                               >gcf.dat
#---------  number of mesh points
#echo    meshpoints 320 160                                                          >>gcf.dat
echo    meshpoints 320 128                                                           >>gcf.dat
#--------- domain quantities 
echo    domain-height 0.5d0 1.d-6                                                    >>gcf.dat
# distribution functions::  np x/lmax delta i/imax ......
echo   surface-distribution 3 0.0 0.0001 .01 0.5  0.001 0.5 1.0 0.0001 1.d0         >>gcf.dat
echo   normal-distribution 4 0.0 1.d-5 .01 0.06 .005 .4 0.68 0.005 .87 1.d0 .1 1.d0 >>gcf.dat
#--------- stretching function 
echo   stretching-function tanh                                                     >>gcf.dat
#-------- volume blend factor
echo   volume-blend 0.60d0                                                           >>gcf.dat
#-------- dissipation factor 
echo   dissipation-factor 1.0d0                                                     >>gcf.dat
#-------- omesh outelet portion
echo   omesh-outlet 32 32                                                           >>gcf.dat
#-------- wake contractionfor omesh
echo   wake-contraction 0.001d0                                                     >>gcf.dat 
#-------- projection towards outer boundary
#echo   projection 10 100 103 0.d0 12.d0                                              >>gcf.dat
echo   projection 10 40 70 0.d0 12.d0                                              >>gcf.dat
#---------move mesh cut
#echo   move-omesh-cut 288                                                            >>gcf.dat
echo   block-size 32                                                                >>gcf.dat

$EPATH/hypgrid <<END
20 .5
END
mv grid.X2D airfoil.x2d
#exit

#======================================================================
# assemble airfoil and tunnel grid to one grid
#======================================================================

## make file for excluding grid blocks

 echo 6   >exclude.dat
 echo 7  >>exclude.dat
 echo 10 >>exclude.dat
 echo 11 >>exclude.dat
 echo 14 >>exclude.dat
 echo 15 >>exclude.dat
#echo 10 >>exclude.dat
#echo 11 >>exclude.dat
#echo 14 >>exclude.dat
#echo 15 >>exclude.dat
#echo 18 >>exclude.dat
#echo 19 >>exclude.dat

$EPATH/assemble-tunnel

cp grid.x2d ../.
cd ..
dg
exit


#======================================================================
# generate 3D grid
#======================================================================

cp 3d.distfunc distfunc.dat

$EPATH/pseudo3d <<END
$LSPAN
$NSPAN
$BCTYPE
32
1
END
