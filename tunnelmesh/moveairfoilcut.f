      program movecutairfoilcut
      implicit none
      integer ni,ncut,i
      character(len=1)::adum
      real(kind=8),allocatable,dimension(:)::x,y
      real(kind=8)::pi,xtmp,ytmp,angle,xrot,yrot
      real(kind=8),dimension(2)::n1,n2,a
      

      open(unit=10,file='airfoil.sf')
      read(10,*)adum,ni
      allocate(x(ni),y(ni))
      do i=1,ni
        read(10,*)x(i),y(i)
      enddo
      close(10)


      print*,' Give number of points to move cut '
      read*,ncut
      ncut=max(1,ncut)

      print*,' Give rotation angle, positive counter clock '
      read*,angle
      print*,' Give centre of rotation x and y '
      read*,xrot,yrot

      pi=4.d0*atan(1.d0)
c-----turning angle
      angle=angle/180.d0*pi
      n1(1)= cos(angle)
      n1(2)= sin(angle)
      n2(1)=-sin(angle)
      n2(2)= cos(angle)

      do i=1,ni
        a(1)=x(i)-xrot
        a(2)=y(i)-yrot
        x(i)=n1(1)*a(1)+n1(2)*a(2)+xrot
        y(i)=n2(1)*a(1)+n2(2)*a(2)+yrot
      enddo 

      open(unit=10,file='airfoil.SF')
      write(10,'(a1,1x,i6)')'#',ni
      do i=ncut,ni
        write(10,*)x(i),y(i)
      enddo
      do i=2,ncut
        write(10,*)x(i),y(i)
      enddo

      stop
      end
 
