      program AssembleTunnelGrid
      implicit none
      integer ni,nblock,nblock1,nblock2,i,j,n,nn
      real(kind=8),dimension(:,:,:),allocatable::x1,y1,x2,y2,x,y
      integer,dimension(:,:,:),allocatable::attr1,attr2
      integer,dimension(:,:,:),allocatable::attr
      integer,dimension(:),allocatable::exclude
     
      open(unit=10,file='tunnel.x2d')
      read(10,*)ni,nblock1
      ni=ni+1
      allocate(x1(ni,ni,nblock1)
     &        ,y1(ni,ni,nblock1)
     &     ,attr1(ni,ni,nblock1))
      do n=1,nblock1
      do j=1,ni
      do i=1,ni
        read(10,*)attr1(i,j,n),x1(i,j,n),y1(i,j,n)
      enddo;enddo
      enddo
      close(10)

      open(unit=10,file='airfoil.x2d')
      read(10,*)i,nblock2
      if(i.ne.ni-1)then
        print*,' Grids not of same block size : '
        stop
      endif
      allocate(x2(ni,ni,nblock2)
     &        ,y2(ni,ni,nblock2)
     &     ,attr2(ni,ni,nblock2))

      do n=1,nblock2
      do j=1,ni
      do i=1,ni
        read(10,*)attr2(i,j,n),x2(i,j,n),y2(i,j,n)
        if(attr2(i,j,n).ne.101)attr2(i,j,n)=1
      enddo;enddo
      enddo
      close(10)

c-------read information about which blocks to exclude
      allocate(exclude(nblock1))
      exclude=1
      open(unit=10,file='exclude.dat')
      do 
        read(10,*,end=111)n
        exclude(n)=0
      enddo
  111 close(10)

c-------compute number of blocks in final grid
      nblock=0
      do n=1,nblock1
        if(exclude(n).eq.1)nblock=nblock+1
      enddo
      nblock=nblock+nblock2
      
c------generate new grid
      allocate(x(ni,ni,nblock),y(ni,ni,nblock),attr(ni,ni,nblock))
      nn=0
c     attr(:,:,:)=1
      do n=1,nblock1
        if(exclude(n).ne.1)cycle 
        nn=nn+1
        attr(:,:,nn)=attr1(:,:,n)
           x(:,:,nn)=x1   (:,:,n)
           y(:,:,nn)=y1   (:,:,n)
      enddo
      do n=1,nblock2
        nn=nn+1
        attr(:,:,nn)=attr2(:,:,n)
           x(:,:,nn)=x2   (:,:,n)
           y(:,:,nn)=y2   (:,:,n)
      enddo
  
      open(unit=10,file='grid.x2d')
      write(10,*)ni-1,nblock
      do n=1,nblock
      do j=1,ni
      do i=1,ni
        write(10,*)attr(i,j,n),x(i,j,n),y(i,j,n)
      enddo;enddo
      enddo
      close(10)
     
      stop
      end
