      module boundary
      integer::imax,isec
      parameter(imax=2000,isec=30)
      integer::nrsector
      integer,dimension(isec)::nisecsurf,nisecdist,nisecpoint
      real(kind=8),dimension(imax,isec)::xface,yface
      real(kind=8),dimension(20,3,isec)::distinp
      end module

      program main
************************************************************************
*
*
************************************************************************
* Author  : Niels N. Sorensen
* Date    :
************************************************************************
      implicit none

c------- READ GEOMETRY DATA
      call ReadGeometry

c------- ASSURE THAT FACES FROM CLOSED CURVES
      call CloseFaces

c------- DISTRIBUT MESH ON FACES
      call FaceMesh

      stop
      end

      subroutine ReadGeometry
************************************************************************
*     Routine for reading the geometry
*
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      use boundary
      implicit none
      integer sec,i
      integer,parameter::mword=20
      integer nrchar(mword),nrword
      character(len=256)InputLine
      character(len=40),dimension(mword)::words
      open(unit=10,file='geometry.dat')

c------- READ SURFACE INFORMATION
      print*,' READING SURFACE INFORMATION '
      read(10,'(a256)')InputLine
      print*,InputLine 
c--------- READ THE NUMBER OF SECTORS ON THE PRESENT FACE (ONLY ONE FACE)
      read(10,'(a256)')InputLine
      call cmdpart(InputLine,mword,nrchar,nrword,words)
      read(words(2),*)nrsector
      print*,'nrsector ',nrsector
      if(nrsector.gt.isec)then
        print*,' The geometry file has to many sections : ',isec
          print*,' Recompile with larger array size : '
          stop
        endif
        do 200 sec=1,nrsector
c----------- READ THE NUMBER OF DATA POINTS ON THE PRESENT SECTOR
          read(10,'(a256)')InputLine
          print*,InputLine 
          call cmdpart(InputLine,mword,nrchar,nrword,words)
          read(words(2),*)nisecsurf(sec)
          if(nisecsurf(sec).gt.imax)then
            print*,' A sections is larger than imax : ',imax
            print*,' Recompile with larger array size : '
            stop
          endif
          do 300 i=1,nisecsurf(sec)
c-------------- READ THE DATA POINTS OF THE PRESENT SECTOR
            read(10,*)xface(i,sec),yface(i,sec)
  300     continue
  200   continue
      print*,' FINISHED READING SURFACE INFORMATION '

c------- READ DISTRIBUTION INFORMATION
      print*,' READING DISTRIBUTION FUNCTIONS '
      read(10,'(a256)')InputLine
      call cmdpart(InputLine,mword,nrchar,nrword,words)
c--------- READ THE NUMBER OF SECTORS ON THE PRESENT FACE
      read(10,'(a256)')InputLine
      do sec=1,nrsector
c----------- READ THE NUMBER OF DATA POINTS ON THE PRESENT SECTOR
        read(10,'(a256)')InputLine
        call cmdpart(InputLine,mword,nrchar,nrword,words)
        read(words(2),*)nisecdist(sec)
        read(words(3),*)nisecpoint(sec)
        print*,'face',1,nisecdist(sec),nisecpoint(sec)
        if(nisecdist(sec).gt.imax)then
          print*,' A dist sections is larger than imax : ',imax
          print*,' Recompile with larger array size : '
          stop
        endif
        if(nisecdist(sec).lt.0)then
          nisecdist(sec)=2
          read(10,*)distinp(1,2,sec),distinp(2,2,sec)
          distinp(1,1,sec)=0.d0;distinp(1,3,sec)=1
          distinp(2,1,sec)=1.d0;distinp(2,3,sec)=nisecpoint(sec)
        else
          do i=1,nisecdist(sec)
c------------ READ THE DATA POINTS OF THE PRESENT SECTOR
c             normalized curve length, actual cell size, point number
            read(10,*)distinp(i,1,sec),distinp(i,2,sec),distinp(i,3,sec)
          enddo
        endif
      enddo
      print*,'finished'

      return
      end

      subroutine CloseFaces
************************************************************************
*     Assure that faces form closed curves. This is done by setting
*     the coordinat values of the last point in a sector equal to
*     the coordinat values of the first point in the following sector.
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      use boundary
      implicit none
      integer sec,i

c------- ASSURE THAT FACES FORM CLOSED CURVES
      do sec =1,nrsector-1
        xface(nisecsurf(sec),sec)=xface(1,sec+1)
        yface(nisecsurf(sec),sec)=yface(1,sec+1)
      enddo
      return
      end

      subroutine FaceMesh
************************************************************************
*
*
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      use boundary
      implicit none
      integer sec,i,nit,ii
      real(kind=8) sp(imax),xps(imax),yps(imax),xf(imax),yf(imax)
     &            ,fx(imax)
      real(kind=8) sd(imax),xd(imax),yd(imax),yds(imax)
      real(kind=8) dx,dy,yp1,ypn,xsp,ysp,xi1,xi2,length
      real(kind=8),dimension(:),allocatable::fs
      real(kind=8),dimension(:,:),allocatable::SurfaceDistInp
      character(len=9) fname
      character(len=1) num
      real(kind=8)::xold,yold
      logical statex

c-------compute number of grid points
      nit=1
      do sec=1,nrsector
        nit=nit+nisecpoint(sec)-1
      enddo

      fname(1:8)='sf.dat'
c-------- OPEN FILE FOR SURFACE DATA
      open(unit=10,file=fname(1:8))
      write(10,'(a1,1x,i6)')'#',nit
      do sec=1,nrsector
c=======spline profile data
        do i=1,nisecsurf(sec)
          xf(i)=xface(i,sec)
          yf(i)=yface(i,sec)
        enddo
        sp(1)=0.d0
        do i=2,nisecsurf(sec)
c-----calculate first derivative
          dx=xf(i)-xf(i-1)
          dy=yf(i)-yf(i-1)
c-----calculate curve length
          sp(i)=sp(i-1)+sqrt(dx*dx+dy*dy)
        enddo
c-----normalize curve length
        length=sp(nisecsurf(sec))
        do i=1,nisecsurf(sec)
          sp(i)=sp(i)/sp(nisecsurf(sec))
        enddo
c-------spline representation of surface (natural cubic spline)
        yp1=1.d32
        ypn=1.d32
        call spline(sp,xf,nisecsurf(sec),yp1,ypn,xps)
        call spline(sp,yf,nisecsurf(sec),yp1,ypn,yps)

c-------- compute distribution fucntion
        if(allocated(fs))deallocate(surfacedistinp,fs)
        allocate(fs(nisecpoint(sec)),SurfaceDistInp(nisecdist(sec),3))
        SurfaceDistInp(1:nisecdist(sec),:)=
     &         DistInp(1:nisecdist(sec),:,sec)
        surfacedistinp(1,3)=1
        surfacedistinp(:,2)=surfacedistinp(:,2)/length
        call DistFunc(nisecdist(sec),SurfaceDistInp,1
     &                ,nisecpoint(sec),1,fs)

c-------distribute points
        xold=0.d0;yold=0.d0
        do i=1,nisecpoint(sec)-1
          xold=xsp;yold=ysp
          call splint(sp,xf,xps,nisecsurf(sec),fs(i),xsp)
          call splint(sp,yf,yps,nisecsurf(sec),fs(i),ysp)
          write(10,*)xsp,ysp
          if(i.eq.2)then
          length=sqrt((xsp-xold)**2+(ysp-yold)**2)
          print*,' length ',sec,length
          endif
        enddo
        length=sqrt((xsp-xold)**2+(ysp-yold)**2)
        print*,' length ',sec,length
        xold=xsp;yold=ysp
      enddo
      call splint(sp,xf,xps,nisecsurf(nrsector),1.d0,xsp)
      call splint(sp,yf,yps,nisecsurf(nrsector),1.d0,ysp)
      write(10,*)xsp,ysp
      close(10)

      return
      end

      subroutine spline(x,y,n,yp1,ypn,y2)
c----------------------------------------------------------------------
c     Given arrays x and y of length n containing a tabulated function,
c     i.e y(i)=f(x(i)), with x(1)<x(2)<...<x(n), and given values yp1
c     and ypn for the first derivative of the interpolating function at
c     points 1 and n, respectively, this routine returns an array y2 of
c     lenght n wich contains the second derivatives of the interpolating
c     function at the tabulated points x(i). Uf yp1 and/or ypn are equal
c     to 1.d30 or larger, the routine is signalled to the corresponding
c     boundary condition for a natural spline, with zero second derivative
c     on that boundary
c
c     Numerical Recipes
c----------------------------------------------------------------------
      implicit none
      integer nmax
      parameter(nmax=1000)
      integer i,n,k
      real(kind=8) x(n),y(n),y2(n),u(nmax)
      real(kind=8) qn,p,un,sig,yp1,ypn


      if (yp1.gt..99e30) then
        y2(1)=0.
        u(1)=0.
      else
        y2(1)=-0.5
        u(1)=(3./(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
      endif
      do 11 i=2,n-1
        sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
        p=sig*y2(i-1)+2.
        y2(i)=(sig-1.)/p
        u(i)=(6.*((y(i+1)-y(i))/(x(i+1)-x(i))-(y(i)-y(i-1))
     *      /(x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*u(i-1))/p
11    continue
      if (ypn.gt..99e30) then
        qn=0.
        un=0.
      else
        qn=0.5
        un=(3./(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
      endif
      y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.)
      do 12 k=n-1,1,-1
        y2(k)=y2(k)*y2(k+1)+u(k)
12    continue
      return
      end

      subroutine splint(xa,ya,y2a,n,x,y)
c----------------------------------------------------------------------
c     Given the arrays xa and ya of length n, wich tabulate a function
c     (with the xa's in order), and given the array y2a, wich is the
c     output from spline above, and given a value of x, this routine
c     returns a cubic-spline interpolated value y.
c
c     Numerical Recipes
c----------------------------------------------------------------------
      implicit none
      integer n
      real(kind=8) xa(n),ya(n),y2a(n),x,y
      real(kind=8) a,b,h
      integer klo,khi,k

      klo=1
      khi=n
1     if (khi-klo.gt.1) then
        k=(khi+klo)/2
        if(xa(k).gt.x)then
          khi=k
        else
          klo=k
        endif
      goto 1
      endif
      h=xa(khi)-xa(klo)
      if (h.eq.0.) then
        print*,'bad xa input.'
        stop
      endif
      a=(xa(khi)-x)/h
      b=(x-xa(klo))/h
      y=a*ya(klo)+b*ya(khi)+
     *      ((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.
      return
      end


      subroutine distfunc(nn,dinp,ival,ndist,errchk,fdist)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      implicit none
      integer::nn,ndist
      real(kind=8),dimension(ndist)::fdist,dy
      real(kind=8),dimension(nn,3)::dinp
      real(kind=8) s0,s1,d0,d1
      real(kind=8) len,delta1,delta2,error
      integer n0,n1,ns,nf
      integer i,j,ival,n,errchk,err


      fdist(1)=0.d0
      s0=dinp(1,1);d0=dinp(1,2);n0=int(dinp(1,3))
      do i=2,nn
        s1=dinp(i,1);d1=dinp(i,2);n1=int(dinp(i,3))
        len=s1-s0
        delta1=d0
        delta2=d1
        err=1
        if(ival.eq.1)call tanhdist(delta1,delta2,len,n0,n1,err,dy(n0))
        if(ival.eq.2)call sinhdist(delta1,delta2,len,n0,n1,err,dy(n0))
        if(errchk.eq.1.and.err.eq.-1)then
          delta1=dy(n0+1)-dy(n0)
          delta2=dy(n1)-dy(n1-1)
          write(*,'(2x,a35)')' Convergence problem in distfunc : '
          write(*,'(2x,a10,i5,i5,i5)')'Section : ',i,n0,n1
          error=abs(d0-delta1)/d0*100;if(d0.lt.0.d0)error=0.d0
          write(*,'(2x,a24,f20.4)')'Start err in percent  : ',error
          error=abs(d1-delta2)/d1*100;if(d1.lt.0.d0)error=0.d0
          write(*,'(2x,a24,f20.4)')'End   err in percent  : ',error
        endif
          delta1=dy(n0+1)-dy(n0)
          delta2=dy(n1)-dy(n1-1)
        do j=n0+1,n1
        fdist(j)=fdist(n0)+dy(j)
        enddo
        s0=s1;d0=d1;n0=n1
      enddo
      return
      do i=9,ndist,16
        fdist(i)=.5*(fdist(i-8)+fdist(i+8))
      enddo
      do i=5,ndist,8
        fdist(i)=.5*(fdist(i-4)+fdist(i+4))
      enddo
      do i=3,ndist,4
        fdist(i)=.5*(fdist(i-2)+fdist(i+2))
      enddo
      do i=2,ndist,2
        fdist(i)=.5*(fdist(i-1)+fdist(i+1))
      enddo
      do n=1,12
        do i=2,ndist-1
          fdist(i)=.25d0*(fdist(i-1)+2*fdist(i)+fdist(i+1))
        enddo
      enddo
      return
   21 format('#',i6)
      end

      subroutine tanhdist(delta1,delta2,len,i1,i2,err,fdist)
************************************************************************
*     TANDIST
*     program for computing the hyperbolic tangent distribution
*     delta1 : grid spacing at i=i1 (first cell)
*     delta2 : grid spacing at i=i2 (last  cell)
*     len    : lenght of curve
*     i1     : indices of first vertex
*     i2     : indices of last vertex
*     fdist  : compute distribution function
*
*
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      integer i1,i2,ni,i,err
      real(kind=8)::delta1,delta2,len
      real(kind=8)::delta,b,a
      real(kind=8)::fdist(i2-i1+1)
      real(kind=8)::transsinh,transtanh,ftmp
      external transsinh,transtanh

      if(i2.eq.i1)return

c------- Normalize delta1 and delta2 with lenght
      delta1=delta1/len
      delta2=delta2/len
c------- Compute number of points
      ni=i2-i1


c------- Assure that B > 1
      if(delta1.le.0.d0.and.1/delta2.lt.ni)then
        delta1=1/(ni**2*delta2*1.02)
      elseif(delta2.le.0d0.and.1/delta1.lt.ni)then
        delta2=1/(ni**2*delta1*1.02)
      endif

c------- Spacing at both end specified
      if(delta1.gt.0.d0.and.delta2.gt.0.d0)then
        a=sqrt(delta2)/sqrt(delta1)
        b=1.d0/(ni*sqrt(delta1*delta2))
        if(b.ge.1.d0)then
          delta=transsinh(b,err)
          do i=1,ni+1
            ftmp=.5*(1+tanh(delta*(real(i-1)/real(ni)-0.5))
     &          /tanh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        else
          delta=transtanh(b,err)
          do i=1,ni+1
            ftmp=.5*(1+sinh(delta*(real(i-1)/real(ni)-0.5))
     &          /sinh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        endif
c------- Spacing only specified at xi=1
      else if(delta1.gt.0.d0)then
        b=1.d0/(ni*delta1)
        delta=transsinh(b,err)
        do i=1,ni+1
          fdist(i)=1.d0+tanh(.5d0*delta*(real(i-1)/real(ni)-1.d0))
     &            /tanh(.5d0*delta)
        enddo
c------- Spacing only specified at xi=ni
      else if(delta2.gt.0.d0)then
        b=1.d0/(ni*delta2)
        delta=transsinh(b,err)
        do i=1,ni+1
          fdist(i)=tanh(.5*delta*real(i-1)/real(ni))/tanh(.5*delta)
        enddo
c------- Error no spacing is given
      else
        print*,' Error from tandist, no cell hight is given '
      endif
c------- Compute len*fdist
      do i=1,ni+1
        fdist(i)=fdist(i)*len
      enddo
      return
      end

      subroutine sinhdist(delta1,delta2,len,i1,i2,err,fdist)
************************************************************************
*     SINHDIST
*     program for computing the hyperbolic sine distribution
*     delta1 : grid spacing at i=i1 (first cell)
*     delta2 : grid spacing at i=i2 (last  cell)
*     len    : lenght of curve
*     i1     : indices of first vertex
*     i2     : indices of last vertex
*     fdist  : compute distribution function
*
*
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      integer i1,i2,ni,i,err
      real(kind=8)::delta1,delta2,len
      real(kind=8)::delta,b,a
      real(kind=8)::fdist(*)
      real(kind=8)::transsinh,transtanh,ftmp
      external transsinh,transtanh

      if(i2.eq.i1)return

c------- Normalize delta1 and delta2 with lenght
      delta1=delta1/len
      delta2=delta2/len
c------- Compute number of points
      ni=i2-i1

c------- Assure that B > 1
      if(delta1.le.0.d0.and.1/delta2.lt.ni)then
        delta1=1/(ni**2*delta2*1.02)
      elseif(delta2.le.0d0.and.1/delta1.lt.ni)then
        delta2=1/(ni**2*delta1*1.02)
      endif

c------- Spacing at both end specified
      if(delta1.gt.0.d0.and.delta2.gt.0.d0)then
        a=sqrt(delta2)/sqrt(delta1)
        b=1.d0/(ni*sqrt(delta1*delta2))
        if(b.ge.1.d0)then
          delta=transsinh(b,err)
          do i=1,ni+1
            ftmp=.5*(1+tanh(delta*(real(i-1)/real(ni)-0.5))
     &          /tanh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        else
          delta=transtanh(b,err)
          do i=1,ni+1
            ftmp=.5*(1+sinh(delta*(real(i-1)/real(ni)-0.5))
     &          /sinh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        endif
c------- Spacing only specified at xi=1
      else if(delta1.gt.0.d0)then
        b=1.d0/(ni*delta1)
        delta=transsinh(b,err)
        do i=1,ni+1
          fdist(i)=sinh(delta*real(i-1)/real(ni))/sinh(delta)
        enddo
c------- Spacing only specified at xi=ni
      else if(delta2.gt.0.d0)then
        b=1.d0/(ni*delta2)
        delta=transsinh(b,err)
        do i=1,ni+1
          fdist(i)=1-sinh(delta*(1-real(i-1)/real(ni)))/sinh(delta)
        enddo
c------- Error no spacing is given
      else
        print*,' Error from tandist, no cell hight is given '
      endif
c------- Compute len*fdist
      do i=1,ni+1
        fdist(i)=fdist(i)*len
      enddo
      return
      end

      double precision function transsinh(b,err)
************************************************************************
*     routine for solving the transendental equation
*
*     b=sinh(delta)/delta
*
*     using NewtonRaphson
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      integer::err
      real(kind=8)::delta,b
      real(kind=8)::delta_old,f,df,rlim
      parameter(rlim=1.d-1)
      integer::n,nmax=250

c------- Preforme Newton iteration to obtain delta
      delta=1.d0
      delta_old=delta
      do n=1,nmax
c--------- Compute f(delta)
c        f =b-sinh(delta)/delta
c        f =b*delta-sinh(delta)
         f =delta/(sinh(delta)+1.d-60)-1.d0/b
c--------- Compute d/d(delta)(f(delta))
c        df=(sinh(delta)-delta*cosh(delta))/delta**2
c        df=b-cosh(delta)
         df=(sinh(delta)-delta*cosh(delta))/sinh(delta)**2
c--------- update delta
        delta=delta-.5*f/df
c       delta=delta-f/df
c       write(*,'(4e16.4)')f,df,f/df,delta
c--------- check convergence criterium
        if(n.eq.nmax.and.abs((delta-delta_old)/delta_old).gt.rlim)then
          err=-1
c         print*,' Convergence problem in transinh dist. function !!! '
c         print*,' residual ',(delta-delta_old)/delta_old
        endif
c--------- update old value
        delta_old=delta
      enddo
      transsinh=delta
      return
      end

      double precision function transtanh(b,err)
************************************************************************
*     routine for solving the transendental equation
*
*     b=tanh(delta/2)/(delta/2)
*
*     using NewtonRaphson
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      integer::err
      real(kind=8)::delta,b
      real(kind=8)::delta_old,f,df,rlim
      parameter(rlim=1.d-1)
      integer::n,nmax=250

      if(b.gt.1.0d0)then
        print*,' Error form sinhdist: Can not be gridded '
c       stop
      endif
c------- Preforme Newton iteration to obtain delta
      delta=1.d0
      delta_old=delta
      do n=1,nmax
c--------- Compute f(delta)
        f =delta/(tanh(.5*delta)+1.d-30)-2./B
c--------- Compute d/d(delta)(f(delta))
        df=(tanh(.5*delta)-delta*(1-tanh(.5*delta)**2)*0.5d0)
     &    /(tanh(.5*delta)**2+1.d-30)
c--------- update delta
        delta=delta-.5*f/df
c       delta=delta-f/df
c       write(*,'(i3,1x,3e16.4)')n,delta,f,df
c--------- check convergence criterium
        if(n.eq.nmax.and.abs((delta-delta_old)/delta_old).gt.rlim)then
          err=-1
c         print*,' Convergence problem in trantanh dist. function !!! '
c         print*,' residual ',(delta-delta_old)/delta_old
        endif
c--------- update old value
        delta_old=delta
      enddo
      transtanh=delta
      return
      end
      subroutine cmdpart(InputLine,mword,nrchar,nrword,words)
c-----------------------------------------------------------------------
c     routine parting command InputLine into words
c----------------------------------------------------------------------
c
c     Author: Jess A. Michelsen and Niels N. Soerensen
c     last revision :march 3, 1998
c-----------------------------------------------------------------------
      implicit none
      integer mword,nrchar(mword),nrword,i
      integer,parameter::mchar=256
      character(len=40),dimension(mword):: words
      character(len=40)::blank
      character(len=256) InputLine
      integer CurrentPosition,RelativeBlankPosition,BlankPosition
C--------------------------------------------- Reset word counter ------
      nrword=0 
      CurrentPosition=1
C--------------------------------------------- Skip if a comment line --
c     if(index(InputLine(1:mchar),'#').gt.0) return
C---------------------------------------- find distance to next blank --
   20 RelativeBlankPosition=index(InputLine(CurrentPosition:mchar),' ')
      if(CurrentPosition.eq.mchar.or.RelativeBlankPosition.eq.0) return
C---------------------------------------- found the end of a word ------
      if(RelativeBlankPosition.gt.1.and.RelativeBlankPosition.le.40)then
        BlankPosition=CurrentPosition-1+RelativeBlankPosition
        nrword=nrword+1
        if(nrword.gt.mword)return
        words(nrword)=InputLine(CurrentPosition:BlankPosition)
        nrchar(nrword)=RelativeBlankPosition-1
      endif
c-----------------------------------------------------------------------
      CurrentPosition=CurrentPosition+RelativeBlankPosition
      goto 20
c-----------------------------------------------------------------------
      end
