      program X2D2x2d
      implicit none
      logical::statex=.false.
      integer::ni
      integer,parameter::idp4=4,idp8=8,ncomp=4
      real(kind=8),dimension(:,:,:,:),allocatable::var
      real(kind=8),dimension(:,:,:),allocatable::x
      integer,dimension(:,:,:),allocatable::attr
      real(kind=8)::dummy
      character*11 fnamd,fnamh
      character*128 fieldname(ncomp)
      integer::idummy,uni
      integer::i,j,k,n,nc,nvar,fileform
      integer::bsize,nblock,b1,iblock

      inquire(file='grid.X2D',exist=statex)
      if(statex)then
        print*,' READING UNFORMATTED GRID FILE'
        open(unit=20,file='grid.X2D',form='unformatted')
        read(20)bsize
        read(20)nblock
        read(20)idummy
        ni=bsize+3
        allocate(x(ni,ni,nblock))
        allocate(var(ncomp,ni,ni,nblock),attr(ni,ni,nblock))
        var=0.d0;x=0.d0
        print*,' Reading Attributs : '
        call ReadBlockInteger(20,ni**2,nblock,attr)
        print*,' Reading x-coord : '
        call ReadBlockReal   (20,ni**2,nblock,x)
        var(1,:,:,:)=x(:,:,:)
        print*,' Reading y-coord : '
        call ReadBlockReal   (20,ni**2,nblock,x)
        var(2,:,:,:)=x(:,:,:)
        print*,' Finished Reading : '
        deallocate(x)
        close(10)
      endif

c-----Write formatted x2d file
      print*,' Writing formatted x2d file : '
      open(unit=4,file='grid.x2d',form='formatted')
      write(4,*)ni-3,nblock
      do n=1,nblock
        print*,' Writing block : ',n
      do j=2,bsize+2
      do i=2,bsize+2
        write(4,'(i6,3e30.15)')attr(i,j,n),var(1,i,j,n)
     &                         ,var(2,i,j,n)
      enddo;enddo
      enddo
      close(4)

      stop
   30 format('')
   31 format('VARIABLES = ',15(A20,','))
   32 format('ZONE I=',i2,', J=',i2,', K=',i2,' F=BLOCK')
      end

      subroutine ReadBlockReal(unit,block,nblock,x)
      implicit none
      integer unit,nblock,block,i,n
      real(kind=8),dimension(block,nblock)::x
      real(kind=8),dimension(block)       ::xblock
        do n=1,nblock
          read(unit)xblock
          do i=1,block
            x(i,n)=xblock(i)
          end do
        end do
      end subroutine ReadBlockReal

      subroutine ReadBlockInteger(unit,block,nblock,x)
      implicit none
      integer unit,nblock,block,i,n,dn
      integer,dimension(block,nblock)::x
      integer,dimension(block)       ::xblock
        do n=1,nblock
          read(unit)xblock
          do i=1,block
            x(i,n)=xblock(i)
          end do
        end do
      end subroutine ReadBlockInteger
