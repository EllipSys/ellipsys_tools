#!/bin/bash

# MakePolar.sh start-aoa finish-aoa increment -n batchname -q quename -v velocity -m machine

# Function for submitting the individual AOA's
#-----------------------------------------------------------------------
# submitAngle AOA VEL JOBNAME MACHINE
#
SubmitAngle () {
MACHINE=$4
JOBNAME=$3
UVEL=$(echo $1 $2| awk '{print $2*cos( $1*0.01745329*0.01 )}')
VVEL=$(echo $1 $2| awk '{print $2*sin( $1*0.01745329*0.01 )}')
echo $UVEL, $VVEL
sed "s/NAME/grid/" $3.inp >t.1
sed "s/UVEL/$UVEL/g" t.1>t.2
sed "s/VVEL/$VVEL/g" t.2>t.1
DNAME=$(echo $1 | awk '{printf "%-2.2f\n" ,$1*.01}')
mkdir a$DNAME
sed "s/NNSNAME/$JOBNAME/"  ./RunBatch1.1  >a$DNAME/RunBatch1.1
mv t.1 a$DNAME/grid.inp
cp a$DNAME/grid.inp a$DNAME/input.dat
cp flowfieldMPI a$DNAME/.
rm  t.2
cp os.database a$DNAME/.
cd a$DNAME
if [ -a ../$3.X3D ]
then
  ln -s ../$3.X3D grid.X3D
  ln -s ../$3.T3D grid.T3D
else
  ln -s ../$3.X2D grid.X2D
  ln -s ../$3.T2D grid.T2D
fi

# Select Machine
if [ $MACHINE == jess ]
then
  qsub RunBatch1.1
fi
if [ $MACHINE == sophia ]
then
  echo submitting on sophia
  sbatch RunBatch1.1
fi

cd ..
}
#-----------------------------------------------------------------------


#### Main script
#-----------------------------------------------------------------------
# MakePolar.sh start-aoa finish-aoa increment -n batchname -q quename -v velocity

AMIN=$1
AMAX=$2
ADELTA=$3

# DEFAULTS
MACHINE=sophia
NNSQNAME=workq
RUNNAME=grid
JOBNAME=grid
EXENAME=flowfieldMPI
PATHNAME=.
VEL=1

OPTIND=4
while getopts ":m:n:q:j:f:v:" optname
    do
      case "$optname" in
        "m")
          MACHINE=$OPTARG
          echo "Option $optname has value $MACHINE"
          ;;
        "n")
          RUNNAME=$OPTARG
          echo "Option $optname has value $RUNNAME"
          ;;
        "q")
          NNSQNAME=$OPTARG
          echo "Option $optname has value $NNSQNAME"
          ;;
        "j")
          JOBNAME=$OPTARG
          echo "Option $optname has value $JOBNAME"
          ;;
        "f")
          EXENAME=$OPTARG
          echo "Option $optname has value $EXENAME"
          ;;
        "v")
          VEL=$OPTARG
          echo "Option $optname has value $VEL"
          ;;
        "?")
          echo "Unknown option $OPTARG"
          ;;
        ":")
          echo "No argument value for option $OPTARG"
          ;;
        *)
        # Should not occur
          echo "Unknown error while processing options"
          ;;
      esac
    done

## CHECK that ExeFile exists
if [ -a $EXENAME ]
then
  echo
  echo " Executable $EXENAME ok "
  cp $EXENAME flowfieldMPI
else
  echo
  echo " No executable $EXENAME exists  "
  exit
fi

## CHECK that X?D file exists
if [ -a $PATHNAME/$JOBNAME.X?D ]
then
  echo
  echo ' X2D/X3D ok '
else
  echo
  echo " X2D/X3D file $JOBNAME do not exist : "
  exit
fi

## CHECK that T?D file exists
if [ -a $PATHNAME/$JOBNAME.T?D ]
then
  echo
  echo ' T2D/T3D ok'
else
  echo
  echo " T2D/T3D file $JOBNAME do not exist : "
  exit
fi
## CHECK that rst file exists
if [ -a $JOBNAME.rst ]
then
  echo
  echo " restart file $JOBNAME.rst exists "
else
  echo
  echo " restart file $JOBNAME.rst do not exist : "
fi


#-----------------------------------------------------------------------
# Generate Runbatch template
#-----------------------------------------------------------------------
# For JESS cluster
if [ $MACHINE == jess ]
then
STR=$'
#!/bin/sh
#PBS -N NNSRNAME
#PBS -r n
#PBS -q NNSQNAME
#PBS -l nodes=NNSNODES:ppn=NNSCORES
#PBS -l walltime=NNSMCPUT
#PBS -W x=NACCESSPOLICY:SINGLEJOB
#PBS -j oe

#NPROCS=`wc -l < $PBS_NODEFILE`
NPROCS=NNSCOREST
echo $NPROCS

#/scratch stuff
cd $PBS_O_WORKDIR
SCRATCHDIR=/scratch/$USER/$PBS_JOBID
### Copy files to the scratch disk of the compute node:
for node in `cat $PBS_NODEFILE | sort -u`
do
echo $node
ssh $node cp -p $HOME/tools/nuf $SCRATCHDIR
ssh $node cp -p $PBS_O_WORKDIR/flowfieldMPI $SCRATCHDIR
ssh $node cp -p $PBS_O_WORKDIR/os.database $SCRATCHDIR 2>/dev/null
done
cp -p ./input.dat $SCRATCHDIR
cp -p NNSPATH/NNSNAME.X?D     $SCRATCHDIR
cp -p NNSPATH/NNSNAME-e??.X?D $SCRATCHDIR
cp -p NNSPATH/NNSNAME.T?D $SCRATCHDIR
cp -p NNSPATH/NNSNAME.ROU $SCRATCHDIR 2>/dev/null
ln -s    $HOME/tools/Savescript $SCRATCHDIR/Savescript 2>/dev/null
cp    ./NNSNAME.rst $SCRATCHDIR 2>/dev/null
cp    ./NNSNAME.force $SCRATCHDIR 2>/dev/null
cp    ./NNSNAME.??Y $SCRATCHDIR 2>/dev/null

### Change working directory to scratch disk
### and do the calculation:

#/scratch stuff
cd $SCRATCHDIR
#cd $PBS_O_WORKDIR

$mpirunn -np $NPROCS ./flowfieldMPI

## Execute pbsnodes command to get information about down nodes
### Move the output to the users directory - the scratch
### disk will be cleaned when 
mv *.o*     $PBS_O_WORKDIR
mv *.res    $PBS_O_WORKDIR
mv *.?pr*   $PBS_O_WORKDIR
mv *.?P?*   $PBS_O_WORKDIR
mv *.?F?*   $PBS_O_WORKDIR
mv *.?H?*   $PBS_O_WORKDIR
mv *.OUT    $PBS_O_WORKDIR
mv *.?force $PBS_O_WORKDIR
mv *.force $PBS_O_WORKDIR
mv *.points $PBS_O_WORKDIR
mv *.plt    $PBS_O_WORKDIR
mv *.RST*   $PBS_O_WORKDIR
mv *.PLT*   $PBS_O_WORKDIR
mv *.xyz*   $PBS_O_WORKDIR
mv *.f      $PBS_O_WORKDIR
mv transition.dat $PBS_O_WORKDIR'
fi

# For Sophia cluster
if [ $MACHINE == sophia ]
then
STR=$'#!/bin/bash
#SBATCH --job-name=NNSRNAME
#SBATCH --workdir=.
#SBATCH --output=/work/users/%u/mpi_%j.out
#SBATCH --error=/work/users/%u/mpi_%j.err
#SBATCH --nodes=NNSNODES
#SBATCH --ntasks=NNSCOREST
#SBATCH --partition=NNSQNAME
#SBATCH --exclusive
#SBATCH --distribution=plane=NNSCORES
#SBATCH --time=0:NNSMCPUT

cd $SLURM_SUBMIT_DIR
SCRATCHDIR=/work/users/${SLURM_JOB_USER}/${SLURM_JOB_USER}-$SLURM_JOB_ID
mkdir $SCRATCHDIR
cp * $SCRATCHDIR/.
echo $SCRATCHDIR > bb_on
echo $SLURM_SUBMIT_DIR >  $SCRATCHDIR/bb_on
cd $SCRATCHDIR


ulimit -s unlimited
module purge
module load hpcx/2.5.0/ompi-mt-intel-19.0.4.243
srun --mpi=pmix_v2 ./flowfieldMPI -m 5 -b 2 -k 1 -s 8

rm grid.X?D grid.T?D grid.C2D grid.rst bb_on flowfield 2>/dev/null
cd $SLURM_SUBMIT_DIR
mv $SCRATCHDIR/* .
mv /work/users/${SLURM_JOB_USER}/mpi_${SLURM_JOB_ID}.out .
mv /work/users/${SLURM_JOB_USER}/mpi_${SLURM_JOB_ID}.err .
rm -rf $SCRATCHDIR
wait'
fi

#-----------------------------------------------------------------------
# Update template with present info
#-----------------------------------------------------------------------
echo "$STR" > ./RunBatch1
if [ -a $JOBNAME.inp  ]
then
  cp $JOBNAME.inp input.dat
  cp $JOBNAME.pro ProjectName 2>/dev/null
  echo 
  echo ' ###########################'
  echo " GIVE NUMBER OF NODES : "
  read CNODES
  echo 
  echo " GIVE NUMBER OF CORES PER NODE : "
  read CCORES
  echo 
  echo " GIVE TOTAL NUMBER OF CORES : "
  read CCOREST
  echo 
  echo " GIVE MAX CPU TIME : "
  read CPUT
  sed "s/NNSNAME/NNSNAME/"                     RunBatch1  >RunBatch1.1;mv RunBatch1.1 RunBatch1.2
  sed "s/NNSQNAME/$NNSQNAME/"                  RunBatch1.2  >RunBatch1.1;mv RunBatch1.1 RunBatch1.2
  sed "s/NNSRNAME/$RUNNAME-NNSNAME/"           RunBatch1.2  >RunBatch1.1;mv RunBatch1.1 RunBatch1.2
  sed "s/NNSNODES/$CNODES/"                    RunBatch1.2  >RunBatch1.1;mv RunBatch1.1 RunBatch1.2
  sed "s/NNSPATH/$PATHNAME/"                   RunBatch1.2  >RunBatch1.1;mv RunBatch1.1 RunBatch1.2
  sed "s/NNSCOREST/$CCOREST/"                  RunBatch1.2  >RunBatch1.1;mv RunBatch1.1 RunBatch1.2
  sed "s/NNSCORES/$CCORES/"                    RunBatch1.2  >RunBatch1.1;mv RunBatch1.1 RunBatch1.2
  sed "s/NNSPATH/$PATHNAME/"                   RunBatch1.2  >RunBatch1.1;mv RunBatch1.1 RunBatch1.2
  sed "s/NNSPATH/$PATHNAME/"                   RunBatch1.2  >RunBatch1.1;mv RunBatch1.1 RunBatch1.2
  sed "s/NNSMCPUT/$CPUT/"                      RunBatch1.2  >RunBatch1.1
else
  echo ''
  echo "the file $jobname.inp do not exist"
  echo ' '
  echo '******* THE PROGRAM IS TERMINATED *******'
fi

#-----------------------------------------------------------------------
# Update template with present info
#-----------------------------------------------------------------------
INDX=$(echo $AMIN   | awk '{print $1*100}')
MAXV=$(echo $AMAX   | awk '{print $1*100}')
INCR=$(echo $ADELTA | awk '{print $1*100}')
echo $INDX $MAXV $INCR
while [ "$INDX" -le "$MAXV" ]
do
  echo AOA $INDX
  SubmitAngle $INDX $VEL $RUNNAME $MACHINE
  INDX=`expr $INDX + "$INCR"`
  sleep 3
done
rm RunBatch*
exit 0
#-----------------------------------------------------------------------

