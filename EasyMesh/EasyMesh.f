      module params
      integer imax,jmax,isec
      parameter(imax=2000,jmax=2000,isec=10)
      end module


      module vertices
      use params
      real(kind=8),dimension(imax,jmax)::x,y
      integer,dimension(imax,jmax)::attr
      end module

      module boundary
      use params
      integer,dimension(4)::sector
      integer,dimension(isec,4)::nisecsurf,nisecdist,nisecpoint
      real(kind=8),dimension(imax,isec,4)::xface,yface
      real(kind=8),dimension(20,3,isec,4)::distinp
      integer::bdim=-1
      end module

      program main
************************************************************************
*
*
************************************************************************
* Author  : Niels N. Sorensen
* Date    :
************************************************************************
      implicit none

c------- READ GEOMETRY DATA
      call ReadGeometry

c------- ASSURE THAT FACES FROM CLOSED CURVES
      call CloseFaces

c------- CONNECT FACES AT CORNERS
      call ConnectFaces

c------- DISTRIBUT MESH ON FACES
      call FaceMesh

c------- GENERATE GRID
      call Transfinit

c------- SMOOTH GRID
c     call SmoothGrid

c------- SUBDIVIDE GRID AND WRITE TO FILE
      call PartGrid

c------- WRITE BOUNDARY TO FILE IN GNUPLOT FORMAT
      call WriteBoundary

      stop
      end

      subroutine ReadGeometry
************************************************************************
*     Routine for reading the geometry
*
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      use params
      use boundary
      implicit none
      integer face,sec,i
      integer,parameter::mword=20
      integer nrchar(mword),nrword
      character(len=256)InputLine
      character(len=40),dimension(mword)::words
      open(unit=10,file='geometry.dat')

c------- READ SURFACE INFORMATION
      print*,' READING SURFACE INFORMATION '
      read(10,'(a256)')InputLine
      do face=1,4
c--------- READ THE NUMBER OF SECTORS ON THE PRESENT FACE
        read(10,'(a256)')InputLine
        call cmdpart(InputLine,mword,nrchar,nrword,words)
        read(words(2),*)sector(face)
        if(sector(face).gt.isec)then
          print*,' The geometry file has to many sections : ',isec
          print*,' Recompile with larger array size : '
          stop
        endif
        do sec=1,sector(face)
c----------- READ THE NUMBER OF DATA POINTS ON THE PRESENT SECTOR
          read(10,'(a256)')InputLine
          call cmdpart(InputLine,mword,nrchar,nrword,words)
          read(words(2),*)nisecsurf(sec,face)
          if(nisecsurf(sec,face).gt.imax)then
            print*,' A sections is larger than imax : ',imax
            print*,' Recompile with larger array size : '
            stop
          endif
          do i=1,nisecsurf(sec,face)
c-------------- READ THE DATA POINTS OF THE PRESENT SECTOR
            read(10,*)xface(i,sec,face),yface(i,sec,face)
          enddo
        enddo
      enddo
      print*,' FINISHED READING SURFACE INFORMATION '
c------- READ DISTRIBUTION INFORMATION
      print*,' READING DISTRIBUTION FUNCTIONS '
      read(10,'(a256)')InputLine
      call cmdpart(InputLine,mword,nrchar,nrword,words)
      do face=1,4
c--------- READ THE NUMBER OF SECTORS ON THE PRESENT FACE
        read(10,'(a256)')InputLine
        do sec=1,sector(face)
c----------- READ THE NUMBER OF DATA POINTS ON THE PRESENT SECTOR
          read(10,'(a256)')InputLine
          call cmdpart(InputLine,mword,nrchar,nrword,words)
          read(words(2),*)nisecdist(sec,face)
          read(words(3),*)nisecpoint(sec,face)
          print*,'face',face,nisecdist(sec,face),nisecpoint(sec,face)
          if(nisecdist(sec,face).gt.imax)then
            print*,' A dist sections is larger than imax : ',imax
            print*,' Recompile with larger array size : '
            stop
          endif
          if(nisecdist(sec,face).gt.0)then
          do i=1,nisecdist(sec,face)
c-------------- READ THE DATA POINTS OF THE PRESENT SECTOR
            read(10,*)distinp(i,1,sec,face),distinp(i,2,sec,face)
     &               ,distinp(i,3,sec,face)
          enddo
          else
            read(10,*)distinp(1,2,sec,face),distinp(2,2,sec,face)
            nisecdist(sec,face)=2
            distinp(1,1,sec,face)=0.d0
            distinp(1,3,sec,face)=1
            distinp(2,1,sec,face)=1.d0
            distinp(2,3,sec,face)=nisecpoint(sec,face)
          endif
        enddo
      enddo
      read(10,'(a256)',end=666)InputLine
      call cmdpart(InputLine,mword,nrchar,nrword,words)
      select case(words(1))
      case('#BLOCK-SIZE') 
        read(words(2),*)bdim
      end select
  666 print*,'finished'

      return
      end

      subroutine CloseFaces
************************************************************************
*     Assure that faces form closed curves. This is done by setting
*     the coordinat values of the last point in a sector equal to
*     the coordinat values of the first point in the following sector.
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      use params
      use boundary
      implicit none
      integer face,sec,i

c------- ASSURE THAT FACES FORM CLOSED CURVES
      do face=1,4
      do sec =1,sector(face)-1
        xface(nisecsurf(sec,face),sec,face)=xface(1,sec+1,face)
        yface(nisecsurf(sec,face),sec,face)=yface(1,sec+1,face)
      enddo;enddo
      return
      end

      subroutine ConnectFaces
************************************************************************
*     Assure that the four boundary curves are connected at the corners.
*     This is done by setting the value to the average of the two
*     curve corner values.
*
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      use params
      use boundary
      implicit none
      real(kind=8) xaverage,yaverage

c------- BOTTOM WEST CORNER
      xaverage=(xface(1,1,1)+xface(1,1,3))*.5
      yaverage=(yface(1,1,1)+yface(1,1,3))*.5
      xface(1,1,1)=xaverage
      yface(1,1,1)=yaverage
      xface(1,1,3)=xaverage
      yface(1,1,3)=yaverage
c------- BOTTOM EAST CORNER
      xaverage=(xface(1,1,2)
     &         +xface(nisecsurf(sector(3),3),sector(3),3))*.5
      yaverage=(yface(1,1,2)
     &         +yface(nisecsurf(sector(3),3),sector(3),3))*.5
      xface(1,1,2)=xaverage
      yface(1,1,2)=yaverage
      xface(nisecsurf(sector(3),3),sector(3),3)=xaverage
      yface(nisecsurf(sector(3),3),sector(3),3)=yaverage
c------- TOP WEST CORNER
      xaverage=(xface(nisecsurf(sector(1),1),sector(1),1)
     &         +xface(1,1,4))*.5
      yaverage=(yface(nisecsurf(sector(1),1),sector(1),1)
     &         +yface(1,1,4))*.5
      xface(nisecsurf(sector(1),1),sector(1),1)=xaverage
      yface(nisecsurf(sector(1),1),sector(1),1)=yaverage
      xface(1,1,4)=xaverage
      yface(1,1,4)=yaverage
c------- TOP EAST CORNER
      xaverage=(xface(nisecsurf(sector(2),2),sector(2),2)
     &         +xface(nisecsurf(sector(4),4),sector(4),4))*.5
      yaverage=(yface(nisecsurf(sector(2),2),sector(2),2)
     &         +yface(nisecsurf(sector(4),4),sector(4),4))*.5
      xface(nisecsurf(sector(2),2),sector(2),2)=xaverage
      yface(nisecsurf(sector(2),2),sector(2),2)=yaverage
      xface(nisecsurf(sector(4),4),sector(4),4)=xaverage
      yface(nisecsurf(sector(4),4),sector(4),4)=yaverage

      return
      end

      subroutine FaceMesh
************************************************************************
*
*
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      use params
      use boundary
      implicit none
      integer::face,sec,i,errcheck
      real(kind=8) sp(imax),xps(imax),yps(imax)
     &            ,xf(imax),yf(imax),fx(imax)
      real(kind=8) sd(imax),xd(imax),yd(imax),yds(imax)
      real(kind=8) dx,dy,yp1,ypn,xsp,ysp,xi1,xi2,length
      real(kind=8),dimension(:,:),allocatable::SurfaceDistInp
      real(kind=8),dimension(:),allocatable::fs
      character(len=9) fname
      character(len=1) num
      logical statex
  
      errcheck=0

      fname(1:4)='face'
      fname(6:9)='.dat'
      do face=1,4
        write(num,'(i1.1)')face
        fname(5:5)=num
        inquire(file=fname(1:9),exist=statex)
c-------- OPEN FILE FOR SURFACE DATA
        if(statex)then
c----------- NOTHING TO DO POINT DISTRIBUTION EXISTS    
        print*,' READING POINT DISTRIBUTION FOR FACE : ',face
        else
c----------- GENERATE POINT DISTRIBUTION FOR FACE
        print*,' GENERATING POINT DISTRIBUTION FOR FACE : ',face
        open(unit=10,file=fname(1:9))
        do sec=1,sector(face)
c-------spline profile data
        do i=1,nisecsurf(sec,face)
          xf(i)=xface(i,sec,face)
          yf(i)=yface(i,sec,face)
        enddo
        sp(1)=0.d0
        do i=2,nisecsurf(sec,face)
c-------calculate first derivative
          dx=xf(i)-xf(i-1)
          dy=yf(i)-yf(i-1)
c-------calculate curve length
          sp(i)=sp(i-1)+sqrt(dx*dx+dy*dy)
        enddo
c-----normalize curve length
        length=sp(nisecsurf(sec,face))
        do i=1,nisecsurf(sec,face)
          sp(i)=sp(i)/sp(nisecsurf(sec,face))
        enddo
c-------spline representation of surface (natural cubic spline)
        yp1=1.d32
        ypn=1.d32
        call spline(sp,xf,nisecsurf(sec,face),yp1,ypn,xps)
        call spline(sp,yf,nisecsurf(sec,face),yp1,ypn,yps)

c------- SPLINE DISTRIBUTION FUNCTION
        if(nisecdist(sec,face).gt.0)then
c-------- compute distribution fucntion
          if(allocated(fs))deallocate(surfacedistinp,fs)
          allocate(fs(nisecpoint(sec,face))
     &            ,SurfaceDistInp(nisecdist(sec,face),3))
          SurfaceDistInp(:,:)=DistInp(1:nisecdist(sec,face),:,sec,face)
          surfacedistinp(1,3)=1
          surfacedistinp(:,2)=surfacedistinp(:,2)/length
          call DistFunc(nisecdist(sec,face),SurfaceDistInp,2
     &       ,nisecpoint(sec,face),errcheck,fs)
          fs(nisecpoint(sec,face))=1.d0

c-------distribute points
          do i=1,nisecpoint(sec,face)-1
            call splint(sp,xf,xps,nisecsurf(sec,face),fs(i),xsp)
            call splint(sp,yf,yps,nisecsurf(sec,face),fs(i),ysp)
            write(10,*)xsp,ysp
          enddo
          
        endif
        enddo
        call splint(sp,xf,xps,nisecsurf(sector(face),face),1.d0,xsp)
        call splint(sp,yf,yps,nisecsurf(sector(face),face),1.d0,ysp)
        write(10,*)xsp,ysp
        close(10)
        endif
      enddo

      return
      end

      subroutine Transfinit
************************************************************************
*
*
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      use params
      use boundary
      use vertices
      implicit none
      integer im,jm,i,j
      real(kind=8) ksi,eta,ksit,etat,dx,dy
      real(kind=8) xi0(imax),xi1(imax),eta0(imax),eta1(imax)

c------- COMPUTE NUMBER OF POINTS IN XI AND ETA DIRECTION
      jm=1
      do i=1,sector(1)
        jm=jm+nisecpoint(i,1)-1
      enddo
      im=1
      do j=1,sector(3)
        im=im+nisecpoint(j,3)-1
      enddo

c------- READ FACES 
      open(unit=10,file='face1.dat')
      do j=1,jm
        read(10,*)x(1 ,j ),y(1 ,j )
      enddo
      close(10)
      open(unit=10,file='face2.dat')
      do j=1,jm
        read(10,*)x(im,j ),y(im,j )
      enddo
      close(10)
      open(unit=10,file='face3.dat')
      do i=1,im
        read(10,*)x(i ,1 ),y(i ,1 )
      enddo
      close(10)
      open(unit=10,file='face4.dat')
      do i=1,im
        read(10,*)x(i ,jm),y(i ,jm)
      enddo
      close(10)

C------- COMPUTE SURFACE DISTRIBUTION
C------- WEST FACE
      eta0(1)=0
      do j=2,jm
        dx=x(1,j)-x(1,j-1)
        dy=y(1,j)-y(1,j-1)
        eta0(j)=eta0(j-1)+sqrt(dx**2+dy**2)
      enddo
      do j=1,jm
        eta0(j)=eta0(j)/eta0(jm)
      enddo
C------- WEST FACE
      eta1(1)=0
      do j=2,jm
        dx=x(im,j)-x(im,j-1)
        dy=y(im,j)-y(im,j-1)
        eta1(j)=eta1(j-1)+sqrt(dx**2+dy**2)
      enddo
      do j=1,jm
        eta1(j)=eta1(j)/eta1(jm)
      enddo
C------- SOUTH FACE
      xi0(1)=0
      do i=2,im
        dx=x(i,1)-x(i-1,1)
        dy=y(i,1)-y(i-1,1)
        xi0(i)=xi0(i-1)+sqrt(dx**2+dy**2)
      enddo
      do i=1,im
        xi0(i)=xi0(i)/xi0(im)
      enddo
C------- NORTH FACE
      xi1(1)=0
      do i=2,im
        dx=x(i,jm)-x(i-1,jm)
        dy=y(i,jm)-y(i-1,jm)
        xi1(i)=xi1(i-1)+sqrt(dx**2+dy**2)
      enddo
      do i=1,im
        xi1(i)=xi1(i)/xi1(im)
      enddo


c------- PERFORME TRANSFINIT INTERPOLATION USING LINEAR INTERPOLATION
      do j=2,jm-1
      do i=2,im-1
c       ksi   =real(i-1)/real(im-1)
c       eta   =real(j-1)/real(jm-1)
        ksit  =real(i-1)/real(im-1)
        etat  =real(j-1)/real(jm-1)
        eta   =(1-ksit)*eta0(j)+ksit*eta1(j)
        ksi   =(1-etat)*xi0 (i)+etat*xi1 (i)
        x(i,j)=(1-ksi)*x(1,j)+ksi*x(im,j )
     &        +(1-eta)*x(i,1)+eta*x(i ,jm)
     &        -(1-ksi)*(1-eta)*x(1 ,1 )
     &        -(1-ksi)*(  eta)*x(1 ,jm)
     &        -(  ksi)*(1-eta)*x(im,1 )
     &        -(  ksi)*(  eta)*x(im,jm)
      enddo;enddo
      do j=2,jm-1
      do i=2,im-1
c       ksi   =real(i-1)/real(im-1)
c       eta   =real(j-1)/real(jm-1)
        ksit  =real(i-1)/real(im-1)
        etat  =real(j-1)/real(jm-1)
        eta   =(1-ksit)*eta0(j)+ksit*eta0(j)
        ksi   =(1-etat)*xi0 (i)+etat*xi1 (i)
        y(i,j)=(1-ksi)*y(1,j)+ksi*y(im,j )
     &        +(1-eta)*y(i,1)+eta*y(i ,jm)
     &        -(1-ksi)*(1-eta)*y(1 ,1 )
     &        -(1-ksi)*(  eta)*y(1 ,jm)
     &        -(  ksi)*(1-eta)*y(im,1 )
     &        -(  ksi)*(  eta)*y(im,jm)
      enddo;enddo
      return
      end

      subroutine SmoothGrid
************************************************************************
*
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
***********************************************************************
      use params
      use boundary
      use vertices
      implicit none
      integer ni,nj,js,i,j,it,itmax
      real(kind=8) xt,yt,len,b
      real(kind=8) ip1,im1,jp1,jm1
      character(len=1) yesno

      nj=1
      do i=1,sector(1)
        nj=nj+nisecpoint(i,1)-1
      enddo
      ni=1
      do i=1,sector(3)
        ni=ni+nisecpoint(i,3)-1
      enddo

      print*,' Smooth Grid  Y/N '
      read*,yesno
      if(yesno.ne.'Y'.or.yesno.ne.'y')return
      print*,' o-mesh Y/N '
      read*,yesno
      itmax=10
      do it=1,itmax
      do j=2,nj-1
        if(yesno.eq.'Y'.or.yesno.eq.'y')then
        ip1=sqrt((x(1,j)-x(2   ,j))**2
     &          +(y(1,j)-y(2   ,j))**2)
        im1=sqrt((x(1,j)-x(ni-1,j))**2
     &          +(y(1,j)-y(ni-1,j))**2)
        len=1/(ip1+im1)
        ip1=ip1*len
        im1=im1*len
        jp1=sqrt((x(1,j)-x(1,j+1))**2
     &          +(y(1,j)-y(1,j+1))**2)
        jm1=sqrt((x(1,j)-x(1,j-1))**2
     &          +(y(1,j)-y(1,j-1))**2)
        len=1/(jp1+jm1)
        jp1=jp1*len
        jm1=jm1*len
        xt=.5*(ip1*x(ni-1,j  )+im1*x(2   ,j  )
     &        +jp1*x(1   ,j-1)+jm1*x(1   ,j+1))
        yt=.5*(ip1*y(ni-1,j  )+im1*y(2   ,j  )
     &        +jp1*y(1   ,j-1)+jm1*y(1   ,j+1))
        x(1 ,j)=.6*x(1 ,j)+.4*xt
        y(1 ,j)=.6*y(1 ,j)+.4*yt
        x(ni,j)=.6*x(ni,j)+.4*xt
        y(ni,j)=.6*y(ni,j)+.4*yt
        endif
        do i=2,ni-1
        ip1=sqrt((x(i,j)-x(i+1,j))**2
     &          +(y(i,j)-y(i+1,j))**2)
        im1=sqrt((x(i,j)-x(i-1,j))**2
     &          +(y(i,j)-y(i-1,j))**2)
        len=1/(ip1+im1)
        ip1=ip1*len
        im1=im1*len
        jp1=sqrt((x(i,j)-x(i,j+1))**2
     &          +(y(i,j)-y(i,j+1))**2)
        jm1=sqrt((x(i,j)-x(i,j-1))**2
     &          +(y(i,j)-y(i,j-1))**2)
        len=1/(jp1+jm1)
        jp1=jp1*len
        jm1=jm1*len
        xt=.5*(ip1*x(i-1,j  )+im1*x(i+1,j  )
     &        +jp1*x(i  ,j-1)+jm1*x(i  ,j+1))
        yt=.5*(ip1*y(i-1,j  )+im1*y(i+1,j  )
     &        +jp1*y(i  ,j-1)+jm1*y(i  ,j+1))
        x(i,j)=.6*x(i,j)+.4*xt
        y(i,j)=.6*y(i,j)+.4*yt
        enddo
      enddo;enddo
      return
      end


      subroutine PartGrid
************************************************************************
*
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
***********************************************************************
      use params
      use boundary
      use vertices
      implicit none
      integer ni,nj,i,j
      integer istart,jstart,io,jo,ii,jj
      integer dim,nbi,nbj,nbm
      integer yesno
      real(kind=8) rest
      open(unit=20,file='grid.x2d')
      open(unit=21,file='easy.dat')



c------- COMPUTE NUMBER OF POINTS IN XI AND ETA DIRECTION
      nj=1
      do i=1,sector(1)
        nj=nj+nisecpoint(i,1)-1
      enddo
      ni=1
      do i=1,sector(3)
        ni=ni+nisecpoint(i,3)-1
      enddo
      print*,ni,nj

c------- attributes
      attr=1
      attr(ni,: )=401
      attr(1 ,: )=201
      attr(: ,nj)=101
      attr(: ,1 )=101

      write(21,*)'#',ni,nj
      do j=1,nj
      do i=1,ni
        write(21,*)attr(i,j),x(i,j),y(i,j)
      enddo;enddo

c-----find max size of multigrid blocks
      dim=int(min(real(ni),real(nj)))
      dim=dim-1

      if(bdim.lt.0)then
  100   rest=mod((ni-1),dim)
     &      +mod((nj-1),dim)
        if(rest.eq.0)goto 200
        dim=(dim-1)/2+1
        goto 100
  200   print*,'*****************************************************'
        print*,'size of multidomaine blocks         : ',dim
        print*,'want to change this yes = 1, no = 2   '
        read(*,*) yesno
        if(yesno.eq.1)then
          print*,'give new dimension  '
          read(*,*)dim
        endif
      else
        dim=bdim
      endif
      nbi=(ni-1)/dim
      nbj=(nj-1)/dim
      nbm=nbi*nbj
      print*,'*****************************************************'
      print*,'size of multidomaine blocks         : ',dim
      print*,'number of block in xi direction     : ',nbi
      print*,'number of block in eta direction    : ',nbj
      print*,'total number of blocks              : ',nbm
      print*,'*******************************************************'

      write(20,*) dim,nbm
      do jj=1,nbj
        jstart=(jj-1)*dim
        do ii=1,nbi
          istart=(ii-1)*dim
          do j=1,dim+1
          jo=jstart+j
          do i=1,dim+1
          io=istart+i
          write(20,*)attr(io,jo),x(io,jo),y(io,jo)
          enddo;enddo
        enddo
      enddo

      return
      end

      subroutine WriteBoundary
************************************************************************
*     Write boundary to file in gnuplot format.
*
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      use params
      use boundary
      implicit none
      integer face,sec,i
      open(unit=11,file='geometry.OUT')

      do face=1,4
        do sec=1,sector(face)
        write(11,*)xface(1,sec,face),yface(1,sec,face)
        do i  =2,nisecsurf(sec,face)
          write(11,*)xface(i,sec,face),yface(i,sec,face)
        enddo;enddo
        write(11,29)
      enddo
      close(29)

      return
   29 format('')
      end

      subroutine spline(x,y,n,yp1,ypn,y2)
c----------------------------------------------------------------------
c     Given arrays x and y of length n containing a tabulated function,
c     i.e y(i)=f(x(i)), with x(1)<x(2)<...<x(n), and given values yp1
c     and ypn for the first derivative of the interpolating function at
c     points 1 and n, respectively, this routine returns an array y2 of
c     length n wich contains the second derivatives of the interpolating
c     function at the tabulated points x(i). Uf yp1 and/or ypn are equal
c     to 1.d30 or larger, the routine is signalled to the corresponding
c     boundary condition for a natural spline, with zero second derivative
c     on that boundary
c
c     Numerical Recipes
c----------------------------------------------------------------------
      implicit none
      integer nmax
      parameter(nmax=1000)
      integer i,n,k
      real(kind=8) x(n),y(n),y2(n),u(nmax)
      real(kind=8) qn,p,un,sig,yp1,ypn


      if (yp1.gt..99e30) then
        y2(1)=0.
        u(1)=0.
      else
        y2(1)=-0.5
        u(1)=(3./(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
      endif
      do 11 i=2,n-1
        sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
        p=sig*y2(i-1)+2.
        y2(i)=(sig-1.)/p
        u(i)=(6.*((y(i+1)-y(i))/(x(i+1)-x(i))-(y(i)-y(i-1))
     *      /(x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*u(i-1))/p
11    continue
      if (ypn.gt..99e30) then
        qn=0.
        un=0.
      else
        qn=0.5
        un=(3./(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
      endif
      y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.)
      do 12 k=n-1,1,-1
        y2(k)=y2(k)*y2(k+1)+u(k)
12    continue
      return
      end

      subroutine splint(xa,ya,y2a,n,x,y)
c----------------------------------------------------------------------
c     Given the arrays xa and ya of length n, wich tabulate a function
c     (with the xa's in order), and given the array y2a, wich is the
c     output from spline above, and given a value of x, this routine
c     returns a cubic-spline interpolated value y.
c
c     Numerical Recipes
c----------------------------------------------------------------------
      implicit none
      integer n
      real(kind=8) xa(n),ya(n),y2a(n),x,y
      real(kind=8) a,b,h
      integer klo,khi,k

      klo=1
      khi=n
1     if (khi-klo.gt.1) then
        k=(khi+klo)/2
        if(xa(k).gt.x)then
          khi=k
        else
          klo=k
        endif
      goto 1
      endif
      h=xa(khi)-xa(klo)
      if (h.eq.0.)then
         print*,'bad xa input.'
         stop
      endif
      a=(xa(khi)-x)/h
      b=(x-xa(klo))/h
      y=a*ya(klo)+b*ya(khi)+
     *      ((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.
      return
      end

      subroutine cmdpart(InputLine,mword,nrchar,nrword,words)
c-----------------------------------------------------------------------
c     routine parting command InputLine into words
c----------------------------------------------------------------------
c
c     Author: Jess A. Michelsen and Niels N. Soerensen
c     last revision :march 3, 1998
c-----------------------------------------------------------------------
      implicit none
      integer mword,nrchar(mword),nrword,i
      integer,parameter::mchar=256
      character(len=40),dimension(mword):: words
      character(len=40)::blank
      character(len=256) InputLine
      integer CurrentPosition,RelativeBlankPosition,BlankPosition
C--------------------------------------------- Reset word counter ------
      nrword=0
      CurrentPosition=1
C--------------------------------------------- Skip if a comment line --
c     if(index(InputLine(1:mchar),'#').gt.0) return
C---------------------------------------- find distance to next blank --
   20 RelativeBlankPosition=index(InputLine(CurrentPosition:mchar),' ')
      if(CurrentPosition.eq.mchar.or.RelativeBlankPosition.eq.0) return
C---------------------------------------- found the end of a word ------
      if(RelativeBlankPosition.gt.1.and.RelativeBlankPosition.le.40)then
        BlankPosition=CurrentPosition-1+RelativeBlankPosition
        nrword=nrword+1
        if(nrword.gt.mword)return
        words(nrword)=InputLine(CurrentPosition:BlankPosition)
        nrchar(nrword)=RelativeBlankPosition-1
      endif
c-----------------------------------------------------------------------
      CurrentPosition=CurrentPosition+RelativeBlankPosition
      goto 20
c-----------------------------------------------------------------------
      end

      subroutine distfunc(nn,dinp,ival,ndist,errchk,fdist)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      implicit none
      integer::nn,ndist
      real(kind=8),dimension(ndist)::fdist,dy
      real(kind=8),dimension(nn,3)::dinp
      real(kind=8) s0,s1,d0,d1
      real(kind=8) len,delta1,delta2,error
      integer n0,n1,ns,nf
      integer i,j,ival,n,errchk,err



      fdist(1)=0.d0
      s0=dinp(1,1);d0=dinp(1,2);n0=int(dinp(1,3))
      do i=2,nn
        s1=dinp(i,1);d1=dinp(i,2);n1=int(dinp(i,3))
c---------if no size given take size from last segment
        if(i.gt.2.and.d0.lt.0.d0)d0=(fdist(n0)-fdist(n0-1))
        len=s1-s0
        delta1=d0
        delta2=d1
        err=1
        if(ival.eq.1)call tanhdist(delta1,delta2,len,n0,n1,err,dy(n0))
        if(ival.eq.2)call sinhdist(delta1,delta2,len,n0,n1,err,dy(n0))
        if(errchk.eq.1.and.err.eq.-1)then
          delta1=dy(n0+1)-dy(n0)
          delta2=dy(n1)-dy(n1-1)
          write(*,'(2x,a35)')' Convergence problem in distfunc : '
          write(*,'(2x,a10,i5,i5,i5)')'Section : ',i,n0,n1
          error=abs(d0-delta1)/d0*100;if(d0.lt.0.d0)error=0.d0
          write(*,'(2x,a24,f20.4)')'Start err in percent  : ',error
          error=abs(d1-delta2)/d1*100;if(d1.lt.0.d0)error=0.d0
          write(*,'(2x,a24,f20.4)')'End   err in percent  : ',error
        endif
        do j=n0+1,n1
        fdist(j)=fdist(n0)+dy(j)
        enddo
        s0=s1;d0=d1;n0=n1
      enddo
      return
      do i=9,ndist,16
        fdist(i)=.5*(fdist(i-8)+fdist(i+8))
      enddo
      do i=5,ndist,8
        fdist(i)=.5*(fdist(i-4)+fdist(i+4))
      enddo
      do i=3,ndist,4
        fdist(i)=.5*(fdist(i-2)+fdist(i+2))
      enddo
      do i=2,ndist,2
        fdist(i)=.5*(fdist(i-1)+fdist(i+1))
      enddo
      do n=1,12
        do i=2,ndist-1
          fdist(i)=.25d0*(fdist(i-1)+2*fdist(i)+fdist(i+1))
        enddo
      enddo
      return
   21 format('#',i6)
      end

      subroutine tanhdist(delta1,delta2,len,i1,i2,err,fdist)
************************************************************************
*     TANDIST
*     program for computing the hyperbolic tangent distribution
*     delta1 : grid spacing at i=i1 (first cell)
*     delta2 : grid spacing at i=i2 (last  cell)
*     len    : lenght of curve
*     i1     : indices of first vertex
*     i2     : indices of last vertex
*     fdist  : compute distribution function
*
*
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      integer i1,i2,ni,i,err
      real(kind=8)::delta1,delta2,len
      real(kind=8)::delta,b,a
      real(kind=8)::fdist(i2-i1+1)
      real(kind=8)::transsinh,transtanh,ftmp
      external transsinh,transtanh

      if(i2.eq.i1)return

c------- Normalize delta1 and delta2 with lenght
      delta1=delta1/len
      delta2=delta2/len
c------- Compute number of points
      ni=i2-i1


c------- Assure that B > 1
      if(delta1.le.0.d0.and.1/delta2.lt.ni)then
        delta1=1/(ni**2*delta2*1.02)
      elseif(delta2.le.0d0.and.1/delta1.lt.ni)then
        delta2=1/(ni**2*delta1*1.02)
      endif

c------- Spacing at both end specified
      if(delta1.gt.0.d0.and.delta2.gt.0.d0)then
        a=sqrt(delta2)/sqrt(delta1)
        b=1.d0/(ni*sqrt(delta1*delta2))
        if(b.ge.1.d0)then
          delta=transsinh(b,err)
          do i=1,ni+1
            ftmp=.5*(1+tanh(delta*(real(i-1)/real(ni)-0.5))
     &          /tanh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        else
          delta=transtanh(b,err)
          do i=1,ni+1
            ftmp=.5*(1+sinh(delta*(real(i-1)/real(ni)-0.5))
     &          /sinh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        endif
c------- Spacing only specified at xi=1
      else if(delta1.gt.0.d0)then
        b=1.d0/(ni*delta1)
        delta=transsinh(b,err)
        do i=1,ni+1
          fdist(i)=1.d0+tanh(.5d0*delta*(real(i-1)/real(ni)-1.d0))
     &            /tanh(.5d0*delta)
        enddo
c------- Spacing only specified at xi=ni
      else if(delta2.gt.0.d0)then
        b=1.d0/(ni*delta2)
        delta=transsinh(b,err)
        do i=1,ni+1
          fdist(i)=tanh(.5*delta*real(i-1)/real(ni))/tanh(.5*delta)
        enddo
c------- Error no spacing is given
      else
        print*,' Error from tandist, no cell hight is given '
      endif
c------- Compute len*fdist
      do i=1,ni+1
        fdist(i)=fdist(i)*len
      enddo
      return
      end

      subroutine sinhdist(delta1,delta2,len,i1,i2,err,fdist)
************************************************************************
*     SINHDIST
*     program for computing the hyperbolic sine distribution
*     delta1 : grid spacing at i=i1 (first cell)
*     delta2 : grid spacing at i=i2 (last  cell)
*     len    : lenght of curve
*     i1     : indices of first vertex
*     i2     : indices of last vertex
*     fdist  : compute distribution function
*
*
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      integer i1,i2,ni,i,err
      real(kind=8)::delta1,delta2,len
      real(kind=8)::delta,b,a
      real(kind=8)::fdist(*)
      real(kind=8)::transsinh,transtanh,ftmp
      external transsinh,transtanh

      if(i2.eq.i1)return

c------- Normalize delta1 and delta2 with lenght
      delta1=delta1/len
      delta2=delta2/len
c------- Compute number of points
      ni=i2-i1

c------- Assure that B > 1
      if(delta1.le.0.d0.and.1/delta2.lt.ni)then
        delta1=1/(ni**2*delta2*1.02)
      elseif(delta2.le.0d0.and.1/delta1.lt.ni)then
        delta2=1/(ni**2*delta1*1.02)
      endif

c------- Spacing at both end specified
      if(delta1.gt.0.d0.and.delta2.gt.0.d0)then
        a=sqrt(delta2)/sqrt(delta1)
        b=1.d0/(ni*sqrt(delta1*delta2))
        if(b.ge.1.d0)then
          delta=transsinh(b,err)
          do i=1,ni+1
            ftmp=.5*(1+tanh(delta*(real(i-1)/real(ni)-0.5))
     &          /tanh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        else
          delta=transtanh(b,err)
          do i=1,ni+1
            ftmp=.5*(1+sinh(delta*(real(i-1)/real(ni)-0.5))
     &          /sinh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        endif
c------- Spacing only specified at xi=1
      else if(delta1.gt.0.d0)then
        b=1.d0/(ni*delta1)
        delta=transsinh(b,err)
        do i=1,ni+1
          fdist(i)=sinh(delta*real(i-1)/real(ni))/sinh(delta)
        enddo
c------- Spacing only specified at xi=ni
      else if(delta2.gt.0.d0)then
        b=1.d0/(ni*delta2)
        delta=transsinh(b,err)
        do i=1,ni+1
          fdist(i)=1-sinh(delta*(1-real(i-1)/real(ni)))/sinh(delta)
        enddo
c------- Error no spacing is given
      else
        print*,' Error from tandist, no cell hight is given '
      endif
c------- Compute len*fdist
      do i=1,ni+1
        fdist(i)=fdist(i)*len
      enddo
      return
      end

      double precision function transsinh(b,err)
************************************************************************
*     routine for solving the transendental equation
*
*     b=sinh(delta)/delta
*
*     using NewtonRaphson
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      integer::err
      real(kind=8)::delta,b
      real(kind=8)::delta_old,f,df,rlim
      parameter(rlim=1.d-1)
      integer::n,nmax=250

c------- Preforme Newton iteration to obtain delta
      delta=1.d0
      delta_old=delta
      do n=1,nmax
c--------- Compute f(delta)
c        f =b-sinh(delta)/delta
c        f =b*delta-sinh(delta)
         f =delta/(sinh(delta)+1.d-60)-1.d0/b
c--------- Compute d/d(delta)(f(delta))
c        df=(sinh(delta)-delta*cosh(delta))/delta**2
c        df=b-cosh(delta)
         df=(sinh(delta)-delta*cosh(delta))/sinh(delta)**2
c--------- update delta
        delta=delta-.5*f/df
c       delta=delta-f/df
c       write(*,'(4e16.4)')f,df,f/df,delta
c--------- check convergence criterium
        if(n.eq.nmax.and.abs((delta-delta_old)/delta_old).gt.rlim)then
          err=-1
c         print*,' Convergence problem in transinh dist. function !!! '
c         print*,' residual ',(delta-delta_old)/delta_old
        endif
c--------- update old value
        delta_old=delta
      enddo
      transsinh=delta
      return
      end

      double precision function transtanh(b,err)
************************************************************************
*     routine for solving the transendental equation
*
*     b=tanh(delta/2)/(delta/2)
*
*     using NewtonRaphson
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      integer::err
      real(kind=8)::delta,b
      real(kind=8)::delta_old,f,df,rlim
      parameter(rlim=1.d-1)
      integer::n,nmax=250

      if(b.gt.1.0d0)then
        print*,' Error form sinhdist: Can not be gridded '
c       stop
      endif
c------- Preforme Newton iteration to obtain delta
      delta=1.d0
      delta_old=delta
      do n=1,nmax
c--------- Compute f(delta)
        f =delta/(tanh(.5*delta)+1.d-30)-2./B
c--------- Compute d/d(delta)(f(delta))
        df=(tanh(.5*delta)-delta*(1-tanh(.5*delta)**2)*.5d0)
     &    /(tanh(.5*delta)**2+1.d-30)
c--------- update delta
        delta=delta-.5*f/df
c       delta=delta-f/df
c       write(*,'(i3,1x,3e16.4)')n,delta,f,df
c--------- check convergence criterium
        if(n.eq.nmax.and.abs((delta-delta_old)/delta_old).gt.rlim)then
          err=-1
c         print*,' Convergence problem in trantanh dist. function !!! '
c         print*,' residual ',(delta-delta_old)/delta_old
        endif
c--------- update old value
        delta_old=delta
      enddo
      transtanh=delta
      return
      end
