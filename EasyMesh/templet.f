      program EasyTemplet
************************************************************************
*     EasyTemplet is a routine to write templets for the meshgeneration
*     program easymesh.
*
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      open(unit=10,file='easytmp.dat')

      write(10,*)'#SURFACE DESCRIPTION'
      write(10,*)'#FACE1 [sections]'
      write(10,*)'#SEC1  [ni]'
      write(10,*)'#FACE2 [sections]'
      write(10,*)'#SEC1  [ni]'
      write(10,*)'#FACE3 [sections]'
      write(10,*)'#SEC1  [ni]'
      write(10,*)'#FACE4 [sections]'
      write(10,*)'#SEC1  [ni]'
      write(10,*)'#DISTRIBUTION FUNCTIONS'
      write(10,*)'#FACE1 [ni] [spline/linear]'
      write(10,*)'#SEC1  [ni]'
      write(10,*)'#FACE2 [ni] [spline/linear]'
      write(10,*)'#SEC1  [ni]'
      write(10,*)'#FACE3 [ni] [spline/linear]'
      write(10,*)'#SEC1  [ni]'
      write(10,*)'#FACE4 [ni] [spline/linear]'
      write(10,*)'#SEC1  [ni]'

      stop
      end
