      program Plot3DtoVTK
c-----------------------------------------------------------------------
c     Author: Frederik Zahle                                           c
c     Institution: Risoe DTU                                           c
c     Date: 11.06.2009                                                 c
c-----------------------------------------------------------------------
c     Program to convert unformatted plot3D xyz(.fvbnd) and .f files   c
c     to VTKMultiGrid data files in ASCII format.                      c
c     The output consists of a grid.vtm file, which is a header file   c
c     that links to a list of files named grid_xxxx.vts. Each file     c
c     contains coordinate and scalar data for only one block.          c

c     The same number of variables are dumped to the vts files         c
c     as well as an attribute array.                                   c
c-----------------------------------------------------------------------
      implicit none

      real(kind=4),dimension(:,:,:,:),allocatable::x,y,z
      real(kind=4),dimension(:,:,:,:,:),allocatable::var
      real(kind=4)::dummy
      integer,dimension(:,:,:,:),allocatable::attr
      integer::i,j,k,n,nn,ni,nblock,nvar
      integer::bc,bc_user,ist,ifi,jst,jfi,kst,kfi,block,orien
      character(len=256) filename1,face
      character(len=10),dimension(:),allocatable::varname
c---- XML headers
      character(len=200)::head1
      head1=' '

c---- read plot3d unformatted grid file grid.xyz -----------------------
      open(unit=4,file='grid.xyz',form='unformatted')
      read(4)nblock
      print*,' Nblock ',nblock
      read(4)(ni,ni,ni,n=1,nblock)
      print*,' Ni ',ni
      allocate(x(ni,ni,ni,nblock),y(ni,ni,ni,nblock),z(ni,ni,ni,nblock))
      allocate(attr(ni,ni,ni,nblock))
      do n=1,nblock
        read(4)
     &    (((x(i,j,k,n),i=1,ni),j=1,ni),k=1,ni),
     &    (((y(i,j,k,n),i=1,ni),j=1,ni),k=1,ni),
     &    (((z(i,j,k,n),i=1,ni),j=1,ni),k=1,ni)
      enddo
      close(4)
      print*,'Read grid.xyz ok '


c---- read plot3d unformatted variable file grid.f ---------------------
      open(unit=4,file='grid.f',form='unformatted')
      read(4)nblock
      read(4)(ni,ni,ni,nvar,n=1,nblock)
      allocate(var(nvar,ni,ni,ni,nblock))

      do n=1,nblock
        read(4)((((var(nn,i,j,k,n),i=1,ni),j=1,ni),k=1,ni),nn=1,nvar)
      enddo
      close(4)
      print*,' Read grid.f ok '

      allocate(varname(nvar));varname=''
      open(unit=4,file='grid.nam',form='formatted')
      do nn=1,nvar
        read(4,*)varname(nn)
      enddo
      close(4)
      print*,' Read grid.nam ok '

c---- read plot3d formatted bnd file -----------------------------------
      attr=1
      open(4,file='grid.xyz.fvbnd',form='formatted')
      read(4,*)face,bc,bc
      read(4,*)face
      read(4,*)face
      read(4,*)face
      read(4,*)face
      read(4,*)face
      read(4,*)face
c---- bc types ---------------------------------------------------------
  111 read(4,*,end=666)bc,block,ist,ifi,jst,jfi,kst,kfi,face,orien
      select case(bc)
      case(1)
         bc_user=101
      case(2)
         bc_user=201
      case(3)
         bc_user=401
      case(4)
         bc_user=601
      end select
      attr(ist:ifi,jst:jfi,kst:kfi,block)=bc_user
      goto 111
      print*,' Read grid.xyz.fvbnd ok '
  666 print*,'Done reading plot3D files ...'

c---- Write VTK MultiBlockDataSet header file --------------------------
      head1(1:47)='   <DataSet index="0000" file="grid_0000.vts"/>'
      open(21,file='grid.vtm',form='formatted')
      write(21,'(A21)')'<?xml version="1.0"?>'
      write(21,'(A112)')'<VTKFile type="vtkMultiBlockDataSet" 
     &version="1.0" byte_order="LittleEndian" 
     &compressor="vtkZLibDataCompressor">"'
      write(21,'(A24)')'  <vtkMultiBlockDataSet>'

c---- for each block ---------------------------------------------------
      do n=1,nblock
         write(head1(20:23),'(i4.4)')n
         write(head1(37:40),'(i4.4)')n
         write(21,'(A52)')head1(1:47)
      enddo
      write(21,'(A25)')'  </vtkMultiBlockDataSet>'
      write(21,'(A10)')'</VTKFile>'

c---- write grid data files for each block -----------------------------
      print*,'writing block data files ...'
      do n=1,nblock
         filename1(1:13)='grid_0000.vts'
         write(filename1(6:9),'(i4.4)')n
         open(unit=22,file=filename1(1:13),form='formatted')
c---- header -----------------------------------------------------------
         write(22,'(A21)')'<?xml version="1.0"?>'
         write(22,'(A112)')'<VTKFile type="StructuredGrid" version="0.1"
     & byte_order="LittleEndian" compressor="vtkZLibDataCompressor">'
         write(22,203)ni-1,ni-1,ni-1
         write(22,204)ni-1,ni-1,ni-1
c---- vector and scalar arrays ----------------------------------------- 
         write(22,'(A52)')'    <PointData Scalars="Scalars" 
     &Vectors="Velocity">'
c---- velocity vectors ------------------------------------------------- 
         write(22,205)'velocity',3,minval(var(3,:,:,:,n))
     &                            ,maxval(var(3,:,:,:,n))
         write(22,'(6f24.10)')(((var(1,i,j,k,n),var(2,i,j,k,n)
     &      ,var(3,i,j,k,n),i=1,ni),j=1,ni),k=1,ni)
         write(22,'(A18)')'      </DataArray>'
         do nn=4,nvar
c----  variables ---------------------------------------------------------
           write(22,205)varname(nn),1,minval(var(nn,:,:,:,n))
     &                            ,maxval(var(nn,:,:,:,n))
           write(22,'(6f24.10)')(((var(nn,i,j,k,n)
     &                       ,i=1,ni),j=1,ni),k=1,ni)
           write(22,'(A18)')'      </DataArray>'
         enddo
c---- attribute -------------------------------------------------------- 
         write(22,206)'attr',1,minval(attr(:,:,:,n))
     &                        ,maxval(attr(:,:,:,n))
         write(22,'(6i5)')(((attr(i,j,k,n)
     &                       ,i=1,ni),j=1,ni),k=1,ni)
         write(22,'(A18)')'      </DataArray>'
c-----------------------------------------------------------------------
         write(22,'(A16)')'    </PointData>'
         write(22,'(A16)')'    <CellData>  '
         write(22,'(A16)')'    </CellData> '
c---- grid points ------------------------------------------------------
         write(22,'(A16)')'    <Points>    '
         write(22,205)'mesh',3,minval(x(:,:,:,n)),
     &                maxval(x(:,:,:,n))
         write(22,'(6f24.10)')(((x(i,j,k,n),y(i,j,k,n),z(i,j,k,n),
     &                 i=1,ni),j=1,ni),k=1,ni)
         write(22,'(A18)')'      </DataArray>'
         write(22,'(A13)')'    </Points>'
c-----------------------------------------------------------------------
         write(22,'(A10)')'  </Piece>'
         write(22,'(A19)')'  </StructuredGrid>'
         write(22,'(A10)')'</VTKFile>'
         close(22)
      enddo

  203 format('  <StructuredGrid WholeExtent="0 ',i3,' 0 ',i3,' 0 ',
     &                                           i3,' ">')
  204 format('  <Piece Extent="0 ',i3,' 0 ',i3,' 0 ',i3,' ">')
  205 format('   <DataArray type="Float32" Name="',A10,'" Number
     &OfComponents="',i3,
     &'" format="ascii" RangeMin="',f16.2,'" RangeMax="',f16.2,'">')
  206 format('   <DataArray type="Int32" Name="',A10,'" Number
     &OfComponents="',i3,
     &'" format="ascii" RangeMin="',i6,'" RangeMax="',i6,'">')
      end
