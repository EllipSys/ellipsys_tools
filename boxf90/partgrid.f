      subroutine partgrid
c----------------------------------------------------------------------
c     splits a singel block grid into cubic multiblock grids 
c----------------------------------------------------------------------
      use params
      use vertices
      implicit none
      integer::i,j,k,l,f,n,nn,idx
      integer::istart,jstart,kstart,io,jo,ko,ii,jj,kk
      integer::dim,nbi,nbj,nbk,nbm
      integer::yesno,factor
      real(kind=8)::rest
c     real(kind=8),dimension(:,:,:,:),allocatable::xt,yt,zt
c     integer,dimension(:,:,:,:),allocatable::attrt
      integer,dimension(6)::sorted=0,face=0
      integer bctype,attrmax

c-------set boundary condtions
      attr=1
c-------sort attributes in decending order
      face=0;sorted=0
      do l=1,6
      attrmax=1
        do f=1,6
          if(setattr(f).ge.attrmax.and.sorted(f).eq.0)then
            attrmax=setattr(f)
            idx=f
          endif
        enddo
        face(l)=idx
        sorted(idx)=1
      enddo

      do l=1,6
        select case(face(l))
c---------west wall
        case(1);attr(1 ,: ,: )=setattr(1)
c---------east wall
        case(2);attr(ni,: ,: )=setattr(2)
c---------south wall
        case(3);attr(: ,1 ,: )=setattr(3)
c---------north wall
        case(4);attr(: ,nj,: )=setattr(4)
c---------bottom wall
        case(5);attr(: ,: ,1 )=setattr(5)
c---------top wall
        case(6);attr(: ,: ,nk)=setattr(6)
        end select
      enddo
      
c-----find max size of multigrid blocks
c-----min dimension
      dim=int(min(real(ni),real(nj),real(nk)))-1
      print*,' initial dimension ',dim

      rest=mod(ni-1,dim)
     &    +mod(nj-1,dim)
     &    +mod(nk-1,dim)
      print*,' rest ',rest
      do while(rest.ne.0.d0.and.dim.ge.3)
        rest=mod(ni-1,dim)
     &      +mod(nj-1,dim)
     &      +mod(nk-1,dim)
        dim=dim/2
        print*,dim
      enddo
      print*,'*******************************************************'
      print*,' 3D block size                      : ',ni,nj,nk
      print*,'size of multidomaine blocks         : ',dim
      print*,'want to change this yes = 1, no = 2   '
      read(*,*) yesno
      if(yesno.eq.1)then
        print*,'give new dimension  '
        read(*,*)dim
      endif
      print*,' Give reduction factor: '
      read*,factor
      nbi=(ni-1)/dim
      nbj=(nj-1)/dim
      nbk=(nk-1)/dim
      nbm=nbi*nbj*nbk
      print*,'*******************************************************'
      print*,'size of multidomaine blocks         : ',dim/factor
      print*,'number of block in xi direction     : ',nbi
      print*,'number of block in eta direction    : ',nbj
      print*,'number of block in zeta direction   : ',nbk
      print*,'total number of blocks              : ',nbm 
      print*,'*******************************************************'

      dim=dim/factor
      allocate(xt(dim+3,dim+3,dim+3,nbm),
     &         yt(dim+3,dim+3,dim+3,nbm),
     &         zt(dim+3,dim+3,dim+3,nbm),
     &      attrt(dim+3,dim+3,dim+3,nbm))

      nn=0

      do kk=1,nbk;kstart=(kk-1)*dim*factor+1
      do jj=1,nbj;jstart=(jj-1)*dim*factor+1
      do ii=1,nbi;istart=(ii-1)*dim*factor+1
        nn=nn+1
        print*,' Writing block ',nn
        print*,istart,jstart,kstart
        do k=2,dim+2;ko=kstart+(k-2)*factor
        do j=2,dim+2;jo=jstart+(j-2)*factor
        do i=2,dim+2;io=istart+(i-2)*factor
           xt(i,j,k,nn)=   x(io,jo,ko)
           yt(i,j,k,nn)=   y(io,jo,ko)
           zt(i,j,k,nn)=   z(io,jo,ko)
           attrt(i,j,k,nn)=attr(io,jo,ko)
        enddo;enddo;enddo
      enddo;enddo;enddo

      call UnfGridWrite(20,dim+3,nbm,xt,yt,zt,attrt)
      return

      deallocate(xt,yt,zt,attrt)

      return
   21 format(i6,3(f30.12))
  666 write(*,*) ' error: too few data in input file (grid.dat)'
      end

      subroutine UnfGridWrite(unitnr,ni,nblock,x,y,z,attr)
C=======================================================================
C     routine for writing unformatted x3d file
C     remember that the grid must positions in the following region 
C
C        x(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C        y(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C        z(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C     attr(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C
C     the remaining spaces are intended for ghost cells
C=======================================================================
      implicit none
      integer unitnr,ni,nblock
      real(kind=8),dimension(ni,ni,ni,nblock)::x,y,z
      integer,dimension(ni,ni,ni,nblock)::attr

C     Code to be used to add ghost cells before call UnfGridWrite
c     do n=1,nblock
c     do k=2,ni-1
c     do j=2,ni-1
c     do i=2,ni-1
c       xg   (i,j,k,n)=x   (i-1,j-1,k-1,n)
c       yg   (i,j,k,n)=y   (i-1,j-1,k-1,n)
c       zg   (i,j,k,n)=z   (i-1,j-1,k-1,n)
c       attrg(i,j,k,n)=attr(i-1,j-1,k-1,n)
c     enddo;enddo;enddo
c     enddo

      print*,' unitnr ',unitnr
      open(unit=unitnr,file='grid.x3dunf',form='unformatted')
      write(unitnr)ni-3
      write(unitnr)nblock
      print*,' Writing Attributes '
      call WriteBlockInteger(unitnr,ni**3,nblock,attr)
      print*,' Writing x '
      call WriteBlockReal   (unitnr,ni**3,nblock,x   )
      print*,' Writing y '
      call WriteBlockReal   (unitnr,ni**3,nblock,y   )
      print*,' Writing z '
      call WriteBlockReal   (unitnr,ni**3,nblock,z   )
      print*,' Finished writing '
      close(unitnr)
      return
      end

      subroutine WriteBlockReal(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      real(kind=8),dimension(block,nblock)::x
      real(kind=8),dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine WriteBlockReal

      subroutine WriteBlockInteger(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      integer,dimension(block,nblock)::x
      integer,dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine WriteBlockInteger
