      subroutine computedist
      use params
      use geometry
      use distributionfunctions
      implicit none
      real(kind=8),dimension(2,3)::dinp
      real(kind=8)::h1,h2,len
      integer::i

      allocate(dist_x(ni),dist_y(nj),dist_z(nk))

      if(nr_dist_x.gt.0)then
        call distfunc(nr_dist_x,dist_inp_x,ni,dist_x)
      else
c-------equidistant distribution
        do i=1,ni
          dist_x(i)=real(i-1)/real(ni-1)
        enddo
      endif

      if(nr_dist_y.gt.0)then
        call distfunc(nr_dist_y,dist_inp_y,nj,dist_y)
      else
c-------equidistant distribution
        do i=1,nj
          dist_y(i)=real(i-1)/real(nj-1)
        enddo
      endif

      if(nr_dist_z.gt.0)then
        call distfunc(nr_dist_z,dist_inp_z,nk,dist_z)
      else
c-------equidistant distribution
        do i=1,nk
          dist_z(i)=real(i-1)/real(nk-1)
        enddo
      endif
      return
      end
