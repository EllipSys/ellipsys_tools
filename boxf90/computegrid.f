      subroutine computegrid
      use params
      use geometry
      use vertices
      use distributionfunctions
      implicit none
      integer i,j,k

      allocate(x(ni,nj,nk)
     &        ,y(ni,nj,nk)
     &        ,z(ni,nj,nk),attr(ni,nj,nk))

      do i=1,ni
      do j=1,nj
      do k=1,nk
        x(i,j,k)=xlen*dist_x(i)+xshift
        y(i,j,k)=ylen*dist_y(j)+yshift
        z(i,j,k)=zlen*dist_z(k)+zshift
      enddo;enddo;enddo

      open(unit=99,file='dz.dat')
      do k=1,nk
        write(99,*)zlen*dist_z(k)+zshift
      enddo
      close(99)
      open(unit=99,file='dx.dat')
      do k=1,ni
        write(99,*)xlen*dist_x(k)+xshift
      enddo
      close(99)
      open(unit=99,file='dy.dat')
      do k=1,nj
        write(99,*)ylen*dist_y(k)+yshift
      enddo
      close(99)

      return
      end
