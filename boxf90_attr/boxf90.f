      module geometry
      real(kind=8)::xlen,ylen,zlen
      real(kind=8)::xshift=0,yshift=0,zshift=0
      end module

      module distributionfunctions
      real(kind=8),dimension(:),allocatable::dist_x
     &                                      ,dist_y
     &                                      ,dist_z
      integer::nr_dist_x=0
     &        ,nr_dist_y=0
     &        ,nr_dist_z=0
      real(kind=8),dimension(:,:),allocatable::dist_inp_x
     &                                        ,dist_inp_y
     &                                        ,dist_inp_z
      end module

      module vertices
      real(kind=8),dimension(:,:,:),allocatable::x,y,z
      integer,dimension(:,:,:),allocatable::attr
      end module

      module params
      integer::ni,nj,nk
      integer,parameter::idp4=4
      integer,dimension(6)::setattr
      end module

      program cubic
C#######################################################################
C     A small program for generating stretch cartesian meshes 
C     
C     Author: Niels N. Soerensen
C     Date: 2-05-2016
C#######################################################################
      implicit none
      integer::bsize,ni
      integer::i,j,k,n
      real(kind=8)::h1,h2,h3,r1
      integer::yesno
c     integer,dimension(:,:,:),allocatable::attr
c     real(kind=8),dimension(:,:,:),allocatable::x,y,z
c     real(kind=8),dimension(:),allocatable::fx,fy,fz

c-------read input
      call ReadInput

c-------compute distribution functions
      call ComputeDist

c-------Compute grid
      call ComputeGrid

c-------partitionate grid
      print*,'Compute grid? yes = 1, no = 2   '
      read(*,*) yesno
      if(yesno.eq.1)call partgrid
      stop
      end 
