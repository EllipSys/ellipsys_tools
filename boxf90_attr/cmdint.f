      module inpwords
      integer,parameter::mword=50
      integer::nrchar(mword),nrword
      integer,parameter::mchar=256
      character(len=40),dimension(mword):: words
      end module

      subroutine ReadInput
c----------------------------------------------------------------------
c     subroutine reading the input parameters from input.dat
c----------------------------------------------------------------------
      use geometry
      use inpwords
      use params
      use distributionfunctions
      implicit none
      logical::linpexist=.false.
      character(len=256)InputLine
      integer::i

      inquire(file='input.dat',exist=linpexist)
      if(linpexist)then
        print*,' Reading input data ! '
        open(unit=10,file='input.dat')
        do
          read(10,'(a256)',end=666)InputLine
          call cmdpart(InputLine)
          if(nrword.gt.1)then
c----------- main interpreter
             select case(words(1))
c------------  number of point in i-direction
             case('ni')
               read(words(2),*)ni
               nj=ni;nk=ni
c------------  number of point in j-direction
             case('nj')
               read(words(2),*)nj
               nk=nj
c------------  number of point in k-direction
             case('nk')
               read(words(2),*)nk
c------------  length in i-direction
             case('xlen')
               read(words(2),*)xlen
               ylen=xlen;zlen=xlen
c------------  length in j-direction
             case('ylen')
               read(words(2),*)ylen
               zlen=ylen
c------------  length in k-direction
             case('zlen')
               read(words(2),*)zlen
c------------  shift domain xshift in x-direction
             case('xshift')
               read(words(2),*)xshift
c------------  shift domain yshift in y-direction
             case('yshift')
               read(words(2),*)yshift
c------------  shift domain zshift in z-direction
             case('zshift')
               read(words(2),*)zshift
c------------  bc in i-direction
             case('xbc')
               read(words(2),*)setattr(1)
               read(words(3),*)setattr(2)
c------------  bc in j-direction
             case('ybc')
               read(words(2),*)setattr(3)
               read(words(3),*)setattr(4)
c------------  bc in z-direction
             case('zbc')
               read(words(2),*)setattr(5)
               read(words(3),*)setattr(6)
c------------  x-distribution distribution funcition 
             case('x-distribution')
               read(words(2),*)nr_dist_x
               allocate(dist_inp_x(nr_dist_x,3))
               do i=1,nr_dist_x
                 read(words(3+(i-1)*3),*)dist_inp_x(i,1)
                 read(words(4+(i-1)*3),*)dist_inp_x(i,2)
                 read(words(5+(i-1)*3),*)dist_inp_x(i,3)
               enddo
c------------  y-distribution distribution funcition 
             case('y-distribution')
               read(words(2),*)nr_dist_y
               allocate(dist_inp_y(nr_dist_y,3))
               do i=1,nr_dist_y
                 read(words(3+(i-1)*3),*)dist_inp_y(i,1)
                 read(words(4+(i-1)*3),*)dist_inp_y(i,2)
                 read(words(5+(i-1)*3),*)dist_inp_y(i,3)
               enddo
c------------  z-distribution distribution funcition 
             case('z-distribution')
               read(words(2),*)nr_dist_z
               allocate(dist_inp_z(nr_dist_z,3))
               do i=1,nr_dist_z
                 read(words(3+(i-1)*3),*)dist_inp_z(i,1)
                 read(words(4+(i-1)*3),*)dist_inp_z(i,2)
                 read(words(5+(i-1)*3),*)dist_inp_z(i,3)
               enddo
c------------  comment line
             case default
               print*, words(1),' command not reconized '
             end select
c----------- call command interpreter part of interface
          endif
        enddo
      else
        print*,' no input file : '
      endif
  666 close(10)
      return
      end

      subroutine cmdpart(InputLine)
c-----------------------------------------------------------------------
c     routine parting command InputLine into words
c----------------------------------------------------------------------
c
c     Author: Jess A. Michelsen and Niels N. Soerensen
c     last revision :march 3, 1998
c-----------------------------------------------------------------------
      use inpwords
      implicit none
      integer i
      character(len=40)::blank
      character(len=256) InputLine
      integer CurrentPosition,RelativeBlankPosition,BlankPosition
C--------------------------------------------- Reset word counter ------
      nrword=0
      CurrentPosition=1
C--------------------------------------------- Skip if a comment line --
      if(index(InputLine(1:mchar),'#').gt.0) return
C---------------------------------------- find distance to next blank --
   20 RelativeBlankPosition=index(InputLine(CurrentPosition:mchar),' ')
      if(CurrentPosition.eq.mchar.or.RelativeBlankPosition.eq.0) return
C---------------------------------------- found the end of a word ------
      if(RelativeBlankPosition.gt.1.and.RelativeBlankPosition.le.40)then
        BlankPosition=CurrentPosition-1+RelativeBlankPosition
        nrword=nrword+1
        if(nrword.gt.mword)return
        words(nrword)=InputLine(CurrentPosition:BlankPosition)
        nrchar(nrword)=RelativeBlankPosition-1
      endif
c-----------------------------------------------------------------------
      CurrentPosition=CurrentPosition+RelativeBlankPosition
      goto 20
c-----------------------------------------------------------------------
      return
      end
