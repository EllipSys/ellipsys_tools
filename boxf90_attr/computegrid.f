      subroutine computegrid
      use params
      use geometry
      use vertices
      use distributionfunctions
      implicit none
      integer i,j,k

      allocate(x(ni,nj,nk)
     &        ,y(ni,nj,nk)
     &        ,z(ni,nj,nk),attr(ni,nj,nk))

      do i=1,ni
      do j=1,nj
      do k=1,nk
        x(i,j,k)=xlen*dist_x(i)+xshift
        y(i,j,k)=ylen*dist_y(j)+yshift
        z(i,j,k)=zlen*dist_z(k)+zshift
      enddo;enddo;enddo

      open(unit=99,file='zDistribution.dat')
      do k=2,nk
        write(99,*)zlen*dist_z(k)+zshift,(dist_z(k)-dist_z(k-1))*zlen
      enddo
      close(99)
      open(unit=99,file='yDistribution.dat')
      do j=2,nj
        write(99,*)ylen*dist_y(j)+yshift,(dist_y(j)-dist_y(j-1))*ylen
      enddo
      close(99)
      open(unit=99,file='xDistribution.dat')
      do i=2,ni
        write(99,*)xlen*dist_x(i)+xshift,(dist_x(i)-dist_x(i-1))*xlen
      enddo
      close(99)

      return
      end
