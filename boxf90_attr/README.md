# BoxF program: Generate a box mesh and sort the attributes (boundary conditions)

### input.dat specification:
    $ ni 1025        #  number of node points in x-dir
    $ nj 129         #  number of node points in y-dir
    $ nk 1281        #  number of node points in z-dir
    $ xlen 7000      #  domain length in x-dir
    $ ylen 500       #  domain length in y-dir
    $ zlen 10000     #  domain length in z-dir
    $ xbc 201  401   #  boundary condition in x-dir
    $ ybc 601  601   #  boundary condition in y-dir
    $ zbc 101  201   #  boundary condition in z-dir
    $ x-distribution 4  0.0 -1 1 0.15 0.0014 33 0.84 0.0 993 1.0 -1 1025       # distribution in x-dir
    $ y-distribution 3  0.0 0.001 1 0.30 0.01 44 1.0 -1 129                    # distribution in y-dir
    $ z-distribution 4  0.0 -1 1 0.042 0.0005 16 0.618 0.0005 1168 1.0 -1 1281 # distribution in z-dir
    $ xshift 0       #  shift in x-dir
    $ yshift 0       #  shift in y-dir
    $ zshift 0       #  shift in z-dir

Each distribution is specified as:
x-distribution <number of control point> <[control point 1]> ... <[control point i]>... <[control point n]> 
where <[control point i]> consist of three numbers: [<distance> <cell size> <node number>].
The distance and cell size are normalized by the domain length. This means that the distance is always
between 0 and 1.
