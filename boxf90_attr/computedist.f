      subroutine computedist
      use params
      use geometry
      use distributionfunctions
      implicit none
      real(kind=8),dimension(2,3)::dinp
      real(kind=8)::h1,h2,len
      integer::i

      allocate(dist_x(ni),dist_y(nj),dist_z(nk))

      if(nr_dist_x.gt.0)then
        call distfunc(nr_dist_x,dist_inp_x,ni,dist_x)
      else
        dinp(1,1)=0.d0;dinp(1,2)=1.d0/real(ni);dinp(1,3)=1.d0
        dinp(2,1)=1.d0;dinp(2,2)=1.d0/real(ni);dinp(2,3)=ni
        call distfunc(2,dinp,ni,dist_x)
      endif

      if(nr_dist_y.gt.0)then
        call distfunc(nr_dist_y,dist_inp_y,nj,dist_y)
      else
        dinp(1,1)=0.d0;dinp(1,2)=1.d0/real(nj);dinp(1,3)=1.d0
        dinp(2,1)=1.d0;dinp(2,2)=1.d0/real(nj);dinp(2,3)=nj
        call distfunc(2,dinp,nj,dist_y)
      endif

      if(nr_dist_z.gt.0)then
        call distfunc(nr_dist_z,dist_inp_z,nk,dist_z)
      else
        dinp(1,1)=0.d0;dinp(1,2)=1.d0/real(nk);dinp(1,3)=1.d0
        dinp(2,1)=1.d0;dinp(2,2)=1.d0/real(nk);dinp(2,3)=nk
        call distfunc(2,dinp,nk,dist_z)
      endif
      return
      end
