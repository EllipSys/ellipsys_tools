      subroutine partgrid
c----------------------------------------------------------------------
c     splits a singel block grid into cubic multiblock grids 
c----------------------------------------------------------------------
      use params
      use vertices
      implicit none
      integer::i,j,k,n,nn,tali,talk,talj
      integer::istart,jstart,kstart,io,jo,ko,ii,jj,kk
      integer::dim,nbi,nbj,nbk,nbm
      integer::yesno,factor
      real(kind=8)::rest
      real(kind=8),dimension(:,:,:,:),allocatable::xt,yt,zt
      integer,dimension(:,:,:,:),allocatable::attrt
      integer,dimension(6)::order

c-------sort the BC array (bubble sort method)
      order = (/1,2,3,4,5,6/)
      do j=1,6
          do i=j+1,6
              if (setattr(order(i)).gt.setattr(order(j))) then
                  k = order(i)
                  order(i) = order(j)
                  order(j) = k
              end if
          end do
      end do

c-------set boundary condtions
      attr=1
      do i=1,6
          select case(order(i))
              case(1) !XBC_1
                  attr(1,:,:)  =  setattr(order(i))
              case(2) !XBC_NI
                  attr(ni,:,:) =  setattr(order(i))
              case(3) !YBC_1
                  attr(:,1,:)  =  setattr(order(i))
              case(4) !YBC_NJ
                  attr(:,nj,:) =  setattr(order(i))
              case(5) !ZBC_1
                  attr(:,:,1)  =  setattr(order(i))
              case(6) !ZBC_NK
                  attr(:,:,nk) =  setattr(order(i))
          end select
          print *, 'BC',order(i), setattr(order(i))
      end do

!c-------nort face
!      attr(:,1 ,:)=601
!c-------south face
!      attr(:,nj,:)=601
!c-------west face
!      attr(1 ,:,:)=601
!c-------east face
!      attr(ni,:,:)=601
!c-------top face
!      attr(:,:,nk)=401
!c-------bottom face
!      attr(:,:,1 )=201
!

c-----find max size of multigrid blocks
c-----min dimension
      dim=int(min(real(ni),real(nj),real(nk)))-1
      print*,' initial dimension ',dim

      rest=mod(ni-1,dim)
     &    +mod(nj-1,dim)
     &    +mod(nk-1,dim)
      print*,' rest ',rest
      do while(rest.ne.0.d0.and.dim.ge.3)
        rest=mod(ni-1,dim)
     &      +mod(nj-1,dim)
     &      +mod(nk-1,dim)
        dim=dim/2
        print*,dim
      enddo
      print*,'*******************************************************'
      print*,' 3D block size                      : ',ni,nj,nk
      print*,'size of multidomaine blocks         : ',dim
      print*,'want to change this yes = 1, no = 2   '
      read(*,*) yesno
      if(yesno.eq.1)then
        print*,'give new dimension  '
        read(*,*)dim
      endif
      print*,' Give reduction factor: '
      read*,factor
      nbi=(ni-1)/dim
      nbj=(nj-1)/dim
      nbk=(nk-1)/dim
      nbm=nbi*nbj*nbk
      print*,'*******************************************************'
      print*,'size of multidomaine blocks         : ',dim/factor
      print*,'number of block in xi direction     : ',nbi
      print*,'number of block in eta direction    : ',nbj
      print*,'number of block in zeta direction   : ',nbk
      print*,'total number of blocks              : ',nbm 
      print*,'*******************************************************'

      dim=dim/factor

      allocate(xt(dim+1,dim+1,dim+1,nbm),
     &         yt(dim+1,dim+1,dim+1,nbm),
     &         zt(dim+1,dim+1,dim+1,nbm),
     &      attrt(dim+1,dim+1,dim+1,nbm))

      nn=0

      do kk=1,nbk;kstart=(kk-1)*dim*factor
      do jj=1,nbj;jstart=(jj-1)*dim*factor
      do ii=1,nbi;istart=(ii-1)*dim*factor
        nn=nn+1
        print*,' Writing block ',nn
        print*,istart,jstart,kstart
        do k=1,dim+1;ko=kstart+(k-1)*factor+1
        do j=1,dim+1;jo=jstart+(j-1)*factor+1
        do i=1,dim+1;io=istart+(i-1)*factor+1
           xt(i,j,k,nn)=   x(io,jo,ko)
           yt(i,j,k,nn)=   y(io,jo,ko)
           zt(i,j,k,nn)=   z(io,jo,ko)
        attrt(i,j,k,nn)=attr(io,jo,ko)
        enddo;enddo;enddo
      enddo;enddo;enddo


c     top    
c     attrt(:,:,dim+1,1)=601
c     attrt(:,:,dim+1,2)=601
c     attrt(:,:,dim+1,3)=601
c     attrt(:,:,dim+1,4)=601
c     attrt(:,:,dim+1,5)=601
c     attrt(:,:,dim+1,6)=601

c     cycle nord syd 
c      attrt(:,1,:,1)=599
c     attrt(:,1,:,2)=598
c     attrt(:,1,:,3)=597
c     attrt(:,dim+1,:,4)=599
c     attrt(:,dim+1,:,5)=598
c     attrt(:,dim+1,:,6)=597
      
c     cycle �st vest 
c     attrt(1,:,:,1)=578
c     attrt(1,:,:,4)=577
c     attrt(dim+1,:,:,3)=578
c     attrt(dim+1,:,:,6)=577


c     bund    
c     attrt(:,:,1,1)=101
c     attrt(:,:,1,2)=101
c     attrt(:,:,1,3)=101
c     attrt(:,:,1,4)=101
c     attrt(:,:,1,5)=101
c     attrt(:,:,1,6)=101


C------make FieldView plot3d unformatted grid file
      open(unit=20,file='grid.xyz',form='formatted')
c     write(20)nbm
c     write(20)(dim+1,dim+1,dim+1,n=1,nbm)
      write(20,*)nbm
      write(20,*)(dim+1,dim+1,dim+1,n=1,nbm)
      do n=1,nbm
c       write(20)
c    &    (((real(xt(i,j,k,n),idp4)
c    &      ,i=1,dim+1),j=1,dim+1),k=1,dim+1),
c    &    (((real(yt(i,j,k,n),idp4)
c    &      ,i=1,dim+1),j=1,dim+1),k=1,dim+1),
c    &    (((real(zt(i,j,k,n),idp4)
c    &      ,i=1,dim+1),j=1,dim+1),k=1,dim+1)
        write(20,*)
     &    (((real(xt(i,j,k,n),idp4)
     &      ,i=1,dim+1),j=1,dim+1),k=1,dim+1),
     &    (((real(yt(i,j,k,n),idp4)
     &      ,i=1,dim+1),j=1,dim+1),k=1,dim+1),
     &    (((real(zt(i,j,k,n),idp4)
     &      ,i=1,dim+1),j=1,dim+1),k=1,dim+1)
      enddo
      close(20)

      open(unit=20,file='grid.x3d')
      write(20,*) dim,nbm
      do n=1,nbm   
      do k=1,dim+1
      do j=1,dim+1
      do i=1,dim+1
        write(20,21)attrt(i,j,k,n)
     &             ,xt(i,j,k,n)
     &             ,yt(i,j,k,n)
     &             ,zt(i,j,k,n)
      enddo;enddo;enddo;enddo
      close(20)

      deallocate(xt,yt,zt)

      return
   21 format(i6,3(f20.10))
  666 write(*,*) ' error: too few data in input file (grid.dat)'
      end
