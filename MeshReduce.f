      program Mesh2FieldView
C######################################################################
C     Author: Niels N. S�rensen
C######################################################################
      implicit none
      logical::statex=.false.
      integer::ni,nic
      integer,parameter::idp4=4,idp8=8,ncomp=4
      real(kind=8),dimension(:,:,:,:),allocatable::x,y,z,xc,yc,zc
      integer,dimension(:,:,:,:),allocatable::attr,attrc
      real(kind=8)::dummy
      character*11 fnamd,fnamh
      character*128 fieldname(ncomp)
      integer::idummy
      integer::i,j,k,n,nc,nvar,fileform
      integer::bsize,nblock,b1,iblock
      integer::ii,jj,kk,factor

      inquire(file='grid.x3dunf',exist=statex)
      if(statex)then
        print*,' READING UNFORMATTED GRID FILE'
        open(unit=20,file='grid.x3dunf',form='unformatted')
        read(20)bsize
        read(20)nblock
        print*,bsize,nblock
        ni=bsize+3
        allocate(x(ni,ni,ni,nblock),y(ni,ni,ni,nblock)
     &          ,z(ni,ni,ni,nblock),attr(ni,ni,ni,nblock))
        attr=0;x=0.d0;y=0.d0;z=0.d0
        print*,' Reading Attributs : '
        call ReadBlockInteger(20,ni**3,nblock,attr)
        print*,' Reading x-coord : '
        call ReadBlockReal   (20,ni**3,nblock,x)
        print*,' Reading y-coord : '
        call ReadBlockReal   (20,ni**3,nblock,y)
        print*,' Reading z-coord : '
        call ReadBlockReal   (20,ni**3,nblock,z)
        print*,' Finished Reading : '
        close(20)
      else
        inquire(file='grid.X3D',exist=statex)
        if(statex)then
          print*,' READING UNFORMATTED GRID FILE'
          open(unit=20,file='grid.X3D',form='unformatted')
          read(20)bsize
          read(20)nblock
          print*,bsize,nblock
          ni=bsize+3
          allocate(x(ni,ni,ni,nblock),y(ni,ni,ni,nblock)
     &            ,z(ni,ni,ni,nblock),attr(ni,ni,ni,nblock))
          attr=0;x=0.d0;y=0.d0;z=0.d0
          print*,' Reading Attributs : '
          call ReadBlockInteger(20,ni**3,nblock,attr)
          print*,' Reading x-coord : '
          call ReadBlockReal   (20,ni**3,nblock,x)
          print*,' Reading y-coord : '
          call ReadBlockReal   (20,ni**3,nblock,y)
          print*,' Reading z-coord : '
          call ReadBlockReal   (20,ni**3,nblock,z)
          print*,' Finished Reading : '
        else
          inquire(file='grid.x3d',exist=statex)
          if(.not.statex)then
            print*,' No grid file, stopping '
            stop
          endif
          print*,' READING FORMATTED GRID FILE'
          open(unit=20,file='grid.x3d')
          read(20,*)bsize,nblock
          ni=bsize+3
          allocate(x(ni,ni,ni,nblock),y(ni,ni,ni,nblock)
     &            ,z(ni,ni,ni,nblock),attr(ni,ni,ni,nblock))
          attr=0;x=0.d0;y=0.d0;z=0.d0
          print*,bsize,nblock
          do n=1,nblock
          do k=2,bsize+2
          do j=2,bsize+2
          do i=2,bsize+2
            read(20,*)attr(i,j,k,n)  
     &              ,x(i,j,k,n)
     &              ,y(i,j,k,n)
     &              ,z(i,j,k,n)
          enddo;enddo;enddo;enddo
        endif
      endif

      print*,' Give Reduction factor : 1,2,4,8 '
      read*,factor
      nic=(ni-3)/factor+3
      allocate(attrc(nic,nic,nic,nblock)
     &           ,xc(nic,nic,nic,nblock)
     &           ,yc(nic,nic,nic,nblock)
     &           ,zc(nic,nic,nic,nblock))
      xc=0.d0;yc=0.d0;zc=0.d0;attrc=0
      do n=1,nblock
      do k=2,nic-1;kk=(k-2)*factor+2
      do j=2,nic-1;jj=(j-2)*factor+2
      do i=2,nic-1;ii=(i-2)*factor+2
           xc(i,j,k,n)=   x(ii,jj,kk,n)
           yc(i,j,k,n)=   y(ii,jj,kk,n)
           zc(i,j,k,n)=   z(ii,jj,kk,n)
        attrc(i,j,k,n)=attr(ii,jj,kk,n)
      enddo;enddo;enddo
      enddo

      open(unit=20,file='coarse.x3dunf',form='unformatted')
      write(20)nic-3
      write(20)nblock
      call WriteBlockInteger(20,nic**3,nblock,attrc)
      call WriteBlockReal   (20,nic**3,nblock,xc  )
      call WriteBlockReal   (20,nic**3,nblock,yc  )
      call WriteBlockReal   (20,nic**3,nblock,zc  )
      close(20)

      stop
      end

      subroutine ReadBlockReal(unit,block,nblock,x)
      implicit none
      integer unit,nblock,block,i,n
      real(kind=8),dimension(block,nblock)::x
      real(kind=8),dimension(block)       ::xblock
        do n=1,nblock
          read(unit)xblock
          do i=1,block
            x(i,n)=xblock(i)
          end do
        end do
      end subroutine ReadBlockReal

      subroutine ReadBlockInteger(unit,block,nblock,x)
      implicit none
      integer unit,nblock,block,i,n
      integer,dimension(block,nblock)::x
      integer,dimension(block)       ::xblock
        do n=1,nblock
          read(unit)xblock
          do i=1,block
            x(i,n)=xblock(i)
          end do
        end do
      end subroutine ReadBlockInteger

      subroutine WriteBlockReal(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      real(kind=8),dimension(block,nblock)::x
      real(kind=8),dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine WriteBlockReal

      subroutine WriteBlockInteger(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      integer,dimension(block,nblock)::x
      integer,dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine WriteBlockInteger
