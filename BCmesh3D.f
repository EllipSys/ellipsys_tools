      program BCmesh2
      implicit none
      integer ni,bsize,nblock,i,j,k,n,factor
      real(kind=8),dimension(:,:,:,:),allocatable::x,y,z
      integer,dimension(:,:,:,:),allocatable::attr
      integer istart,jstart,kstart,io,jo,ko,ii,jj,kk
      integer dim,nbi,nbm
      integer yesno
      real(kind=8)::clim
      integer::aselect,aset
      integer,parameter::mword=40
      integer nrchar(mword),nrword
      character(len=256)InputLine
      character(len=40),dimension(mword)::words
      logical::lrecogn=.false.
      open(unit=10,file='grid.x3d')
      open(unit=11,file='grid.X3D')

c-------read mesh
      print*,' Reading mesh file : '
      read(10,*)bsize,nblock
      ni=bsize+3
      allocate(x(ni,ni,ni,nblock)
     &        ,y(ni,ni,ni,nblock)
     &        ,z(ni,ni,ni,nblock))
      allocate(attr(ni,ni,ni,nblock))
      do n=1,nblock
      do k=2,bsize+2
      do j=2,bsize+2
      do i=2,bsize+2
        read(10,*)attr(i,j,k,n),
     &               x(i,j,k,n),
     &               y(i,j,k,n),
     &               z(i,j,k,n)
      enddo;enddo;enddo;enddo
      close(10)


      print*,' Processing BC file '
      open(unit=10,file='bc.dat')
      do
      read(10,'(a256)',end=100)InputLine
      call cmdpart(InputLine,mword,nrchar,nrword,words)
C------------------------------------- Skip blank lines and comments ---
      if(nrword.le.1) then
        cycle
C---------------------------------- Skip rest of file if stop appears --
      elseif(words(1)(1:4).eq.'stop'.or.words(1)(1:4).eq.'STOP') then
      exit
c----------------------------- command InputLine interpreter -----------
      else
        lrecogn=.false.
        select case(words(1)(1:nrchar(1)))
C------------------------------ Keyword not recognized -----------------
        case default
        lrecogn=.false.
C------------------------------ make outer/top BC  horizontal ----------
        case('horizontal')
          if(nrword.ge.2)then
            if(words(2)(1:4).eq.'true')
     &        call horizontal(ni,nblock,bsize,attr,x,y,z)
          endif
C------------------------------ determine meshtype ---------------------
        case('x')
          if(nrword.ge.5)then
            read(words(3),*)clim
            read(words(4),*)aselect
            read(words(5),*)aset
            select case(words(2)(1:nrchar(2)))
              case('gt')
                call setBC(ni,nblock,bsize, 1,clim,aselect,aset,x,attr)
              case('lt')
                call setBC(ni,nblock,bsize,-1,clim,aselect,aset,x,attr)
            end select
          endif
        case('y')
          if(nrword.ge.5)then
            read(words(3),*)clim
            read(words(4),*)aselect
            read(words(5),*)aset
            select case(words(2)(1:nrchar(2)))
              case('gt')
                call setBC(ni,nblock,bsize, 1,clim,aselect,aset,y,attr)
              case('lt')
                call setBC(ni,nblock,bsize,-1,clim,aselect,aset,y,attr)
            end select
          endif
        case('z')
          if(nrword.ge.5)then
            read(words(3),*)clim
            read(words(4),*)aselect
            read(words(5),*)aset
            select case(words(2)(1:nrchar(2)))
              case('gt')
                call setBC(ni,nblock,bsize, 1,clim,aselect,aset,z,attr)
              case('lt')
                call setBC(ni,nblock,bsize,-1,clim,aselect,aset,z,attr)
            end select
          endif
        end select
      endif
      enddo

c     call setBC(ni,nblock,bsize, 1,maxx,102,401,x,attr)
c     call setBC(ni,nblock,bsize,-1,maxx,102,201,x,attr)
c     call setBC(ni,nblock,bsize, 1,maxy,101,101,y,attr)

  100 dim=bsize
      print*,'*******************************************************'
      print*,'size of multidomaine blocks         : ',dim
      print*,'want to change this yes = 1, no = 2   '
      read(*,*) yesno
      if(yesno.eq.1)then
        print*,'give new dimension  '
        read(*,*)dim
      endif
      nbi=(bsize)/dim
      nbm=nbi*nbi*nbi*(nblock)
      print*,'*******************************************************'
      print*,'size of multidomaine blocks         : ',dim
      print*,'number of block in xi direction     : ',nbi
      print*,'number of block in eta direction    : ',nbi
      print*,'number of block in zeta direction   : ',nbi
      print*,'total number of blocks              : ',nbm
      print*,'*******************************************************'

      print*,' Give reduction factor'
      read*,factor

      write(11,*) dim/factor,nbm
      do n=1,nblock
      do kk=1,nbi;kstart=(kk-1)*dim
      do jj=1,nbi;jstart=(jj-1)*dim
      do ii=1,nbi;istart=(ii-1)*dim
        do k=1,dim+1,factor;ko=kstart+k+1
        do j=1,dim+1,factor;jo=jstart+j+1
        do i=1,dim+1,factor;io=istart+i+1
            write(11,*)attr(io,jo,ko,n)
     &                   ,x(io,jo,ko,n)
     &                   ,y(io,jo,ko,n)
     &                   ,z(io,jo,ko,n)
        enddo;enddo;enddo
      enddo;enddo;enddo;enddo
      stop
      end

      subroutine setBC(ni,nblock,bsize,modus,coordval,attval,attset
     &                ,coord,attr)
      implicit none
      integer::ni,nblock,bsize,modus,attval,attset
      real(kind=8)::coordval
      real(kind=8),dimension(ni,ni,ni,nblock)::coord
      integer,dimension(ni,ni,ni,nblock)::attr

      call setBCface(ni,nblock,bsize,modus,coordval,attval,attset
     &                ,coord,attr)
c     call setBCcell(ni,nblock,bsize,modus,coordval,attval,attset
c    &                ,coord,attr)

      return
      end

      subroutine setBCface(ni,nblock,bsize,modus,coordval,attval,attset
     &                ,coord,attr)
C=======================================================================
c     routine for setting boundary conditions based on attribute and 
c     coordinate value
c     the routine checks all boundary faces of all blocks
c     if attr is equal to attval
c     and coord is larger than coordval
c     the attribute is changed to attset
c     if modus is -1 it checks for less than coordval instead
C=======================================================================
      implicit none
      integer::ni,nblock,bsize,modus,attval,attset
      integer::i,j,k,n,kmin,cells
      real(kind=8)::coordval
      real(kind=8),dimension(ni,ni,ni,nblock)::coord
      integer,dimension(ni,ni,ni,nblock)::attr

      do n=1,nblock
c---------west
        if(attr(2      ,bsize/2,bsize/2,n).eq.attval)then
          if(modus*coord(2,bsize/2,bsize/2,n).gt.modus*coordval)then
            attr(2,2:bsize+2,2:bsize+2,n)=attset
            print*,' block : ',n,' face : ',1,' attr : ',attset
          endif
        endif
c---------east
        if(attr(bsize+2,bsize/2,bsize/2,n).eq.attval)then
          if(modus*coord(bsize+2,bsize/2,bsize/2,n)
     &                            .gt.modus*coordval)then
            attr(bsize+2,2:bsize+2,2:bsize+2,n)=attset
            print*,' block : ',n,' face : ',2,' attr : ',attset
          endif
        endif
c---------south
        if(attr(bsize/2,2      ,bsize/2,n).eq.attval)then
          if(modus*coord(bsize/2,2,bsize/2,n)
     &                            .gt.modus*coordval)then
            attr(2:bsize+2,2,2:bsize+2,n)=attset
            print*,' block : ',n,' face : ',3,' attr : ',attset
          endif
        endif
c---------north
        if(attr(bsize/2,bsize+2,bsize/2,n).eq.attval)then
          if(modus*coord(bsize/2,bsize+2,bsize/2,n)
     &                            .gt.modus*coordval)then
            attr(2:bsize+2,bsize+2,2:bsize+2,n)=attset
            print*,' block : ',n,' face : ',4,' attr : ',attset
          endif
        endif
c---------bottom
        if(attr(bsize/2,bsize/2,2      ,n).eq.attval)then
          if(modus*coord(bsize/2,bsize/2,2,n)
     &                            .gt.modus*coordval)then
            attr(2:bsize+2,2:bsize+2,2,n)=attset
            print*,' block : ',n,' face : ',5,' attr : ',attset
          endif
        endif
c---------top
        if(attr(bsize/2,bsize/2,bsize+2,n).eq.attval)then
          if(modus*coord(bsize/2,bsize/2,bsize+2,n)
     &                            .gt.modus*coordval)then
            attr(2:bsize+2,2:bsize+2,bsize+2,n)=attset
            print*,' block : ',n,' face : ',6,' attr : ',attset
          endif
        endif
      enddo

      return
      end

      subroutine setBCcell(ni,nblock,bsize,modus,coordval,attval,attset
     &                ,coord,attr)
C=======================================================================
c     routine for setting boundary conditions based on attribute and 
c     coordinate value
c     the routine checks all boundary faces of all blocks
c     if attr is equal to attval
c     and coord is larger than coordval
c     the attribute is changed to attset
c     if modus is -1 it checks for less than coordval instead
C=======================================================================
      implicit none
      integer::ni,nblock,bsize,modus,attval,attset
      integer::i,j,k,n,kmin,cells,nset,bic,bjc,bkc
      integer::f,m,l,ncf,icf,jcf,kcf,b1,b2
      real(kind=8)::coordval
      real(kind=8),dimension(ni,ni,ni,nblock)::coord
      integer,dimension(ni,ni,ni,nblock)::attr

      print*,' BCse is called '
      b1=bsize+1;b2=bsize+2
      do n=1,nblock;do f=1,6
        nset=0
C----------------------------------------------------------------------- 
        select case(f)
         case(1,3,5);ncf= 2;
         case(2,4,6);ncf=b2;
        end select
C--------------------------------------- loop over face ----------------
        do m=2,b2; do l=2,b2
C----------------------------------------------------------------------- 
          select case(f)
            case(1:2)
              icf=ncf;jcf=l;kcf=m
              bic=ncf;bjc=b2/2;bkc=b2/2
            case(3:4)
              icf=m;jcf=ncf;kcf=l
              bic=b2/2;bjc=ncf;bkc=b2/2
            case(5:6)
              icf=l;jcf=m;kcf=ncf
              bic=b2/2;bjc=b2/2;bkc=ncf
          end select
          if(attr(icf,jcf,kcf,n).eq.attval.and.
     &       attr(bic,bjc,bkc,n).gt.100)then
            if(modus*coord(icf,jcf,kcf,n).gt.modus*coordval)then
              attr(icf,jcf,kcf,n)=attset
              nset=nset+1
c             print*,' block : ',n,' face : ',1,' attr : ',attset
            endif
          endif
        enddo;enddo
        if(nset.gt.0)write(*,101)nset,attset,f,n
      enddo;enddo

  101 format(i4,' attr = ',i4,' set on face ',i3,' block ',i6) 
      return
      end

      subroutine horizontal(ni,nblock,bsize,attr,x,y,z)
      implicit none
      integer::ni,nblock,bsize
      integer::i,j,k,n,kmin,cells
      real(kind=8),dimension(ni,ni,ni,nblock)::x,y,z
      real(kind=8),dimension(ni)::cellheight
      real(kind=8)top_cell,bot_cell,top_level,bot_level
     &            ,domain_height,factor
      integer,dimension(ni,ni,ni,nblock)::attr

      cells=0
      top_level=0.d0
      bot_level=0.d0
      top_cell=0.d0
      bot_cell=0.d0
      kmin=ni/2
      do n=1,nblock
        print*,' attr top : ',attr(bsize/2,bsize/2,bsize+2,n)
        if(attr(bsize/2,bsize/2,bsize+2,n).eq.102)then
          print*,' top block : ',n
          cells=cells+(bsize+1)*(bsize+1)
          do j=2,bsize+2
          do i=2,bsize+2
            top_cell=top_cell+z(i,j,bsize+2,n)-z(i,j,bsize+1,n)
            bot_cell=bot_cell+z(i,j,kmin+1,n)-z(i,j,kmin,n)
            top_level=top_level+z(i,j,bsize+2,n)
            bot_level=bot_level+z(i,j,kmin,n)
          enddo;enddo
        endif
      enddo
      top_cell=top_cell/cells
      bot_cell=bot_cell/cells
      top_level=top_level/cells
      bot_level=bot_level/cells
      print*,' bot_level, top_level : ',bot_level,top_level
      print*,' bot_cell, top_cell : ',bot_cell,top_cell

      domain_height=top_level-bot_level
      domain_height=top_level-bot_level
      call tanhdist(bot_cell,top_cell,domain_height
     &                 ,kmin,bsize+2,cellheight(kmin))
      cellheight=cellheight+bot_level

      do n=1,nblock
        if(attr(bsize/2,bsize/2,bsize+2,n).eq.102)then
          do k=kmin,bsize+2
c           factor=(k-kmin)/(bsize+2-kmin)
            factor=tanh((2.d0*(k-kmin)/(bsize+2-kmin))**4)
            do j=2,bsize+2
            do i=2,bsize+2
              z(i,j,k,n)=z(i,j,k,n)*(1-factor)+cellheight(k)*factor
            enddo;enddo
          enddo
        endif
      enddo

 
      return
      end

      subroutine tanhdist(delta1,delta2,len,i1,i2,fdist)
************************************************************************
*     TANDIST
*     program for computing the hyperbolic tangent distribution
*     delta1 : grid spacing at i=i1 (first cell)
*     delta2 : grid spacing at i=i2 (last  cell)
*     len    : lenght of curve
*     i1     : indices of first vertex
*     i2     : indices of last vertex
*     fdist  : computed distribution function
*
*
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      integer i1,i2,ni,i
      real(kind=8)::delta1,delta2,len
      real(kind=8)::delta,b,a
      real(kind=8)::fdist(i2-i1+1)
      real(kind=8)::transsinh,transtanh,ftmp
      external transsinh,transtanh

      if(i2.eq.i1)return

c------- Normalize delta1 and delta2 with lenght
      delta1=delta1/len
      delta2=delta2/len
c------- Compute number of points
      ni=i2-i1

c------- Assure that B > 1
      if(delta1.le.0.d0.and.1/delta2.lt.ni)then
        delta1=1/(ni**2*delta2*1.02)
      elseif(delta2.le.0d0.and.1/delta1.lt.ni)then
        delta2=1/(ni**2*delta1*1.02)
      endif

c------- Spacing at both end specified
      if(delta1.gt.0.d0.and.delta2.gt.0.d0)then
        a=sqrt(delta2)/sqrt(delta1)
        b=1.d0/(ni*sqrt(delta1*delta2))
        if(b.ge.1.d0)then
          delta=transsinh(b)
          do i=1,ni+1
            ftmp=.5*(1+tanh(delta*(real(i-1)/real(ni)-0.5))
     &          /tanh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        else
          delta=transtanh(b)
          do i=1,ni+1
            ftmp=.5*(1+sinh(delta*(real(i-1)/real(ni)-0.5))
     &          /sinh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        endif
c------- Spacing only specified at xi=1
      else if(delta1.gt.0.d0)then
        b=1.d0/(ni*delta1)
        delta=transsinh(b)
        do i=1,ni+1
          fdist(i)=1.d0+tanh(.5d0*delta*(real(i-1)/real(ni)-1.d0))
     &            /tanh(.5d0*delta)
        enddo
c------- Spacing only specified at xi=ni
      else if(delta2.gt.0.d0)then
        b=1.d0/(ni*delta2)
        delta=transsinh(b)
        do i=1,ni+1
          fdist(i)=tanh(.5*delta*real(i-1)/real(ni))/tanh(.5*delta)
        enddo
c------- Error no spacing is given
      else
        print*,' Error from tandist, no cell hight is given '
      endif
c------- Compute len*fdist
      do i=1,ni+1
        fdist(i)=fdist(i)*len
      enddo
      return
      end

      subroutine sinhdist(delta1,delta2,len,i1,i2,fdist)
************************************************************************
*     SINHDIST
*     program for computing the hyperbolic sine distribution
*     delta1 : grid spacing at i=i1 (first cell)
*     delta2 : grid spacing at i=i2 (last  cell)
*     len    : lenght of curve
*     i1     : indices of first vertex
*     i2     : indices of last vertex
*     fdist  : compute distribution function
*
*
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      real(kind=8)::delta1,delta2,len
      real(kind=8)::delta,b,a
      real(kind=8)::fdist(*)
      real(kind=8)::transsinh,transtanh,ftmp
      external transsinh,transtanh
      integer i1,i2,ni,i

      if(i2.eq.i1)return

c------- Normalize delta1 and delta2 with lenght
      delta1=delta1/len
      delta2=delta2/len
c------- Compute number of points
      ni=i2-i1

c------- Assure that B > 1
      if(delta1.le.0.d0.and.1/delta2.lt.ni)then
        delta1=1/(ni**2*delta2*1.02)
      elseif(delta2.le.0d0.and.1/delta1.lt.ni)then
        delta2=1/(ni**2*delta1*1.02)
      endif

c------- Spacing at both end specified
      if(delta1.gt.0.d0.and.delta2.gt.0.d0)then
        a=sqrt(delta2)/sqrt(delta1)
        b=1.d0/(ni*sqrt(delta1*delta2))
        if(b.ge.1.d0)then
          delta=transsinh(b)
          do i=1,ni+1
            ftmp=.5*(1+tanh(delta*(real(i-1)/real(ni)-0.5))
     &          /tanh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        else
          delta=transtanh(b)
          do i=1,ni+1
            ftmp=.5*(1+sinh(delta*(real(i-1)/real(ni)-0.5))
     &          /sinh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        endif
c------- Spacing only specified at xi=1
      else if(delta1.gt.0.d0)then
        b=1.d0/(ni*delta1)
        delta=transsinh(b)
        do i=1,ni+1
          fdist(i)=sinh(delta*real(i-1)/real(ni))/sinh(delta)
        enddo
c------- Spacing only specified at xi=ni
      else if(delta2.gt.0.d0)then
        b=1.d0/(ni*delta2)
        delta=transsinh(b)
        do i=1,ni+1
          fdist(i)=1-sinh(delta*(1-real(i-1)/real(ni)))/sinh(delta)
        enddo
c------- Error no spacing is given
      else
        print*,' Error from tandist, no cell hight is given '
      endif
c------- Compute len*fdist
      do i=1,ni+1
        fdist(i)=fdist(i)*len
      enddo
      return
      end

      double precision function transsinh(b)
************************************************************************
*     routine for solving the transendental equation
*
*     b=sinh(delta)/delta
*
*     using NewtonRaphson
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      real(kind=8)::delta,b
      real(kind=8)::delta_old,f,df,rlim
      parameter(rlim=1.d-9)
      integer n

c------- Preforme Newton iteration to obtain delta
      delta=1.d0
      delta_old=delta
      do n=1,25
c--------- Compute f(delta)
c        f =b-sinh(delta)/delta
c        f =b*delta-sinh(delta)
         f =delta/sinh(delta)-1/b
c--------- Compute d/d(delta)(f(delta))
c        df=(sinh(delta)-delta*cosh(delta))/delta**2
c        df=b-cosh(delta)
         df=(sinh(delta)-delta*cosh(delta))/sinh(delta)**2
c--------- update delta
        delta=delta-f/df
c       write(*,'(4e16.4)')f,df,f/df,delta
c--------- check convergence criterium
c       if(abs((delta-delta_old)/delta_old).lt.rlim)goto 200
c--------- update old value
c       delta_old=delta
      enddo
      transsinh=delta
      return
      end

      double precision function transtanh(b)
************************************************************************
*     routine for solving the transendental equation
*
*     b=tanh(delta/2)/(delta/2)
*
*     using NewtonRaphson
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      real(kind=8)::delta,b
      real(kind=8)::delta_old,f,df,rlim
      parameter(rlim=1.d-6)
      integer n

      if(b.gt.1.0d0)then
        print*,' Error form sinhdist: Can not be gridded '
c       stop
      endif
c------- Preforme Newton iteration to obtain delta
      delta=1.d0
      delta_old=delta
      do n=1,25
c--------- Compute f(delta)
        f =delta/(tanh(.5*delta)+1.d-30)-2/B
c--------- Compute d/d(delta)(f(delta))
        df=(tanh(.5*delta)-delta*(1-tanh(.5*delta)**2))
     &    /(tanh(.5*delta)**2+1.d-30)
c--------- update delta
        delta=delta-f/df
c       write(*,'(i3,1x,3e16.4)')n,delta,f,df
c--------- check convergence criterium
c       if(abs((delta-delta_old)/delta_old).lt.rlim)goto 200
c--------- update old value
c       delta_old=delta
      enddo
      transtanh=delta
      return
      end

      subroutine cmdpart(InputLine,mword,nrchar,nrword,words)
c-----------------------------------------------------------------------
c     routine parting command InputLine into words
c----------------------------------------------------------------------
c
c     Author: Jess A. Michelsen and Niels N. Soerensen
c     last revision :march 3, 1998
c-----------------------------------------------------------------------
      implicit none
      integer mword,nrchar(mword),nrword,i
      integer,parameter::mchar=256
      character(len=40),dimension(mword):: words
      character(len=40)::blank
      character(len=256) InputLine
      integer CurrentPosition,RelativeBlankPosition,BlankPosition
C--------------------------------------------- Reset word counter ------
      nrword=0
      CurrentPosition=1
C--------------------------------------------- Skip if a comment line --
      if(index(InputLine(1:mchar),'#').gt.0) return
C---------------------------------------- find distance to next blank --
   20 RelativeBlankPosition=index(InputLine(CurrentPosition:mchar),' ')
      if(CurrentPosition.eq.mchar.or.RelativeBlankPosition.eq.0) return
C---------------------------------------- found the end of a word ------
      if(RelativeBlankPosition.gt.1.and.RelativeBlankPosition.le.40)then
        BlankPosition=CurrentPosition-1+RelativeBlankPosition
        nrword=nrword+1
        if(nrword.gt.mword)return
        words(nrword)=InputLine(CurrentPosition:BlankPosition)
        nrchar(nrword)=RelativeBlankPosition-1
      endif
c-----------------------------------------------------------------------
      CurrentPosition=CurrentPosition+RelativeBlankPosition
      goto 20
c-----------------------------------------------------------------------
      end
