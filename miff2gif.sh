#!/bin/bash
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#   miff2gif
#   created by Frederik Zahle
#   25.03.2009
#   You are free to copy or modify whatever you like
#   in this script.
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#   input animation time step (real time in seconds)
dt=0.0221403
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#   output gif animation resolution
res_gif_x=800
res_gif_y=600
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#   output gif frame delay in 1/100th of a second (default is 5)"
delay=5
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#   top left title
title="Nordtank 500 - Vorticity Magnitude"
title1="Computed using EllipSys3D"
txt_colour="black"
addbox="false"
#addbox="true"
#boxcolour="#FFFFFF" #white
boxcolour="#FFFFFF95" #white transparent
box_y1=75 #box height in pixels
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++




#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
nfile=$( find *.miff | wc -l )
if [[ $nfile -gt 1 ]]; then
    echo ""
    echo "Error - multiple .miff files found in \
current directory - exiting."
    echo ""
exit
fi
input_filename=$(find *.miff)
output_filename=${input_filename%.miff}.gif
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
identify -format " %w \n" $input_filename > width
identify -format " %h \n" $input_filename > height 
identify -format " %n \n" $input_filename > nframe 
res_x=$( cat width  |head -1|awk '{print $1}' )
res_y=$( cat height |head -1|awk '{print $1}' )
nframes=$( cat nframe |head -1|awk '{print $1}' )
rm width height nframe
box_y1=$(( $res_y - $box_y1 )) 
if [[ $res_x -lt $res_gif_x ]]; then
     echo ""
     echo "Requested x-resolution is greater than miff x-resolution"
     echo "Using miff resolution" $res_x
     echo ""
     res_gif_x=$res_x
fi
if [[ $res_y -lt $res_gif_y ]]; then
     echo ""
     echo "Requested y-resolution is greater than miff x-resolution"
     echo "Using miff y-resolution" $res_y
     echo ""
     res_gif_y=$res_y
fi
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++

echo ""
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "You have specified the following input parameters:"
echo "Input filename:   " $input_filename
echo "Output filename:  " $output_filename
echo "Input  Resolution:" $res_x "x" $res_y
echo "Output Resolution:" $res_gif_x "x" $res_gif_y
echo "Time step:        " $dt
echo "Number of frames: " $nframes
echo "Animation delay:  " $delay
echo "Title line 1:     " $title
echo "Title line 2:     " $title1
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++"
echo ""

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#   the miff file is split into frames
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
width=30
echo "Splitting miff file ... "
tput sc
tt=0
if [[ $tt -eq 0 ]]; then
convert +adjoin $input_filename split%04d.gif | (
    while [[ $frame -lt $nframes ]]
    do
        frame=$(find  split*.gif 2>/dev/null| wc -l)
        percent=$(( 100 * $frame / $nframes ))
        bar=$(( $width * $frame / $nframes ))
        if [[ $frame -gt $framem1 ]]; then 
        tput el1
        tput rc
        printf "Extracting frame %3d  [" $frame
        for i in $(seq 1 $bar ); do printf "=" 1>&2; done
        for i in $(seq $bar $(( $width-1 ))); do printf " " 1>&2; done
        percent=$(( 100 * $frame / $nframes ))
        printf "] %3d%%" $percent 1>&2
#       printf "%3d%% [" $percent 1>&2
#       bar=$(( $width * $frame / $nframes ))
#       for i in $(seq 1 $bar ); do printf "=" 1>&2; done
#       for i in $(seq $bar $(( $width-1 ))); do printf " " 1>&2; done
        fi
        framem1=$frame
    done
)
fi
#sleep 1
echo "  Done!"
echo ""
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
time=$( printf "%7.3f\n" 0 )
txt_time_x=$(( $res_x - 120 ))
txt_time_y=20
txt_risoe_x=0
txt_risoe_y=$(expr $res_y - 60)
txt_dtu_x=$(expr $res_x - 260)
txt_dtu_y=$(expr $res_y - 60)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#progress bar stuff....
tput sc
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#   logos are added to each frame
echo "Adding titles and logoes ..."
tt=0
if [[ $tt -eq 0 ]]; then
for file in split*.gif
do
	frame=$(expr $frame + 1 )
	time1="Time = $time s"
        if [[ $addbox == "true" ]]; then

	 convert $file -font Helvetica-Bold -fill $txt_colour -pointsize 16 \
 	-delay $delay \
 	-draw "text 10,20 '$title'" \
 	-draw "text 10,40 '$title1'" \
 	-draw "text "$txt_time_x","$txt_time_y" '$time1'" \
        -fill "$boxcolour" -draw "rectangle 0,"$box_y1" "$res_x","$res_y"" \
 	-draw "image Over "$txt_risoe_x","$txt_risoe_y" 0,0 /home/frza/bin/risoelogo.gif" \
 	-draw "image Over "$txt_dtu_x","$txt_dtu_y" 0,0 /home/frza/bin/dtulogo.gif" \
 	-border 2x2 \
 	tmp${file#split} 

        else

	 convert $file -font Helvetica-Bold -fill $txt_colour -pointsize 16 \
 	-delay $delay \
 	-draw "text 10,20 '$title'" \
 	-draw "text 10,40 '$title1'" \
 	-draw "text "$txt_time_x","$txt_time_y" '$time1'" \
 	-draw "image Over "$txt_risoe_x","$txt_risoe_y" 0,0 risoelogo.gif" \
 	-draw "image Over "$txt_dtu_x","$txt_dtu_y" 0,0 dtulogo.gif" \
 	-border 2x2 \
 	tmp${file#split} 

        fi

 	time=$(echo $time  | awk '{printf "%7.3f\n",$time+'$dt' }')
#++++++ progress bar +++++++++++++++++++++++++++++++++++++++++++++++++++
        tput el1
        tput rc
        printf "Creating frame %3d    [" $frame
        bar=$(( $width * $frame / $nframes ))
        for i in $(seq 1 $bar ); do printf "=" 1>&2; done
        for i in $(seq $bar $(( $width-1 ))); do printf " " 1>&2; done
        percent=$(( 100 * $frame / $nframes ))
        printf "] %3d%%" $percent 1>&2
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
done
fi
echo "  Done!"
echo ""
nframe=$(($nframes - 5))
totsize=$( stat -c %s tmp0001.gif)
totsize=$(( $totsize * $nframe ))
echo -n "creating gif animation ..."
convert -quality 95 -antialias -size "$res_gif_x"x"$res_gif_y" \
-delay $delay tmp00* $output_filename # | (
#   while [[ $tmpsize -lt $totsize ]]
#   do
#       tmpsize=$( stat -c %s $output_filename 2>/dev/null )
#       if [[ $tmpsize -eq 0 ]]; then
#       tmpsize=0
#       fi
#       percent=$(( 100 * $tmpsize / $totsize ))
#       bar=$(( $width * $tmpsize / $totsize ))
#       if [[ $tmpsize -gt $tmpsizem1 ]]; then
#       tput el1
#       tput rc
#       printf "File size %3d  [" $tmpsize
#       for i in $(seq 1 $bar ); do printf "=" 1>&2; done
#       for i in $(seq $bar $(( $width-1 ))); do printf " " 1>&2; done
#       percent=$(( 100 * $tmpsize / $totsize ))
#       printf "] %3d%%" $percent 1>&2
#       tmpsizem1=$tmpsize+1048576
#       fi
#       tmpsizem1=$tmpsize
#    done
#)
echo "  Done!"
echo ""
echo -n "cleaning up ..."
#rm split*.gif 2>/dev/null
echo "Done!"
