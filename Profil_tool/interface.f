      subroutine Interface(val)
c-----------------------------------------------------------------------
c
c-----------------------------------------------------------------------
      implicit none
      integer::val
    
      if(val.eq.1)then
        call InputFlap
        call InputAngle
        call InputSmooth
        call InputTranslate
        call InputMoveCut
        call InputOpenTrailingEdge
        call InputScale
        call InputRotation
        call InputReverse
      else
        call ComputeFlap
        call ComputeAngle
        call ComputeSmooth
        call ComputeTranslate
        call ComputeMoveCut
        call ComputeOpenTrailingEdge
        call ComputeScale
        call ComputeRotation
        call ComputeReverse
      endif
      return
      end
