c***********************************************************************
c     subroutine computes the geometrical angle of incidence 
c     of the airfoil
c
c***********************************************************************
      module anglemodule
      logical::langle=.false.
      end module

      subroutine InputAngle
C=======================================================================
      use params
      use inpwords
      use anglemodule
      implicit none
      integer::yesno=2

      if(linpexist)then
        select case(words(1)(1:nrchar(1)))
        case('angle')
          langle=.true.
        end select
      else
        print*,' Compute geometrical properties of airfoil: yes=1/no=2 '
        read*,yesno
        if(yesno.eq.1)then
          langle=.true.
        endif
      endif
      return
C=======================================================================
      end
      
      subroutine ComputeAngle
C=======================================================================
      use params
      use vertices
      use anglemodule
      implicit none
      real(kind=8)::alpha,pi,lenghtmax,lenght,ymin,ymax
      real(kind=8)::factor,maxthickness,thickness,yval,angle
      real(kind=8),dimension(ni)::xt,yt
      real(kind=8),dimension(2)::n1,n2,a
      integer::i,j,ilenght

      if(.not.langle)return
c-----pi
      pi=4.d0*atan(1.d0)

c-----find leading edge of profile
      lenghtmax=1.d-6
      do i=1,ni
        lenght=sqrt((x(i)-x(1))**2+(y(i)-y(1))**2)
        if(lenght.gt.lenghtmax)then
          lenghtmax=lenght
          ilenght=i
        endif
      enddo

c-----calculate coord angle
      alpha=atan((y(ilenght)-y(1))/(x(ilenght)-x(1)))*180.d0/pi
      open(unit=17,file='chord.dat')
      write(17,*)x(1),y(1)
      write(17,*)x(ilenght),y(ilenght)
      close(17)
      print*,' Angle positive nose up ',-alpha

c-----rotate airfoil in order to compute thickness
      angle=alpha/180.d0*pi
      n1(1)= cos(angle)
      n1(2)= sin(angle)
      n2(1)=-sin(angle)
      n2(2)= cos(angle)

      do i=1,ni
        a(1)=x(i)
        a(2)=y(i)
        xt(i)=n1(1)*a(1)+n1(2)*a(2)
        yt(i)=n2(1)*a(1)+n2(2)*a(2)
      enddo

c------determine approximate thickness of airfoil
      maxthickness=0.d0
      open(unit=11,file='thickness.dat')
      do i=2,ilenght
        do j=ilenght,ni
          if(xt(j  ).le.xt(i).and.
     &      xt(j+1).ge.xt(i))then
            factor=(xt(i)-xt(j))/(xt(j+1)-xt(j))
            yval=factor*yt(j+1)+(1-factor)*yt(j)
            thickness=yval-yt(i)
            if(thickness.gt.maxthickness)maxthickness=thickness
            write(11,*)xt(i),thickness,yval,yt(i)
          endif
        enddo
      enddo

      print*,' Chord     : ',lenghtmax
      print*,' Thickness : ',maxthickness/lenghtmax

C=======================================================================
      end
c***********************************************************************
c
c***********************************************************************
      module flapmodule
      real(kind=8)::xrot,angle
      logical::lflap=.false.
      end module

      subroutine Inputflap
C=======================================================================
      use params
      use inpwords
      use flapmodule
      implicit none
      integer::yesno=2

      if(linpexist)then
        select case(words(1)(1:nrchar(1)))
        case('flap')
          read(words(2),*)xrot
          read(words(3),*)angle
          lflap=.true.
        end select
      else
        print*,' Generate flap : yes=1/no=2 '
        read*,yesno
        if(yesno.eq.1)then
          lflap=.true.
          print*,' Give chordwise position : '
          read*,xrot
          print*,' Give flap deflection : '
          read*,angle
        endif
      endif
      return
C=======================================================================
      end
      
      subroutine Computeflap
C=======================================================================
      use params
      use vertices
      use flapmodule
      implicit none
      real(kind=8)::yrot,pi
      real(kind=8)::a1,a2,b1,b2,xcut,ycut,factor
      real(kind=8),dimension(2)::n1,n2,a
      real(kind=8),dimension(ni)::xt,yt,x0,y0
      integer ni2,i,j,irot,icut,jcut

      if(.not.lflap)return
c-----determine y-rotation point
      do i=ni,2,-1
        if(xrot.le.x(i).and.xrot.ge.x(i-1))then
          factor=(x(i)-xrot)/(x(i)-x(i-1))
          yrot=factor*y(i-1)+(1-factor)*y(i)
          irot=i
          print*,yrot,i
        endif
      enddo

      pi=4.d0*atan(1.d0)
c-----turning angle
      angle=angle/180.d0*pi
      n1(1)= cos(angle)
      n1(2)= sin(angle)
      n2(1)=-sin(angle)
      n2(2)= cos(angle)
      do i=1,ni
        a(1)=x(i)-xrot
        a(2)=y(i)-yrot
        xt(i)=n1(1)*a(1)+n1(2)*a(2)+xrot
        yt(i)=n2(1)*a(1)+n2(2)*a(2)+yrot
      enddo

      do j=2,ni
        a2=(yt(j)-yt(j-1))/(xt(j)-xt(j-1))
        b2=yt(j)-a2*xt(j)
        do i=2,ni/2
          a1=(y(i)-y(i-1))/(x(i)-x(i-1))
          b1=y(i)-a1*x(i)
          xcut=(b2-b1)/(a1-a2)
          ycut=a1*xcut+b1
          if((xcut- x(i-1))*(xcut-x(i)).le.0.d0.and.
     &       (xcut-xt(j-1))*(xcut-xt(j)).le.0.d0)then
               print*,' cut is found ',i,j,xcut,ycut
               icut=i;jcut=j
            goto 100
          endif
        enddo
      enddo

 
  100 ni2=ni+jcut-icut+2
      
      print*,ni
      x0=x;y0=y
      deallocate(x,y);allocate(x(ni2),y(ni2))
      x=0.d0;y=0.d0
      j=0
      do i=1,jcut-1
        j=j+1
        x(j)=xt(i)
        y(j)=yt(i)
      enddo
      j=j+1
      print*,'a ',j
      x(j)=xcut;y(j)=ycut
      do i=icut,irot-1
        j=j+1
        x(j)=x0(i)
        y(j)=y0(i)
      enddo
      j=j+1
      print*,'b ',j
      x(j)=xrot;y(j)=yrot
      do i=irot,ni
        j=j+1
        x(j)=xt(i)
        y(j)=yt(i)
      enddo
      ni=ni2
      print*,'c ',j
      return
C=======================================================================
      end
c***********************************************************************
c     move cut the specified angle clockwise
c***********************************************************************
      module movecutmodule
      logical::lmovecut=.false.
      real(kind=8)::alfa=0.d0
      end module

      subroutine InputMoveCut
C=======================================================================
      use params
      use inpwords
      use movecutmodule
      implicit none
      integer::yesno=2

      if(linpexist)then
        select case(words(1)(1:nrchar(1)))
        case('movecut')
          lmovecut=.true.
          if(nrword.ge.2)read(words(2),*)alfa
        end select
      else
        print*,' Move airfoil cut: yes=1/no=2 '
        read*,yesno
        if(yesno.eq.1)then
          lmovecut=.true.
          print*,' Give anti clock wise angle to move cut : '
          read*,alfa
        endif
      endif
      return
C=======================================================================
      end
      
      subroutine ComputeMoveCut
C=======================================================================
      use params
      use vertices
      use movecutmodule
      implicit none
      real(kind=8),dimension(ni)::xt,yt
      real(kind=8)::gamma,pi,c1,c2,rx,ry,xc,yc
      integer::i,imove

      if(.not.lmovecut)return
c-------pi
      pi=4*atan(1.d0)

c-------compute center of gravity
      xc=0.d0
      yc=0.d0
      do i=1,ni
        xc=xc+x(i)
        yc=yc+y(i)
      enddo
      xc=xc/real(ni)
      yc=yc/real(ni)

c-------compute angle
      gamma=pi/180*alfa+atan((y(1)-yc)/(x(1)-xc+1.d-30))

c-------compute coordinats of direction vector
      rx=cos(gamma)
      ry=sin(gamma)

c-------find intersection with profile definition
      do i=1,ni-1
        c1=rx*y(i  )-ry*x(i  )
        c2=rx*y(i+1)-ry*x(i+1)
        if(c1.gt.0.d0.and.c2.lt.0.d0)then
          imove=i
          exit
        endif
      enddo

      do i=imove,ni
        xt(i-imove+1)=x(i)
        yt(i-imove+1)=y(i)
      enddo
      do i=2,imove
        xt(ni-imove+i)=x(i)
        yt(ni-imove+i)=y(i)
      enddo

      do i=1,ni
        x(i)=xt(i)
        y(i)=yt(i)
      enddo
      return
C=======================================================================
      end
c***********************************************************************
c
c***********************************************************************
      module OpenTrailingEdgemodule
      logical::lOpenTrailingEdge=.false.
      real(kind=8)::yopen=0.d0
      integer::nsmooth=0
      end module

      subroutine InputOpenTrailingEdge
C=======================================================================
      use params
      use inpwords
      use OpenTrailingEdgemodule
      implicit none
      integer::yesno=2

      if(linpexist)then
        select case(words(1)(1:nrchar(1)))
        case('open-trailing-edge')
          if(nrword.ge.2)read(words(2),*)yopen
          if(nrword.ge.3)read(words(3),*)nsmooth
          lOpenTrailingEdge=.true.
        end select
      else
        print*,'  Open Trailing Edge yes=1/no=2 : '
        read*,yesno
        if(yesno.eq.1)then
          lOpenTrailingEdge=.true.
          print*,' Give airfoil opening : '
          read*,yopen
          print*,' Give number of smoothing passes : '
          read*,nsmooth
        endif
      endif
      return
C=======================================================================
      end
      
      subroutine ComputeOpenTrailingEdge
C=======================================================================
      use params
      use vertices
      use OpenTrailingEdgemodule
      implicit none
      real(kind=8),dimension(4*ni)::xn,yn
      real(kind=8)::lenght,delta,length,x0,y0,slope,xi
      integer i
      integer nextra,inum,i1num,i2num,i1,i2,j,nstart

      if(.not.lOpenTrailingEdge)return

      nstart=1

      print*,'yopen : ',yopen
c-----open lower side
      do i=1,int(ni*.25)+nstart
          xi=abs((x(i)-x(int(ni*.25)+nstart))
     &          /(x(1)-x(int(ni*.25)+nstart)))
          y(i)=y(i)-.5*yopen*xi
      enddo
c-----open lower side
      do i=int(.75*ni)+nstart,ni
          xi=abs((x(i)-x(int(.75*ni)+nstart))
     &          /(x(ni)-x(int(.75*ni)+nstart)))
          y(i)=y(i)+.5*yopen*xi
      enddo
c-------close trailing edge

c-----compute hight of trailing edge
      delta=y(ni)-y(1)
      print*,' Trailing edge opening : ',delta

c-----compute new coordinats
      nextra=24
      inum=0
      i1num=0
      i2num=0

c------- lower vertical piece
      do i=1,nextra
        inum=inum+1
        i1num=i1num+1
        xn(inum)=.5*(x(1)+x(ni))
     &          -.5*(x(ni)-x(1))*real(i-1)/real(nextra-1)
        yn(inum)=y(1)+.5*delta-.5*delta*(i-1.)/(nextra-1.)
      enddo

c------- lower horizontal piece
      length = 0.
      do i=1,ni
         length = length
     &              + sqrt((x(i+1)-x(i))**2+(y(i+1)-y(i))**2)
         if(length.gt.delta/2.)exit
      enddo

      i1 = i+1

      do  j=1,i1-1
        x0=xn(inum)
        y0=yn(inum)
        slope = (y(j+1)-y0)/(x(j+1)-x0)
        do i=1,nextra/(i1-1)
          inum=inum+1
          i1num=i1num+1
          xn(inum)=x0 + (i-1.)*(x(j+1)-x0)/(nextra/(i1-1)-1.)
          yn(inum)=y0 + slope *(xn(inum)-x0)
        enddo
      enddo

c------- upper horizontal piece
      length = 0.
      do i=ni,1,-1
         length = length
     &              + sqrt((x(i-1)-x(i))**2 + (y(i-1)-y(i))**2)
         if(length.gt.delta/2.)exit
      enddo

      i2 = i-1

      do i=i1+1,i2
        inum=inum+1
        xn(inum)=x(i)
        yn(inum)=y(i)
      enddo

      do j=i2,ni-1
        x0=xn(inum)
        y0=yn(inum)
        slope = (y(j+1)-y0)/(x(j+1)-x0)
        do i=1,nextra/(ni-i2)
          inum=inum+1
          i2num=i2num+1
          xn(inum)=x0 + (i-1.)*(x(j+1)-x0)/(nextra/(ni-i2)-1.)
          yn(inum)=y0 + slope *(xn(inum)-x0)
        enddo
      enddo

c------- lower vertical piece
      do  i=1,nextra
        inum=inum+1
        i2num=i2num+1
        xn(inum)=x(ni)
     &          -.5*(x(ni)-x(1))*real(i-1)/real(nextra-1)
        yn(inum)=y(ni)-.5*delta*(i-1.)/(nextra-1.)
      enddo

c     print*,'Enter number of smoothing cycles:'
c     read*,nsmooth

      do j=1,nsmooth
        do i = 2,i1num-1
          xn(i)=.25*(xn(i-1)+2.*xn(i)+xn(i+1))
          yn(i)=.25*(yn(i-1)+2.*yn(i)+yn(i+1))
        enddo
        do i = inum-1,inum-i2num+1,-1
         xn(i)=.25*(xn(i-1)+2.*xn(i)+xn(i+1))
         yn(i)=.25*(yn(i-1)+2.*yn(i)+yn(i+1))
        enddo
      enddo

      deallocate(x,y)
      ni=inum
      allocate(x(ni),y(ni))
      do i=1,ni
        x(i)=xn(i)
        y(i)=yn(i)
      enddo
      return
C=======================================================================
      end
c***********************************************************************
c
c***********************************************************************
      module reversemodule
      logical::lreverse=.false.
      end module

      subroutine InputReverse
C=======================================================================
      use params
      use inpwords
      use reversemodule
      implicit none
      integer::yesno=2

      if(linpexist)then
        select case(words(1)(1:nrchar(1)))
        case('reverse')
          lreverse=.true.
        end select
      else
        print*,' Reverse direction of airfoil coordinats: yes=1/no=2 '
        read*,yesno
        if(yesno.eq.1)then
          lreverse=.true.
        endif
      endif
      return
C=======================================================================
      end
      
      subroutine ComputeReverse
C=======================================================================
      use params
      use vertices
      use reversemodule
      implicit none
      real(kind=8),dimension(ni)::xt,yt
      integer::i

      if(.not.lreverse)return
      do i=1,ni
        xt(i)=x(ni+1-i)
        yt(i)=y(ni+1-i)
      enddo
      do i=1,ni
        x(i)=xt(i)
        y(i)=yt(i)
      enddo
      return
C=======================================================================
      end
C***********************************************************************
c     subroutine rotates the profile data anti clock wise
c     through angle alphaturn
C
C***********************************************************************
      module rotationmodule
      real(kind=8)::angle,xrot,yrot
      logical lrotate
      end module

      subroutine InputRotation
C=======================================================================
      use params
      use inpwords
      use rotationmodule
      implicit none
      integer::yesno=2

      if(linpexist)then
        select case(words(1)(1:nrchar(1)))
        case('rotate')
          lrotate=.true.
          read(words(2),*)angle
          read(words(3),*)xrot
          read(words(4),*)yrot
          print*,' rotate : ',lrotate,angle,xrot,yrot
        end select
      else
        print*,' Rotate airfoil: yes=1/no=2 ' 
        read*,yesno
        if(yesno.eq.1)then
          lrotate=.true.
          print*,' Give rotation centre (x_rot,y_rot) :'
          read*,xrot,yrot
          print*,' Give anti clock wise angle : '
          read*,angle
        endif
      endif
      return
C=======================================================================
      end
      
      subroutine ComputeRotation
C=======================================================================
      use params
      use vertices
      use rotationmodule
      implicit none
      real(kind=8)::pi,xtemp,ytemp
      real(kind=8),dimension(2)::n1,n2,a
      integer i

      if(.not.lrotate)return
      pi=4.d0*atan(1.d0)
c-----turning angle
      angle=angle/180.d0*pi
      n1(1)= cos(angle)
      n1(2)= sin(angle)
      n2(1)=-sin(angle)
      n2(2)= cos(angle)

      do i=1,ni
        a(1)=x(i)-xrot
        a(2)=y(i)-yrot
        x(i)=n1(1)*a(1)+n1(2)*a(2)+xrot
        y(i)=n2(1)*a(1)+n2(2)*a(2)+yrot
      enddo
      return
C=======================================================================
      end
c***********************************************************************
c     model scaling the airfoil geometry according to xscale,yscale
c     input
c     scale xscale yscale
c***********************************************************************
      module scalemodule
      logical::lscale=.false.
      real(kind=8)::xscale=1.d0,yscale=1.d0
      end module

      subroutine Inputscale
C=======================================================================
      use params
      use inpwords
      use scalemodule
      implicit none
      integer::yesno=2

      if(linpexist)then
        select case(words(1)(1:nrchar(1)))
        case('scale')
          read(words(2),*)xscale
          read(words(3),*)yscale
          lscale=.true.
        end select
      else
        print*,' Scale airfoil: yes=1/no=2 '
        read*,yesno
        if(yesno.eq.1)then
          lscale=.true.
          print*,' Give scaling factor for x-direction : '
          read*,xscale
          print*,' Give scaling factor for y-direction : '
          read*,yscale
        endif
      endif
      return
C=======================================================================
      end
      
      subroutine Computescale
C=======================================================================
      use params
      use vertices
      use scalemodule
      implicit none
      integer i

      if(.not.lscale)return
      do i=1,ni
        x(i)=x(i)*xscale
        y(i)=y(i)*xscale*yscale
      enddo
      return
C=======================================================================
      end
c***********************************************************************
c
c***********************************************************************
      module smoothmodule
      logical::lsmooth=.false.
      integer::nsmooth=0
      real(kind=8)::factor=0.05
      end module

      subroutine InputSmooth
C=======================================================================
      use params
      use inpwords
      use smoothmodule
      implicit none
      integer::yesno=2

      if(linpexist)then
        select case(words(1)(1:nrchar(1)))
        case('smooth')
          read(words(2),*)nsmooth
          if(nrword.ge.3)read(words(3),*)factor
          lsmooth=.true.
        end select
      else
        print*,' Smooth airfoil: yes=1/no=2 '
        read*,yesno
        if(yesno.eq.1)then
          lsmooth=.true.
          print*,' Give number of smoothing sweeps : '
          read*,nsmooth
          print*,' Give smoothing factor [0:1] : '
          read*,factor
        endif
      endif
      return
C=======================================================================
      end
      
      subroutine ComputeSmooth
C=======================================================================
      use params
      use vertices
      use smoothmodule
      implicit none
      integer::i,n

      if(.not.lsmooth)return
      print*,'ComputeSmooth',nsmooth,factor
c-----smooth curve
      do n=1,nsmooth
      do i=2,ni-1
        x(i)=factor*(.5*(x(i-1)+x(i+1)))+(1-factor)*x(i)
        y(i)=factor*(.5*(y(i-1)+y(i+1)))+(1-factor)*y(i)
      enddo;enddo
      return
C=======================================================================
      end
c***********************************************************************
c     model performing a translation acording to xtran,ytran
c     input format:
c     translate xtran ytran
c***********************************************************************
      module translatemodule
      logical::ltranslate=.false.
      real(kind=8)::xtran,ytran
      end module

      subroutine Inputtranslate
C=======================================================================
      use params
      use inpwords
      use translatemodule
      implicit none
      integer::yesno=2

      if(linpexist)then
        select case(words(1)(1:nrchar(1)))
        case('translate')
          read(words(2),*)xtran
          read(words(3),*)ytran
          ltranslate=.true.
        end select
      else
        print*,' Translate airfoil: yes=1/no=2 ' 
        read*,yesno
        if(yesno.eq.1)then
          ltranslate=.true.
          print*,' Give translation length in x-direction : '
          read*,xtran
          print*,' Give translation length in y-direction : '
          read*,ytran
        endif
      endif
      return
C=======================================================================
      end
      
      subroutine Computetranslate
C=======================================================================
      use params
      use vertices
      use translatemodule
      implicit none
      integer::i

      if(.not.ltranslate)return
      do i=1,ni
        x(i)=x(i)+xtran
        y(i)=y(i)+ytran
      enddo
      return
C=======================================================================
      end
