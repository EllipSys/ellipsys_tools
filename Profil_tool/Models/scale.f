c***********************************************************************
c     model scaling the airfoil geometry according to xscale,yscale
c     input
c     scale xscale yscale
c***********************************************************************
      module scalemodule
      logical::lscale=.false.
      real(kind=8)::xscale=1.d0,yscale=1.d0
      end module

      subroutine Inputscale
C=======================================================================
      use params
      use inpwords
      use scalemodule
      implicit none
      integer::yesno=2

      if(linpexist)then
        select case(words(1)(1:nrchar(1)))
        case('scale')
          read(words(2),*)xscale
          read(words(3),*)yscale
          lscale=.true.
        end select
      else
        print*,' Scale airfoil: yes=1/no=2 '
        read*,yesno
        if(yesno.eq.1)then
          lscale=.true.
          print*,' Give scaling factor for x-direction : '
          read*,xscale
          print*,' Give scaling factor for y-direction : '
          read*,yscale
        endif
      endif
      return
C=======================================================================
      end
      
      subroutine Computescale
C=======================================================================
      use params
      use vertices
      use scalemodule
      implicit none
      integer i

      if(.not.lscale)return
      do i=1,ni
        x(i)=x(i)*xscale
        y(i)=y(i)*xscale*yscale
      enddo
      return
C=======================================================================
      end
