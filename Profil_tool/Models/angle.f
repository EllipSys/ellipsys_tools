c***********************************************************************
c     subroutine computes the geometrical angle of incidence 
c     of the airfoil
c
c***********************************************************************
      module anglemodule
      logical::langle=.false.
      end module

      subroutine InputAngle
C=======================================================================
      use params
      use inpwords
      use anglemodule
      implicit none
      integer::yesno=2

      if(linpexist)then
        select case(words(1)(1:nrchar(1)))
        case('angle')
          langle=.true.
        end select
      else
        print*,' Compute geometrical properties of airfoil: yes=1/no=2 '
        read*,yesno
        if(yesno.eq.1)then
          langle=.true.
        endif
      endif
      return
C=======================================================================
      end
      
      subroutine ComputeAngle
C=======================================================================
      use params
      use vertices
      use anglemodule
      implicit none
      real(kind=8)::alpha,pi,lenghtmax,lenght,ymin,ymax
      real(kind=8)::factor,maxthickness,thickness,yval,angle
      real(kind=8),dimension(ni)::xt,yt
      real(kind=8),dimension(2)::n1,n2,a
      integer::i,j,ilenght

      if(.not.langle)return
c-----pi
      pi=4.d0*atan(1.d0)

c-----find leading edge of profile
      lenghtmax=1.d-6
      do i=1,ni
        lenght=sqrt((x(i)-x(1))**2+(y(i)-y(1))**2)
        if(lenght.gt.lenghtmax)then
          lenghtmax=lenght
          ilenght=i
        endif
      enddo

c-----calculate coord angle
      alpha=atan((y(ilenght)-y(1))/(x(ilenght)-x(1)))*180.d0/pi
      open(unit=17,file='chord.dat')
      write(17,*)x(1),y(1)
      write(17,*)x(ilenght),y(ilenght)
      close(17)
      print*,' Angle positive nose up ',-alpha

c-----rotate airfoil in order to compute thickness
      angle=alpha/180.d0*pi
      n1(1)= cos(angle)
      n1(2)= sin(angle)
      n2(1)=-sin(angle)
      n2(2)= cos(angle)

      do i=1,ni
        a(1)=x(i)
        a(2)=y(i)
        xt(i)=n1(1)*a(1)+n1(2)*a(2)
        yt(i)=n2(1)*a(1)+n2(2)*a(2)
      enddo

c------determine approximate thickness of airfoil
      maxthickness=0.d0
      open(unit=11,file='thickness.dat')
      do i=2,ilenght
        do j=ilenght,ni
          if(xt(j  ).le.xt(i).and.
     &      xt(j+1).ge.xt(i))then
            factor=(xt(i)-xt(j))/(xt(j+1)-xt(j))
            yval=factor*yt(j+1)+(1-factor)*yt(j)
            thickness=yval-yt(i)
            if(thickness.gt.maxthickness)maxthickness=thickness
            write(11,*)xt(i),thickness,yval,yt(i)
          endif
        enddo
      enddo

      print*,' Chord     : ',lenghtmax
      print*,' Thickness : ',maxthickness/lenghtmax

C=======================================================================
      end
