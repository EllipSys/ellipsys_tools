c***********************************************************************
c
c***********************************************************************
      module flapmodule
      real(kind=8)::xrot,angle
      logical::lflap=.false.
      end module

      subroutine Inputflap
C=======================================================================
      use params
      use inpwords
      use flapmodule
      implicit none
      integer::yesno=2

      if(linpexist)then
        select case(words(1)(1:nrchar(1)))
        case('flap')
          read(words(2),*)xrot
          read(words(3),*)angle
          lflap=.true.
        end select
      else
        print*,' Generate flap : yes=1/no=2 '
        read*,yesno
        if(yesno.eq.1)then
          lflap=.true.
          print*,' Give chordwise position : '
          read*,xrot
          print*,' Give flap deflection : '
          read*,angle
        endif
      endif
      return
C=======================================================================
      end
      
      subroutine Computeflap
C=======================================================================
      use params
      use vertices
      use flapmodule
      implicit none
      real(kind=8)::yrot,pi
      real(kind=8)::a1,a2,b1,b2,xcut,ycut,factor
      real(kind=8),dimension(2)::n1,n2,a
      real(kind=8),dimension(ni)::xt,yt,x0,y0
      integer ni2,i,j,irot,icut,jcut

      if(.not.lflap)return
c-----determine y-rotation point
      do i=ni,2,-1
        if(xrot.le.x(i).and.xrot.ge.x(i-1))then
          factor=(x(i)-xrot)/(x(i)-x(i-1))
          yrot=factor*y(i-1)+(1-factor)*y(i)
          irot=i
          print*,yrot,i
        endif
      enddo

      pi=4.d0*atan(1.d0)
c-----turning angle
      angle=angle/180.d0*pi
      n1(1)= cos(angle)
      n1(2)= sin(angle)
      n2(1)=-sin(angle)
      n2(2)= cos(angle)
      do i=1,ni
        a(1)=x(i)-xrot
        a(2)=y(i)-yrot
        xt(i)=n1(1)*a(1)+n1(2)*a(2)+xrot
        yt(i)=n2(1)*a(1)+n2(2)*a(2)+yrot
      enddo

      do j=2,ni
        a2=(yt(j)-yt(j-1))/(xt(j)-xt(j-1))
        b2=yt(j)-a2*xt(j)
        do i=2,ni/2
          a1=(y(i)-y(i-1))/(x(i)-x(i-1))
          b1=y(i)-a1*x(i)
          xcut=(b2-b1)/(a1-a2)
          ycut=a1*xcut+b1
          if((xcut- x(i-1))*(xcut-x(i)).le.0.d0.and.
     &       (xcut-xt(j-1))*(xcut-xt(j)).le.0.d0)then
               print*,' cut is found ',i,j,xcut,ycut
               icut=i;jcut=j
            goto 100
          endif
        enddo
      enddo

 
  100 ni2=ni+jcut-icut+2
      
      print*,ni
      x0=x;y0=y
      deallocate(x,y);allocate(x(ni2),y(ni2))
      x=0.d0;y=0.d0
      j=0
      do i=1,jcut-1
        j=j+1
        x(j)=xt(i)
        y(j)=yt(i)
      enddo
      j=j+1
      print*,'a ',j
      x(j)=xcut;y(j)=ycut
      do i=icut,irot-1
        j=j+1
        x(j)=x0(i)
        y(j)=y0(i)
      enddo
      j=j+1
      print*,'b ',j
      x(j)=xrot;y(j)=yrot
      do i=irot,ni
        j=j+1
        x(j)=xt(i)
        y(j)=yt(i)
      enddo
      ni=ni2
      print*,'c ',j
      return
C=======================================================================
      end
