C***********************************************************************
c     subroutine rotates the profile data anti clock wise
c     through angle alphaturn
C
C***********************************************************************
      module rotationmodule
      real(kind=8)::angle,xrot,yrot
      logical lrotate
      end module

      subroutine InputRotation
C=======================================================================
      use params
      use inpwords
      use rotationmodule
      implicit none
      integer::yesno=2

      if(linpexist)then
        select case(words(1)(1:nrchar(1)))
        case('rotate')
          lrotate=.true.
          read(words(2),*)angle
          read(words(3),*)xrot
          read(words(4),*)yrot
          print*,' rotate : ',lrotate,angle,xrot,yrot
        end select
      else
        print*,' Rotate airfoil: yes=1/no=2 ' 
        read*,yesno
        if(yesno.eq.1)then
          lrotate=.true.
          print*,' Give rotation centre (x_rot,y_rot) :'
          read*,xrot,yrot
          print*,' Give anti clock wise angle : '
          read*,angle
        endif
      endif
      return
C=======================================================================
      end
      
      subroutine ComputeRotation
C=======================================================================
      use params
      use vertices
      use rotationmodule
      implicit none
      real(kind=8)::pi,xtemp,ytemp
      real(kind=8),dimension(2)::n1,n2,a
      integer i

      if(.not.lrotate)return
      pi=4.d0*atan(1.d0)
c-----turning angle
      angle=angle/180.d0*pi
      n1(1)= cos(angle)
      n1(2)= sin(angle)
      n2(1)=-sin(angle)
      n2(2)= cos(angle)

      do i=1,ni
        a(1)=x(i)-xrot
        a(2)=y(i)-yrot
        x(i)=n1(1)*a(1)+n1(2)*a(2)+xrot
        y(i)=n2(1)*a(1)+n2(2)*a(2)+yrot
      enddo
      return
C=======================================================================
      end
