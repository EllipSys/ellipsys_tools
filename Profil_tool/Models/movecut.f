c***********************************************************************
c     move cut the specified angle clockwise
c***********************************************************************
      module movecutmodule
      logical::lmovecut=.false.
      real(kind=8)::alfa=0.d0
      end module

      subroutine InputMoveCut
C=======================================================================
      use params
      use inpwords
      use movecutmodule
      implicit none
      integer::yesno=2

      if(linpexist)then
        select case(words(1)(1:nrchar(1)))
        case('movecut')
          lmovecut=.true.
          if(nrword.ge.2)read(words(2),*)alfa
        end select
      else
        print*,' Move airfoil cut: yes=1/no=2 '
        read*,yesno
        if(yesno.eq.1)then
          lmovecut=.true.
          print*,' Give anti clock wise angle to move cut : '
          read*,alfa
        endif
      endif
      return
C=======================================================================
      end
      
      subroutine ComputeMoveCut
C=======================================================================
      use params
      use vertices
      use movecutmodule
      implicit none
      real(kind=8),dimension(ni)::xt,yt
      real(kind=8)::gamma,pi,c1,c2,rx,ry,xc,yc
      integer::i,imove

      if(.not.lmovecut)return
c-------pi
      pi=4*atan(1.d0)

c-------compute center of gravity
      xc=0.d0
      yc=0.d0
      do i=1,ni
        xc=xc+x(i)
        yc=yc+y(i)
      enddo
      xc=xc/real(ni)
      yc=yc/real(ni)

c-------compute angle
      gamma=pi/180*alfa+atan((y(1)-yc)/(x(1)-xc+1.d-30))

c-------compute coordinats of direction vector
      rx=cos(gamma)
      ry=sin(gamma)

c-------find intersection with profile definition
      do i=1,ni-1
        c1=rx*y(i  )-ry*x(i  )
        c2=rx*y(i+1)-ry*x(i+1)
        if(c1.gt.0.d0.and.c2.lt.0.d0)then
          imove=i
          exit
        endif
      enddo

      do i=imove,ni
        xt(i-imove+1)=x(i)
        yt(i-imove+1)=y(i)
      enddo
      do i=2,imove
        xt(ni-imove+i)=x(i)
        yt(ni-imove+i)=y(i)
      enddo

      do i=1,ni
        x(i)=xt(i)
        y(i)=yt(i)
      enddo
      return
C=======================================================================
      end
