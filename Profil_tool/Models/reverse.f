c***********************************************************************
c
c***********************************************************************
      module reversemodule
      logical::lreverse=.false.
      end module

      subroutine InputReverse
C=======================================================================
      use params
      use inpwords
      use reversemodule
      implicit none
      integer::yesno=2

      if(linpexist)then
        select case(words(1)(1:nrchar(1)))
        case('reverse')
          lreverse=.true.
        end select
      else
        print*,' Reverse direction of airfoil coordinats: yes=1/no=2 '
        read*,yesno
        if(yesno.eq.1)then
          lreverse=.true.
        endif
      endif
      return
C=======================================================================
      end
      
      subroutine ComputeReverse
C=======================================================================
      use params
      use vertices
      use reversemodule
      implicit none
      real(kind=8),dimension(ni)::xt,yt
      integer::i

      if(.not.lreverse)return
      do i=1,ni
        xt(i)=x(ni+1-i)
        yt(i)=y(ni+1-i)
      enddo
      do i=1,ni
        x(i)=xt(i)
        y(i)=yt(i)
      enddo
      return
C=======================================================================
      end
