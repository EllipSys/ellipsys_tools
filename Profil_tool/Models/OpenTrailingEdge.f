c***********************************************************************
c
c***********************************************************************
      module OpenTrailingEdgemodule
      logical::lOpenTrailingEdge=.false.
      real(kind=8)::yopen=0.d0
      integer::nsmooth=0
      end module

      subroutine InputOpenTrailingEdge
C=======================================================================
      use params
      use inpwords
      use OpenTrailingEdgemodule
      implicit none
      integer::yesno=2

      if(linpexist)then
        select case(words(1)(1:nrchar(1)))
        case('open-trailing-edge')
          if(nrword.ge.2)read(words(2),*)yopen
          if(nrword.ge.3)read(words(3),*)nsmooth
          lOpenTrailingEdge=.true.
        end select
      else
        print*,'  Open Trailing Edge yes=1/no=2 : '
        read*,yesno
        if(yesno.eq.1)then
          lOpenTrailingEdge=.true.
          print*,' Give airfoil opening : '
          read*,yopen
          print*,' Give number of smoothing passes : '
          read*,nsmooth
        endif
      endif
      return
C=======================================================================
      end

      subroutine ComputeOpenTrailingEdge
C=======================================================================
      use params
      use vertices
      use OpenTrailingEdgemodule
      implicit none
      real(kind=8),dimension(4*ni)::xn,yn
      real(kind=8)::lenght,delta,length,x0,y0,slope,xi
      integer i
      integer nextra,inum,i1num,i2num,i1,i2,j,nstart

      if(.not.lOpenTrailingEdge)return

      nstart=1

      print*,'yopen : ',yopen
c-----open lower side
      do i=1,int(ni*.25)+nstart
          xi=abs((x(i)-x(int(ni*.25)+nstart))
     &          /(x(1)-x(int(ni*.25)+nstart)))
          y(i)=y(i)-.5*yopen*xi
      enddo
c-----open lower side
      do i=int(.75*ni)+nstart,ni
          xi=abs((x(i)-x(int(.75*ni)+nstart))
     &          /(x(ni)-x(int(.75*ni)+nstart)))
          y(i)=y(i)+.5*yopen*xi
      enddo
c-------close trailing edge

c-----compute hight of trailing edge
      delta=y(ni)-y(1)
      print*,' Trailing edge opening : ',delta

c-----compute new coordinats
      nextra=24
      inum=0
      i1num=0
      i2num=0

c------- lower vertical piece
      do i=1,nextra
        inum=inum+1
        i1num=i1num+1
        xn(inum)=.5*(x(1)+x(ni))
     &          -.5*(x(ni)-x(1))*real(i-1)/real(nextra-1)
        yn(inum)=y(1)+.5*delta-.5*delta*(i-1.)/(nextra-1.)
      enddo

c------- lower horizontal piece
      length = 0.
      do i=1,ni
         length = length
     &              + sqrt((x(i+1)-x(i))**2+(y(i+1)-y(i))**2)
         if(length.gt.delta/2.)exit
      enddo

      i1 = i+1

      do  j=1,i1-1
        x0=xn(inum)
        y0=yn(inum)
        slope = (y(j+1)-y0)/(x(j+1)-x0)
        do i=1,nextra/(i1-1)
          inum=inum+1
          i1num=i1num+1
          xn(inum)=x0 + (i-1.)*(x(j+1)-x0)/(nextra/(i1-1)-1.)
          yn(inum)=y0 + slope *(xn(inum)-x0)
        enddo
      enddo

c------- upper horizontal piece
      length = 0.
      do i=ni,1,-1
         length = length
     &              + sqrt((x(i-1)-x(i))**2 + (y(i-1)-y(i))**2)
         if(length.gt.delta/2.)exit
      enddo

      i2 = i-1

      do i=i1+1,i2
        inum=inum+1
        xn(inum)=x(i)
        yn(inum)=y(i)
      enddo

      do j=i2,ni-1
        x0=xn(inum)
        y0=yn(inum)
        slope = (y(j+1)-y0)/(x(j+1)-x0)
        do i=1,nextra/(ni-i2)
          inum=inum+1
          i2num=i2num+1
          xn(inum)=x0 + (i-1.)*(x(j+1)-x0)/(nextra/(ni-i2)-1.)
          yn(inum)=y0 + slope *(xn(inum)-x0)
        enddo
      enddo

c------- lower vertical piece
      do  i=1,nextra
        inum=inum+1
        i2num=i2num+1
        xn(inum)=x(ni)
     &          -.5*(x(ni)-x(1))*real(i-1)/real(nextra-1)
        yn(inum)=y(ni)-.5*delta*(i-1.)/(nextra-1.)
      enddo

c     print*,'Enter number of smoothing cycles:'
c     read*,nsmooth

      do j=1,nsmooth
        do i = 2,i1num-1
          xn(i)=.25*(xn(i-1)+2.*xn(i)+xn(i+1))
          yn(i)=.25*(yn(i-1)+2.*yn(i)+yn(i+1))
        enddo
        do i = inum-1,inum-i2num+1,-1
         xn(i)=.25*(xn(i-1)+2.*xn(i)+xn(i+1))
         yn(i)=.25*(yn(i-1)+2.*yn(i)+yn(i+1))
        enddo
      enddo

      deallocate(x,y)
      ni=inum
      allocate(x(ni),y(ni))
      do i=1,ni
        x(i)=xn(i)
        y(i)=yn(i)
      enddo
      return
C=======================================================================
      end
