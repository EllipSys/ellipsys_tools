c***********************************************************************
c     model performing a translation acording to xtran,ytran
c     input format:
c     translate xtran ytran
c***********************************************************************
      module translatemodule
      logical::ltranslate=.false.
      real(kind=8)::xtran,ytran
      end module

      subroutine Inputtranslate
C=======================================================================
      use params
      use inpwords
      use translatemodule
      implicit none
      integer::yesno=2

      if(linpexist)then
        select case(words(1)(1:nrchar(1)))
        case('translate')
          read(words(2),*)xtran
          read(words(3),*)ytran
          ltranslate=.true.
        end select
      else
        print*,' Translate airfoil: yes=1/no=2 ' 
        read*,yesno
        if(yesno.eq.1)then
          ltranslate=.true.
          print*,' Give translation length in x-direction : '
          read*,xtran
          print*,' Give translation length in y-direction : '
          read*,ytran
        endif
      endif
      return
C=======================================================================
      end
      
      subroutine Computetranslate
C=======================================================================
      use params
      use vertices
      use translatemodule
      implicit none
      integer::i

      if(.not.ltranslate)return
      do i=1,ni
        x(i)=x(i)+xtran
        y(i)=y(i)+ytran
      enddo
      return
C=======================================================================
      end
