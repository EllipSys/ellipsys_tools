c***********************************************************************
c
c***********************************************************************
      module smoothmodule
      logical::lsmooth=.false.
      integer::nsmooth=0
      real(kind=8)::factor=0.05
      end module

      subroutine InputSmooth
C=======================================================================
      use params
      use inpwords
      use smoothmodule
      implicit none
      integer::yesno=2

      if(linpexist)then
        select case(words(1)(1:nrchar(1)))
        case('smooth')
          read(words(2),*)nsmooth
          if(nrword.ge.3)read(words(3),*)factor
          lsmooth=.true.
        end select
      else
        print*,' Smooth airfoil: yes=1/no=2 '
        read*,yesno
        if(yesno.eq.1)then
          lsmooth=.true.
          print*,' Give number of smoothing sweeps : '
          read*,nsmooth
          print*,' Give smoothing factor [0:1] : '
          read*,factor
        endif
      endif
      return
C=======================================================================
      end
      
      subroutine ComputeSmooth
C=======================================================================
      use params
      use vertices
      use smoothmodule
      implicit none
      integer::i,n

      if(.not.lsmooth)return
      print*,'ComputeSmooth',nsmooth,factor
c-----smooth curve
      do n=1,nsmooth
      do i=2,ni-1
        x(i)=factor*(.5*(x(i-1)+x(i+1)))+(1-factor)*x(i)
        y(i)=factor*(.5*(y(i-1)+y(i+1)))+(1-factor)*y(i)
      enddo;enddo
      return
C=======================================================================
      end
