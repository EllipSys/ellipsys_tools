      module params
      integer::ni
      logical::linpexist=.false.,lradius=.false.
      real(kind=8)::radius
      end module

      module vertices
      real(kind=8),dimension(:),allocatable::x,y,z
      logical::l3d=.false.
      end module

      module inpwords
      integer,parameter::mword=20
      integer::nrchar(mword),nrword
      integer,parameter::mchar=256
      character(len=40),dimension(mword):: words
      end module

      program ProfilTool
c----------------------------------------------------------------------
c     profil is a program for manipulating airfoil profil data
c     the profile data must be given in a file called "prof.dat"
c     and the first line must be the number of points, and the
c     following lines the data points  x and y
c     the following opotunities exists
c     smooth a profile curve
c     turn the profile curve around (1,0)
c     finde the angle of the profile coord
c     scale the profile curve
c----------------------------------------------------------------------
      use params
      use vertices
      implicit none

c-------read profile date
      call ReadProfile

c-------read input
      call ReadInput

c-------Execute models
      call Interface(2)

c-------Output new profile date
      call OutputProfile

      stop
      end

      subroutine ReadProfile
c----------------------------------------------------------------------
c     subroutine reading the airfoil shape from the prof.dat file.
c     The first line must be the number of points, and the following 
c     lines the data points x and y
c----------------------------------------------------------------------
      use params
      use vertices
      implicit none
      logical::lexist
      integer::i
      character(len=1)::dum
      

      inquire(file='prof.dat',exist=lexist)
      if(.not.lexist)then

      else
c---------read profile data assuming x,y,z 
        open(unit=10,file='prof.dat')
        read(10,*)dum,ni
        print*,'start',ni
        allocate(x(ni),y(ni),z(ni))
        print*,' number of points : ',ni
        do i=1,ni
         read(10,*,end=665)x(i),y(i),z(i)
        enddo
        l3d=.true.
        print*,' ########### 3d ####'
        close(10)
        return
c-------rad profile data assuming x,y
  665   close(10)
        open(unit=10,file='prof.dat')
        read(10,*)dum,ni
        print*,'start',ni
        do i=1,ni
         read(10,*,end=666)x(i),y(i)
        enddo
        close(10)
      endif
      return
  666 print*,' Error to few date in prof.dat file '
      stop
      end

      subroutine ReadInput
c----------------------------------------------------------------------
c     subroutine reading the input parameters. If the file prof.inp
c     exist, the date is taken from here, else the input is 
c     interactive
c----------------------------------------------------------------------
      use params
      use vertices
      use inpwords
      implicit none
      character(len=256)InputLine

      inquire(file='prof.inp',exist=linpexist)
      if(linpexist)then
        print*,' Reading prof.inp ! '
        open(unit=10,file='prof.inp')
        do
          read(10,'(a256)',end=666)InputLine
          call cmdpart(InputLine)
c----------- main interpreter
             select case(words(1))
c------------  radius for station is given
             case('radius')
               read(words(2),*)radius
               lradius=.true.
             end select
c----------- call command interpreter part of interface
          call interface(1)
        enddo        
      else
        print*,' no input file : '
        call interface(1)
c       stop
      endif
  666 close(10)
      return
      end

      subroutine OutputProfile
c----------------------------------------------------------------------
c     subroutine writing the processed airfoil date to file
c----------------------------------------------------------------------
      use params
      use vertices
      implicit none
      integer::i

      open(unit=10,file='prof.new')

      if(lradius)then
        write(10,FMT='(a,1x,i6,f9.4)')'#',ni,radius
      else
        write(10,FMT='(a,1x,i6)')'#',ni
      endif
      if(.not.l3d)then
        do i=1,ni
          write(10,*)x(i),y(i)
        enddo
      else
        do i=1,ni
          write(10,*)x(i),y(i),z(i)
        enddo
      endif
      return
      end

      subroutine cmdpart(InputLine)
c-----------------------------------------------------------------------
c     routine parting command InputLine into words
c----------------------------------------------------------------------
c
c     Author: Jess A. Michelsen and Niels N. Soerensen
c     last revision :march 3, 1998
c-----------------------------------------------------------------------
      use inpwords
      implicit none
      integer i
      character(len=40)::blank
      character(len=256) InputLine
      integer CurrentPosition,RelativeBlankPosition,BlankPosition
C--------------------------------------------- Reset word counter ------
      nrword=0
      CurrentPosition=1
C--------------------------------------------- Skip if a comment line --
c     if(index(InputLine(1:mchar),'#').gt.0) return
C---------------------------------------- find distance to next blank --
   20 RelativeBlankPosition=index(InputLine(CurrentPosition:mchar),' ')
      if(CurrentPosition.eq.mchar.or.RelativeBlankPosition.eq.0) return
C---------------------------------------- found the end of a word ------
      if(RelativeBlankPosition.gt.1.and.RelativeBlankPosition.le.40)then
        BlankPosition=CurrentPosition-1+RelativeBlankPosition
        nrword=nrword+1
        if(nrword.gt.mword)return
        words(nrword)=InputLine(CurrentPosition:BlankPosition)
        nrchar(nrword)=RelativeBlankPosition-1
      endif
c-----------------------------------------------------------------------
      CurrentPosition=CurrentPosition+RelativeBlankPosition
      goto 20
c-----------------------------------------------------------------------
      return
      end
