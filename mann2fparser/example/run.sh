# Mann box is defined as:
#
# x3
# 
# ^ cross
# | plane
# | 
# |------> x2
# \
#  \
#   \
#    \
#    _\/
#     x1 = is often considered as time axis.
# 
# Usage: ./mann2fparser <conversion> <x1shift> <x2shift> <x3shift> <Nx1> <Nx2> <Nx3> <Lx1> <Lx2> <Lx3> <U> <file1> <file2> <file3>
# In the conversion, "t" is used to indicate which axis should be use to march in time 
# using Taylor's frozen turbulence hypothesis. In the example below, x1 is the time axis,
# and x2-x3-planes are stored, where x2=u and x3=v. In other words, the z-axis is the stream-wise axis 
# (as often used in full rotor simulations). The ohter variables are:
# x1shift : shift x1 coordinate 
# x2shift : shift x2 coordinate 
# x3shift : shift x3 coordinate 
# Nx1      : number of cells in x1 direction
# Nx2      : number of cells in x2 direction
# Nx3      : number of cells in x3 direction
# Lx1      : box length in x1 direction
# Lx2      : box length in x2 direction
# Lx3      : box length in x3 direction
# U        : velocity of planes
# file1    : file name in x1 direction
# file2    : file name in x2 direction
# file3    : file name in x3 direction
./mann2fparser txy 0.0 0.0 0.0 64 64 64 256.0 256.0 256.0 10.0 sim_u.bin sim_v.bin sim_w.bin
