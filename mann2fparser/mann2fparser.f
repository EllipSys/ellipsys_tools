      program mann2fparser
      implicit none
      integer::i,j,k,recnr,h,l,recordlength,ifile
      integer::nt,nx,ny,nz,ndata
      real*4::dummy
      integer,parameter::idp=kind(1d0)
      real(idp)::Tstart,Tend,dt,dx,dy,dz,tmove,xmove,ymove,zmove,uH
      real(idp),allocatable,dimension(:)::x,y,z,Umean,reczero
      real(idp),allocatable,dimension(:,:)::var,Urms
      integer ifirst,ilast,namelenin,namelenout
      integer,parameter::wordlen=1024  
      character(len=wordlen)::infile,outfile,text
      character(len=wordlen),dimension(3)::infiles
      character(len=5),dimension(3)::outfiles
      character(len=3)::conversion
      integer,dimension(3)::n,ifileslen
      real(idp),dimension(3)::Lbox,delta,xshift

C---- User input from command line -------------------------------------
      call getarg(1,conversion)
      call getarg(2,text);read(text,*)xshift(1)
      call getarg(3,text);read(text,*)xshift(2)
      call getarg(4,text);read(text,*)xshift(3)
      call getarg(5,text);read(text,*)n(1)
      call getarg(6,text);read(text,*)n(2)
      call getarg(7,text);read(text,*)n(3)
      call getarg(8,text);read(text,*)Lbox(1)
      call getarg(9,text);read(text,*)Lbox(2)
      call getarg(10,text);read(text,*)Lbox(3)
      call getarg(11,text);read(text,*)uH
      call getarg(12,text);read(text,*)infiles(1)
      call getarg(13,text);read(text,*)infiles(2)
      call getarg(14,text);read(text,*)infiles(3)

      print*,n
      print*,Lbox
      print*,uH

      do i=1,3
        do ilast=1,256
          if(infiles(i)(ilast:ilast).eq.' ') exit
        enddo
        ifileslen(i)=ilast
      enddo
      namelenin=ilast

c---- Convert to desired coordinate system -----------------------------
      nt=1;nx=1;ny=1;nz=1
      tmove=0;xmove=0;ymove=0;zmove=0
      do i=1,3
        select case(conversion(i:i))
        case('t')
          nt=n(i);dt=Lbox(i)/uH/n(i);delta(i)=dt;tmove=xshift(i)
          outfiles(i)='t.dat'
        case('x')
          nx=n(i);dx=Lbox(i)/n(i);delta(i)=dx;xmove=xshift(i)
          outfiles(i)='u.dat'
        case('y')
          ny=n(i);dy=Lbox(i)/n(i);delta(i)=dy;ymove=xshift(i)
          outfiles(i)='v.dat'
        case('z')
          nz=n(i);dz=Lbox(i)/n(i);delta(i)=dz;zmove=xshift(i)
          outfiles(i)='w.dat'
        end select
      enddo
      ! Rename t.dat. Check which velocity is missing, and repace t.dat
      ! with that velocity. 
      
      
      Tstart=0d0
      Tend=nt*dt
      ndata=nx*ny*nz

      write(*,10)' n1=',n(1),'  n2=',n(2),'  n3=',n(3)
      write(*,11)' dx1=',delta(1),' dx2=',delta(2),' dx3=',delta(3)
      print*,'x1->',conversion(1:1),
     &       '          x2->',conversion(2:2),
     &       '          x3->',conversion(3:3)
      write(*,12)' nt=',nt,'  nx=',nx,'  ny=',ny,'  nz=',nz
      write(*,13)' tshift=',tmove,' xshift=',xmove,
     &           ' yshift=',ymove,' zshift=',zmove
      write(*,*)'output=',outfiles(1),' ',outfiles(2),' ',outfiles(3)
   10 format(a,i10,a,i10,a,i10)
   11 format(a,f10.4,a,f10.4,a,f10.4)
   12 format(a,i10,a,i10,a,i10,a,i10)
   13 format(a,f10.4,a,f10.4,a,f10.4,a,f10.4)

      allocate(x(nx),y(ny),z(nz),var(nt,ndata),
     &         Urms(3,ndata),Umean(ndata))

      do ifile=1,3
      
        print*,'Convert Mann turbulence file '
     &,infiles(ifile)(1:ifileslen(ifile)),'to unformatted fparser file '
     &,outfiles(ifile)(1:5)


C------ Read file --------------------------------------------------------
        recordlength=4
        open(unit=1,file=infiles(ifile)(1:ifileslen(ifile)),
     &       access='direct',form='unformatted',recl=recordlength)
        do k=1,n(1);
           do i=1,n(2); do j=1,n(3);
   
              recnr=j+(i-1)*n(3)+(k-1)*n(2)*n(3)
             read(1,rec=recnr)dummy
             l=j+(i-1)*n(3)
             var(k,l)=dble(dummy);
           enddo;enddo
        enddo
        close(1)

C------ Write fparser file -----------------------------------------------
        recordlength=max(32,4*ndata*2)
c        recordlength=max(8,ndata*2)
        open(1,file=outfiles(ifile)(1:5),access='DIRECT',
     &         recl=recordlength)
        ! assure that header is unique
        allocate(reczero(ndata))
        reczero=0.d0
        write(1,rec=1)reczero
        deallocate(reczero)
        ! Header
        write(1,rec=1)nt+1,nx,ny,nz,Tstart,Tend
         ! Grid
         if(nx.gt.1)then
           do i=1,nx
             x(i)=dx*(real(i,idp)-0.5d0)+xmove
           enddo
           write(1,rec=2)x
        endif
        if(ny.gt.1)then
           do j=1,ny
             y(j)=dy*(real(j,idp)-0.5d0)+ymove
           enddo
           write(1,rec=3)y
        endif
        if(nz.gt.1)then
           do k=1,nz
             z(k)=dz*(real(k,idp)-0.5d0)+zmove
           enddo
           write(1,rec=4)z
        endif
        ! Data
        do h=1,nt
c          write(1,rec=4+h)var(h,:)
          ! Reverse in time as used in windturb
          write(1,rec=4+h)var(nt-h+1,:)
        enddo
c        write(1,rec=4+h)var(1,:)
        ! Reverse in time as used in windturb
        write(1,rec=4+h)var(nt,:)
        close(1)
C---- Calculate turbulent intensity
        Umean=0d0
        Urms(ifile,:)=0d0
        do h=1,nt;
           Umean=Umean+var(h,:)
           Urms(ifile,:)=Urms(ifile,:)+var(h,:)**2
        enddo
        ! Time averaged
        Umean=Umean/dble(nt)
        Urms(ifile,:)=sqrt(abs(Urms(ifile,:)/dble(nt)
     &                    -(Umean)**2))

        print*,'Urms_meanAll',sum(Urms(ifile,:))/dble(ndata)
      enddo
      print*,'Average turbulence intensity is:',sqrt(1d0/3d0*
     &      (sum(Urms(1,:))**2+sum(Urms(2,:))**2+sum(Urms(3,:))**2))/uH
     &      /ndata
      end
           
