# mann2fparser

This tool can convert Mann turbulence to fparser inlet files 
that can used as inlet conditions in EllipSys.

### Generate Mann turbulence

The mann turbulence files can be generated using the HAWC2's 
command line version of the Mann model.
http://www.hawc2.dk/Download/Pre-processing-tools

The following files are needed:  

mann_turb_x64.exe  
mann_x64.dll  
resourse_x64.dat  

The files are already available in this tool in case the download link is broken.

Using the Jess cluster one could do:

    $ wine mann_turb_x64.exe sim 0.219 89.53 0.0 -100 64 64 64 4.0 4.0 4.0 true

where the input corresponds to (from left to right):  

| Variable | Description                                                           |
| -------- | --------------------------------------------------------------------- | 
| sim      | name of the output files                                              |
| αε^(2/3) | α is the Kolmogorov constant and ε is the rate of viscous dissipation |
| L        | a characteristic length scale                                         |
| Γ        | stretching parameter                                                  |
| Seed nr  | a user specified integer seed for the random number generator         |
| N_u      | number of cells in the flow direction                                 |
| N_v      | number of cells in the horizontal cross-flow direction                |
| N_w      | number of cells in the vertical direction                             |
| delta_u  | grid spacing in the flow direction                                    |
| delta_v  | grid spacing in the horizontal cross-flow direction                   |
| delta_w  | grid spacing in the vertical direction                                |
| HF comp  | high frequency correction (true/false)                                |         

Three files are written that correspond to the turbulent fluctuations in x, y and z direction.

### Build mann2fparser

Simply build mann2fparser by running:

    $ ./build.sh

The default compiler is ifort, which needs a flag -assume byterecl to be consistent
with gfortran. One can also use gfortran by uncommenting the corresponding line in build.sh.

### Convert mann turbulence to fparser files

Once you have created the 3 mann turbulence files you can convert them by linking them to 
the example forder:

    $ cd example
    $ cd ln -s ../*.bin .

You can run mann2fparser through a bash script run.sh:

    $ ./run.sh

where the input to mann2fparser is also given and explained. 
The fparser output files are binary and can be converted to ascii files using the fparser_convert tool
