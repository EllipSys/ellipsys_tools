      program HYAverage
c-----------------------------------------------------------------------
c     program for time averaging a standard project.?HY file and
c     computing the rms of the force history 
c     the user must give the first and last iteration to average
c     the user must give the number of slices in the spanwise direction
c
c-----------------------------------------------------------------------
      implicit none
      real(kind=8),dimension(:,:),allocatable::slices,aslices,sslices
      integer:: nstep,nstepstart,nstepfinish,it,l,nn,nsl
      integer:: nslice,Nplanes=0,endf
      character(len=128) filename
      character(len=1) adum

      print*,'Input filename:'
      read*, filename

      print*,' Give first, last iteration : '
      read*,nstepstart,nstepfinish

      print*,' Give number of slices/sections : '
      read*,Nplanes
      allocate(slices(4,Nplanes),aslices(4,Nplanes),sslices(4,Nplanes))

      open(unit=10,file=filename)
      read(10,*)adum
      read(10,*)adum
      read(10,*)adum
      nsl=0
      do
      read(10,*,iostat=endf)nstep,((slices(l,nn),l=1,4),nn=1,Nplanes)

      if(endf.eq.0)then
        if(nstep.ge.nstepstart.and.nstep.le.nstepfinish)then
          if(nsl.eq.0)then
c------------averaged value
            aslices(1:4,:)=slices(1:4,:)
c------------average of squared value
            sslices(1:4,:)=slices(1:4,:)**2
            nsl=1
          else
c------------averaged value
            aslices(1:4,:)=(aslices(1:4,:)*nsl+slices(1:4,:))/(nsl+1.d0)
c------------average of squared value
            sslices(1:4,:)=(sslices(1:4,:)*nsl**2+slices(1:4,:)**2)
     &                    /(nsl+1.d0)**2
            nsl=nsl+1
          endif
        endif
      else
        exit
      endif 
      enddo
      close(10)
c-------compute the standard deviation/rms
      sslices(1:4,:)=sqrt(abs(sslices(1:4,:)-aslices(1:4,:)**2))
 
      open(unit=11,file='meanHY.dat')
      print*,' nsl : ',nsl
      sslices(1:4,:)=sqrt(sslices(1:4,:))
      write(11,*)
     & '#  position FX_ave FY_ave FZ_ave RMS(FX) RMS(FY) RMS(FZ) '
      do nn=1,Nplanes
        write(11,333)slices(1,nn)
     &              ,aslices(2,nn),aslices(3,nn),aslices(4,nn)
     &              ,sslices(2,nn),sslices(3,nn),sslices(4,nn)
      enddo
      close(11)

      stop
  333 format(1x,7(f30.12))
      end
