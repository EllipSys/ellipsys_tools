      module TerrainSurface
      integer::nis,njs
      real(kind=8) xlo ,xhi ,ylo ,yhi ,zlo ,zhi
      real(kind=8),allocatable,dimension(:,:)::xsurf ,ysurf ,zsurf
      end module

      module vertices
      real(kind=8),allocatable,dimension(:,:,:)::x,y,z
      integer,allocatable,dimension(:,:,:)::attr
      end module

      module TerrainControl
      integer::ni=0,nj=0,nb=0
      integer::nsmooth,cdim
      integer::ismooth=0,jsmooth=0
      integer::NrXDist=0,NrYDist=0
      integer::NrHeights
      real(kind=8)::xmax_x2d,xmin_x2d,ymax_x2d,ymin_x2d
      real(kind=8)::xcent=0.d0,ycent=0.d0,alpha=270
      real(kind=8)::xbufferinl=0.d0,xbufferout=0.d0,ybuffer=0.d0
     &             ,zupstream=0.d0,zdownstream
      real(kind=8)::xbfacout=8,xbexpout=4,xbfacinl=8,xbexpinl=4
     &             ,ybfac=8,ybexp=4
      real(kind=8)::RGHlimit=1.d-3,RGHbelow=0.03,RGHabove=0.03
      logical::yperiodic=.false.,lroughness=.false.
      real(kind=8)::xlength= 10.d0,ylength= 10.d0
      real(kind=8),dimension(:,:),allocatable::XDistInp,YDistInp
      real(kind=8),dimension(:),allocatable::ExtHeight
      end module

      program main
C-----------------------------------------------------------------------
C     program for generating surface terrain
C-----------------------------------------------------------------------
      implicit none

c-------read grid control file
      call ReadGridControlFile
c-------read x2d or generate grid
      call readx2d
      call Generate2DSurf
      call transformmesh(1.d0)
c-------interpolate in hight informatioin from the surfer file
      call HightInterpolation
c-------write surface mesh
      call writemesh
c-------write tecplot file of rotate and translated surface mesh
c     call plotmesh(.true.)
      call plotmesh(.false.)
c-------interpolate in roughness information from surfer file
      call RoughnessInterpolation
c-------write roughness file
      call writeroughness
c------ determine terrain height at extract points
      call ExtractPoints
c-------write tecplot file of rotate and translated surface mesh
      call plotrough
      stop
      end

      subroutine readx2d
c----------------------------------------------------------------------
c     distribute points on surface
c----------------------------------------------------------------------
      use TerrainControl
      use vertices
      implicit none
      integer i,j,n
      integer::ndist
      real(kind=8)::xi1,xi2
   
      if(ni.ne.0)return

      open(unit=10,file='grid.x2d')
      read(10,*)ni,nb
      ni=ni+1
c-------allocate arrays
      allocate(x(ni,ni,nb),y(ni,ni,nb),z(ni,ni,nb))
      allocate(attr(ni,ni,nb))
      do n=1,nb
      do j=1,ni
      do i=1,ni
        read(10,*)attr(i,j,n),x(i,j,n),y(i,j,n)
      enddo;enddo;enddo
      x=x+xcent
      y=y+ycent
      xmax_x2d=maxval(x)
      xmin_x2d=minval(x)
      ymax_x2d=maxval(y)
      ymin_x2d=minval(y)
      z=0.d0
      close(10)

      return
      end

      subroutine transformmesh(sign)
c----------------------------------------------------------------------
c     distribute points on surface
c----------------------------------------------------------------------
      use TerrainControl
      use vertices
      implicit none
      integer i,j,n
      real(kind=8)::pi,angle,sign,xt,yt
      real(kind=8),dimension(2)::n1,n2

      pi=4.d0*atan(1.d0)
      angle=sign*alpha/180.d0*pi
      n1(1)= cos(angle)
      n1(2)= sin(angle)
      n2(1)=-sin(angle)
      n2(2)= cos(angle)

      do n=1,nb
      do j=1,ni
      do i=1,ni
        xt=x(i,j,n)-xcent
        yt=y(i,j,n)-ycent
        x(i,j,n)=n1(1)*xt+n1(2)*yt+xcent
        y(i,j,n)=n2(1)*xt+n2(2)*yt+ycent
      enddo;enddo
      enddo

      return
      end

      subroutine HightInterpolation
c-----------------------------------------------------------------------
c
c-----------------------------------------------------------------------
      use vertices
      use TerrainSurface
      use TerrainControl
      implicit none
      character(len=14)::id
      logical statex
      integer i,j,n,nn,istart,jstart,ifinish,jfinish
      real(kind=8),dimension(2)::n1,n2
      real(kind=8)::angle,pi
      character(len=128) filename,filename2

      print*,' ----- Performing height interpolation -----'
      z=0.d0
      filename(1:4)='surf'
      filename(7:10)='.grd'
      do n=1,99
        write(filename(5:6),'(i2.2)')n
        inquire(file=filename(1:10),exist=statex) 
        if(.not.statex)exit
        write(*,fmt="('Opening file ',a10)")filename(1:10)
   
        open(unit=10,file=filename(1:10))
c-------read input from surface file
        read(10,21) id
c-------read number of grid lines in x and y
        read(10,*) nis,njs
c-------read min and max value in x
        read(10,*) xlo,xhi
c-------read min and max value in y
        read(10,*) ylo,yhi
c-------read min and max value in z
        read(10,*) zlo,zhi
        write(*,"(' Ni, Xlo, Xhi, Xcent : ',i6,3f16.4)")
     &       nis,xlo,xhi,.5*(xlo+xhi)
        write(*,"(' Nj, Ylo, Yhi, Ycent : ',i6,3f16.4)")
     &       njs,ylo,yhi,.5*(ylo+yhi)

        allocate(xsurf(nis,njs),ysurf(nis,njs),zsurf(nis,njs))
        read(10,*)((zsurf(i,j),i=1,nis),j=1,njs)
        close(10)
c-------compute xsurf and ysurf coordinats
        do j=1,njs
        do i=1,nis
          xsurf(i,j)=real(i-1)/real(nis-1)*(xhi-xlo)+xlo
          ysurf(i,j)=real(j-1)/real(njs-1)*(yhi-ylo)+ylo
        enddo;enddo

c       call plotsurfdat(10,filename)
        filename2(1:7)=filename(1:7)
        filename2(8:10)='xyz'
        call plotxyz(nis,njs,1,xsurf,ysurf,zsurf,10,filename2)

c------- interpolate surface points
        if(n.eq.1)then
          do nn=1,nb
          do j=1,ni
          do i=1,ni
            call IntpolBuffer(x(i,j,nn),y(i,j,nn),z(i,j,nn))
          enddo;enddo;enddo
        else
          do nn=1,nb
          do j=1,ni
          do i=1,ni
            call IntpolInternal(x(i,j,nn),y(i,j,nn),z(i,j,nn))
          enddo;enddo;enddo
        endif
        deallocate(xsurf,ysurf,zsurf)
          
      enddo
   21 format(a14)
      end

      subroutine RoughnessInterpolation
c-----------------------------------------------------------------------
c
c-----------------------------------------------------------------------
      use vertices
      use TerrainSurface
      use TerrainControl
      implicit none
      character(len=14)::id
      logical statex
      integer i,j,n,nn,istart,jstart,ifinish,jfinish
      real(kind=8),dimension(2)::n1,n2
      real(kind=8)::angle,pi
      character(len=128) filename
                                                                                                                   
      print*,' ----- Performing roughness interpolation ----- '
      filename(1:5)='rough'
      filename(8:11)='.grd'
      do n=1,99
        write(filename(6:7),'(i2.2)')n
        inquire(file=filename(1:11),exist=statex)
        if(.not.statex)exit
        write(*,fmt="('Opening file ',a11)")filename(1:11)
        lroughness=.true.

        open(unit=10,file=filename(1:11))
c-------read input from surface file
        read(10,21) id
c-------read number of grid lines in x and y
        read(10,*) nis,njs
c-------read min and max value in x
        read(10,*) xlo,xhi
c-------read min and max value in y
        read(10,*) ylo,yhi
c-------read min and max value in z
        read(10,*) zlo,zhi
        write(*,"(' Ni, Xlo, Xhi, Xcent : ',i6,3f16.4)")
     &       nis,xlo,xhi,.5*(xlo+xhi)
        write(*,"(' Nj, Ylo, Yhi, Ycent : ',i6,3f16.4)")
     &       njs,ylo,yhi,.5*(ylo+yhi)
                                                                                                                   
        allocate(xsurf(nis,njs),ysurf(nis,njs),zsurf(nis,njs))
        do j=1,njs
          read(10,*)(zsurf(i,j),i=1,nis)
        enddo
        close(10)
        print*,' finished reading ' ,filename(1:11)
c-------compute xsurf and ysurf coordinats
        do j=1,njs
        do i=1,nis
          xsurf(i,j)=real(i-1)/real(nis-1)*(xhi-xlo)+xlo
          ysurf(i,j)=real(j-1)/real(njs-1)*(yhi-ylo)+ylo
        enddo;enddo
                                                                                                                   
        call plotsurfdat(11,filename)
                                                                                                                   
c------- interpolate surface points
        if(n.eq.1)then
          do nn=1,nb
          do j=1,ni
          do i=1,ni
            call Intpol(x(i,j,nn),y(i,j,nn),z(i,j,nn))
          enddo;enddo;enddo
        else
          do nn=1,nb
          do j=1,ni
          do i=1,ni
            call IntpolInternal(x(i,j,nn),y(i,j,nn),z(i,j,nn))
          enddo;enddo;enddo
        endif
        deallocate(xsurf,ysurf,zsurf)
      enddo
c-------dummy roughness
      if(.not.lroughness)then
        print*,' Making approximate roughness file !!! '
        do n=1,nb
        do j=1,ni
        do i=1,ni
          if(z(i,j,n).lt.RGHlimit)then
            z(i,j,n)=RGHbelow
          else
            z(i,j,n)=RGHabove
          endif
        enddo;enddo;enddo
      lroughness=.true.
      endif
   21 format(a14)
      end

      subroutine ExtractPoints
c-----------------------------------------------------------------------
c
c-----------------------------------------------------------------------
      use vertices
      use TerrainSurface
      use TerrainControl
      implicit none
      character(len=14)::id
      logical statex
      integer i,j,n,nn,istart,jstart,ifinish,jfinish,npoints,inam
      real(kind=8),dimension(2)::n1,n2
      real(kind=8)::angle,pi,xtmp,ytmp,ztmp
      character(len=128) filename
      real(kind=8),dimension(:),allocatable::xpoint,ypoint,zpoint
      character(len=30),dimension(:),allocatable::posnam

      print*,' ----- Computing Position of Extract Points ----- '
      filename(1:17)='extractpoints.dat'
      inquire(file=filename(1:17),exist=statex)
      if(.not.statex)return
      open(unit=10,file=filename(1:17))
      read(10,*)npoints
      allocate(xpoint(npoints),ypoint(npoints),zpoint(npoints)
     &        ,posnam(npoints))
      posnam(1:npoints)='position'
      zpoint=0.d0

      pi=4.d0*atan(1.d0)
      angle=-alpha/180.d0*pi
      n1(1)= cos(angle)
      n1(2)= sin(angle)
      n2(1)=-sin(angle)
      n2(2)= cos(angle)
      do n=1,npoints
        read(10,*)xtmp,ytmp,ztmp,posnam(n)
c       xpoint(n)=n1(1)*(xtmp-xcent)+n1(2)*(ytmp-ycent)
c       ypoint(n)=n2(1)*(xtmp-xcent)+n2(2)*(ytmp-ycent)
        xpoint(n)=xtmp
        ypoint(n)=ytmp
      enddo

      close(10)

      filename(1:4)='surf'
      filename(7:10)='.grd'
      do n=1,99
        write(filename(5:6),'(i2.2)')n
        write(*,*)filename(1:10)
        inquire(file=filename(1:10),exist=statex)
        if(.not.statex)exit
        write(*,fmt="('Opening file ',a10)")filename(1:10)

        open(unit=10,file=filename(1:10))
c-------read input from surface file
        read(10,21) id
c-------read number of grid lines in x and y
        read(10,*) nis,njs
c-------read min and max value in x
        read(10,*) xlo,xhi
c-------read min and max value in y
        read(10,*) ylo,yhi
c-------read min and max value in z
        read(10,*) zlo,zhi
        write(*,"(' Ni, Xlo, Xhi, Xcent : ',i6,3f16.4)")
     &       nis,xlo,xhi,.5*(xlo+xhi)
        write(*,"(' Nj, Ylo, Yhi, Ycent : ',i6,3f16.4)")
     &       njs,ylo,yhi,.5*(ylo+yhi)

        allocate(xsurf(nis,njs),ysurf(nis,njs),zsurf(nis,njs))
        do j=1,njs
          read(10,*)(zsurf(i,j),i=1,nis)
        enddo
c-------compute xsurf and ysurf coordinats
        do j=1,njs
        do i=1,nis
          xsurf(i,j)=real(i-1)/real(nis-1)*(xhi-xlo)+xlo
          ysurf(i,j)=real(j-1)/real(njs-1)*(yhi-ylo)+ylo
        enddo;enddo

c------- interpolate surface points
        if(n.eq.1)then
          do nn=1,npoints
            call IntpolBuffer(xpoint(nn),ypoint(nn),zpoint(nn))
          enddo
        else
          do nn=1,npoints
            call IntpolInternal(xpoint(nn),ypoint(nn),zpoint(nn))
          enddo
        endif
        deallocate(xsurf,ysurf,zsurf)

      enddo

      filename(1:4)='ext-'
      write(filename(5:7),'(i3.3)')int(alpha+270)
      filename(8:11)='.dat'
      open(unit=10,file=filename(1:11))
      do n=1,npoints
        do i=1,NrHeights
         write(16,'(3f33.16)')xtmp,ytmp,zpoint(n)+ExtHeight(i)
         write(10,'(3f33.16)')xpoint(n),ypoint(n),zpoint(n)+ExtHeight(i)
        enddo
      enddo
      close(10)
      close(11)

      open(unit=10,file='fvparticles.fvp')
      write(10,'(a15)')'FVPARTICLES 2 1'
      write(10,'(a9)')'Tag Names'
      write(10,'(i6)')npoints
      do n=1,npoints
        write(10,'(a30)')posnam(n)
      enddo
      write(10,'(a14)')'Variable Names'
      write(10,'(i3)')1
      write(10,'(a14)')'Terrain_Height'
      write(10,'(i3)')2
      write(10,'(i3)')1
      write(10,'(4f28.10)')xpoint(1),ypoint(1),zpoint(1)+50,zpoint(1)
      write(10,'(4f28.10)')xpoint(1),ypoint(1),zpoint(1)+50,zpoint(1)
      do n=2,npoints
      write(10,'(i3)')1
      write(10,'(i3)')n
      write(10,'(4f28.10)')xpoint(n),ypoint(n),zpoint(n)+50,zpoint(n)
      enddo
      close(10)

      filename(1:4)='ext-'
c-------in actual coordinats computation coordinats
      write(filename(5:7),'(i3.3)')int(alpha+270)
      filename(8:11)='.xyz'
      inam=11
      call plotxyz(npoints,1,1,xpoint,ypoint,zpoint
     &            ,inam,filename(1:11))


      return
   21 format(a14)
      end



      subroutine IntpolBuffer(xval,yval,value)
c----------------------------------------------------------------------
c     Interpolate between information from surfer file.
c     assumes computation with flow in the transformed x-direction
c
c----------------------------------------------------------------------
      use TerrainSurface
      use TerrainControl
      implicit none
      integer i,j,n,ii,jj
      real(kind=8)::l1,l2,factor,xval,yval,value
      real(kind=8),dimension(2)::n1,n2
      real(kind=8)::angle,pi,dx,dy,dx2,dy2,xtmp,ytmp,ztmp
      real(kind=8)::xtest,ytest
      real(KIND=8)::xxdirmin,yxdirmin,xxdirmax,yxdirmax
      real(KIND=8)::xydirmin,yydirmin,xydirmax,yydirmax

c-------rotate the point back to the non-rotate x,y coordinat system
      pi=4.d0*atan(1.d0)
      angle=-alpha/180.d0*pi
      n1(1)= cos(angle)
      n1(2)= sin(angle)
      n2(1)=-sin(angle)
      n2(2)= cos(angle)
   
      xtest=n1(1)*(xval-xcent)+n1(2)*(yval-ycent)+xcent
      ytest=n2(1)*(xval-xcent)+n2(2)*(yval-ycent)+ycent

      angle= alpha/180.d0*pi
      n1(1)= cos(angle)
      n1(2)= sin(angle)
      n2(1)=-sin(angle)
      n2(2)= cos(angle)
c-----compute the limits of the smoothing domain
c-------x-dir
      xxdirmin=n1(1)*(xmin_x2d-xcent)+n1(2)*(ytest-ycent)+xcent
      yxdirmin=n2(1)*(xmin_x2d-xcent)+n2(2)*(ytest-ycent)+ycent
      xxdirmax=n1(1)*(xmax_x2d-xcent)+n1(2)*(ytest-ycent)+xcent
      yxdirmax=n2(1)*(xmax_x2d-xcent)+n2(2)*(ytest-ycent)+ycent
c-------y-dir
      xydirmin=n1(1)*(xtest-xcent)+n1(2)*(ymin_x2d-ycent)+xcent
      yydirmin=n2(1)*(xtest-xcent)+n2(2)*(ymin_x2d-ycent)+ycent
      xydirmax=n1(1)*(xtest-xcent)+n1(2)*(ymax_x2d-ycent)+xcent
      yydirmax=n2(1)*(xtest-xcent)+n2(2)*(ymax_x2d-ycent)+ycent
    

c-----interpolate grid surface
      dx=(xhi-xlo)/real(nis-1)
      dy=(yhi-ylo)/real(njs-1)
        xtmp=xval
        ytmp=yval
        xtmp=min(max(xtmp,xlo),xhi)
        ytmp=min(max(ytmp,ylo),yhi)
        ii=int((xtmp-xlo)/dx)+1
        jj=int((ytmp-ylo)/dy)+1
        l1=(xtmp-xsurf(ii,jj))
     &    /(xsurf(ii+1,jj)-xsurf(ii,jj))
        l2=(ytmp-ysurf(ii,jj))
     &    /(ysurf(ii,jj+1)-ysurf(ii,jj))
        value=(1-l1)*(1-l2)*zsurf(ii           ,jj  )
     &       +l1*(1-l2)*    zsurf(min(ii+1,nis),jj  )
     &       +(1-l1)*l2*    zsurf(ii           ,min(jj+1,njs))
     &       +l1*l2*        zsurf(min(ii+1,nis),min(jj+1,njs))
c---------- max y-buffer layer
        if(ytest.gt.ymax_x2d-ybuffer)then
          xtmp=xydirmax
          ytmp=yydirmax
          xtmp=min(max(xtmp,xlo),xhi)
          ytmp=min(max(ytmp,ylo),yhi)
          ii=int((xtmp-xlo)/dx)+1
          jj=int((ytmp-ylo)/dy)+1
          l1=(xtmp-xsurf(ii,jj))
     &      /(xsurf(ii+1,jj)-xsurf(ii,jj))
          l2=(ytmp-ysurf(ii,jj))
     &      /(ysurf(ii,jj+1)-ysurf(ii,jj))
          ztmp  =(1-l1)*(1-l2)*zsurf(ii           ,jj  )
     &          +l1*(1-l2)*    zsurf(min(ii+1,nis),jj  )
     &          +(1-l1)*l2*    zsurf(ii           ,min(jj+1,njs))
     &          +l1*l2*        zsurf(min(ii+1,nis),min(jj+1,njs))
          factor=(ytest-ymax_x2d+ybuffer)/ybuffer
          factor=tanh(ybfac*factor**ybexp)
          value=value*(1-factor)+factor*ztmp
        endif
c---------- min y-buffer layer
        if(ytest.lt.ymin_x2d+ybuffer)then
          if(.not.yperiodic)then
            xtmp=xydirmin
            ytmp=yydirmin
          else
            xtmp=xydirmax
            ytmp=yydirmax
          endif
          xtmp=min(max(xtmp,xlo),xhi)
          ytmp=min(max(ytmp,ylo),yhi)
          ii=int((xtmp-xlo)/dx)+1
          jj=int((ytmp-ylo)/dy)+1
          l1=(xtmp-xsurf(ii,jj))
     &      /(xsurf(ii+1,jj)-xsurf(ii,jj))
          l2=(ytmp-ysurf(ii,jj))
     &      /(ysurf(ii,jj+1)-ysurf(ii,jj))
          ztmp  =(1-l1)*(1-l2)*zsurf(ii           ,jj  )
     &          +l1*(1-l2)*    zsurf(min(ii+1,nis),jj  )
     &          +(1-l1)*l2*    zsurf(ii           ,min(jj+1,njs))
     &          +l1*l2*        zsurf(min(ii+1,nis),min(jj+1,njs))
          factor=(ymin_x2d+ybuffer-ytest)/ybuffer
          factor=tanh(ybfac*factor**ybexp)
          value=value*(1-factor)+factor*ztmp
        endif
c--------- min x-buffer layer
        if(xtest.lt.xmin_x2d+xbufferinl)then
          ztmp=zupstream
          factor=((xmin_x2d+xbufferinl-xtest)/xbufferinl)
          factor=tanh(xbfacinl*factor**xbexpinl)
          value=value*(1-factor)+ztmp*factor
        endif
c--------- max x-buffer layer
        if(xtest.gt.xmax_x2d-xbufferout)then
          ztmp=zdownstream
          factor=((xtest-xmax_x2d+xbufferout)/xbufferout)
          factor=tanh(xbfacout*factor**xbexpout)
          value=value*(1-factor)+ztmp*factor
        endif
      return
      end

      subroutine intpol(xval,yval,value)
c----------------------------------------------------------------------
c     Interpolate between information from additional surfer file.
c     interpolation is only done in the area inside the smoothing 
c     buffers
c
c----------------------------------------------------------------------
      use TerrainSurface
      use TerrainControl
      implicit none
      integer i,j,n,ii,jj
      real(kind=8)::l1,l2,factor,xmin,ymin,xmax,ymax
      real(kind=8),dimension(2)::n1,n2
      real(kind=8)::angle,pi,dx,dy,dx2,dy2,xtmp,ytmp,ztmp
     &              ,xval,yval,value

      pi=4.d0*atan(1.d0)
      angle=alpha/180.d0*pi
      n1(1)= cos(angle)
      n1(2)= sin(angle)
      n2(1)=-sin(angle)
      n2(2)= cos(angle)
      xmin=xmin_x2d+xbufferinl
      xmax=xmax_x2d-xbufferout
      ymin=ymin_x2d+ybuffer
      ymax=ymax_x2d-ybuffer

c-----interpolate grid surface
      dx=(xhi-xlo)/real(nis-1)
      dy=(yhi-ylo)/real(njs-1)
c       xtmp=n1(1)*xval+n1(2)*yval+xcent
c       ytmp=n2(1)*xval+n2(2)*yval+ycent
        xtmp=xval
        ytmp=yval
        xtmp=min(max(xtmp,xlo),xhi)
        ytmp=min(max(ytmp,ylo),yhi)
        ii=int((xtmp-xlo)/dx)+1
        jj=int((ytmp-ylo)/dy)+1
        l1=(xtmp-xsurf(ii,jj))
     &     /(xsurf(ii+1,jj)-xsurf(ii,jj))
        l2=(ytmp-ysurf(ii,jj))
     &     /(ysurf(ii,jj+1)-ysurf(ii,jj))
         value=(1-l1)*(1-l2)*zsurf(ii  ,jj  )
     &        +l1*(1-l2)*    zsurf(ii+1,jj  )
     &        +(1-l1)*l2*    zsurf(ii  ,jj+1)
     &        +l1*l2*        zsurf(ii+1,jj+1)
      return
      end

      subroutine IntpolInternal(xval,yval,value)
c----------------------------------------------------------------------
c     Interpolate between information from additional surfer file.
c     interpolation is only done in the area inside the smoothing
c     buffers
c
c----------------------------------------------------------------------
      use TerrainSurface
      use TerrainControl
      implicit none
      integer i,j,n,ii,jj
      real(kind=8)::l1,l2,factor,xmin,ymin,xmax,ymax
      real(kind=8),dimension(2)::n1,n2
      real(kind=8)::angle,pi,dx,dy,dx2,dy2,xtmp,ytmp,ztmp
      real(kind=8)::factori,factorj,xval,yval,value
                                                                                                                     
      pi=4.d0*atan(1.d0)
      angle=alpha/180.d0*pi
      n1(1)= cos(angle)
      n1(2)= sin(angle)
      n2(1)=-sin(angle)
      n2(2)= cos(angle)
      xmin=xmin_x2d+xbufferinl
      xmax=xmax_x2d-xbufferout
      ymin=ymin_x2d+ybuffer
      ymax=ymax_x2d-ybuffer

c-----interpolate grid surface
      dx=(xhi-xlo)/real(nis-1)
      dy=(yhi-ylo)/real(njs-1)
c       xtmp=n1(1)*xval+n1(2)*yval+xcent
c       ytmp=n2(1)*xval+n2(2)*yval+ycent
        xtmp=xval
        ytmp=yval
        if(xtmp.ge.xlo.and.xtmp.le.xhi.and.
     &     ytmp.ge.ylo.and.ytmp.le.yhi)then
             ii=int((xtmp-xlo)/dx)+1
             jj=int((ytmp-ylo)/dy)+1
             l1=(xtmp-xsurf(ii,jj))
     &         /(xsurf(ii+1,jj)-xsurf(ii,jj))
             l2=(ytmp-ysurf(ii,jj))
     &         /(ysurf(ii,jj+1)-ysurf(ii,jj))
             ztmp=(1-l1)*(1-l2)*zsurf(ii  ,jj  )
     &            +l1*(1-l2)*    zsurf(ii+1,jj  )
     &            +(1-l1)*l2*    zsurf(ii  ,jj+1)
     &            +l1*l2*        zsurf(ii+1,jj+1)
             factori=1;factorj=1
             if(ii.le.ismooth)then
               factori=real(ii-1)/real(ismooth-1)
             elseif(ii.ge.nis-ismooth)then
               factori=real(nis-ii)/real(ismooth-1)
             endif
             if(jj.le.jsmooth)then
               factorj=real(jj-1)/real(jsmooth-1)
             elseif(jj.ge.njs-jsmooth)then
               factorj=real(njs-jj)/real(jsmooth-1)
             endif
             factor=min(factori,factorj)
             value=ztmp*factor+value*(1-factor)
        endif
      return
      end


      subroutine ReadGridControlFile
c======================================================================
c     
c======================================================================
      use TerrainControl
      implicit none
      character(len=1) ctemp
      integer::i
      integer,parameter::mword=40
      integer nrchar(mword),nrword
      character(len=256)InputLine
      character(len=40),dimension(mword)::words
      logical::lrecogn=.false.
      open(unit=9,file='gcf.dat')

      do
      read(9,'(a256)',end=100)InputLine
      call cmdpart(InputLine,mword,nrchar,nrword,words)
C------------------------------------- Skip blank lines and comments ---
      if(nrword.le.1) then
        cycle
C---------------------------------- Skip rest of file if stop appears --
        elseif(words(1)(1:4).eq.'stop'.or.words(1)(1:4).eq.'STOP') then
        exit
c----------------------------- command InputLine interpreter -----------
      else
        call SurfGridCMD(mword,nrword,nrchar,words,lrecogn)
        if(.not.lrecogn)then
          print*,' Keyword not recognized ! '
          print*,' # ',InputLine
          stop
        endif
      endif
      enddo
  100 call SurfGridDef
      return
      end

      subroutine SurfGridCMD(mword,nrword,nrchar,words,lrecogn) 
c======================================================================
c     
c======================================================================
      use TerrainControl
      implicit none
      integer::i
      integer::mword
      integer::nrchar(mword),nrword
      character(len=40),dimension(mword)::words
      logical::lrecogn

      lrecogn=.false.
      select case(words(1)(1:nrchar(1)))
C------------------------------ Keyword not recognized -----------------
        case default
        lrecogn=.false.
c------------------------------ number of mesh point in ksi and eta ----
        case('horizontal_points')
          lrecogn=.true.
          if(nrword.ge.2)read(words(2),*)ni
          if(nrword.ge.3)read(words(3),*)nj
c-----   distribution function x-direction
c-----   with the format  number of points np followed by np triplets of (l/lmax delta n/np) 
        case('horizontal_xdist')
          lrecogn=.true.
          read(words(2),*)NrXDist
          allocate(XDistInp(NrXDist,3))
          do i=1,NrXDist
            read(words(3+(i-1)*3),*)XDistInp(i,1)
            read(words(4+(i-1)*3),*)XDistInp(i,2)
            read(words(5+(i-1)*3),*)XDistInp(i,3)
          enddo
c-----   distribution function y-direction
c-----   with the format  number of points np followed by np triplets of (l/lmax delta n/np) 
        case('horizontal_ydist')
          lrecogn=.true.
          read(words(2),*)NrYDist
          allocate(YDistInp(NrYDist,3))
          do i=1,NrYDist
            read(words(3+(i-1)*3),*)YDistInp(i,1)
            read(words(4+(i-1)*3),*)YDistInp(i,2)
            read(words(5+(i-1)*3),*)YDistInp(i,3)
          enddo
C----------------------------- Horizontal Domain Size  -----------------
        case('horizontal_domain_size')
          lrecogn=.true.
          read(words(2),*)xlength
          read(words(3),*)ylength
C------------------------------ flow angle         ---------------------
        case('flow_angle')
          lrecogn=.true.
          if(nrword.ge.2)read(words(2),*)alpha
          alpha=alpha-270
C------------------------------ coordinat centre   ---------------------
        case('domain_centre')
          lrecogn=.true.
          if(nrword.ge.2)read(words(2),*)xcent
          if(nrword.ge.3)read(words(3),*)ycent
C------------------------------ buffer layer in ydirection--------------
        case('buffer_layer_y-direction')
          lrecogn=.true.
          if(nrword.ge.2)read(words(2),*)ybuffer
        case('buffer_factor_y-direction')
          lrecogn=.true.
          if(nrword.ge.2)read(words(2),*)ybfac
        case('buffer_exponent_y-direction')
          lrecogn=.true.
          if(nrword.ge.2)read(words(2),*)ybexp
        case('periodic_y-direction')
          lrecogn=.true.
          if(nrword.ge.2)yperiodic=words(2)(1:nrchar(2)).eq.'true'
C------------------------------ buffer layer in xdirection--------------
        case('buffer_layer_inlet')
          lrecogn=.true.
          if(nrword.ge.2)read(words(2),*)xbufferinl
        case('buffer_factor_inlet')
          lrecogn=.true.
          if(nrword.ge.2)read(words(2),*)xbfacinl
        case('buffer_exponent_inlet')
          lrecogn=.true.
          if(nrword.ge.2)read(words(2),*)xbexpinl
C------------------------------ buffer layer in xdirection--------------
        case('buffer_layer_outlet')
          lrecogn=.true.
          if(nrword.ge.2)read(words(2),*)xbufferout
        case('buffer_factor_outlet')
          lrecogn=.true.
          if(nrword.ge.2)read(words(2),*)xbfacout
        case('buffer_exponent_outlet')
          lrecogn=.true.
          if(nrword.ge.2)read(words(2),*)xbexpout
C------------------------------ buffer layer in xdirection--------------
        case('terrain_height')
          lrecogn=.true.
          if(nrword.ge.2)read(words(2),*)zupstream
          zdownstream=zupstream
        case('terrain_height_upstream')
          lrecogn=.true.
          if(nrword.ge.2)read(words(2),*)zupstream
        case('terrain_height_downstream')
          lrecogn=.true.
          if(nrword.ge.2)read(words(2),*)zdownstream
        case('smooth_overlap')
          lrecogn=.true.
          if(nrword.ge.2)read(words(2),*)ismooth
          if(nrword.ge.3)read(words(2),*)jsmooth
c-----   extraction heights 
c-----   with the format  number of heights followed by the levels
        case('extraction_heights')
          lrecogn=.true.
          read(words(2),*)NrHeights
          allocate(ExtHeight(NrHeights))
          do i=1,NrHeights
            read(words(3+(i-1)),*)ExtHeight(i)
          enddo
c-----   simple roughness specification
        case('roughness')
          lrecogn=.true.
          read(words(2),*)RGHlimit
          read(words(3),*)RGHbelow
          read(words(4),*)RGHabove
        end select
      return
      end


      subroutine SurfGridDEF
c======================================================================
c     Set default parameters for horizontal grid     
c======================================================================
      use TerrainControl
      implicit none

      if(NrXDist.eq.0.and.ni.ne.0)then
        NrXDist=2
        allocate(XDistInp(2,3))
        XDistInp(1,1)=0.d0
        XDistInp(1,2)=1.d0/real(ni)
        XDistInp(1,3)=1.d0/real(ni)
        XDistInp(2,1)=1.d0
        XDistInp(2,2)=1.d0/real(ni)
        XDistInp(2,3)=1.d0
      endif
      if(NrYDist.eq.0.and.nj.ne.0)then
        NrYDist=2
        allocate(YDistInp(2,3))
        YDistInp(1,1)=0.d0
        YDistInp(1,2)=1.d0/real(nj)
        YDistInp(1,3)=1.d0/real(nj)
        YDistInp(2,1)=1.d0
        YDistInp(2,2)=1.d0/real(nj)
        YDistInp(2,3)=1.d0
      endif

      return
      end

      subroutine cmdpart(InputLine,mword,nrchar,nrword,words)
c-----------------------------------------------------------------------
c     routine parting command InputLine into words
c----------------------------------------------------------------------
c
c     Author: Jess A. Michelsen and Niels N. Soerensen
c     last revision :march 3, 1998
c-----------------------------------------------------------------------
      implicit none
      integer mword,nrchar(mword),nrword,i
      integer,parameter::mchar=256
      character(len=40),dimension(mword):: words
      character(len=40)::blank
      character(len=256) InputLine
      integer CurrentPosition,RelativeBlankPosition,BlankPosition
C--------------------------------------------- Reset word counter ------
      nrword=0
      CurrentPosition=1
C--------------------------------------------- Skip if a comment line --
      if(index(InputLine(1:mchar),'#').gt.0) return
C---------------------------------------- find distance to next blank --
   20 RelativeBlankPosition=index(InputLine(CurrentPosition:mchar),' ')
      if(CurrentPosition.eq.mchar.or.RelativeBlankPosition.eq.0) return
C---------------------------------------- found the end of a word ------
      if(RelativeBlankPosition.gt.1.and.RelativeBlankPosition.le.40)then
        BlankPosition=CurrentPosition-1+RelativeBlankPosition
        nrword=nrword+1
        if(nrword.gt.mword)return
        words(nrword)=InputLine(CurrentPosition:BlankPosition)
        nrchar(nrword)=RelativeBlankPosition-1
      endif
c-----------------------------------------------------------------------
      CurrentPosition=CurrentPosition+RelativeBlankPosition
      goto 20
c-----------------------------------------------------------------------
      end


      subroutine WriteMesh
      use vertices
      use TerrainControl
      implicit none
      integer i,j,n,dim,istart,jstart,nbi,nbm,ii,jj,io,jo,yesno
      real(kind=8) rest
      character(len=50)::filename
     

      dim=ni-1
      print*,'*******************************************************'
      print*,'size of multidomaine blocks         : ',dim
      print*,'want to change this yes = 1, no = 2   '
      read(*,*) yesno
      if(yesno.eq.1)then
        print*,'give new dimension  '
        read(*,*)dim
      endif
      cdim=dim
      nbi=(ni-1)/dim
      nbm=nbi**2*nb
      print*,'*******************************************************'
      print*,'size of multidomaine blocks         : ',dim
      print*,'total number of blocks              : ',nbm
      print*,'*******************************************************'

      filename(1:5)='grid-'
      write(filename(6:8),'(i3.3)')int(alpha+270)
      filename(9:12)='.X2D'
      open(unit=10,file=filename(1:12))
      write(10,*) dim,nbm
      do n=1,nb
      do jj=1,nbi;jstart=(jj-1)*dim
      do ii=1,nbi;istart=(ii-1)*dim
        do j=1,dim+1;jo=jstart+j
        do i=1,dim+1;io=istart+i
          write(10,'(i8,3e25.16)')attr(io,jo,n)
     &                 ,x(io,jo,n)
     &                 ,y(io,jo,n)
     &                 ,z(io,jo,n)
        enddo;enddo
      enddo;enddo
      enddo
      close(10)

      return
      end

                                                                                                            
      subroutine WriteRoughness
      use vertices
      use TerrainControl
      implicit none
      integer i,j,n,dim,istart,jstart,nbi,nbm,ii,jj,io,jo,yesno
      real(kind=8) rest
      character(len=50)::filename
                                                                                                                     
      if(.not.lroughness)return
                   
                                                                                                                     
      dim=cdim
      nbi=(ni-1)/dim
      nbm=nbi**2*nb
                                                                                                                     
      filename(1:5)='grid-'
      write(filename(6:8),'(i3.3)')int(alpha+270)
      filename(9:12)='.ROU'
      open(unit=10,file=filename(1:12))
      write(10,*) dim,nbm
      do n=1,nb
      do jj=1,nbi;jstart=(jj-1)*dim
      do ii=1,nbi;istart=(ii-1)*dim
        do j=1,dim+1;jo=jstart+j
        do i=1,dim+1;io=istart+i
          write(10,'(3e25.16)')x(io,jo,n)
     &                        ,y(io,jo,n)
     &                        ,z(io,jo,n)
        enddo;enddo
      enddo;enddo
      enddo
      close(10)
                                                                                                                     
      return
      end

      subroutine plotsurfdat(nlen,filename)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      use TerrainSurface
      use TerrainControl
      implicit none
      integer i,j,n,nc,ncm,nlen
      character(len=10)filename,filename2
      character(len=128),dimension(4)::fieldname
      real(kind=8),dimension(:,:,:),allocatable::var

      filename2(1:nlen-3)=filename(1:nlen-3)
      filename2(nlen-2:nlen)='tec'

      fieldname(1)(1:128)='X'
      fieldname(2)(1:128)='Y'
      fieldname(3)(1:128)='Z'
      fieldname(4)(1:128)='ATTRIBUTE'
      ncm=4

      allocate(var(4,nis,njs))
      var(1,:,:)=xsurf(:,:)
      var(2,:,:)=ysurf(:,:)
      var(3,:,:)=zsurf(:,:)
      var(4,:,:)=1.d0

      open(unit=12,file=filename2(1:nlen))
c-----make tecplot ascii data file
  777 write(12,*)'TITLE = SURFACE-FILE'
      write(12,31)(fieldname(nc)(1:19),nc=1,ncm)

c-----write the individual blocks (zones in tecplot terminologi)
      write(12,32)nis,njs,1
c-------write the individual variables
      do nc=1,ncm
      do j=1,njs
      do i=1,nis
        write(12,*)var(nc,i,j)
      enddo;enddo;enddo
      close(12)

      deallocate(var)

      return
   30 format('')
   31 format('VARIABLES = ',15(A20,','))
   32 format('ZONE I=',i6,', J=',i6,', K=',i6,' F=BLOCK')
      end
 
      subroutine plotmesh(transform)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      use vertices
      use TerrainControl
      implicit none
      logical transform,ltec
      integer i,j,n,nc,ncm,nlen,inam
      character(len=17)cformat
      character(len=128)::filename
      character(len=128),dimension(4)::fieldname
      real(kind=8),dimension(:,:,:,:),allocatable::var
      real(kind=8),dimension(2)::n1,n2
      real(kind=8)::angle,pi,xtmp,ytmp,ztmp

      ltec=.false.

      fieldname(1)(1:128)='X'
      fieldname(2)(1:128)='Y'
      fieldname(3)(1:128)='Z'
      ncm=3

      pi=4.d0*atan(1.d0)
      angle=(alpha)/180.d0*pi
      n1(1)= cos(angle)
      n1(2)= sin(angle)
      n2(1)=-sin(angle)
      n2(2)= cos(angle)

      allocate(var(3,ni,ni,nb))
      if(transform)then
        var(1,:,:,:)=n1(1)*x(:,:,:)+n1(2)*y(:,:,:)+xcent
        var(2,:,:,:)=n2(1)*x(:,:,:)+n2(2)*y(:,:,:)+ycent
        var(3,:,:,:)=z(:,:,:)
      else
        var(1,:,:,:)=x(:,:,:)
        var(2,:,:,:)=y(:,:,:)
        var(3,:,:,:)=z(:,:,:)
      endif

      if(ltec)then
      if(transform)then
        open(unit=12,file='grid.tec')
        write(12,*)'TITLE = rotated and translated'
      else
        open(unit=12,file='grid-o.tec')
        write(12,*)'TITLE = not transformed'
      endif
c-----make tecplot ascii data file
      write(12,31)(fieldname(nc)(1:19),nc=1,ncm)
c-----write the individual blocks in tecplot format
      do n=1,nb
      write(12,32)ni,ni,1
c-------write the individual variables
      do nc=1,ncm
      do j=1,ni
      do i=1,ni
        write(12,*)var(nc,i,j,n)
      enddo;enddo;enddo
      enddo
      close(12)
      else
c-----write the individual blocks in fieldview format
      if(transform)then
        inam=12
        filename(1:5)='grid-'
        write(filename(6:8),'(i3.3)')int(alpha+270)
        filename(9:12)='.xyz'
      else
        filename(1:10)='grid-comp-'
        write(filename(11:13),'(i3.3)')int(alpha+270)
        filename(14:17)='.xyz'
        inam=17
      endif
        call plotxyz(ni,ni,nb,var(1,:,:,:),var(2,:,:,:),var(3,:,:,:)
     &              ,inam,filename(1:inam))         
      endif

      deallocate(var)
      return
   30 format('')
   31 format('VARIABLES = ',15(A20,','))
   32 format('ZONE I=',i6,', J=',i6,', K=',i6,' F=BLOCK')
      end

      subroutine plotrough
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      use vertices
      use TerrainControl
      implicit none
      integer i,j,n,nc,ncm,nlen
      character(len=128),dimension(4)::fieldname
      real(kind=8),dimension(:,:,:,:),allocatable::var
      real(kind=8),dimension(2)::n1,n2
      real(kind=8)::angle,pi,xtmp,ytmp,ztmp


      fieldname(1)(1:128)='X'
      fieldname(2)(1:128)='Y'
      fieldname(3)(1:128)='Roughness'
      ncm=3

      pi=4.d0*atan(1.d0)
      angle=(alpha)/180.d0*pi
      n1(1)= cos(angle)
      n1(2)= sin(angle)
      n2(1)=-sin(angle)
      n2(2)= cos(angle)

      allocate(var(3,ni,ni,nb))
c     var(1,:,:,:)=n1(1)*x(:,:,:)+n1(2)*y(:,:,:)+xcent
c     var(2,:,:,:)=n2(1)*x(:,:,:)+n2(2)*y(:,:,:)+ycent
c     var(3,:,:,:)=z(:,:,:)
      var(1,:,:,:)=x(:,:,:)
      var(2,:,:,:)=y(:,:,:)
      var(3,:,:,:)=z(:,:,:)

      open(unit=12,file='rough.tec')
c-----make tecplot ascii data file
  777 write(12,*)'TITLE = roughness data'
      write(12,31)(fieldname(nc)(1:19),nc=1,ncm)

c-----write the individual blocks (zones in tecplot terminologi)
      do n=1,nb
      write(12,32)ni,ni,1
c-------write the individual variables
      do nc=1,ncm
      do j=1,ni
      do i=1,ni
        write(12,*)var(nc,i,j,n)
      enddo;enddo;enddo
      enddo
      close(12)


      deallocate(var)

      return
   30 format('')
   31 format('VARIABLES = ',15(A20,','))
   32 format('ZONE I=',i6,', J=',i6,', K=',i6,' F=BLOCK')
      end

      subroutine plotxyz(ni,nj,nb,x,y,z,inam,fname)
      implicit none
      integer ni,nj,nb,inam,i,j,n
      character(len=inam)::fname
      real(kind=8),dimension(ni,nj,nb)::x,y,z
      open(unit=10,file=fname(1:inam))
      write(10,*)nb
      write(10,*)(ni,nj,1,n=1,nb)
      do n=1,nb
      write(10,*)((x(i,j,n),i=1,ni),j=1,nj)
     &          ,((y(i,j,n),i=1,ni),j=1,nj)
     &          ,((z(i,j,n),i=1,ni),j=1,nj)
      enddo
      close(10)
      return
      end
 
      subroutine Generate2DSurf
C-----------------------------------------------------------------------
C     Program generating cartesian 2d grid surface
C-----------------------------------------------------------------------
      use TerrainControl
      use vertices
      implicit none
      integer i,j,n,ii,jj,dim,yesno,nbi,nbj,nbm
     &       ,istart,jstart,io,jo
      real(kind=8)::rest
      real(kind=8),allocatable,dimension(:)::fxs,fys
      real(kind=8),allocatable,dimension(:,:)::xh,yh
      integer,allocatable,dimension(:,:)::attrh
    
      if(allocated(x))return
      allocate(fxs(ni),fys(nj))
      if(NrXDist.ne.0)then
        XDistInp(:,3)=XDistInp(:,3)*ni
        XDistInp(1,3)=1
        call DistFunc(NrXDist,XDistInp,1,ni,fxs)
      endif
      if(NrYDist.ne.0)then
        YDistInp(:,3)=YDistInp(:,3)*nj
        YDistInp(1,3)=1
        call DistFunc(NrYDist,YDistInp,1,nj,fys)
      endif
      allocate(xh(ni,nj),yh(ni,nj),attrh(ni,nj))
      do j=1,nj
      do i=1,ni
        xh(i,j)=fxs(i)*xlength+xcent-xlength*.5d0
        yh(i,j)=fys(j)*ylength+ycent-ylength*.5d0
      enddo;enddo
      attrh=1
      attrh(1 ,: )=101
      attrh(ni,: )=101
      attrh(: ,1 )=102
      attrh(: ,nj)=102
c     attrh(1 ,: )=111
c     attrh(ni,: )=111
c     attrh(: ,1 )=111
c     attrh(: ,nj)=111
      attrh(1 ,1 )=111
      attrh(1 ,nj)=111
      attrh(ni,1 )=111
      attrh(ni,nj)=111

c------split domain into multiblock
      print*,' Give domain size : '
      read*,dim
      nbi=(ni-1)/dim
      nbj=(nj-1)/dim
      ni=dim+1
      nb=nbi*nbj
      print*,'*******************************************************'
      print*,'size of multidomaine blocks         : ',dim
      print*,'number of block in xi direction     : ',nbi
      print*,'number of block in eta direction    : ',nbj
      print*,'total number of blocks              : ',nb
      print*,'*******************************************************'

c-----store grid in multiblock format
      allocate(x(ni,ni,nb),y(ni,ni,nb),z(ni,ni,nb))

      allocate(attr(ni,ni,nb))
      n=0
      do jj=1,nbj;jstart=(jj-1)*dim
      do ii=1,nbi;istart=(ii-1)*dim
        n=n+1
        do j=1,dim+1;jo=jstart+j
        do i=1,dim+1;io=istart+i
          x(i,j,n)=xh(io,jo)
          y(i,j,n)=yh(io,jo)
          attr(i,j,n)=attrh(io,jo)
        enddo;enddo
      enddo;enddo
      xmax_x2d=maxval(x)
      xmin_x2d=minval(x)
      ymax_x2d=maxval(y)
      ymin_x2d=minval(y)
    
c-------
      open(unit=10,file='cyclic.txt')
      do jj=1,nbj
        write(10,*)' x-direction-cyclic ',1+(jj-1)*nbi,1,jj*nbi,2
      enddo
      do ii=1,nbi
        write(10,*)' y-direction-cyclic ',ii,3,ii+nbi*(nbj-1),4
      enddo
      close(10)
       

      return
      end


      subroutine distfunc(nn,dinp,ival,ndist,fdist)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      implicit none
      integer::nn,ndist
      real(kind=8),dimension(ndist)::fdist,dy
      real(kind=8),dimension(nn,3)::dinp
      real*8 s0,s1,d0,d1
      real*8 len,delta1,delta2
      integer n0,n1,ns,nf
      integer i,j,ival


      fdist(1)=0.d0
      s0=dinp(1,1);d0=dinp(1,2);n0=int(dinp(1,3))
      do i=2,nn
        s1=dinp(i,1);d1=dinp(i,2);n1=int(dinp(i,3))
        len=s1-s0
        delta1=d0
        delta2=d1
        if(ival.eq.1)call tanhdist(delta1,delta2,len,n0,n1,dy(n0))
        if(ival.eq.2)call sinhdist(delta1,delta2,len,n0,n1,dy(n0))
        do j=n0+1,n1
        fdist(j)=fdist(n0)+dy(j)
        enddo
        s0=s1;d0=d1;n0=n1
      enddo
      return
   21 format('#',i6)
      end

      subroutine tanhdist(delta1,delta2,len,i1,i2,fdist)
************************************************************************
*     TANDIST
*     program for computing the hyperbolic tangent distribution
*     delta1 : grid spacing at i=i1 (first cell)
*     delta2 : grid spacing at i=i2 (last  cell)
*     len    : lenght of curve
*     i1     : indices of first vertex
*     i2     : indices of last vertex
*     fdist  : compute distribution function
*
*
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      integer i1,i2,ni,i
      real(kind=8)::delta1,delta2,len
      real(kind=8)::delta,b,a
      real(kind=8)::fdist(i2-i1+1)
      real(kind=8)::transsinh,transtanh,ftmp
      external transsinh,transtanh

      if(i2.eq.i1)return

c------- Normalize delta1 and delta2 with lenght
      delta1=delta1/len
      delta2=delta2/len
c------- Compute number of points
      ni=i2-i1

c------- Assure that B > 1
      if(delta1.le.0.d0.and.1/delta2.lt.ni)then
        delta1=1/(ni**2*delta2*1.02)
      elseif(delta2.le.0d0.and.1/delta1.lt.ni)then
        delta2=1/(ni**2*delta1*1.02)
      endif

c------- Spacing at both end specified
      if(delta1.gt.0.d0.and.delta2.gt.0.d0)then
        a=sqrt(delta2)/sqrt(delta1)
        b=1.d0/(ni*sqrt(delta1*delta2))
        if(b.ge.1.d0)then
          delta=transsinh(b)
          do i=1,ni+1
            ftmp=.5*(1+tanh(delta*(real(i-1)/real(ni)-0.5))
     &          /tanh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        else
          delta=transtanh(b)
          do i=1,ni+1
            ftmp=.5*(1+sinh(delta*(real(i-1)/real(ni)-0.5))
     &          /sinh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        endif
c------- Spacing only specified at xi=1
      else if(delta1.gt.0.d0)then
        b=1.d0/(ni*delta1)
        delta=transsinh(b)
        do i=1,ni+1
          fdist(i)=1.d0+tanh(.5d0*delta*(real(i-1)/real(ni)-1.d0))
     &            /tanh(.5d0*delta)
        enddo
c------- Spacing only specified at xi=ni
      else if(delta2.gt.0.d0)then
        b=1.d0/(ni*delta2)
        delta=transsinh(b)
        do i=1,ni+1
          fdist(i)=tanh(.5*delta*real(i-1)/real(ni))/tanh(.5*delta)
        enddo
c------- Error no spacing is given
      else
        print*,' Error from tandist, no cell hight is given '
      endif
c------- Compute len*fdist
      do i=1,ni+1
        fdist(i)=fdist(i)*len
      enddo
      return
      end

      subroutine sinhdist(delta1,delta2,len,i1,i2,fdist)
************************************************************************
*     SINHDIST
*     program for computing the hyperbolic sine distribution
*     delta1 : grid spacing at i=i1 (first cell)
*     delta2 : grid spacing at i=i2 (last  cell)
*     len    : lenght of curve
*     i1     : indices of first vertex
*     i2     : indices of last vertex
*     fdist  : compute distribution function
*
*
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      real(kind=8)::delta1,delta2,len
      real(kind=8)::delta,b,a
      real(kind=8)::fdist(*)
      real(kind=8)::transsinh,transtanh,ftmp
      external transsinh,transtanh
      integer i1,i2,ni,i

      if(i2.eq.i1)return

c------- Normalize delta1 and delta2 with lenght
      delta1=delta1/len
      delta2=delta2/len
c------- Compute number of points
      ni=i2-i1

c------- Assure that B > 1
      if(delta1.le.0.d0.and.1/delta2.lt.ni)then
        delta1=1/(ni**2*delta2*1.02)
      elseif(delta2.le.0d0.and.1/delta1.lt.ni)then
        delta2=1/(ni**2*delta1*1.02)
      endif

c------- Spacing at both end specified
      if(delta1.gt.0.d0.and.delta2.gt.0.d0)then
        a=sqrt(delta2)/sqrt(delta1)
        b=1.d0/(ni*sqrt(delta1*delta2))
        if(b.ge.1.d0)then
          delta=transsinh(b)
          do i=1,ni+1
            ftmp=.5*(1+tanh(delta*(real(i-1)/real(ni)-0.5))
     &          /tanh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        else
          delta=transtanh(b)
          do i=1,ni+1
            ftmp=.5*(1+sinh(delta*(real(i-1)/real(ni)-0.5))
     &          /sinh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        endif
c------- Spacing only specified at xi=1
      else if(delta1.gt.0.d0)then
        b=1.d0/(ni*delta1)
        delta=transsinh(b)
        do i=1,ni+1
          fdist(i)=sinh(delta*real(i-1)/real(ni))/sinh(delta)
        enddo
c------- Spacing only specified at xi=ni
      else if(delta2.gt.0.d0)then
        b=1.d0/(ni*delta2)
        delta=transsinh(b)
        do i=1,ni+1
          fdist(i)=1-sinh(delta*(1-real(i-1)/real(ni)))/sinh(delta)
        enddo
c------- Error no spacing is given
      else
        print*,' Error from tandist, no cell hight is given '
      endif
c------- Compute len*fdist
      do i=1,ni+1
        fdist(i)=fdist(i)*len
      enddo
      return
      end

      double precision function transsinh(b)
************************************************************************
*     routine for solving the transendental equation
*
*     b=sinh(delta)/delta
*
*     using NewtonRaphson
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      real(kind=8)::delta,b
      real(kind=8)::delta_old,f,df,rlim
      parameter(rlim=1.d-9)
      integer n

c------- Preforme Newton iteration to obtain delta
      delta=1.d0
      delta_old=delta
      do n=1,25
c--------- Compute f(delta)
c        f =b-sinh(delta)/delta
c        f =b*delta-sinh(delta)
         f =delta/sinh(delta)-1/b
c--------- Compute d/d(delta)(f(delta))
c        df=(sinh(delta)-delta*cosh(delta))/delta**2
c        df=b-cosh(delta)
         df=(sinh(delta)-delta*cosh(delta))/sinh(delta)**2
c--------- update delta
        delta=delta-f/df
c       write(*,'(4e16.4)')f,df,f/df,delta
c--------- check convergence criterium
c       if(abs((delta-delta_old)/delta_old).lt.rlim)goto 200
c--------- update old value
c       delta_old=delta
      enddo
      transsinh=delta
      return
      end

      double precision function transtanh(b)
************************************************************************
*     routine for solving the transendental equation
*
*     b=tanh(delta/2)/(delta/2)
*
*     using NewtonRaphson
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      real(kind=8)::delta,b
      real(kind=8)::delta_old,f,df,rlim
      parameter(rlim=1.d-6)
      integer n

      if(b.gt.1.0d0)then
        print*,' Error form sinhdist: Can not be gridded '
c       stop
      endif
c------- Preforme Newton iteration to obtain delta
      delta=1.d0
      delta_old=delta
      do n=1,25
c--------- Compute f(delta)
        f =delta/(tanh(.5*delta)+1.d-30)-2/B
c--------- Compute d/d(delta)(f(delta))
        df=(tanh(.5*delta)-delta*(1-tanh(.5*delta)**2))
     &    /(tanh(.5*delta)**2+1.d-30)
c--------- update delta
        delta=delta-f/df
c       write(*,'(i3,1x,3e16.4)')n,delta,f,df
c--------- check convergence criterium
c       if(abs((delta-delta_old)/delta_old).lt.rlim)goto 200
c--------- update old value
c       delta_old=delta
      enddo
      transtanh=delta
      return
      end
