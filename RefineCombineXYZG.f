      module params
      integer::nb
      integer::ni,nk,nic,nkc,nkf
      integer koffset
      end module

      module coords 
c-------coordinats of final grid
      real(kind=8),dimension(:,:,:,:),allocatable::x,y,z
      integer,dimension(:,:,:,:),allocatable::attr
c-------coordinats of coarse grid
      real(kind=8),dimension(:,:,:,:),allocatable::xc,yc,zc
c-------coordinats of fine inner grid
      real(kind=8),dimension(:,:,:,:),allocatable::xf,yf,zf
      end module

      program refine
c-----------------------------------------------------------------------
c     Program for comibining two meshes in plot3d (xyz) format, one
c     fine near the geometry, and an outer coarse mesh.
c     input
c     coarse.xyz, (ni1,ni1,nkc) file holding the full coarse grid
c     fine.xyz,   (ni, ni, nkf) file holding the inner fine grid
c
c     1----------nkf
c     |      |    |
c     |      1-------------nkc
c     |      |
c     |      |
c     1--------------------nk
c            |
c          koffset    
c     
c     first the coarse grid is refined in the plane to have ni,ni cells
c     next the two meshes are merged and the final grid has nkf+nk-1 cells
c     in the k-direction
c
c     Author: Niels N. Sorensen
c-----------------------------------------------------------------------
      use params
      use coords
      implicit none

c--------read original coarse grid, coarse grid is defined by nic,nkc,nb
      call ReadGridCoarse

c-------read original fine grid, the fine inner grid is defined by ni,nkf,nb
      call ReadGridFine
      
c-------refine coarse grid by splining existing i,j grid lines
      call RefineCoarseGrid

c-------blend the refined coarse grid and the original fine grid
      call CombineGrids

c-------partitionate the grid and write to file
      call partgrid(ni,nk,nb,x,y,z)

      stop
      end

      subroutine ReadGridCoarse
c-----------------------------------------------------------------------
c
c-----------------------------------------------------------------------
      use params
      use coords
      implicit none
      integer::i,j,k,n

c     inquire(file='coarse.xyz',exist=statex)
      print*,' READING COARSE GRID FILE'
      open(unit=10,file='coarse.xyz',form='unformatted')
      read(10)nb
      read(10)(nic,nic,nkc,n=1,nb)
      allocate(xc(nic,nic,nkc,nb),yc(nic,nic,nkc,nb),zc(nic,nic,nkc,nb))
      do n=1,nb
        read(10)(((xc(i,j,k,n),i=1,nic),j=1,nic),k=1,nkc),
     &          (((yc(i,j,k,n),i=1,nic),j=1,nic),k=1,nkc),
     &          (((zc(i,j,k,n),i=1,nic),j=1,nic),k=1,nkc)
      enddo
      close(10)
      print*,' FINISHED READING COARSE GRID ni,nk : ',nic,nkc

      return
      end

      subroutine ReadGridFine
c-----------------------------------------------------------------------
c
c-----------------------------------------------------------------------
      use params
      use coords
      implicit none
      integer::i,j,k,n

      print*,' READING FINE GRID FILE'
      open(unit=10,file='fine.xyz',form='unformatted')
      read(10)nb
      read(10)(ni,ni,nkf,n=1,nb)
      allocate(xf(ni,ni,nkf,nb),yf(ni,ni,nkf,nb),zf(ni,ni,nkf,nb))
      do n=1,nb
        read(10)(((xf(i,j,k,n),i=1,ni),j=1,ni),k=1,nkf),
     &          (((yf(i,j,k,n),i=1,ni),j=1,ni),k=1,nkf),
     &          (((zf(i,j,k,n),i=1,ni),j=1,ni),k=1,nkf)
      enddo
      close(10)
      print*,' FINISHED READING FINE GRID ni,nk : ',ni,nkf

      return
      end

      subroutine RefineCoarseGrid
c-----------------------------------------------------------------------
c
c-----------------------------------------------------------------------
      use params
      use coords
      implicit none
      integer::i,j,k,n,factor
      real(kind=8),dimension(nic,nic,nkc)::x1,y1,z1
      real(kind=8),dimension(ni,ni,nkc)::xrs,yrs,zrs

c-------allocate fine grid
      print*,' Give k-offset : '
      read*,koffset
      nk=nkc+koffset
      print*,' REFINEMENT FACTOR : ',(ni-1)/(nic-1)
      allocate(x(ni,ni,nk,nb)
     &        ,y(ni,ni,nk,nb)
     &        ,z(ni,ni,nk,nb))
      x=0.d0;y=0.d0;z=0.d0
  
      do n=1,nb
c-------store coarse block in single block format
        x1(1:nic,1:nic,1:nkc)=xc(1:nic,1:nic,1:nkc,n)
        y1(1:nic,1:nic,1:nkc)=yc(1:nic,1:nic,1:nkc,n)
        z1(1:nic,1:nic,1:nkc)=zc(1:nic,1:nic,1:nkc,n)
       call refineblock(nic,nkc,x1,y1,z1,ni,xrs,yrs,zrs)
c-------store in refined multiblock grid
        do k=1,nkc
        do j=1,ni
        do i=1,ni
           x(i,j,k+koffset,n)=xrs(i,j,k)
           y(i,j,k+koffset,n)=yrs(i,j,k)
           z(i,j,k+koffset,n)=zrs(i,j,k)
        enddo;enddo;enddo
      enddo

c-----make FieldView plot3d unformatted grid file
      open(unit=4,file='refined.xyz',form='unformatted')
      write(4)nb
      write(4)(ni,ni,nk,n=1,nb)
      do n=1,nb
        write(4)
     &    (((x(i,j,k,n),i=1,ni),j=1,ni),k=1,nk),
     &    (((y(i,j,k,n),i=1,ni),j=1,ni),k=1,nk),
     &    (((z(i,j,k,n),i=1,ni),j=1,ni),k=1,nk)
      enddo
      close(4)
      print*,' FINISHED WRITING GRID. '


      print*,' FINISHED REFINING COARSE GRID. '
      return
      end

      subroutine refineblock(ni,nk,x1,y1,z1,nir,xr,yr,zr)
c-----------------------------------------------------------------------
c
c-----------------------------------------------------------------------
      implicit none
      integer ni,nir,nk,i,j,k,factor
      real(kind=8),dimension(ni,ni,nk)::x1,y1,z1
      real(kind=8),dimension(ni)::x1d,y1d,z1d,s,xs,ys,zs
      real(kind=8),dimension(nir,nir,nk)::xr,yr,zr
      real(kind=8)::xps,yps,zps,xi
  
c-----refine in i-direction
      factor=(nir-1)/(ni-1)
      do k=1,nk
      do j=1,ni
c-------spline x,y and z in i-direction as function of i-index
        do i=1,ni
        x1d(i)=x1(i,j,k)
        y1d(i)=y1(i,j,k)
        z1d(i)=z1(i,j,k)
        enddo
        call SplineCurve(ni,x1d,y1d,z1d,s,xs,ys,zs)
        do i=1,nir
          xi=real(i-1)/real(nir-1)
          call splint(s,x1d,xs,ni,xi,xps)
          call splint(s,y1d,ys,ni,xi,yps)
          call splint(s,z1d,zs,ni,xi,zps)
          xr(i,1+(j-1)*factor,k)=xps
          yr(i,1+(j-1)*factor,k)=yps
          zr(i,1+(j-1)*factor,k)=zps
        enddo
      enddo;enddo
c-----refine in j-direction
      do k=1,nk
      do i=1,nir
c-------spline x,y and z in j-direction as function of j-index
        do j=1,ni
        x1d(j)=xr(i,1+(j-1)*factor,k)
        y1d(j)=yr(i,1+(j-1)*factor,k)
        z1d(j)=zr(i,1+(j-1)*factor,k)
        enddo
        call SplineCurve(ni,x1d,y1d,z1d,s,xs,ys,zs)
        do j=1,nir
          xi=real(j-1)/real(nir-1)
          call splint(s,x1d,xs,ni,xi,xps)
          call splint(s,y1d,ys,ni,xi,yps)
          call splint(s,z1d,zs,ni,xi,zps)
          xr(i,j,k)=xps
          yr(i,j,k)=yps
          zr(i,j,k)=zps
        enddo
      enddo;enddo

      return
      end

      subroutine PartGrid(ni,nk,nb,x,y,z)
      implicit none
      integer ni,nk,nb,i,j,k,n
      real(kind=8),dimension(ni,ni,nk,nb)::x,y,z
      real(kind=8)::zlim
      integer,dimension(ni,ni,nk,nb)::attr

      integer::nn,nib
      integer::istart,jstart,kstart,io,jo,ko,ii,jj,kk,iii,jjj,kkk
      integer::dim,nbi,nbj,nbk,nbm
      integer::yesno,factor
      real(kind=8)::rest
      real(kind=8),dimension(:,:,:,:),allocatable::xt,yt,zt
      integer,dimension(:,:,:,:),allocatable::attrt
      real(kind=8)OutletAngle,len,a2r

      a2r=atan(1.d0)/45d0

c--------specify attributs
c---------determine outlet location based on OutletAngle
      attr=1
      attr(:,:,1,:)=101
      attr(:,:,nk,:)=401
      print*,' Give Outlet Angle : '
      read*,OutletAngle
      len=maxval(z(1:ni,1:ni,nk,:))
      print*,' Max domain radius : ',len
      len=len*cos(a2r*OutletAngle)
      print*,' Max z-coordinat for outlet region ',len
      do n=1,nb
      do j=1,ni
      do i=1,ni
        if(z(i,j,nk,n).lt.len)attr(i,j,nk,n)=201
      enddo;enddo
      enddo

c-----make FieldView plot3d unformatted grid file
      open(unit=4,file='grid.xyz',form='unformatted')
      write(4)nb
      write(4)(ni,ni,nk,n=1,nb)
      do n=1,nb
        write(4)
     &    (((x(i,j,k,n),i=1,ni),j=1,ni),k=1,nk),
     &    (((y(i,j,k,n),i=1,ni),j=1,ni),k=1,nk),
     &    (((z(i,j,k,n),i=1,ni),j=1,ni),k=1,nk)
      enddo
      close(4)
      print*,' FINISHED WRITING GRID. '


c-----find max size of multigrid blocks
c-----min dimension
      dim=int(min(real(ni-1),real(nk-1)))

      rest=mod(ni-1,dim)
     &    +mod(ni-1,dim)
     &    +mod(nk-1,dim)
      do while(rest.ne.0)
        rest=mod(ni-1,dim)
     &      +mod(ni-1,dim)
     &      +mod(nk-1,dim)
        dim=dim/2+1
      enddo
      print*,'*******************************************************'
      print*,' 3D block size                      : ',ni-1
      print*,'size of multidomaine blocks         : ',dim
      print*,'want to change this yes = 1, no = 2   '
      read(*,*) yesno
      if(yesno.eq.1)then
        print*,'give new dimension  '
        read(*,*)dim
      endif
c     print*,' Give reduction factor: '
c     read*,factor
      factor=1
      nbi=(ni-1)/dim
      nbj=(ni-1)/dim
      nbk=(nk-1)/dim
      nbm=nbi*nbj*nbk*nb
      print*,'*******************************************************'
      print*,'size of multidomaine blocks         : ',dim/factor
      print*,'nbi                                 : ',nbi
      print*,'nbj                                 : ',nbj
      print*,'nbk      l                          : ',nbk
      print*,'total number of blocks              : ',nbm
      print*,'*******************************************************'

      allocate(attrt(dim+3,dim+3,dim+3,nbm))
      attrt=0
      do n=1,nb
      do kk=1,nbk;kstart=(kk-1)*dim
      do jj=1,nbj;jstart=(jj-1)*dim
      do ii=1,nbi;istart=(ii-1)*dim
        nn=(n-1)*nbi*nbj*nbk+ii+(jj-1)*nbi+(kk-1)*nbi*nbj
        do k=1,dim+1,factor;ko=kstart+k
        do j=1,dim+1,factor;jo=jstart+j
        do i=1,dim+1,factor;io=istart+i
          attrt(i+1,j+1,k+1,nn)=attr(io,jo,ko,n)
        enddo;enddo;enddo
      enddo;enddo;enddo
      enddo
      allocate(xt(dim+3,dim+3,dim+3,nbm))
      xt=0
      do n=1,nb
      do kk=1,nbk;kstart=(kk-1)*dim
      do jj=1,nbj;jstart=(jj-1)*dim
      do ii=1,nbi;istart=(ii-1)*dim
        nn=(n-1)*nbi*nbj*nbk+ii+(jj-1)*nbi+(kk-1)*nbi*nbj
        do k=1,dim+1,factor;ko=kstart+k
        do j=1,dim+1,factor;jo=jstart+j
        do i=1,dim+1,factor;io=istart+i
             xt(i+1,j+1,k+1,nn)=x(io,jo,ko,n)
        enddo;enddo;enddo
      enddo;enddo;enddo
      enddo
      allocate(yt(dim+3,dim+3,dim+3,nbm))
      yt=0
      do n=1,nb
      do kk=1,nbk;kstart=(kk-1)*dim
      do jj=1,nbj;jstart=(jj-1)*dim
      do ii=1,nbi;istart=(ii-1)*dim
        nn=(n-1)*nbi*nbj*nbk+ii+(jj-1)*nbi+(kk-1)*nbi*nbj
        do k=1,dim+1,factor;ko=kstart+k
        do j=1,dim+1,factor;jo=jstart+j
        do i=1,dim+1,factor;io=istart+i
             yt(i+1,j+1,k+1,nn)=y(io,jo,ko,n)
        enddo;enddo;enddo
      enddo;enddo;enddo
      enddo
      allocate(zt(dim+3,dim+3,dim+3,nbm))
      zt=0
      do n=1,nb
      do kk=1,nbk;kstart=(kk-1)*dim
      do jj=1,nbj;jstart=(jj-1)*dim
      do ii=1,nbi;istart=(ii-1)*dim
        nn=(n-1)*nbi*nbj*nbk+ii+(jj-1)*nbi+(kk-1)*nbi*nbj
        do k=1,dim+1,factor;ko=kstart+k
        do j=1,dim+1,factor;jo=jstart+j
        do i=1,dim+1,factor;io=istart+i
             zt(i+1,j+1,k+1,nn)=z(io,jo,ko,n)
        enddo;enddo;enddo
      enddo;enddo;enddo
      enddo

c-------unformatted write
      nib=dim+3
      call UnfGridWrite(20,nib,nbm,xt,yt,zt,attrt)
      deallocate(attrt,xt,yt,zt)

      return
      end

      subroutine CombineGrids
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      use params
      use coords
      implicit none
      integer i,j,k,n,kfinish,kstart
      real(kind=8)::factor,mfactor

  
      print*,' Give k-start  => ',koffset+1,' and  < ',nkf
      read*,kstart
      print*,' Give k-finish  > ',kstart   ,' and  < ',nkf
      read*,kfinish
      do n=1,nb
      do k=1,koffset
      do j=1,ni
      do i=1,ni
        x(i,j,k,n)=xf(i,j,k,n)
        y(i,j,k,n)=yf(i,j,k,n)
        z(i,j,k,n)=zf(i,j,k,n)
      enddo;enddo;enddo
      enddo
      do n=1,nb
      do k=koffset+1,nkc+koffset
      do j=1,ni
      do i=1,ni
        if(k.le.kstart)then
          factor=0.d0
        elseif(k.gt.kstart.and.k.lt.kfinish)then
          factor=real(k-kstart)/real(kfinish-kstart)
        else
          factor=1.d0
        endif
        if (n.eq.1.and.j.eq.1.and.i.eq.1)print*,k,factor
        mfactor=1-factor
        x(i,j,k,n)=x(i,j,k,n)*factor+xf(i,j,k,n)*mfactor
        y(i,j,k,n)=y(i,j,k,n)*factor+yf(i,j,k,n)*mfactor
        z(i,j,k,n)=z(i,j,k,n)*factor+zf(i,j,k,n)*mfactor
      enddo;enddo;enddo
      enddo
      print*,' FINISHED COMBINING GRIDS. '
      return
      end
      

      subroutine SplineCurve(ni,x,y,z,s,xs,ys,zs)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      implicit none
      integer ni,i
      real(kind=8),dimension(ni)::x,y,z,s,xs,ys,zs
      real(kind=8)::y1,yn

c-----spline profile data
      s(1)=0.d0
      do i=2,ni
        s(i)=real(i-1)/real(ni-1)
      enddo
c-----spline representation of surface (natural cubic spline)
      y1=1.d32
      yn=1.d32
      call spline(s,x,ni,y1,yn,xs)
      call spline(s,y,ni,y1,yn,ys)
      call spline(s,z,ni,y1,yn,zs)

      return
      end



      subroutine spline(x,y,n,yp1,ypn,y2)
c----------------------------------------------------------------------
c     Given arrays x and y of length n containing a tabulated function,
c     i.e y(i)=f(x(i)), with x(1)<x(2)<...<x(n), and given values yp1
c     and ypn for the first derivative of the interpolating function at
c     points 1 and n, respectively, this routine returns an array y2 of 
c     lenght n wich contains the second derivatives of the interpolating 
c     function at the tabulated points x(i). Uf yp1 and/or ypn are equal 
c     to 1.d30 or larger, the routine is signalled to the corresponding
c     boundary condition for a natural spline, with zero second derivative
c     on that boundary
c
c     Numerical Recipes
c----------------------------------------------------------------------
      implicit none
      integer nmax
      parameter(nmax=1000)
      real*8 x(n),y(n),y2(n),u(nmax)
      real*8 qn,p,un,sig,yp1,ypn
      integer i,n,k

      if (yp1.gt..99e30) then
        y2(1)=0.d0
        u(1)=0.d0
      else
        y2(1)=-0.5d0
        u(1)=(3./(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
      endif
      do 11 i=2,n-1
        sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
        p=sig*y2(i-1)+2.d0
        y2(i)=(sig-1.d0)/p
        u(i)=(6.*((y(i+1)-y(i))/(x(i+1)-x(i))-(y(i)-y(i-1))
     *      /(x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*u(i-1))/p
11    continue
      if (ypn.gt..99e30) then
        qn=0.
        un=0.
      else
        qn=0.5d0
        un=(3.d0/(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
      endif
      y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.d0)
      do 12 k=n-1,1,-1
        y2(k)=y2(k)*y2(k+1)+u(k)
12    continue
      return
      end

      subroutine splint(xa,ya,y2a,n,x,y)
c----------------------------------------------------------------------
c     Given the arrays xa and ya of length n, wich tabulate a function
c     (with the xa's in order), and given the array y2a, wich is the 
c     output from spline above, and given a value of x, this routine 
c     returns a cubic-spline interpolated value y. 
c
c     Numerical Recipes
c----------------------------------------------------------------------
      implicit none
      integer n
      real*8 xa(n),ya(n),y2a(n),x,y
      real*8 a,b,h
      integer klo,khi,k

      klo=1
      khi=n
1     if (khi-klo.gt.1) then
        k=(khi+klo)/2
        if(xa(k).gt.x)then
          khi=k
        else
          klo=k
        endif
      goto 1
      endif
      h=xa(khi)-xa(klo)
      if (h.eq.0.) pause 'bad xa input.'
      a=(xa(khi)-x)/h
      b=(x-xa(klo))/h
      y=a*ya(klo)+b*ya(khi)+
     *      ((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.d0
      return
      end

      subroutine ReadBlockReal(unit,block,nblock,x)
      implicit none
      integer unit,nblock,block,i,n
      real(kind=8),dimension(block,nblock)::x
      real(kind=8),dimension(block)       ::xblock
        do n=1,nblock
          read(unit)xblock
          do i=1,block
            x(i,n)=xblock(i)
          end do
        end do
      end subroutine ReadBlockReal

      subroutine ReadBlockInteger(unit,block,nblock,x)
      implicit none
      integer unit,nblock,block,i,n
      integer,dimension(block,nblock)::x
      integer,dimension(block)       ::xblock
        do n=1,nblock
          read(unit)xblock
          do i=1,block
            x(i,n)=xblock(i)
          end do
        end do
      end subroutine ReadBlockInteger

      subroutine UnfGridWrite(unitnr,ni,nblock,x,y,z,attr)
C=======================================================================
C     routine for writing unformatted x3d file
C     remember that the grid must positions in the following region 
C
C        x(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C        y(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C        z(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C     attr(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C
C     the remaining spaces are intended for ghost cells
C=======================================================================
      implicit none
      integer unitnr,ni,nblock
      real(kind=8),dimension(ni,ni,ni,nblock)::x,y,z
      integer,dimension(ni,ni,ni,nblock)::attr

C     Code to be used to add ghost cells before call UnfGridWrite
c     do n=1,nblock
c     do k=2,ni-1
c     do j=2,ni-1
c     do i=2,ni-1
c       xg   (i,j,k,n)=x   (i-1,j-1,k-1,n)
c       yg   (i,j,k,n)=y   (i-1,j-1,k-1,n)
c       zg   (i,j,k,n)=z   (i-1,j-1,k-1,n)
c       attrg(i,j,k,n)=attr(i-1,j-1,k-1,n)
c     enddo;enddo;enddo
c     enddo

      print*,' unitnr ',unitnr
      open(unit=unitnr,file='grid.x3dunf',form='unformatted')
      write(unitnr)ni-3
      write(unitnr)nblock
      print*,' Writing Attributes '
      call WriteBlockInteger(unitnr,ni**3,nblock,attr)
      print*,' Writing x '
      call WriteBlockReal   (unitnr,ni**3,nblock,x   )
      print*,' Writing y '
      call WriteBlockReal   (unitnr,ni**3,nblock,y   )
      print*,' Writing z '
      call WriteBlockReal   (unitnr,ni**3,nblock,z   )
      print*,' Finished writing '
      close(unitnr)
      return
      end

      subroutine WriteBlockReal(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      real(kind=8),dimension(block,nblock)::x
      real(kind=8),dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine WriteBlockReal

      subroutine WriteBlockInteger(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      integer,dimension(block,nblock)::x
      integer,dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine WriteBlockInteger
