      module params
C========================================= Real precision definition ===
      integer,parameter::idp=kind(1d0)
C============================================= filenames ===============
      character(len=128) project      ! project, 1'st part of filenames
      integer namelen                 ! length of project name
C============================================= main parameters =========
      integer bsizes(5),bsize         ! bsize      of MG levels
      integer nis(5),ni               ! ni         of MG levels
      integer MGlev                   ! # of MG levels in application
      integer:: MGcoarse=5            ! # of MG levels in inner loop
      integer nblock                  ! # of blocks in application
      integer ncomp                   ! # of BC components
      integer:: InterpolationOrder=4  ! interpolation to cell faces
C============================================= frame of reference ======
      logical:: Polar       = .false.  ! F: cartesian, T: polar
      logical:: PolarMetrics= .false.  ! F: cartesian, T: polar
C============================================= parallel parameters =====
      integer:: nprocs     = 1        ! # of processors
      integer:: MyProcNum  = 1        ! present processors number
      integer:: BPPglobal             ! # of blocks per processor (max)
      integer:: BPP                   ! # of blocks on present processor
C=======================================================================
      integer,dimension(:),allocatable::BPPs ! # of blocks on processors
C=======================================================================
      end module

      module restart
C-----------------------------------------------------------------------
c     restart module
C-----------------------------------------------------------------------
      use params
      implicit none
      integer::ivar
      real(kind=idp)::rvar
      logical::lvar
      character(len=20)::cvar
      integer          ,dimension(:),allocatable::iarray
      real(kind=idp)     ,dimension(:),allocatable::rarray
      logical          ,dimension(:),allocatable::larray
      character(len=20),dimension(:),allocatable::carray
C-----------------------------------------------------------------------
      end module

      program readrst
C=======================================================================
C     A small utility for reading the new restart file
C     
C     Author: Niels N. Sorensen
C=======================================================================
      use restart
      implicit none
      character(len=20)::Label,Type,Action
      integer::Size
      logical Recognized,MoreFields
      integer::x3dunit=10

      open(unit=x3dunit,file='grid.rst',form='unformatted')
      read(x3dunit)Label(1:20);print*,' Label ',label(1:20)
      read(x3dunit)Label(1:20);print*,' Label ',label(1:20)

      if(label.ne.'New Restart Version ')then
        print*,' Not New Restart Format '
        print*,' Can not be read '
      endif

      do 
c-------read headder
      read(x3dunit)Label(1:20)
      read(x3dunit)Type(1:20)
      read(x3dunit)Action(1:20)
      read(x3dunit)size
      print*,Label,Type,Action,size
      select case(Action(1:len_trim(Action)))
c-------Bcast
        case('BCAST')
        select case(Type(1:len_trim(type)))
        case('CHARACTER')
          if(size.eq.1)then
            call ReadBcastScalarCharacter(x3dunit,20,CVAR,MoreFields)
          else
            if(allocated(CARRAY))deallocate(CARRAY)
            allocate(CARRAY(size))
            call ReadBcastArrayCharacterUNF(x3dunit,20,size,CARRAY)
          endif
        case('INTEGER')
          if(size.eq.1)then
            call ReadBcastScalarInteger(x3dunit,IVAR)
          else
            if(allocated(IARRAY))deallocate(IARRAY)
            allocate(IARRAY(size))
            call ReadBcastArrayInteger (x3dunit,size,IARRAY)
          endif
        case('REAL')
          if(size.eq.1)then
            call ReadBcastScalarReal   (x3dunit,RVAR)
          else
            if(allocated(RARRAY))deallocate(RARRAY)
            allocate(RARRAY(size))
            call ReadBcastArrayReal   (x3dunit,size,RARRAY)
          endif
        case('LOGICAL')
          if(size.eq.1)then
            call ReadBcastScalarLogical(x3dunit,LVAR)
          else
            if(allocated(LARRAY))deallocate(LARRAY)
            allocate(LARRAY(size))
            call ReadBcastArrayLogical (x3dunit,LARRAY)
          endif
        end select
      case('SCATTER')
c-------Scatter
c-------Scatter
        select case(Type(1:len_trim(type)))
        case('INTEGER')
          if(allocated(IARRAY))deallocate(IARRAY)
          allocate(IARRAY(size*BPP))
          call ReadScatterInteger(x3dunit,size,IARRAY)
        case('REAL')
          if(allocated(RARRAY))deallocate(RARRAY)
          allocate(RARRAY(size*BPP))
          call ReadScatterReal(x3dunit,size,RARRAY)
        case('LOGICAL')
          if(allocated(LARRAY))deallocate(LARRAY)
          allocate(LARRAY(size*BPP))
          call ReadScatterLogical(x3dunit,size,LARRAY)
        end select
      case('STOP')
        close(x3dunit)
        exit
      end select

      select case(label)
        case('nblock              ')
          BPP=IVAR
        case('bsize               ')
          bsize=IVAR
C----------Insert action here

C----------
      end select
      enddo

c-------deallocate work arrays
      if(allocated(IARRAY))deallocate(IARRAY)
      if(allocated(RARRAY))deallocate(RARRAY)
      if(allocated(LARRAY))deallocate(LARRAY)
      if(allocated(CARRAY))deallocate(CARRAY)


      print*,' #### FINISHED READING ########'
      print*,' NBLOCK  : ',BPP
      print*,' BSIZE   : ',bsize
      stop
      end

c>    @ingroup platform
      subroutine ReadScatterReal(unit,block,x)
      use params
      implicit none
      integer unit,i,n,dn,block
      real(kind=idp),dimension(block*BPP)::x
      real(kind=idp),dimension(:),allocatable::xblock
C-----------------------------------------------------------------------
          allocate(xblock(block))
C--------------------------------- Read ROOTs own blocks ---------------
            do n=1,BPP;dn=(n-1)*block
              read(unit)xblock
              do i=1,block;x(i+dn)=xblock(i);end do
            end do
C-----------------------------------------------------------------------
          deallocate(xblock)
C-----------------------------------------------------------------------
      end subroutine ReadScatterReal

c>    @ingroup platform
      subroutine ReadScatterInteger(unit,block,x)
      use params
      implicit none
      integer unit,i,n,dn,block
      integer,dimension(block*BPP)::x
      integer,dimension(:),allocatable::xblock
C-----------------------------------------------------------------------
          allocate(xblock(block))
C--------------------------------- Read ROOTs own blocks ---------------
            do n=1,BPP;dn=(n-1)*block
              read(unit)xblock
              do i=1,block;x(i+dn)=xblock(i);end do
            end do
C-----------------------------------------------------------------------
          deallocate(xblock)
C-----------------------------------------------------------------------
      end subroutine ReadScatterInteger

c>    @ingroup platform
      subroutine ReadScatterLogical(unit,block,x)
      use params
      implicit none
      integer unit,i,n,dn,block
      logical,dimension(block*BPP)::x
      logical,dimension(:),allocatable::xblock
C-----------------------------------------------------------------------
          allocate(xblock(block))
C--------------------------------- Read ROOTs own blocks ---------------
            do n=1,BPP;dn=(n-1)*block
              read(unit)xblock
              do i=1,block;x(i+dn)=xblock(i);end do
            end do
C-----------------------------------------------------------------------
          deallocate(xblock)
C-----------------------------------------------------------------------
      end subroutine ReadScatterLogical

      subroutine ReadBcastScalarCharacter(unit,length,x,lexist)
      use params
      implicit none
      integer unit,length
      logical lexist
      character(len=length) x
C-----------------------------------------------------------------------
      if(MyProcNum.eq.1) then
        lexist=.false.
        read(unit,end=1)x
        lexist=.true.
    1   continue
      end if
C-----------------------------------------------------------------------
      if(lexist)print*,'Restart label ',x
C-----------------------------------------------------------------------
      end

c>    @ingroup platform
      subroutine ReadBcastScalarReal(unit,x)
      use params
      implicit none
      integer unit
      real(kind=idp) x
C-----------------------------------------------------------------------
      read(unit)x
C-----------------------------------------------------------------------
      end

c>    @ingroup platform
      subroutine ReadBcastScalarInteger(unit,x)
      use params
      implicit none
      integer unit
      integer x
C-----------------------------------------------------------------------
      read(unit)x
C-----------------------------------------------------------------------
      end

c>    @ingroup platform
      subroutine ReadBcastScalarLogical(unit,x)
      use params
      implicit none
      integer unit
      logical x
C-----------------------------------------------------------------------
      read(unit)x
C-----------------------------------------------------------------------
      end

c>    @ingroup platform
      subroutine BcastScalarLogical(x)
      implicit none
      logical x
      end

c>    @ingroup platform
      subroutine ReadBcastArrayCharacter(unit,count,x)
      use params
      implicit none
      integer unit,count,i
      character(len=2048) x(count)
C-----------------------------------------------------------------------
      do i=1,count
      read(unit,'(a2048)')x(i)
      end do
C-----------------------------------------------------------------------
      end

c>    @ingroup platform
      subroutine ReadBcastArrayCharacterUNF(unit,length,count,x)
      use params
      implicit none
      integer unit,length,count
      character(len=length) x(count)
C-----------------------------------------------------------------------
      read(unit)x
C-----------------------------------------------------------------------
      end

c>    @ingroup platform
      subroutine ReadBcastArrayReal(unit,count,x)
      use params
      implicit none
      integer unit,count
      real(kind=idp) x(count)
C-----------------------------------------------------------------------
      read(unit)x
C-----------------------------------------------------------------------
      end

c>    @ingroup platform
      subroutine ReadBcastArrayInteger(unit,count,x)
      use params
      implicit none
      integer unit,count
      integer x(count)
C-----------------------------------------------------------------------
      read(unit)x
C-----------------------------------------------------------------------
      end

c>    @ingroup platform
      subroutine ReadBcastArrayLogical(unit,count,x)
      use params
      implicit none
      integer unit,count
      logical x(count)
C-----------------------------------------------------------------------
      read(unit)x
C-----------------------------------------------------------------------
      end
