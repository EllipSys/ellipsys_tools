This Python based tool can be used to create an Actuator Disk (AD) 
grid for AD simulations in EllipSys3D. The tool reads a file with 
blade forces. The example file "upwind-DES.1FY" represents calculated 
blade forces obtained from a full rotor CFD simulation of the NREL-5MW 
reference wind turbine. One can use any other force file, using the 
same columns: 
* col (1) radius  force along the blade fy fz  
* col (2)-(4) integrated pressure forces with
  * (2) Fx: tangential force (driving force)
  * (3) Fy: radial force
  * (4) Fz: thrust force 
* col (5)-(7) integrated viscous force.
If not all 6 forces are known, one can use columns with zeros. Note 
that at least the tangential pressure force (driving force) and the 
thrust pressure force should be non zero. At the moment radial blade 
forces are not used.

## Usage

    $ python generate_AD_gridfile.py <input.dat>

## Examples of input files of NREL-5MW reference wind turbine

1. input_NREL-5MW_normalized.dat -> Create a normalized AD grid with a total
thirust and torque of 1 and -1, respectively.
2. input_NREL-5MW_full.dat -> Create a scaled AD grid with fully distributed 
forces.
3. input_NREL-5MW_full_no_scaling.dat -> Create a AD grid with fully distributed
forces without any scaling. This is simply the original rotor. 
4. input_NREL-5MW_full_no_scaling_no_rotation.dat -> Create a AD grid with fully distributed
forces without scaling and rotational forces. 

## Usage in EllipSys3D

Example 1) can used as: 

acdisc 1 loading coef_distrib

Examples 2)-4) can used as:

acdisc 1 loading full_distrib 

## Examples of input files of other wind turbines

1. input_500NTK_normalized.dat -> Nordtank 500 kW
2. input_DTU-10MW_normalized.dat -> DTU 10 MW reference wind turbine
