#!/usr/bin/env python
# Original script made by Pierre-Elouan Rethore  
# Script modified by Paul van der Laan
# 
import re
from scipy.integrate import quad
from scipy.interpolate import interp1d
import numpy as np
from InputReader import InputFile
import sys, getopt

pi = np.pi

## Input file
input_file = InputFile(sys.argv[1])

# Original rotor input
forcefile = input_file['forcefile']
U = input_file['U']
RPM = input_file['RPM']
R = input_file['R']
rho = input_file['rho']

# AD grid
ntheta = input_file['ntheta']

# Desired rotor output
outfile = input_file['outfile']
U_set = input_file['U_set']
RPM_set = input_file['RPM_set']
R_set = input_file['R_set']
rho_set = input_file['rho_set']
Ct_set = input_file['Ct_set']
Cp_set = input_file['Cp_set']

print "Read input file:",sys.argv[1]
print ""
print "The AD is based on force file:  "+str(forcefile)
print "The original U   = "+str(U)
print "The original R   = "+str(R)
print "The original RPM = "+str(RPM)
print "The original rho = "+str(rho)
print ""
print "The desired U    = "+str(U_set)
print "The desired R    = "+str(R_set)
print "The desired Ct   = "+str(Ct_set)
print "The desired Cp   = "+str(Cp_set)
print "The desired rho  = "+str(rho_set)

### Read the 1FY file from EllipSys3d
fid = open(str(forcefile),'r')

all = fid.read()
fid.close()
lines = all.splitlines()
### Remove all the comments
non_comments = np.nonzero(np.array([len(re.findall('^[\s]*#',lines[i]))==0 for i in range(len(lines))]))[0]
### Transform string to float64
data = np.array([lines[non_comments[i]].split() for i in range(len(non_comments))],dtype='float64')
### Create the force array
# P: last force point at the tip y=63.5 m is skipped somehow.
data = np.vstack([np.zeros(data.shape[1]),data[0:data.shape[0]-1,:]])
# nrad is number of points in radial direction and it is bound to the number of points used in the force input file:
nrad = data.shape[0]

print ""
print "Create AD grid with "+str(nrad)+" radial points and "+str(ntheta)+" azimuthal points."
print ""


### Define the index of the different colums
iFxp = 1  ## Pressure Force X
iFyp = 2  ## Pressure Force Y
iFzp = 3  ## Pressure Force Z
iFxv = 4  ## Viscous  Force X
iFyv = 5  ## Viscous  Force Y
iFzv = 6  ## Viscous  Force Z


r = np.zeros(nrad)
Fx = np.zeros(nrad)
Fy = np.zeros(nrad)
Fz = np.zeros(nrad)
circle = np.zeros(nrad-1)

torque=0
for iA in range(nrad-1):
    ### Radial position of the integration on the blade
    r[iA] = 0.5 * (data[iA,0] + data[iA+1,0])
    dr = data[iA+1,0] - data[iA,0]
    # Cell centered forces for 3 blades [N]
    # Fx: tangential force (driving force)
    # Fy: radial force
    # Fz: thrust force 
    Fx[iA] = 0.5 * 3 * (data[iA,iFxp] + data[iA,iFxv] + data[iA+1,iFxp] + data[iA+1,iFxv]) * dr
    Fy[iA] = 0.5 * 3 * (data[iA,iFyp] + data[iA,iFyv] + data[iA+1,iFyp] + data[iA+1,iFyv]) * dr
    Fz[iA] = 0.5 * 3 * (data[iA,iFzp] + data[iA,iFzv] + data[iA+1,iFzp] + data[iA+1,iFzv]) * dr
    circle[iA] = (2*pi) * r[iA] * dr
    torque=torque+r[iA]*Fx[iA]
 
r[-1] = R    
#print Fx
### Sanity check the C_T of the wind turbine ------------------------------------
Ct = sum(Fz)/(0.5*rho*pi*R**2*U**2)
print "The original thrust = ", sum(Fz)
print "The original Ct     = ", Ct
# Power check
U_rot=RPM/60.0*2*pi
power=-U_rot*torque
Cp=power/(0.5*rho*pi*R**2*U**3)
print "The original torque = ", torque
print "The original Cp     = ", Cp
print ""
if Cp_set == -1:
    Cp_set=Cp
    print "No scaling for Cp is applied"
elif Cp_set == 0:
    print "No rotational forces"

if Ct_set == -1:
    Ct_set=Ct
    print "No scaling for Ct is applied"

########################################
# Scale forces with desired Ct and Cp
########################################

# Thrust
Fz=(Fz/sum(Fz))*Ct_set*0.5*rho_set*pi*R_set**2*U_set**2 
Ct_mod = sum(Fz)/(0.5*rho_set*pi*R_set**2*U_set**2)

# Driving force
RPM_set
U_rot=RPM_set/60.0*2*pi
torque_set=-(Cp_set*0.5*rho_set*pi*R_set**2*U_set**3)/U_rot

# Scaled force in [N]
Fx_scaled=(Fx/R)/torque*R**2
r_scaled=r/R
torque_scaled=torque

# Rescale driving force
Fx=Fx_scaled*torque_set/R_set

# Neglect Radial forces
Fy=0*Fy

########################################
### Sanity check of the circle sum ----------------------
#print "Test the circle area: ", sum(circle)/(pi*R**2)
fid = open(str(outfile),'w')
fid.write('%d %d %d\n'%(nrad,ntheta,6))
sumFz2=0
torqueCheck=0
for iA in range(nrad):
    for iT in range(ntheta):
        br=r[iA]
        alpha = 2.0 * pi * iT / ntheta + pi/2.0 # Hence, alpha is defined from positive y-axis, clockwise.
        x2 = -br * np.sin(alpha)/R #Horizontal
        y2 = br * np.cos(alpha)/R #Height
        Fx2 = Fx[iA]/ntheta * np.cos(alpha) - Fy[iA]/ntheta * np.sin(alpha)
        Fy2 = Fx[iA]/ntheta * np.sin(alpha) + Fy[iA]/ntheta * np.cos(alpha)
        Fz2 = -Fz[iA]/ntheta
        # Change rotational direction:
        #Fx2 = -Fx[iA]/ntheta * np.cos(alpha) - Fy[iA]/ntheta * np.sin(alpha)
        #Fy2 = -Fx[iA]/ntheta * np.sin(alpha) + Fy[iA]/ntheta * np.cos(alpha)
        fid.write("%12.8f %12.8f %12.8f %12.8f %12.8f %12.8f\n"%(x2,y2,0.0,Fz2,Fx2,Fy2))
        torqueCheck=torqueCheck+Fx[iA]/ntheta*br/R*R_set
fid.close()

print ""
print "Check AD grid output"
print "Thrust       = ",sum(Fz)
print "Ct           = ",sum(Fz)/(0.5*rho_set*U_set**2*R_set**2*pi)
print "Torque       = ",torqueCheck
U_rot=RPM_set/60.0*2*pi
power=-U_rot*torqueCheck
print "Cp           = ",power/(0.5*rho_set*pi*R_set**2*U_set**3)



