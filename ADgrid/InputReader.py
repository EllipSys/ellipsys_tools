# $Rev::             $: Revision of last commit
# $Author::          $: Author of last commit
# $Date::            $: Date of last commit

import numpy as np
from numpy import array
""" Read the input file and add it to a dictionary """
def str2num(string):
    """ If it's a int, return an int
        If it's a float, return an float (works with 1.00e-4 & 1.00d-4 form as well)
        If it's a string, return an string """
    try:
        return int(string)
    except:
        try: 
            return float(string.lower().replace('d','e'))
        except:
            return string

def writestuff(fid,x):
    """ write x into it's correct format into fid """
    if isinstance(x, int):
        fid.write('%d'%x)
    elif isinstance(x, float):
        if abs(x)<1E-3:
            fid.write('%e'%x)
        else:
            fid.write('%f'%x)
    elif isinstance(x, np.ndarray):
        if len(x.shape)==1:
            fid.write('array([')
            for i in range(x.size):
                writestuff(fid,x[i])
                if i < x.size - 1:
                    fid.write(', ')
            fid.write('])')
        elif len(x.shape)==2:
            fid.write('array([')
            for i in range(x.shape[0]):
                fid.write('[')
                for j in range(x.shape[1]):
                    writestuff(fid,x[i,j])
                    if j < x.shape[1] - 1:
                        fid.write(', ')
                fid.write(']')
                if i < x.shape[0] - 1:
                    fid.write(', ')
            fid.write('])')
    else:
        fid.write('%s'%x)
        

class InputFile:
    """ Create a dictionary (in self.Dic) which give access to all the elements in the input file.
        The function self.write() write the modified dictionary into a file
    """
    def __init__(self, inp):
        """ 
        Create the dictionary, and load the file called filename. 

        \param inp   an input (filename or dictionary)
        """
        if isinstance(inp, str):
            ### Hopefully it is a filename
            filename = inp
            # Init the input dictionary
            self.filename = filename
            self.Dic = {}
            self.ordered_keys = []
            self.comments = []
            self.load(filename)
        elif isinstance(inp, dict):
            self.Dic = inp
        # TODO: when there is no filename?
    
    def __getitem__(self, stname):
        """ Return the dictionary content stname """
        return self.Dic[stname]
    
    def load(self, filename='input.dat'):
        import re
        """ Loading the input file:
        If no filename is provided, it loads the input.dat file. """
        # Split the file in lines
        lines = open(filename).read().splitlines()
        for i in range(len(lines)):
            # Split the line
            st = lines[i].split()
            if len(st)>0:
                # check if it is not a comment
                if st[0][0] <> '%' and st[0][0] <> '#': 
                    i_array = lines[i].find('array')
                    if i_array>-1:
                        ### it's an array
                        self.Dic[st[0]] = eval(lines[i][i_array:])
                    else:
                        # Prepare the rest of the line by removing the comments 
                        # and unnecessary characteres
                        st2 = re.split('[;%#]', ' '.join(st[1:]))
                        # add the line to the input dictionary
                        self.Dic[st[0]] = st2[0].split()
                        self.ordered_keys.append(st[0])
                        for i in range(len(self.Dic[st[0]])):
                            self.Dic[st[0]][i] = str2num(self.Dic[st[0]][i])
                        if len(self.Dic[st[0]]) == 1:
                            self.Dic[st[0]] = self.Dic[st[0]][0]
                else:
                    self.comments.append(lines[i])
                    self.ordered_keys.append(len(self.comments)-1)
    
    def write(self, filename=''):
        """ Write down the input file """
        # If there is no filename given, use the one given at the input
        if len(filename) == 0:
            filename = self.filename
        else:
            self.filename = filename
        fid = open(filename,'w')
        for key in self.ordered_keys:
            if isinstance(key,int):
                ### it's a comment
                fid.write('%s\n'%self.comments[key])
            else:
                value = self.Dic[key]
                fid.write('%s'%key)
                for i in range(20-len(key)):
                    fid.write(' ')
                if isinstance(value, list):
                    for i in value:
                        writestuff(fid,i)
                        fid.write('   ')
                else:
                    writestuff(fid,value)
                fid.write('\n')
        fid.close()

