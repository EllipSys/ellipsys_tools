      program pseudo3d
C======================================================================
C     Small program for generating a 3D mesh for a zig-zag tape
c     based on extrution/interpolation between two 2D grid slice.
c     The program assumes that the slices are single block 
c     o-mesh type 'grid.DAT' from hypgrid2d
C
C     Author: Niels N. S�rensen
C======================================================================
      implicit none
      real(kind=8),dimension(:,:),allocatable::x2d,z2d,x2c,z2c
      real(kind=8),dimension(:,:,:),allocatable::x,y,z
      integer,dimension(:,:,:),allocatable::attr
      real(kind=8)::pi,alpha,len,factor
      integer ni,nj,nk,i,j,k,attr2d 

      pi=4.d0*atan(1.d0)

      print*,' Give spanwise length : '
      read*,len

c--------allocate 2d vertices
      open(unit=10,file='gridA.DAT')
      read(10,*)ni,nk
      allocate(x2d(ni,nk),z2d(ni,nk))
      do k=1,nk
      do i=1,ni
        read(10,*)attr2d,x2d(i,k),z2d(i,k)
      enddo;enddo
      close(10)
c--------allocate 2d vertices
      open(unit=10,file='gridB.DAT')
      read(10,*)ni,nk
      allocate(x2c(ni,nk),z2c(ni,nk))
      do k=1,nk
      do i=1,ni
        read(10,*)attr2d,x2c(i,k),z2c(i,k)
      enddo;enddo
      close(10)

c--------allocate 3d vertices
      print*,' Give number of vertices in j-direction : '
      read*,nj
      allocate(x(ni,nj,nk),y(ni,nj,nk),z(ni,nj,nk),attr(ni,nj,nk))

c--------compute 3d vertice location
      do k=1,nk
      do j=1,(nj-1)/2+1
      do i=1,ni
        factor=real(j-1)/real((nj-1)/2)
        if(k.eq.1.and.i.eq.1)print*,j,factor
        x(i,j,k)=x2d(i,k)*factor+x2c(i,k)*(1-factor)
        y(i,j,k)=.5*len*factor
        z(i,j,k)=z2d(i,k)*factor+z2c(i,k)*(1-factor)
      enddo;enddo;enddo
      print*,' Second half : '
      do k=1,nk
      do j=(nj-1)/2+1,nj
      do i=1,ni
        factor=real(j-(nj-1)/2-1)/real((nj-1)/2)
        if(k.eq.1.and.i.eq.1)print*,j,factor
        x(i,j,k)=x2c(i,k)*factor+x2d(i,k)*(1-factor)
        y(i,j,k)=.5*(len+len*factor)
        z(i,j,k)=z2c(i,k)*factor+z2d(i,k)*(1-factor)
      enddo;enddo;enddo
c-------assure identical periodic planes
      x(1:ni,1,1:nk)=x2c(1:ni,1:nk)
      z(1:ni,1,1:nk)=0.d0
      z(1:ni,1,1:nk)=z2c(1:ni,1:nk)
      x(1:ni,nj,1:nk)=x2c(1:ni,1:nk)
      z(1:ni,nj,1:nk)=len
      z(1:ni,nj,1:nk)=z2c(1:ni,1:nk)
    
c--------boundary condition
      attr=1
      attr(:,nj,:)=599
      attr(:,1 ,:)=599
      attr(:,:,nk)=401
      attr(33:ni-32,:,nk)=201
      attr(:,:,1)=101

      call UnformattedWrite(ni,nj,nk,x,y,z,attr)

      stop
   21 format(i6,3(f18.12))
  666 write(*,*) ' error: too few data in input file (grid.dat)'
      end

      subroutine UnformattedWrite(ni,nj,nk,x,y,z,attr)
C=======================================================================
C
C=======================================================================
      implicit none
      integer::ni,nj,nk,i,j,k,n,io,jo,ko
      integer::nbi,nbj,nbk,nbm,nib
      integer::dim,factor,ii,jj,kk,istart,jstart,kstart
      real(kind=8),dimension(ni,nj,nk)::x,y,z
      Integer,dimension(ni,nj,nk)::attr
      real(kind=8),dimension(:,:,:,:),allocatable::xb,yb,zb
      integer,dimension(:,:,:,:),allocatable::attrb

      print*,'*******************************************************'
      print*,' Mesh size ni,nj,nk : ',ni,nj,nk
      print*,' Give block size : '
      read*,dim
      print*,' Give reduction factor: '
      read*,factor
      nbi=ni/dim
      nbj=nj/dim
      nbk=nk/dim
      nbm=nbi*nbj*nbk
      print*,'*******************************************************'
      print*,'size of multidomaine blocks         : ',dim/factor
      print*,'number of block in xi direction     : ',nbi
      print*,'number of block in eta direction    : ',nbj
      print*,'number of block in zeta direction   : ',nbk
      print*,'total number of blocks              : ',nbm
      print*,'*******************************************************'

      nib=dim+3
      allocate(xb(nib,nib,nib,nbm)
     &        ,yb(nib,nib,nib,nbm)
     &        ,zb(nib,nib,nib,nbm)
     &     ,attrb(nib,nib,nib,nbm))
      xb=0.d0;yb=0.d0;zb=0.d0;attrb=0
      n=0
      do kk=1,nbk; kstart=(kk-1)*dim
      do jj=1,nbj; jstart=(jj-1)*dim
      do ii=1,nbi; istart=(ii-1)*dim
        n=n+1
        do k=2,dim+2,factor; ko=kstart+k-1
        do j=2,dim+2,factor; jo=jstart+j-1
        do i=2,dim+2,factor; io=istart+i-1
          xb(i,j,k,n)=x(io,jo,ko)
          yb(i,j,k,n)=y(io,jo,ko)
          zb(i,j,k,n)=z(io,jo,ko)
          attrb(i,j,k,n)=attr(io,jo,ko)
        enddo;enddo;enddo
      enddo;enddo;enddo

      print*,' test ',nib,n
c     call UnfGridWrite(20,dim/factor,nbm,xb,yb,zb,attrb)
      call UnfGridWrite(20,nib,n,xb,yb,zb,attrb)
      return
      end



      subroutine UnfGridWrite(unitnr,ni,nblock,x,y,z,attr)
C=======================================================================
C     routine for writing unformatted x3d file
C     remember that the grid must positions in the following region 
C
C        x(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C        y(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C        z(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C     attr(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C
C     the remaining spaces are intended for ghost cells
C=======================================================================
      implicit none
      integer unitnr,ni,nblock
      real(kind=8),dimension(ni,ni,ni,nblock)::x,y,z 
      integer,dimension(ni,ni,ni,nblock)::attr

C     Code to be used to add ghost cells before call UnfGridWrite
c     do n=1,nblock
c     do k=2,ni-1
c     do j=2,ni-1
c     do i=2,ni-1
c       xg   (i,j,k,n)=x   (i-1,j-1,k-1,n)
c       yg   (i,j,k,n)=y   (i-1,j-1,k-1,n)
c       zg   (i,j,k,n)=z   (i-1,j-1,k-1,n)
c       attrg(i,j,k,n)=attr(i-1,j-1,k-1,n)
c     enddo;enddo;enddo
c     enddo

      open(unit=unitnr,file='grid.x3dunf',form='unformatted')
      write(unitnr)ni-3
      write(unitnr)nblock
      call WriteBlockInteger(20,ni**3,nblock,attr)
      call WriteBlockReal   (20,ni**3,nblock,x   )
      call WriteBlockReal   (20,ni**3,nblock,y   )
      call WriteBlockReal   (20,ni**3,nblock,z   )
      close(unitnr)
      return
      end

      subroutine WriteBlockReal(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      real(kind=8),dimension(block,nblock)::x
      real(kind=8),dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine WriteBlockReal

      subroutine WriteBlockInteger(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      integer,dimension(block,nblock)::x
      integer,dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine WriteBlockInteger
