      program quarteniontest
C#######################################################################
C     A program for computing the necessary transformation to allign
C     the normal and the first vector of a plane defined by two vectors, 
C     with the normal and the first vector of another plane defined by
C     two vectors. (normals are computed by cross product)
C     For ellipsys purpose this would typically be used to allign e.g.
C     a blade axis (used as the first vector of the first plane) with
C     the x-axis of the cartesian intial system (used as the first vector 
C     of the second plane) 
C     
C     The program delivers the rotation quartenion needed to performe 
C     this allignment, which can be used directly in the EllipSys input 
C     file to assure that e.g. three rotor blades are alligned with the 
C     x-axis for extracting output.
C
C     Author: Niels N. Sørensen
C#######################################################################
      implicit none
      integer::i
      real(kind=8),dimension(4)::u1,u2,u3
     &                          ,v1,v2,v3
     &                          ,q,qc,p,pt
     &                          ,u1b,u2b
      real(kind=8)::len,pi

      pi=atan(1.d0)*4.d0

c-------read vectors into quartenion
      print*,' ################################################  '
      print*,'                                     '
      print*,' Give two vectors to define a plane :'
      print*,'                                     '
      print*,' ##################################  '
      print*,'                                     '
      print*,' Give first vector u1_x, u1_y, u1_z  : '
      print*,'                                     '
      u1=0.d0
      read(*,*)u1(2),u1(3),u1(4)
      len=1.d0/sqrt(u1(2)**2+u1(3)**2+u1(4)**2)
      u1=u1*len

      print*,' Give second vector u2_x, u2_y, u2_z  : '
      u2=0.d0
      read(*,*)u2(2),u2(3),u2(4)
      len=1.d0/sqrt(u2(2)**2+u2(3)**2+u2(4)**2)
      u2=u2*len

      print*,' ################################################  '
      print*,'                                     '
      print*,' Give two vectors defining a plane to allign with :'
      print*,'                                     '
      print*,' ################################################  '
      print*,'                                     '
      print*,' Give axis to allign first vector to  v1_x, v1_y, v1_z  :'
      print*,'                                     '
      v1=0.d0
      read(*,*)v1(2),v1(3),v1(4)
      len=1.d0/sqrt(v1(2)**2+v1(3)**2+v1(4)**2)
      v1=v1*len

      print*,' Give second vector defining the plane v2_x, v2_y, v2_z :'
      print*,'                                     '
      v2=0.d0
      read(*,*)v2(2),v2(3),v2(4)
      len=1.d0/sqrt(v2(2)**2+v2(3)**2+v2(4)**2)
      v2=v2*len

      u3=0.d0
      u3(2)=u1(3)*u2(4)-u1(4)*u2(3)
      u3(3)=u1(4)*u2(2)-u1(2)*u2(4)
      u3(4)=u1(2)*u2(3)-u1(3)*u2(2)
      len=1.d0/sqrt(u3(2)**2+u3(3)**2+u3(4)**2)
      u3=u3*len

      u1b=u1;u2b=u2
      call ComputeTransQuart(u1,u2,v1,v2,q,qc)
      u1=u1b;u2=u2b


      print*,' first vector in org coordinat system : '
      print*,u1(2:4)
      print*,'                                     '
      call quartmult(u1,qc,pt)
      call quartmult(q,pt,p)
      print*,' first vector in new coordinat system : '
      print*,p(2:4)
      print*,'                                     '

      print*,' second vector in org coordinat system : '
      print*,u2(2:4)
      print*,'                                     '
      call quartmult(u2,qc,pt)
      call quartmult(q,pt,p)
      print*,' second vector in new coordinat system : '
      print*,p(2:4)
      print*,'                                     '

      print*,'  Normal in org coordinat system : '
      print*,u3(2:4)
      print*,'                                     '
      call quartmult(u3,qc,pt)
      call quartmult(q,pt,p)
      print*,' Inormal in new coordinat system : '
      print*,p(2:4)
      print*,'                                     '
      

      print*,' ################################### '
      print*,' # Rotation Quartenion : ########### ' 
      print*,q
      print*,' ################################### '
c     call quartmult(q,qc,pt)
c     print*,' pt : ',pt
c     call quartmult(qc,q,pt)
c     print*,' pt : ',pt

      stop
      end

      subroutine ComputeTransQuart(va1,vb1,va2,vb2,q,qc)
c-----------------------------------------------------------------------
c     Compute the necessary transformation quartenions that will
c     allign the plane extended by va1 and vb1 to be alligned with
c     the plane extended by va2 and vb2. This is done by first alligning
c     the plane normals, and then alligning va1 and va2
c
c     The tangent vectors are quartenions with zero scalar value
c
c     Format for quaternions
c     q=(s,v1,v2,v3)^T
c-----------------------------------------------------------------------
      implicit none
      real(kind=8),dimension(4)::va1,vb1,n1,va2,vb2,n2,w1,w2,p,q,qc,s,sc
      real(kind=8)::len

c-------normalize quartenions
      len=1.d0/sqrt(va1(2)**2+va1(3)**2+va1(4)**2)
      va1=va1*len
      len=1.d0/sqrt(vb1(2)**2+vb1(3)**2+vb1(4)**2)
      vb1=vb1*len
      len=1.d0/sqrt(va2(2)**2+va2(3)**2+va2(4)**2)
      va2=va2*len
      len=1.d0/sqrt(vb2(2)**2+vb2(3)**2+vb2(4)**2)
      vb2=vb2*len
 
c-------determine rotation quartenion to allign the two plane normals
      w1=0
      w1(2)=va1(3)*vb1(4)-va1(4)*vb1(3)
      w1(3)=va1(4)*vb1(2)-va1(2)*vb1(4)
      w1(4)=va1(2)*vb1(3)-va1(3)*vb1(2)
      w2=0
      w2(2)=va2(3)*vb2(4)-va2(4)*vb2(3)
      w2(3)=va2(4)*vb2(2)-va2(2)*vb2(4)
      w2(4)=va2(2)*vb2(3)-va2(3)*vb2(2)
      call rotationquart(w1,w2,q,qc)
c-------rotate to allign with original normal
c-------vector 1
      call QuartMult(va1,qc,p)
      call QuartMult(q,p,va1)
c-------store first rotation
      s=q;sc=qc
c-------determine rotation quartenion to allign the transformed va1 with va2
      w1=0;w1(2)=va1(2);w1(3)=va1(3);w1(4)=va1(4)
      w2=0;w2(2)=va2(2);w2(3)=va2(3);w2(4)=va2(4)
      call rotationquart(w1,w2,q,qc)
c-------combined transformations
      call QuartMult(sc,qc,p);qc=p
      call QuartMult(q ,s ,p);q =p
c     print*,' q(1), qc(1) : ',q(1),qc(1)
c     print*,' q(2), qc(2) : ',q(2),qc(2)
c     print*,' q(3), qc(3) : ',q(3),qc(3)
c     print*,' q(4), qc(4) : ',q(4),qc(4)
      return
      end


      subroutine rotationquart(p,q,r,rc)
c-----------------------------------------------------------------------
c     Determine the quartenion and its conjugated that will rotate the 
c     vector stored in p to be alligned with the vector stored in q by
c     the operation
c
c     p_transformed=r x (p x rc), where x is quartenion multiplication
c
c
c     Format for quaternions
c     q=(s,v1,v2,v3)^T
c-----------------------------------------------------------------------
      implicit none
      integer::i,j,dim
      real(kind=8),dimension(4)::p,q,r,rc
      real(kind=8),dimension(3)::t
      real(kind=8)::len

      r(1)=(p(2)*q(2)+p(3)*q(3)+p(4)*q(4))
      len=sqrt((p(2)**2+p(3)**2+p(4)**2)
     &        *(q(2)**2+q(3)**2+q(4)**2))   
      if(r(1)/len.eq.-1.d0)then
c-------rotate 180 around any orthogonal vector
       r=p
       r(1)=0.d0
       t(1:3)=abs(p(2:4))
       dim=3
       i=maxloc(t,dim)
       j=minloc(t,dim)
       print*,' i,j ',i,j
       r(j+1)=-p(i+1)
       r(i+1)= p(j+1)
      else
        r(1)=r(1)+len
        r(2)=p(3)*q(4)-p(4)*q(3)
        r(3)=p(4)*q(2)-p(2)*q(4)
        r(4)=p(2)*q(3)-p(3)*q(2)
      endif
      len=1/sqrt(r(1)**2+r(2)**2+r(3)**2+r(4)**2)
      r=r*len
c-------conjugated
      rc=-r;rc(1)=-rc(1)

      return
      end

      subroutine quartmult(p,q,r)
c-----------------------------------------------------------------------
c     multiplication of two quartenions
c     q=(s,v1,v2,v3)^T
c-----------------------------------------------------------------------
      implicit none
      real(kind=8),dimension(4)::p,q,r
      real(kind=8)::len

      r(1)=p(1)*q(1)-(p(2)*q(2)+p(3)*q(3)+p(4)*q(4))
      r(2)=p(1)*q(2)+q(1)*p(2)+p(3)*q(4)-p(4)*q(3)
      r(3)=p(1)*q(3)+q(1)*p(3)+p(4)*q(2)-p(2)*q(4)
      r(4)=p(1)*q(4)+q(1)*p(4)+p(2)*q(3)-p(3)*q(2)

      return
      end
