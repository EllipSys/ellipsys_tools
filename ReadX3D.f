      program ReadX3D
      implicit none
      integer(kind=4):: ni,nblock
      integer(kind=8):: msize,msize2
      logical lexist
     
      inquire(file='grid.X3D',exist=lexist) 
      if(lexist)then
        open(unit=10,file='grid.X3D',form='unformatted')
        print*,' Reading grid.X3D : '
        goto 100
      endif
      inquire(file='dummy.X3D',exist=lexist) 
      if(lexist)then
        open(unit=10,file='dummy.X3D',form='unformatted')
        print*,' Reading dummy.X3D : '
        goto 100
      endif
      inquire(file='grid.x3dunf',exist=lexist) 
      if(lexist)then
        open(unit=10,file='grid.x3dunf',form='unformatted')
        print*,' Reading grid.x3dunf : '
        goto 100
      endif
      inquire(file='dummy.x3dunf',exist=lexist) 
      if(lexist)then
        open(unit=10,file='dummy.x3dunf',form='unformatted')
        print*,' Reading dummy.x3dunf : '
        goto 100
      endif

  100 read(10)ni
      read(10)nblock
      msize=ni**3
      msize=msize*nblock
      msize2=(ni+3)**3
      msize2=msize2*nblock

      print*,' Number of blocks : ',nblock
      print*,' Size of blocks   : ',ni
      print*,' Mesh       size x 1e-6 : ',int(msize*1e-6)
      print*,' Mesh Ghost size x 1e-6 : ',int(msize2*1e-6)
      stop
      end
