      module params
      integer,parameter::idp4=4,idp=8
      integer::bsize,nblock,ni
      integer,dimension(:,:,:),allocatable::attr
      end

      program RunTimePlot
c-----------------------------------------------------------------------
c     Program to postprocess unformatted transient data files from     c 
c     EllipSys2D.                                                      c
c     Output file format: Plot3D                                       c
c     Author: Frederik Zahle                                           c
c     Date:   11.04.2008                                               c
c-----------------------------------------------------------------------
      use params
c-----------------------------------------------------------------------
      implicit none
      real(kind=idp4),dimension(:,:,:,:),allocatable::u,v,w,p,psurf
      real(kind=idp4),dimension(:,:,:,:),allocatable::te,omega,vis
      integer::i,j,k,n,nstep,nplot,npl,nvar,procnumber
      integer::yesno,nlen1
      real(kind=idp)::time,procpref,pref,l1
      character(len=256) filename,filename1,filename2,filename3
      integer,parameter::reduce=1
      logical::shift


      call SYSTEM("ls -1 *_ave_*.PLT > filenames")
      call SYSTEM("wc -l filenames > numfiles")
      open(10,file='filenames',status='unknown')
      open(20,file='numfiles',status='unknown')
      read(20,*)nplot

      print*,'Number of plot files to process:',nplot

c     print*,'Treat system as periodic? yes=1,no=0'
c     read*,yesno
c     if(yesno.eq.1)then
c     print*,'enter domain width'
c     read*,l1
c     endif

      filename(1:18)='grid_ave_00000.PLT'

      do npl=1,nplot
      read(10,*)filename
      open(unit=1,file=filename,form='unformatted')

      read(1) bsize
      read(1) nblock 
      read(1) nstep
      read(1) time
      read(1) nvar
      print*,'nvar =',nvar
      print*,'bsize =',bsize
      ni=bsize+3
      allocate(u(ni,ni,ni,nblock)
     &        ,v(ni,ni,ni,nblock)
     &        ,w(ni,ni,ni,nblock)
     &        ,p(ni,ni,ni,nblock)
     &        ,vis(ni,ni,ni,nblock)
     &        ,te(ni,ni,ni,nblock)
     &        ,omega(ni,ni,ni,nblock))

      call ReadReal(1,ni**3,u)
      call ReadReal(1,ni**3,v)
      call ReadReal(1,ni**3,w)
      call ReadReal(1,ni**3,p)
c     call ReadInteger(1,ni**2,donor)
      if(nvar.gt.3)then
         call ReadReal(1,ni**3,vis)
         call ReadReal(1,ni**3,te)
         call ReadReal(1,ni**3,omega)
      endif
      shift=.false.

c     call MoveToVertex(u)
c     call MoveToVertex(v)
c     call MoveToVertex(w)
c     call MoveToVertex(p)
c     call MoveToVertex(vis)
c     call MoveToVertex(te)
c     call MoveToVertex(omega)

c     do n=1,nblock
c     do j=1,bsize+2
c     do i=1,bsize+2
c        if(attr(i,j,n).gt.250.and.
c    &      attr(i,j,n).lt.260)iblank(i,j,n)=1.d0
c     enddo;enddo
c     enddo

c---- Write FieldView plot3d unformatted grid file ---------------------
      filename2(1:16)='grid_ave_00000.f'
      write(filename2(10:14),'(i5.5)')npl

      filename3(1:18)='grid_ave_00000.nam'
      write(filename3(10:14),'(i5.5)')npl

c---- write grid.f file ------------------------------------------------
      open(unit=3,file=filename2(1:16),form='unformatted')
      rewind(3)
      if(nvar.eq.4)then
      nvar=4
      write(3)nblock
      write(3)((bsize+1)/reduce+1
     &        ,(bsize+1)/reduce+1
     &        ,(bsize+1)/reduce+1,nvar,n=1,nblock)
      do n=1,nblock
      write(3)
     &   (((real(u(i,j,k,n),idp4)
     &     ,i=1,bsize+2,reduce)
     &     ,j=1,bsize+2,reduce)
     &     ,k=1,bsize+2,reduce),
     &   (((real(v(i,j,k,n),idp4)
     &     ,i=1,bsize+2,reduce)
     &     ,j=1,bsize+2,reduce)
     &     ,k=1,bsize+2,reduce),
     &   (((real(w(i,j,k,n),idp4)
     &     ,i=1,bsize+2,reduce)
     &     ,j=1,bsize+2,reduce)
     &     ,k=1,bsize+2,reduce),
     &   (((real(p(i,j,k,n),idp4)
     &     ,i=1,bsize+2,reduce)
     &     ,j=1,bsize+2,reduce)
     &     ,k=1,bsize+2,reduce)
      enddo
      else
      nvar=7
      write(3)nblock
      write(3)((bsize+1)/reduce+1
     &        ,(bsize+1)/reduce+1
     &        ,(bsize+1)/reduce+1,nvar,n=1,nblock)
      do n=1,nblock
      write(3)
     &   (((real(u(i,j,k,n),idp4)
     &     ,i=1,bsize+2,reduce)
     &     ,j=1,bsize+2,reduce)
     &     ,k=1,bsize+2,reduce),
     &   (((real(v(i,j,k,n),idp4)
     &     ,i=1,bsize+2,reduce)
     &     ,j=1,bsize+2,reduce)
     &     ,k=1,bsize+2,reduce),
     &   (((real(w(i,j,k,n),idp4)
     &     ,i=1,bsize+2,reduce)
     &     ,j=1,bsize+2,reduce)
     &     ,k=1,bsize+2,reduce),
     &   (((real(p(i,j,k,n),idp4)
     &     ,i=1,bsize+2,reduce)
     &     ,j=1,bsize+2,reduce)
     &     ,k=1,bsize+2,reduce),
     &   (((real(te(i,j,k,n),idp4)
     &     ,i=1,bsize+2,reduce)
     &     ,j=1,bsize+2,reduce)
     &     ,k=1,bsize+2,reduce),
     &   (((real(omega(i,j,k,n),idp4)
     &     ,i=1,bsize+2,reduce)
     &     ,j=1,bsize+2,reduce) 
     &     ,k=1,bsize+2,reduce),
     &   (((real(vis(i,j,k,n),idp4)
     &     ,i=1,bsize+2,reduce)
     &     ,j=1,bsize+2,reduce)
     &     ,k=1,bsize+2,reduce)
      enddo
      endif
      close(3)

c     do n=1,nblock
c     write(33,*)
c    &   (((real(attr(i,j,n),idp4)
c    &     ,i=2,bsize+2,reduce)
c    &     ,j=2,bsize+2,reduce)
c    &     ,k=1,2,reduce),
c    &    (((real(x(i,j,n),idp4)
c    &      ,i=2,bsize+2,reduce)
c    &      ,j=2,bsize+2,reduce)
c    &      ,k=1,2,reduce)
c     enddo
c     close(33)

c---- write grid.nam file ------------------------------------------------
      open(unit=4,file=filename3(1:18))
      rewind(4)
      if(nvar.eq.5)then
         write(4,*)'u-velocity ; velocity'
         write(4,*)'v-velocity'
         write(4,*)'w-velocity'
         write(4,*)'pressure'
      else
         write(4,*)'u-velocity ; velocity'
         write(4,*)'v-velocity'
         write(4,*)'w-velocity'
         write(4,*)'pressure'
         write(4,*)'te'
         write(4,*)'omega'
         write(4,*)'vis'
      endif
      close(4)

c---- write grid.xyz.bnd file ------------------------------------------
c     call plotfieldview(reduce,npl)

      deallocate(u,v,w,p,vis,te,omega)
      enddo


      call SYSTEM("rm filenames numfiles")

c-----------------------------------------------------------------------
      end

      subroutine ReadReal(unit,block,x)
      use params
      implicit none
      integer unit,i,n,np,dn,count,block
      real(kind=idp4),dimension(block*nblock)::x
      real(kind=idp4),dimension(:),allocatable::xblock
C-----------------------------------------------------------------------
      allocate(xblock(block))
C--------------------------------- Read ROOTs own blocks ---------------
         do n=1,nblock;dn=(n-1)*block
            read(unit)xblock
            do i=1,block;x(i+dn)=xblock(i);end do
         end do
      end

      subroutine ReadInteger(unit,block,x)
      use params
      implicit none
      integer unit,i,n,np,dn,count,block
      integer,dimension(block*nblock)::x
      integer,dimension(:),allocatable::xblock

C-----------------------------------------------------------------------
      allocate(xblock(block))
C--------------------------------- Read ROOTs own blocks ---------------
         do n=1,nblock;dn=(n-1)*block
            read(unit)xblock
            do i=1,block;x(i+dn)=xblock(i);end do
         end do
      end

      subroutine plotfieldview(reduce,npl)
c----------------------------------------------------------------------
c     make plot3d grid and boundary file
c----------------------------------------------------------------------
      use params
      implicit none
*     integer,parameter::idp4=4
      integer::i,j,f,n,imin,imax,jmin,jmax,l,lmin,lmax,icc,jcc
     &        ,icca,jcca,k
      integer::attrmin,attrmax,nwallblock,npl,reduce
      logical::bctrue=.false.
*     real(kind=8),dimension(bsize+1,bsize+1,nb)::x,y
*     real(kind=8),dimension(bsize+1,bsize+1,2,nb)::z
*     integer,dimension(bsize+1,bsize+1,nb)::attr
      character(len=256) filename1

c---- make FieldView plot3d fvbnd file ---------------------------------
      filename1(1:19)='grid00000.xyz.fvbnd'
      write(filename1(5:9),'(i5.5)')npl
      open(unit=4,file=filename1(1:19),form='formatted')
      write(4,*)'FVBND 1 4'
      write(4,*)' wall'
      write(4,*)' inlet'
      write(4,*)' outlet'
      write(4,*)' cyclic'
      write(4,*)' symmetry'
c-----------------------------------------------------------------------
      write(4,*)'BOUNDARIES'
c--------------------------------------------- boundary regions -------
c---------------------------------------------walls --------------------
      nwallblock=0
      do i=1,5
      select case(i)
       case(1) 
         attrmin=101;attrmax=200
       case(2) 
         attrmin=201;attrmax=300
       case(3)  
         attrmin=401;attrmax=500
       case(4) 
         attrmin=501;attrmax=600
       case(5) 
         attrmin=601;attrmax=700
      end select 

      BLOCKS: do n=1,nblock
        FACE: do f=1,4
c--------------------------------------------- loop over face ----------
        do l=2,bsize+2
C-----------------------------------------------------------------------
          select case(f)
          case(1)
            icc=2;jcc=l
          case(2)
            icc=bsize+2;jcc=l
          case(3)
            icc=l;jcc=2
          case(4)
            icc=l;jcc=bsize+2
          end select
          if(.not.bctrue.and.l.lt.bsize+2.and.
     &                      (attr(icc,jcc,n).ge.attrmin.and.
     &                       attr(icc,jcc,n).le.attrmax))then
             bctrue=.true.
             lmin=l
          endif
c        if(bctrue)print*,'bctrue!!',bctrue
          if(bctrue.and.((attr(icc,jcc,n).lt.attrmin.or.
     &                   attr(icc,jcc,n).gt.attrmax).or
     &                   .l.gt.bsize))then
            bctrue=.false.
            lmax=l
c-----------eventual remove cell at upper end 
            if(attr(icc,jcc,n).gt.attrmax.or
     &        .attr(icc,jcc,n).lt.100)lmax=l-1
c-----------check lower end for additional cell
            if(lmin.gt.1)then
            select case(f)
            case(1)
              icca=1;jcca=lmin-1
            case(2)
              icca=bsize+1;jcca=lmin-1
            case(3)
              icca=lmin-1;jcca=1
            case(4)
              icca=lmin-1;jcca=bsize+1
            end select
            if(attr(icca,jcca,n).lt.attrmin)lmin=lmin-1
            endif
c------------------ write segment to file ------------------------------
             if(lmax.gt.lmin)then
             select case(f)
             case(1)
               write(4,'(8i5,1a4,i4)')i,n,1,1,lmin,lmax,1,2
     &                          ,'F',(-1)**f
             case(2)
                write(4,'(8i5,1a4,i4)')i,n,bsize+1,bsize+1,lmin,lmax,1,2
     &                         ,'F',(-1)**f
             case(3)
                write(4,'(8i5,1a4,i4)')i,n,lmin,lmax,1,1,1,2
     &                         ,'F',(-1)**f
             case(4)
               write(4,'(8i5,1a4,i4)')i,n,lmin,lmax,bsize+1,bsize+1,1,2
     &                         ,'F',(-1)**f
             end select
             endif
             lmin=bsize+2
           endif
         end do
      end do FACE
      end do BLOCKS
c-----------------------------------------------------------------------
      end do
      close(4)
c----------------------------------------------------------------------
      return
      end

      subroutine MoveToVertex(CC)
C-----------------------------------------------------------------------      
C  Interpolate data from cell centers to cell vertices.
C  The routine is intended for postprocessing purposes.
C  Reasonable values in ghostlayers are assumed.      
C-----------------------------------------------------------------------      
      use params
C-----------------------------------------------------------------------      
      implicit none
      integer i,j,k,n
      real(kind=idp4),dimension(ni,ni,ni,nblock):: CC,CV
C------------------------- Interpolate to cell vertices ----------------      
      do n=1,nblock
      do k=2,bsize+2; do j=2,bsize+2; do i=2,bsize+2
        CV(i,j,k,n)=.125d0*(CC(i,j  ,k  ,n)+CC(i-1,j  ,k  ,n)
     &                     +CC(i,j  ,k-1,n)+CC(i-1,j  ,k-1,n)
     &                     +CC(i,j-1,k  ,n)+CC(i-1,j-1,k  ,n)
     &                     +CC(i,j-1,k-1,n)+CC(i-1,j-1,k-1,n))
      end do; end do; end do
      end do
C-----------------------------------------------------------------------      
      CC=CV
C-----------------------------------------------------------------------      
      end

