      module coords 
      integer::ni,bsize,nblock
      real(kind=8),dimension(:,:,:),allocatable::x,y,z
      integer,dimension(:,:,:),allocatable::attr
      integer,dimension(:,:),allocatable::refine
      end module

      program refine
c-----------------------------------------------------------------------
c     program for refining 2D surface mesh
c     by a factor of two in both directions
c-----------------------------------------------------------------------
      implicit none

c------read original grid
      call readgrid
      
c------refine grid by splining existing grid lines
      call refinegrid

      stop
      end

      subroutine readgrid
c-----------------------------------------------------------------------
c
c-----------------------------------------------------------------------
      use coords
      implicit none
      integer::i,j,k,n

c-----read grid
      open(unit=20,file='org.x2d')
      print*,' READING GRID '
      read(20,*)bsize,nblock
      print*,bsize,nblock
      ni=bsize+1

      allocate(x(ni,ni,nblock),y(ni,ni,nblock),z(ni,ni,nblock)
     &        ,attr(ni,ni,nblock))
      do n=1,nblock
      do j=1,bsize+1
      do i=1,bsize+1
        read(20,*)attr(i,j,n),x(i,j,n),y(i,j,n),z(i,j,n)
      enddo;enddo;enddo
      close(20)


c------read refinement instructions
      allocate(refine(2,nblock))
c     open(unit=20,file='refinement.dat')
      do n=1,nblock
        refine(1,n)=1;refine(2,n)=1
c       read(20,*)refine(1,n),refine(2,n)
      enddo
c     close(20)
      return
      end

      subroutine refinegrid
c-----------------------------------------------------------------------
c
c-----------------------------------------------------------------------
      use coords
      implicit none
      integer::i,j,n,ii,jj,id,jd,ir,jr
      real(kind=8),dimension(ni,ni)::x1,y1,z1
      integer,dimension(ni,ni)::attr1
      real(kind=8),dimension(:,:),allocatable::xr,yr,zr
      integer,dimension(:,:),allocatable::attrr

  
      open(unit=10,file='grid.x2d')
c-------comnpute number of blocks in refined grid
      write(10,*)bsize*2,nblock
      do n=1,nblock
c-------refine block
        x1=x(:,:,n)
        y1=y(:,:,n)
        z1=z(:,:,n)
        attr1=attr(:,:,n)
        ir=bsize*2+1
        jr=bsize*2+1
        if(allocated(attrr))deallocate(attrr,xr,yr,zr)
        allocate(attrr(ir,jr),xr(ir,jr),yr(ir,jr),zr(ir,jr))
        call refineblock(ni,attr1,x1,y1,z1,ir,jr,attrr,xr,yr,zr)

c-------write block to output file
        do jd=1,jr
        do id=1,ir
          write(10,'(i6,3e25.16)')attrr(id,jd)
     &                 ,xr(id,jd)
     &                 ,yr(id,jd)
     &                 ,zr(id,jd)
        enddo;enddo
   
      enddo
      close(10)
      return
      end

      subroutine refineblock(ni,attr1,x1,y1,z1,ir,jr,attrr,xr,yr,zr)
c-----------------------------------------------------------------------
c
c-----------------------------------------------------------------------
      implicit none
      integer ni,ir,jr,i,j
      real(kind=8),dimension(ni,ni)::x1,y1,z1
      real(kind=8),dimension(ni)::x1d,y1d,z1d,s,xs,ys,zs
      real(kind=8),dimension(ir,jr)::xr,yr,zr
      integer,dimension(ni,ni)::attr1
      integer,dimension(ir,jr)::attrr
      real(kind=8)::xps,yps,zps,xi

c-------set attributes at common points between coarse and fine
      do j=1,ni
      do i=1,ni
          attrr(1+(i-1)*2,1+(j-1)*2)=attr1(i,j)
      enddo;enddo
c------- i-direction
      do j=1,jr,2
      do i=2,ir-1,2
        attrr(i,j)=min(attrr(i-1,j),attrr(i+1,j))
      enddo;enddo
c------- j-direction
      do j=2,jr-1,2
      do i=2,ir-1
        attrr(i,j)=min(attrr(i,j-1),attrr(i,j+1))
      enddo;enddo
  
c-----refine in i-direction
      do j=1,ni
c-------spline x,y and z in i-direction as function of i-index
        do i=1,ni
        x1d(i)=x1(i,j)
        y1d(i)=y1(i,j)
        z1d(i)=z1(i,j)
        enddo
        call SplineCurve(ni,x1d,y1d,z1d,s,xs,ys,zs)
        do i=1,ir
          xi=real(i-1)/real(ir-1)
          call splint(s,x1d,xs,ni,xi,xps)
          call splint(s,y1d,ys,ni,xi,yps)
          call splint(s,z1d,zs,ni,xi,zps)
          xr(i,(j-1)*2+1)=xps
          yr(i,(j-1)*2+1)=yps
          zr(i,(j-1)*2+1)=zps
        enddo
      enddo
c-----refine in j-direction
      do i=1,ir
c-------spline x,y and z in j-direction as function of j-index
        do j=1,ni
        x1d(j)=xr(i,(j-1)*2+1)
        y1d(j)=yr(i,(j-1)*2+1)
        z1d(j)=zr(i,(j-1)*2+1)
        enddo
        call SplineCurve(ni,x1d,y1d,z1d,s,xs,ys,zs)
        do j=1,jr
          xi=real(j-1)/real(jr-1)
          call splint(s,x1d,xs,ni,xi,xps)
          call splint(s,y1d,ys,ni,xi,yps)
          call splint(s,z1d,zs,ni,xi,zps)
          xr(i,j)=xps
          yr(i,j)=yps
          zr(i,j)=zps
        enddo
      enddo
       
      return
      end

      subroutine SplineCurve(ni,x,y,z,s,xs,ys,zs)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      implicit none
      integer ni,i
      real(kind=8),dimension(ni)::x,y,z,s,xs,ys,zs
      real(kind=8)::dx,dy,y1,yn

c-----spline profile data
      s(1)=0.d0
      do i=2,ni
        s(i)=real(i-1)/real(ni-1)
      enddo
c-----spline representation of surface (natural cubic spline)
      y1=1.d32
      yn=1.d32
      call spline(s,x,ni,y1,yn,xs)
      call spline(s,y,ni,y1,yn,ys)
      call spline(s,z,ni,y1,yn,zs)

      return
      end


      subroutine spline(x,y,n,yp1,ypn,y2)
c----------------------------------------------------------------------
c     Given arrays x and y of length n containing a tabulated function,
c     i.e y(i)=f(x(i)), with x(1)<x(2)<...<x(n), and given values yp1
c     and ypn for the first derivative of the interpolating function at
c     points 1 and n, respectively, this routine returns an array y2 of 
c     lenght n wich contains the second derivatives of the interpolating 
c     function at the tabulated points x(i). Uf yp1 and/or ypn are equal 
c     to 1.d30 or larger, the routine is signalled to the corresponding
c     boundary condition for a natural spline, with zero second derivative
c     on that boundary
c
c     Numerical Recipes
c----------------------------------------------------------------------
      implicit none
      integer nmax
      parameter(nmax=1000)
      real*8 x(n),y(n),y2(n),u(nmax)
      real*8 qn,p,un,sig,yp1,ypn
      integer i,n,k

      if (yp1.gt..99e30) then
        y2(1)=0.d0
        u(1)=0.d0
      else
        y2(1)=-0.5d0
        u(1)=(3./(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
      endif
      do 11 i=2,n-1
        sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
        p=sig*y2(i-1)+2.d0
        y2(i)=(sig-1.d0)/p
        u(i)=(6.*((y(i+1)-y(i))/(x(i+1)-x(i))-(y(i)-y(i-1))
     *      /(x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*u(i-1))/p
11    continue
      if (ypn.gt..99e30) then
        qn=0.
        un=0.
      else
        qn=0.5d0
        un=(3.d0/(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
      endif
      y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.d0)
      do 12 k=n-1,1,-1
        y2(k)=y2(k)*y2(k+1)+u(k)
12    continue
      return
      end

      subroutine splint(xa,ya,y2a,n,x,y)
c----------------------------------------------------------------------
c     Given the arrays xa and ya of length n, wich tabulate a function
c     (with the xa's in order), and given the array y2a, wich is the 
c     output from spline above, and given a value of x, this routine 
c     returns a cubic-spline interpolated value y. 
c
c     Numerical Recipes
c----------------------------------------------------------------------
      implicit none
      integer n
      real*8 xa(n),ya(n),y2a(n),x,y
      real*8 a,b,h
      integer klo,khi,k

      klo=1
      khi=n
1     if (khi-klo.gt.1) then
        k=(khi+klo)/2
        if(xa(k).gt.x)then
          khi=k
        else
          klo=k
        endif
      goto 1
      endif
      h=xa(khi)-xa(klo)
      if (h.eq.0.) pause 'bad xa input.'
      a=(xa(khi)-x)/h
      b=(x-xa(klo))/h
      y=a*ya(klo)+b*ya(khi)+
     *      ((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.d0
      return
      end
