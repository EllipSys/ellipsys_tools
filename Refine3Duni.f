c This is not working yet
      module params
      integer::nblock
      integer::ni,bsize
      integer::nir,bsizer
      end module

      module coords 
      real(kind=8),dimension(:,:,:,:),allocatable::x,y,z,xr,yr,zr
      integer,dimension(:,:,:,:),allocatable::attr,attrr
      end module

      program refine
c-----------------------------------------------------------------------
c     program for refining 3D volume mesh
c     by a factor of two in all directions
c-----------------------------------------------------------------------
      use params
      use coords
      implicit none
      integer n,i,j,k

c--------read original grid
      call readgrid
      
c-------refine grid by splining existing grid lines
      call refinegrid

c-------write out refined grid
      print*,' nir ',nir
      call UnfGridWrite(10,nir,nblock,xr,yr,zr,attrr)

      stop


        open(unit=20,file='out.x3d')
        write(20,*)nir-3,nblock
        do n=1,nblock
        do k=2,bsizer+2
        do j=2,bsizer+2
        do i=2,bsizer+2
          write(20,'(i4,3e30.20)')attrr(i,j,k,n),
     &                  xr(i,j,k,n),
     &                  yr(i,j,k,n),
     &                  zr(i,j,k,n)
        enddo;enddo;enddo
        enddo
        close(20)

      stop
      end

      subroutine readgrid
c-----------------------------------------------------------------------
c
c-----------------------------------------------------------------------
      use params
      use coords
      implicit none
      logical statex
      integer::i,j,k,n

      inquire(file='grid.x3dunf',exist=statex)
      if(statex)then
        print*,' READING UNFORMATTED GRID FILE'
        open(unit=20,file='grid.x3dunf',form='unformatted')
        read(20)bsize
        read(20)nblock
        print*,bsize,nblock
        ni=bsize+3
        allocate(x(ni,ni,ni,nblock)
     &          ,y(ni,ni,ni,nblock)
     &          ,z(ni,ni,ni,nblock)
     &       ,attr(ni,ni,ni,nblock))
        x=0.d0;y=0.d0;z=0.d0;attr=0
        print*,' Reading Attributs : '
        call ReadBlockInteger(20,ni**3,nblock,attr)
        print*,' Reading x-coord : '
        call ReadBlockReal   (20,ni**3,nblock,x)
        print*,' Reading y-coord : '
        call ReadBlockReal   (20,ni**3,nblock,y)
        print*,' Reading z-coord : '
        call ReadBlockReal   (20,ni**3,nblock,z)
        print*,' Finished Reading : '
        close(20)
      else
        inquire(file='grid.x3d',exist=statex)
        if(.not.statex)then
          print*,' No grid file, stopping '
          stop
        endif
        print*,' READING FORMATTED GRID FILE'
        open(unit=20,file='grid.x3d')
        read(20,*)bsize,nblock
        ni=bsize+3
        allocate(x(ni,ni,ni,nblock)
     &          ,y(ni,ni,ni,nblock)
     &          ,z(ni,ni,ni,nblock)
     &       ,attr(ni,ni,ni,nblock))
        print*,bsize,nblock
        do n=1,nblock
        do k=2,bsize+2
        do j=2,bsize+2
        do i=2,bsize+2
          read(20,*)attr(i,j,k,n)
     &            ,x(i,j,k,n)
     &            ,y(i,j,k,n)
     &            ,z(i,j,k,n)
        enddo;enddo;enddo;enddo
        close(20)
      endif

      return
      end

      subroutine refinegrid
c-----------------------------------------------------------------------
c
c-----------------------------------------------------------------------
      use params
      use coords
      implicit none
      integer::i,j,k,n,ii,jj,id,jd,b2,b2r
      real(kind=8),dimension(ni,ni,ni)::x1,y1,z1
      integer,dimension(ni,ni,ni)::attr1
      real(kind=8),dimension(:,:,:),allocatable::xrs,yrs,zrs
      integer,dimension(:,:,:),allocatable::attrrs

c-------allocate fine grid
      bsizer=2*bsize
      nir=bsizer+3
      allocate(xr   (nir,nir,nir,nblock)
     &        ,yr   (nir,nir,nir,nblock)
     &        ,zr   (nir,nir,nir,nblock)
     &        ,attrr(nir,nir,nir,nblock))
      xr=0.d0;yr=0.d0;zr=0.d0;attrr=0
  
      b2 =bsize+2
      b2r=bsizer+2
      if(allocated(attrrs))deallocate(attrrs,xrs,yrs,zrs)
      allocate(attrrs(nir,nir,nir)
     &           ,xrs(nir,nir,nir)
     &           ,yrs(nir,nir,nir)
     &           ,zrs(nir,nir,nir))
      do n=1,nblock
        print*,' Refining block : ',n
c-------refine block
        x1   (2:b2,2:b2,2:b2)=x   (2:b2,2:b2,2:b2,n)
        y1   (2:b2,2:b2,2:b2)=y   (2:b2,2:b2,2:b2,n)
        z1   (2:b2,2:b2,2:b2)=z   (2:b2,2:b2,2:b2,n)
        attr1(2:b2,2:b2,2:b2)=attr(2:b2,2:b2,2:b2,n)
       call refineblock(ni,attr1,x1,y1,z1,nir,attrrs,xrs,yrs,zrs)
c-------store in refined multiblock grid
        do k=2,b2r
        do j=2,b2r
        do i=2,b2r
           xr(i,j,k,n)=xrs(i,j,k)
           yr(i,j,k,n)=yrs(i,j,k)
           zr(i,j,k,n)=zrs(i,j,k)
        attrr(i,j,k,n)=attrrs(i,j,k)
        enddo;enddo;enddo
      enddo
      return
      end

      subroutine refineblock(ni,attr1,x1,y1,z1,nir,attrr,xr,yr,zr)
c-----------------------------------------------------------------------
c
c-----------------------------------------------------------------------
      implicit none
      integer ni,nir,i,j,k,ni1d
      real(kind=8),dimension(ni,ni,ni)::x1,y1,z1
      integer,dimension(ni,ni,ni)::attr1
      real(kind=8),dimension(ni-2)::x1d,y1d,z1d,s,xs,ys,zs
      real(kind=8),dimension(nir,nir,nir)::xr,yr,zr
      integer,dimension(nir,nir,nir)::attrr
      real(kind=8)::xps,yps,zps,xi
  
c-------set attributes at common points between coarse and fine
      do k=2,ni-1
      do j=2,ni-1
      do i=2,ni-1
          attrr(2+(i-2)*2,2+(j-2)*2,2+(k-2)*2)=attr1(i,j,k)
      enddo;enddo;enddo
c------- i-direction
      do k=2,nir-1,2
      do j=2,nir-1,2
      do i=3,nir-1,2
        attrr(i,j,k)=min(attrr(i-1,j,k),attrr(i+1,j,k))
      enddo;enddo;enddo
c------- j-direction
      do k=2,nir-1,2
      do j=3,nir-1,2
      do i=2,nir-1
        attrr(i,j,k)=min(attrr(i,j-1,k),attrr(i,j+1,k))
      enddo;enddo;enddo
c------- k-direction
      do k=3,nir-1,2
      do j=2,nir-1
      do i=2,nir-1
        attrr(i,j,k)=min(attrr(i,j,k-1),attrr(i,j,k+1))
      enddo;enddo;enddo
c     attrr=1

      ni1d=ni-2
c-----refine in i-direction
      do k=2,ni-1
      do j=2,ni-1
c-------spline x,y and z in i-direction as function of i-index
        do i=2,ni-1
        x1d(i-1)=x1(i,j,k)
        y1d(i-1)=y1(i,j,k)
        z1d(i-1)=z1(i,j,k)
        enddo
        call SplineCurve(ni1d,x1d,y1d,z1d,s,xs,ys,zs)
        do i=2,nir-1
          xi=real(i-2)/real(nir-3)
          call splint(s,x1d,xs,ni1d,xi,xps)
          call splint(s,y1d,ys,ni1d,xi,yps)
          call splint(s,z1d,zs,ni1d,xi,zps)
          xr(i,2+(j-2)*2,2+(k-2)*2)=xps
          yr(i,2+(j-2)*2,2+(k-2)*2)=yps
          zr(i,2+(j-2)*2,2+(k-2)*2)=zps
        enddo
      enddo;enddo
c-----refine in j-direction
      do k=2,ni-1
      do i=2,nir-1
c-------spline x,y and z in j-direction as function of j-index
        do j=2,ni-1
        x1d(j-1)=xr(i,2+(j-2)*2,2+(k-2)*2)
        y1d(j-1)=yr(i,2+(j-2)*2,2+(k-2)*2)
        z1d(j-1)=zr(i,2+(j-2)*2,2+(k-2)*2)
        enddo
        call SplineCurve(ni1d,x1d,y1d,z1d,s,xs,ys,zs)
        do j=2,nir-1
          xi=real(j-2)/real(nir-3)
          call splint(s,x1d,xs,ni1d,xi,xps)
          call splint(s,y1d,ys,ni1d,xi,yps)
          call splint(s,z1d,zs,ni1d,xi,zps)
          xr(i,j,2+(k-2)*2)=xps
          yr(i,j,2+(k-2)*2)=yps
          zr(i,j,2+(k-2)*2)=zps
        enddo
      enddo;enddo
c-----refine in k-direction
      do j=2,nir-1
      do i=2,nir-1
c-------spline x,y and z in j-direction as function of j-index
        do k=2,ni-1
        x1d(k-1)=xr(i,j,2+(k-2)*2)
        y1d(k-1)=yr(i,j,2+(k-2)*2)
        z1d(k-1)=zr(i,j,2+(k-2)*2)
        enddo
        call SplineCurve(ni1d,x1d,y1d,z1d,s,xs,ys,zs)
        do k=2,nir-1
          xi=real(k-2)/real(nir-3)
          call splint(s,x1d,xs,ni1d,xi,xps)
          call splint(s,y1d,ys,ni1d,xi,yps)
          call splint(s,z1d,zs,ni1d,xi,zps)
          xr(i,j,k)=xps
          yr(i,j,k)=yps
          zr(i,j,k)=zps
        enddo
      enddo;enddo

      return
      end

      subroutine SplineCurve(ni,x,y,z,s,xs,ys,zs)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      implicit none
      integer ni,i
      real(kind=8),dimension(ni)::x,y,z,s,xs,ys,zs
      real(kind=8)::dx,dy,y1,yn

c-----spline profile data
      s(1)=0.d0
      do i=2,ni
        s(i)=real(i-1)/real(ni-1)
      enddo
c-----spline representation of surface (natural cubic spline)
      y1=1.d32
      yn=1.d32
      call spline(s,x,ni,y1,yn,xs)
      call spline(s,y,ni,y1,yn,ys)
      call spline(s,z,ni,y1,yn,zs)

      return
      end


      subroutine spline(x,y,n,yp1,ypn,y2)
c----------------------------------------------------------------------
c     Given arrays x and y of length n containing a tabulated function,
c     i.e y(i)=f(x(i)), with x(1)<x(2)<...<x(n), and given values yp1
c     and ypn for the first derivative of the interpolating function at
c     points 1 and n, respectively, this routine returns an array y2 of 
c     lenght n wich contains the second derivatives of the interpolating 
c     function at the tabulated points x(i). Uf yp1 and/or ypn are equal 
c     to 1.d30 or larger, the routine is signalled to the corresponding
c     boundary condition for a natural spline, with zero second derivative
c     on that boundary
c
c     Numerical Recipes
c----------------------------------------------------------------------
      implicit none
      integer nmax
      parameter(nmax=1000)
      real*8 x(n),y(n),y2(n),u(nmax)
      real*8 qn,p,un,sig,yp1,ypn
      integer i,n,k

      if (yp1.gt..99e30) then
        y2(1)=0.d0
        u(1)=0.d0
      else
        y2(1)=-0.5d0
        u(1)=(3./(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
      endif
      do 11 i=2,n-1
        sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
        p=sig*y2(i-1)+2.d0
        y2(i)=(sig-1.d0)/p
        u(i)=(6.*((y(i+1)-y(i))/(x(i+1)-x(i))-(y(i)-y(i-1))
     *      /(x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*u(i-1))/p
11    continue
      if (ypn.gt..99e30) then
        qn=0.
        un=0.
      else
        qn=0.5d0
        un=(3.d0/(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
      endif
      y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.d0)
      do 12 k=n-1,1,-1
        y2(k)=y2(k)*y2(k+1)+u(k)
12    continue
      return
      end

      subroutine splint(xa,ya,y2a,n,x,y)
c----------------------------------------------------------------------
c     Given the arrays xa and ya of length n, wich tabulate a function
c     (with the xa's in order), and given the array y2a, wich is the 
c     output from spline above, and given a value of x, this routine 
c     returns a cubic-spline interpolated value y. 
c
c     Numerical Recipes
c----------------------------------------------------------------------
      implicit none
      integer n
      real*8 xa(n),ya(n),y2a(n),x,y
      real*8 a,b,h
      integer klo,khi,k

      klo=1
      khi=n
1     if (khi-klo.gt.1) then
        k=(khi+klo)/2
        if(xa(k).gt.x)then
          khi=k
        else
          klo=k
        endif
      goto 1
      endif
      h=xa(khi)-xa(klo)
      if (h.eq.0.) pause 'bad xa input.'
      a=(xa(khi)-x)/h
      b=(x-xa(klo))/h
      y=a*ya(klo)+b*ya(khi)+
     *      ((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.d0
      return
      end

      subroutine ReadBlockReal(unit,block,nblock,x)
      implicit none
      integer unit,nblock,block,i,n
      real(kind=8),dimension(block,nblock)::x
      real(kind=8),dimension(block)       ::xblock
        do n=1,nblock
          read(unit)xblock
          do i=1,block
            x(i,n)=xblock(i)
          end do
        end do
      end subroutine ReadBlockReal

      subroutine ReadBlockInteger(unit,block,nblock,x)
      implicit none
      integer unit,nblock,block,i,n
      integer,dimension(block,nblock)::x
      integer,dimension(block)       ::xblock
        do n=1,nblock
          read(unit)xblock
          do i=1,block
            x(i,n)=xblock(i)
          end do
        end do
      end subroutine ReadBlockInteger

      subroutine UnfGridWrite(unitnr,ni,nblock,x,y,z,attr)
C=======================================================================
C     routine for writing unformatted x3d file
C     remember that the grid must positions in the following region 
C
C        x(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C        y(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C        z(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C     attr(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C
C     the remaining spaces are intended for ghost cells
C=======================================================================
      implicit none
      integer unitnr,ni,nblock
      real(kind=8),dimension(ni,ni,ni,nblock)::x,y,z
      integer,dimension(ni,ni,ni,nblock)::attr

C     Code to be used to add ghost cells before call UnfGridWrite
c     do n=1,nblock
c     do k=2,ni-1
c     do j=2,ni-1
c     do i=2,ni-1
c       xg   (i,j,k,n)=x   (i-1,j-1,k-1,n)
c       yg   (i,j,k,n)=y   (i-1,j-1,k-1,n)
c       zg   (i,j,k,n)=z   (i-1,j-1,k-1,n)
c       attrg(i,j,k,n)=attr(i-1,j-1,k-1,n)
c     enddo;enddo;enddo
c     enddo

      print*,' unitnr ',unitnr
      open(unit=unitnr,file='out.x3dunf',form='unformatted')
      write(unitnr)ni-3
      write(unitnr)nblock
      print*,' Writing Attributes '
      call WriteBlockInteger(unitnr,ni**3,nblock,attr)
      print*,' Writing x '
      call WriteBlockReal   (unitnr,ni**3,nblock,x   )
      print*,' Writing y '
      call WriteBlockReal   (unitnr,ni**3,nblock,y   )
      print*,' Writing z '
      call WriteBlockReal   (unitnr,ni**3,nblock,z   )
      print*,' Finished writing '
      close(unitnr)
      return
      end

      subroutine WriteBlockReal(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      real(kind=8),dimension(block,nblock)::x
      real(kind=8),dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine WriteBlockReal

      subroutine WriteBlockInteger(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      integer,dimension(block,nblock)::x
      integer,dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine WriteBlockInteger
