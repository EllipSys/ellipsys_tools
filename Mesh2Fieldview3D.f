c     $Rev:: 8           $: Revision of last commit
c     $Author:: nsqr     $: Author of last commit
c     $Date:: 2011-08-02#$: Date of last commit
      module params 
c----------------------------------- common variables ------------------
      integer::bsize,nblock
      real(kind=8),dimension(:,:,:,:,:),allocatable::var
      integer,dimension(:,:,:,:),allocatable::attr
      integer,dimension(:,:,:,:),allocatable::attrcf
      end module


      program Mesh2FieldView
      use params
      implicit none
      integer::ni
      integer,parameter::idpo=8,ncomp=4
      real(kind=4)::dummy
      character*11 fnamd,fnamh
      character*128 fieldname(ncomp)
      real(kind=8),allocatable,dimension(:,:,:,:)::x
      integer::idummy
      integer::i,j,k,n,nc,nvar,fileform
      integer::b1,iblock
      logical::statex

      inquire(file='grid.x3dunf',exist=statex)
      if(statex)then
        print*,' READING UNFORMATTED GRID FILE'
        open(unit=20,file='grid.x3dunf',form='unformatted')
        read(20)bsize
        read(20)nblock
        print*,bsize,nblock
        ni=bsize+3
        allocate(x(ni,ni,ni,nblock))
        allocate(var(ncomp,ni,ni,ni,nblock),attr(ni,ni,ni,nblock))
        var=0.d0;x=0.d0
        print*,' Reading Attributs : '
        call ReadBlockInteger(20,ni**3,nblock,attr)
        print*,' Reading x-coord : '
        call ReadBlockReal   (20,ni**3,nblock,x)
        var(1,:,:,:,:)=x(:,:,:,:)
        print*,' Reading y-coord : '
        call ReadBlockReal   (20,ni**3,nblock,x)
        var(2,:,:,:,:)=x(:,:,:,:)
        print*,' Reading z-coord : '
        call ReadBlockReal   (20,ni**3,nblock,x)
        var(3,:,:,:,:)=x(:,:,:,:)
        print*,' Finished Reading : '
        deallocate(x)
        close(10)
      else
        inquire(file='grid.x3d',exist=statex)
        if(.not.statex)then
          print*,' No grid file, stopping '
          stop
        endif
        print*,' READING FORMATTED GRID FILE'
        open(unit=20,file='grid.x3d')
        read(20,*)bsize,nblock
        ni=bsize+3
        allocate(var(ncomp,ni,ni,ni,nblock),attr(ni,ni,ni,nblock))
        print*,bsize,nblock
        do n=1,nblock
        do k=2,bsize+2
        do j=2,bsize+2
        do i=2,bsize+2
          read(20,*)attr(i,j,k,n)  
     &            ,var(1,i,j,k,n)
     &            ,var(2,i,j,k,n)
     &            ,var(3,i,j,k,n)
        enddo;enddo;enddo;enddo
      endif

      print*,' Write unformatted=0, formatted=1 '
      read*,fileform
      if(fileform.eq.0)then
c-------make FieldView plot3d unformatted grid file
        open(unit=4,file='grid.xyz',form='unformatted')
        write(4)nblock
        write(4)(bsize+1,bsize+1,bsize+1,n=1,nblock)
        print*,'writing grid.xyz ...' 
        do n=1,nblock
          write(4)
     &      (((real(var(1,i,j,k,n),idpo)
     &        ,i=2,bsize+2),j=2,bsize+2),k=2,bsize+2),
     &      (((real(var(2,i,j,k,n),idpo)
     &        ,i=2,bsize+2),j=2,bsize+2),k=2,bsize+2),
     &      (((real(var(3,i,j,k,n),idpo)
     &        ,i=2,bsize+2),j=2,bsize+2),k=2,bsize+2)
        enddo
        close(4)
c-------make FieldView plot3d unformatted variable file
        print*,'writing grid.f ...'
        open(unit=4,file='grid.f',form='unformatted')
        write(4)nblock
        nvar = 1
        write(4)(bsize+1,bsize+1,bsize+1,nvar,n=1,nblock)
        do n=1,nblock
          write(4)(((real(attr(i,j,k,n),idpo)
     &              ,i=2,bsize+2),j=2,bsize+2),k=2,bsize+2)
        enddo
        close(4)
      else
c-----make FieldView plot3d formatted grid file
        open(unit=4,file='grid.xyz',form='formatted')
        write(4,*)nblock
        write(4,*)(bsize+1,bsize+1,bsize+1,n=1,nblock)
        print*,'writing grid.xyz ...' 
        do n=1,nblock
          write(4,*)
     &      (((real(var(1,i,j,k,n),idpo)
     &        ,i=2,bsize+2),j=2,bsize+2),k=2,bsize+2),
     &      (((real(var(2,i,j,k,n),idpo)
     &        ,i=2,bsize+2),j=2,bsize+2),k=2,bsize+2),
     &      (((real(var(3,i,j,k,n),idpo)
     &        ,i=2,bsize+2),j=2,bsize+2),k=2,bsize+2)
        enddo
        close(4)
c-------make FieldView plot3d formatted variable file
        open(unit=4,file='grid.f',form='formatted')
        write(4,*)nblock
        nvar = 1
        write(4,*)(bsize+1,bsize+1,bsize+1,nvar,n=1,nblock)
        print*,'writing grid.f ...'
        do n=1,nblock
          write(4,*)(((real(attr(i,j,k,n),idpo)
     &              ,i=2,bsize+2),j=2,bsize+2),k=2,bsize+2)
        enddo
        close(4)
      endif


      print*,'writing grid.nam ...'
      open(unit=22,file='grid.nam')
      write(22,*)'attr'
      close(22)

      print*,'writing grid.xyz.fvbnd ...'
      call ExtractWallBoundaryFile(nblock,ni,bsize,attr)

      stop
   30 format('')
   31 format('VARIABLES = ',15(A20,','))
   32 format('ZONE I=',i2,', J=',i2,', K=',i2,' F=BLOCK')
      end

      subroutine ExtractWallBoundaryFile(nblock,ni,bsize,attr)
c-----------------------------------------------------------------------
      implicit none
      character(len=13)zonename
      character(len=256) filename1
      integer ni,nblock,bsize
      integer,dimension(ni,ni,ni,nblock)::attr
      integer,dimension(ni,ni,6,nblock)::attrcv,attrcc
      integer lmin,lmax,mmin,mmax,llmax,n,l,m,i,j,k,ll,mm
      integer natr,imin,imax,jmin,jmax,kmin,kmax
      integer attrmin,attrmax
      integer bcpatch,f
      logical,dimension(ni,ni)::check
c-------face array storage of vertex attributs -------------------------
      attrcv=0;attrcc=0
      do n=1,nblock
      do m=2,bsize+2
      do l=2,bsize+2
        attrcv(l,m,1,n)=attr(2      ,l,m,n)
      enddo;enddo
      do m=2,bsize+2
      do l=2,bsize+2
        attrcv(l,m,2,n)=attr(bsize+2,l,m,n)
      enddo;enddo
      do m=2,bsize+2
      do l=2,bsize+2
        attrcv(m,l,3,n)=attr(l,2      ,m,n)
      enddo;enddo
      do m=2,bsize+2
      do l=2,bsize+2
        attrcv(m,l,4,n)=attr(l,bsize+2,m,n)
      enddo;enddo
      do m=2,bsize+2
      do l=2,bsize+2
        attrcv(l,m,5,n)=attr(l,m,2      ,n)
      enddo;enddo
      do m=2,bsize+2
      do l=2,bsize+2
        attrcv(l,m,6,n)=attr(l,m,bsize+2,n)
      enddo;enddo
      enddo
c-------cell face storage of cell face attributes ----------------------
      do n=1,nblock
      do f=1,6
      do m=2,bsize+1
      do l=2,bsize+1
        if(min(attrcv(l  ,m  ,f,n)
     &        ,attrcv(l+1,m  ,f,n)
     &        ,attrcv(l  ,m+1,f,n)
     &        ,attrcv(l+1,m+1,f,n)).gt.1)
     &    attrcc(l,m,f,n)=max(attrcv(l  ,m  ,f,n) 
     &                       ,attrcv(l+1,m  ,f,n)
     &                       ,attrcv(l  ,m+1,f,n)
     &                       ,attrcv(l+1,m+1,f,n))
      enddo;enddo
      enddo
      enddo
c-----------------------------------------------------------------------
      filename1(1:14)='grid.xyz.fvbnd'
        print*,'nblock = ',nblock
        open(unit=7, file=filename1(1:14),FORM='formatted')
        write(7,*)'FVBND 1 4'
        write(7,*)' wall'
        write(7,*)' inlet'
        write(7,*)' outlet'
        write(7,*)' periodic'
        write(7,*)' symmetry'
c-----------------------------------------------------------------------
        write(7,*)'BOUNDARIES'
c-----------------------------------------------------------------------
      natr=5
      ATTR_LOOP: do i=1,natr
        bcpatch=0
        select case(i)
        case(1)
          attrmin=101;attrmax=200
        case(2)
          attrmin=201;attrmax=300
        case(3)
          attrmin=401;attrmax=500
        case(4)
          attrmin=501;attrmax=600
        case(5)
          attrmin=601;attrmax=700
        end select
        BLOCK_LOOP: do n=1,nblock
        FACE_LOOP:  do f=1,6
c-----------mark all cell centres for checking
          check=.true.
c------------------ write segment to file ------------------------------
          do m=2,bsize+1
          do l=2,bsize+1
c---------find l,m minmum cell center of present patch in unchecked region
            if((attrcc(l,m,f,n).ge.attrmin.and.
     &          attrcc(l,m,f,n).le.attrmax).and.check(l,m))then
              lmin=l;mmin=m
c------------find max segment size in l-direction
            SEGMENT_L: do ll=lmin,bsize+1           
              if((attrcc(ll,mmin,f,n).lt.attrmin.or.
     &            attrcc(ll,mmin,f,n).gt.attrmax).or.
     &           (.not.check(ll,mmin)))exit SEGMENT_L
              lmax=ll
            enddo SEGMENT_L
c-------------find max segment size in m-direction
            SEGMENT_M: do mm=mmin,bsize+1           
            do ll=lmin,lmax
              if((attrcc(ll,mm,f,n).lt.attrmin.or.
     &            attrcc(ll,mm,f,n).gt.attrmax))exit SEGMENT_M
              if(ll.eq.lmax)mmax=mm
              enddo
           enddo SEGMENT_M
           check(lmin:lmax,mmin:mmax)=.false.
c------------write the patch to the boundary file
           lmax=lmax+1;mmax=mmax+1
           call WriteSegment(i,n,f,bsize,lmin,lmax,mmin,mmax)
           bcpatch=bcpatch+1
           lmin=2;mmin=32;lmax=2;mmax=32
           write(*,101)attrmin,n,f,bcpatch
        endif
       enddo
       enddo
      enddo FACE_LOOP
      enddo BLOCK_LOOP
      enddo ATTR_LOOP
      close(7)
      return
  101 format(' Patch: attr ',i3,' block ',i5,' face ',i1,' Patch ',i6)
      end

      subroutine WriteSegment(i,n,f,bsize,lmin,lmax,mmin,mmax)
      implicit none
      integer i,n,f,lmin,lmax,mmin,mmax,bsize
      integer imin,jmin,kmin,imax,jmax,kmax

      lmin=lmin-1;lmax=lmax-1;mmin=mmin-1;mmax=mmax-1
      select case(f)
      case(1)
       imin=1;imax=1;jmin=lmin;jmax=lmax;kmin=mmin;kmax=mmax
      case(2)
       imin=bsize+1;imax=bsize+1
       jmin=lmin;jmax=lmax;kmin=mmin;kmax=mmax
      case(3)
       imin=mmin;imax=mmax;jmin=1;jmax=1;kmin=lmin;kmax=lmax
      case(4)
       imin=mmin;imax=mmax;jmin=bsize+1;jmax=bsize+1
       kmin=lmin;kmax=lmax
      case(5)
       imin=lmin;imax=lmax;jmin=mmin;jmax=mmax;kmin=1;kmax=1
      case(6)
       imin=lmin;imax=lmax;jmin=mmin;jmax=mmax
       kmin=bsize+1;kmax=bsize+1
      end select
         write(7,'(8i6,1a4,i4)')i,n,imin,imax,jmin,jmax,kmin,kmax
     &                         ,'F',(-1)**f
c----------------------------------------------------------------------
      return
      end


      subroutine ReadBlockReal(unit,block,nblock,x)
      implicit none
      integer unit,nblock,block,i,n
      real(kind=8),dimension(block,nblock)::x
      real(kind=8),dimension(block)       ::xblock
        do n=1,nblock
          read(unit)xblock
          do i=1,block
            x(i,n)=xblock(i)
          end do
        end do
      end subroutine ReadBlockReal

      subroutine ReadBlockInteger(unit,block,nblock,x)
      implicit none
      integer unit,nblock,block,i,n
      integer,dimension(block,nblock)::x
      integer,dimension(block)       ::xblock
        do n=1,nblock
          read(unit)xblock
          do i=1,block
            x(i,n)=xblock(i)
          end do
        end do
      end subroutine ReadBlockInteger

