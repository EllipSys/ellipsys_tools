# fparser_convert

This tool can be used to create binary input files for the function parser. 

### Usage

Usage for converting a formatted file to a binary file:

    $ fparser_convert 0 <formatted_file> <binary_file>

Usage for converting a binary file to a formatted file:

    $ fparser_convert 1 <binary_file> <formatted_file>

Simply build fparser_convert by running:

    $ ./build.sh

The default compiler is ifort, which needs a flag -assume byterecl to be consistent
with gfortran. One can also use gfortran by uncommenting the corresponding line in build.sh.

### Format

The binary input file is stored in records for quick access during tun time:
The data consists of one flow variable that may be a function of two spatial 
variables and time. The spatial variables must be Cartesian.

| Record # | Description      |
| -------- | ---------------- |
| 1        | size information |
| 2        | x grid           |
| 3        | y grid           |
| 4        | z grid           |
| 5        | variable at t=t1 |
| 6        | variable at t=t2 |
| ...      |                  |
| 4+n      | variable at t=tn |


The formatted input file looks like:

|             |
| ----------- |
| nt nx ny nx |
| Tstart Tend |
| x(1)        |
| x(2)        |
| ...         |
| x(nx)       |
| y(1)        |
| y(2)        |
| ...         |
| y(ny)       |
| z(1)        |
| z(2)        |
| ...         |
| z(nz)       |
| var(t(1))   |
| var(t(2))   |
| ...         |
| var(t(n))   |

Here the first header line consists of nt (number of time intervals+1), and nx, 
ny, nz (number of grid points in x, y and z, respectively). The second header 
line provides the start and end time. Subsequently, a Cartesian grid is stored
if the variable is a function of space. If nx=1, no information of x is stored,
which also applies to ny and nz. The variable may be a function of maximal two
spatial coordinates (2D data). After the grid, one variable is stored for each
time interval. For cyclic transient data: var(t(1))=var(t(n)). If var is also
a function of space it stored as a flattened array:

|                               |
| ----------------------------- |
| var(t(1),x(1),y(1),z(1))      |
| var(t(1),x(1),y(1),z(2))      |
| ...                           |
| var(t(1),x(1),y(1),z(nz))     |
| var(t(1),x(1),y(2),z(1)))     |
| var(t(1),x(1),y(2),z(2)))     |
| ...                           |
| var(t(1),x(1),y(2),z(nz)))    |
| ...                           |
| var(t(1),x(1),y(ny),z(nz)))   |
| var(t(1),x(2),y(1),z(1)))     |
| ...                           |
| var(t(1),x(nx),y(ny),z(nz)))  |
| var(t(2),x(1),y(1),z(1)))     |

### Examples

Two examples of formatted input files are provided in this folder:

|                     |                                                                                                                                                      |
| ------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------- |
| Twall_formatted.dat | A cyclic transient wall temperature of an atmospheric diurnal cycle. The data has 24 hourly intervals between 0-86400 s. Grid data is not necessary. |
| u_formatted.dat     | A steady velocity profile, as function of z.                                                                                                         |
