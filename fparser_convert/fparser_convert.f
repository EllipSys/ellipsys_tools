      program fparser_convert
      integer nt,nx,ny,nz,recordlength,h,i,j,k,l,ndata
      integer,parameter::idp=kind(1d0),idp4=4
      real(idp)::Tstart,Tend,q(4),shift(3)
      real(idp),allocatable,dimension(:)::x,y,z,reczero
      real(idp),allocatable,dimension(:,:)::var
      real(idp4),allocatable,dimension(:)::x4,y4,z4,reczero4
      real(idp4),allocatable,dimension(:,:)::var4
      integer,parameter::mword=30,wordlen=1024,InputLineLen=2048
      character(len=wordlen)::infile,outfile,flag,modus
      integer nrchar(mword),nrword
      character(len=wordlen),dimension(mword):: words
      character(len=InputLineLen),dimension(5):: header
      character(len=256) text
      integer ifirst,ilast,namelenin,namelenout,version,minheadersize,
     &        iprecision
      logical lexist
      integer(kind=8) :: reclen
C----- User input from command line ------------------------------------
      call getarg(1,flag)
      call getarg(2,infile)
      call getarg(3,outfile)

      do ilast=1,256
        if(infile(ilast:ilast).eq.' ') exit
      enddo
      namelenin=ilast
      do ilast=1,256
        if(outfile(ilast:ilast).eq.' ') exit
      enddo
      namelenout=ilast

      if(flag(1:1).eq.'0')then
        print*,'Convert formatted file '
     &    ,infile(1:namelenin),'to unformatted file '
     &    ,outfile(1:namelenout)
C------ Read formatted file --------------------------------------------
        open(unit=1,file=infile(1:namelenin))
         ! Header
         read(1,'(a2048)')header(1)
         call cmdpart(header(1),InputLineLen,MWord,WordLen
     &              ,nrchar,nrword,words)
         read(words(1),*)version
         if(version.ge.1)then
           ! Old version
           print*,'Old version'
           minheadersize=32
           iprecision=2 ! Old format can only handle double precision
           nt=version
           read(words(2),*)nx
           read(words(3),*)ny
           read(words(4),*)nz
           read(1,'(a2048)')header(2)
            call cmdpart(header(2),InputLineLen,MWord,WordLen
     &                  ,nrchar,nrword,words)
            read(words(1),*)Tstart
            read(words(2),*)Tend
            print*,'size',nt,nx,ny,nz
            print*,'time',Tstart,Tend
         elseif(version.eq.-1)then
           ! New version with extended header
           print*,'New version'
           minheadersize=96
           read(1,'(a2048)')header(2)
           call cmdpart(header(2),InputLineLen,MWord,WordLen
     &                 ,nrchar,nrword,words)
           read(words(1),*)nt
           read(words(2),*)nx
           read(words(3),*)ny
           read(words(4),*)nz
           read(words(5),*)iprecision
           read(1,'(a2048)')header(3)
           call cmdpart(header(3),InputLineLen,MWord,WordLen
     &                 ,nrchar,nrword,words)
           read(words(1),*)Tstart
           read(words(2),*)Tend
           read(1,'(a2048)')header(4)
           call cmdpart(header(4),InputLineLen,MWord,WordLen
     &                 ,nrchar,nrword,words)
           read(words(1),*)shift(1)
           read(words(2),*)shift(2)
           read(words(3),*)shift(3)
           read(1,'(a2048)')header(5)
           call cmdpart(header(5),InputLineLen,MWord,WordLen
     &                 ,nrchar,nrword,words)
           read(words(1),*)q(1)
           read(words(2),*)q(2)
           read(words(3),*)q(3)
           read(words(4),*)q(4)
           print*,'size',nt,nx,ny,nz
           print*,'iprecision',iprecision
           print*,'time',Tstart,Tend
           print*,'shift',shift
           print*,'quaternion',q
           if(iprecision.eq.1)then
             print*,'Single precision'
           elseif(iprecision.eq.1)then
             print*,'Double precision'
           endif
         endif
         ! Grid
         ndata=nx*ny*nz
         allocate(x(nx),y(ny),z(nz),var(nt,ndata))
         if(nx.gt.1)then
           do i=1,nx
             read(1,*)x(i)
           enddo
         endif
         if(ny.gt.1)then
           do i=1,ny
             read(1,*)y(i)
           enddo
         endif
         if(nz.gt.1)then
           do i=1,nz
             read(1,*)z(i)
           enddo
         endif
         ! Data
         do h=1,nt
           do i=1,ndata
             read(1,*)var(h,i)
c             print*,i,var(h,i)
           enddo
         enddo
         close(1)
C--------- Write unformatted file --------------------------------------
         recordlength=max(minheadersize,4*ndata*iprecision)
         open(1,file=outfile(1:namelenout),access='DIRECT',
     &        recl=recordlength)
         ! assure that header is unique
         if(iprecision.eq.1)then
           allocate(reczero4(ndata))
           reczero4=0.d0
           write(1,rec=1)reczero4
           deallocate(reczero4)
         elseif(version.eq.-1)then
           allocate(reczero(ndata))
           reczero=0.d0
           write(1,rec=1)reczero
           deallocate(reczero)
         endif
         ! Header
         if(version.ge.1)then
           ! assure that header is unique
           allocate(reczero(ndata))
           reczero=0.d0
           write(1,rec=1)reczero
           deallocate(reczero)

           write(1,rec=1)nt,nx,ny,nz,Tstart,Tend
         elseif(version.eq.-1)then
           write(1,rec=1)version,nt,nx,ny,nz,iprecision,
     &                   Tstart,Tend,shift,q
         endif
         if(iprecision.eq.1)then
           ! Grid
           if(nx.gt.1)then
              write(1,rec=2)real(x, idp4)
           endif
           if(ny.gt.1)then
              write(1,rec=3)real(y, idp4)
           endif
           if(nz.gt.1)then
              write(1,rec=4)real(z, idp4)
           endif
           ! Data
           do h=1,nt
             write(1,rec=4+h)real(var(h,:), idp4)
           enddo
         elseif(iprecision.eq.2)then
           ! Grid
           if(nx.gt.1)then
              write(1,rec=2)x
           endif
           if(ny.gt.1)then
              write(1,rec=3)y
           endif
           if(nz.gt.1)then
              write(1,rec=4)z
           endif
           ! Data
           do h=1,nt
             write(1,rec=4+h)var(h,:)
           enddo
          endif
         close(1)
       elseif(flag(1:1).eq.'1')then
          print*,'Convert unformatted file '
     &    ,infile(1:namelenin),'to formatted file '
     &    ,outfile(1:namelenout)
C--------- Read unformatted file ---------------------------------------
         ! Get version
         open(1,file=infile(1:namelenin),access='DIRECT',
     &           recl=4)
         read(1,rec=1)version
         if(version.ge.1)then
           ! Old version
           print*,'Old version'
           minheadersize=32
           iprecision=2
         elseif(version.eq.-1)then
           ! New version with extended header
           print*,'New version'
           minheadersize=96
         endif
         close(1)
         ! Header
         open(1,file=infile(1:namelenin),access='DIRECT', 
     &           recl=minheadersize)
         if(version.ge.1)then
           read(1,rec=1)nt,nx,ny,nz,Tstart,Tend
           print*,'size',nt,nx,ny,nz
           print*,'time',Tstart,Tend
         elseif(version.eq.-1)then
           read(1,rec=1)version,nt,nx,ny,nz,iprecision,Tstart,Tend,
     &                  shift,q
           print*,'size',nt,nx,ny,nz
           print*,'iprecision',iprecision
           print*,'time',Tstart,Tend
           print*,'shift',shift
           print*,'quaternion',q
           if(iprecision.eq.1)then
             print*,'Single precision'
           elseif(iprecision.eq.2)then
             print*,'Double precision'
           endif
         endif
         ndata=nx*ny*nz
         close(1)
         recordlength=max(minheadersize,4*ndata*iprecision)
         open(1,file=infile(1:namelenin),access='DIRECT',
     &          recl=recordlength)
         if(iprecision.eq.1)then
           allocate(x4(nx),y4(ny),z4(nz),var4(nt,ndata))
           if(nx.gt.1)then
             read(1,rec=2)x4
           endif
           if(ny.gt.1)then
             read(1,rec=3)y4
           endif
           if(nz.gt.1)then
             read(1,rec=4)z4
           endif
           ! Data
           do h=1,nt
             read(1,rec=4+h)var4(h,:)
           enddo
         elseif(iprecision.eq.2)then
           allocate(x(nx),y(ny),z(nz),var(nt,ndata))
           ! Grid
           open(1,file=infile(1:namelenin),access='DIRECT', 
     &            recl=recordlength)
           if(nx.gt.1)then
             read(1,rec=2)x
           endif
           if(ny.gt.1)then
             read(1,rec=3)y
           endif
           if(nz.gt.1)then
             read(1,rec=4)z
           endif
           ! Data
           do h=1,nt
             read(1,rec=4+h)var(h,:)
           enddo
         endif
         close(1)
C--------- Write formatted file ----------------------------------------
         open(1,file=outfile(1:namelenout)) 
         ! Header
         if(version.ge.1)then
            write(1,*)nt,nx,ny,nz
            write(1,*)Tstart,Tend
         elseif(version.eq.-1)then
            write(1,*)version
            write(1,*)nt,nx,ny,nz,iprecision
            write(1,*)Tstart,Tend
            write(1,*)shift
            write(1,30)q
         endif
         if(iprecision.eq.1)then
           ! Grid
           if(nx.gt.1)then
             do i=1,nx
               write(1,*)x4(i)
             enddo
           endif
           if(ny.gt.1)then
             do i=1,ny
               write(1,*)y4(i)
             enddo
           endif
           if(nz.gt.1)then
             do i=1,nz
               write(1,*)z4(i)
             enddo
           endif
           ! Data
           do h=1,nt
             do i=1,ndata
               write(1,*)var4(h,i)
             enddo
           enddo
           deallocate(x4,y4,z4,var4)
         elseif(iprecision.eq.2)then
           ! Grid
           if(nx.gt.1)then
             do i=1,nx
               write(1,*)x(i)
             enddo
           endif
           if(ny.gt.1)then
             do i=1,ny
               write(1,*)y(i)
             enddo
           endif
           if(nz.gt.1)then
             do i=1,nz
               write(1,*)z(i)
             enddo
           endif
           ! Data
           do h=1,nt
             do i=1,ndata
               write(1,*)var(h,i)
             enddo
           enddo
           deallocate(x,y,z,var)
         endif
         close(1)
       endif
   30 format(4x,7(e16.9),1x,$)
       end
