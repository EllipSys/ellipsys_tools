      program MeshReadWrite
      implicit none
      logical::statex=.false.
      integer::ni
      integer,parameter::idp8=8,ncomp=4
      real(kind=8),dimension(:,:,:,:),allocatable::x,y,z
      integer,dimension(:,:,:,:),allocatable::attr
      real(kind=8),dimension(:,:,:,:),allocatable::xt,yt,zt
      integer,dimension(:,:,:,:),allocatable::attrt
      real(kind=8)::dummy
      character*11 fnamd,fnamh
      character*128 fieldname(ncomp)
      integer::idummy
      integer::i,j,k,n,nc,nvar,fileform
      integer::bsize,nblock,b1,iblock
      integer nn,val
      integer,allocatable,dimension(:)::number
      real::rand,srand
      real(kind=8)::r1
      logical::ncheck

      inquire(file='grid.x3dunf',exist=statex)
      if(statex)then
        print*,' READING UNFORMATTED GRID FILE'
        open(unit=20,file='grid.x3dunf',form='unformatted')
        read(20)bsize
        read(20)nblock
        print*,bsize,nblock
        ni=bsize+3
        allocate(x(ni,ni,ni,nblock)
     &          ,y(ni,ni,ni,nblock)
     &          ,z(ni,ni,ni,nblock)
     &       ,attr(ni,ni,ni,nblock))
        allocate(xt(ni,ni,ni,nblock)
     &          ,yt(ni,ni,ni,nblock)
     &          ,zt(ni,ni,ni,nblock)
     &       ,attrt(ni,ni,ni,nblock))
        allocate(number(nblock))
        x=0.d0;y=0.d0;z=0.d0;attr=0
        print*,' Reading Attributs : '
        call ReadBlockInteger(20,ni**3,nblock,attr)
        print*,' Reading x-coord : '
        call ReadBlockReal   (20,ni**3,nblock,x)
        print*,' Reading y-coord : '
        call ReadBlockReal   (20,ni**3,nblock,y)
        print*,' Reading z-coord : '
        call ReadBlockReal   (20,ni**3,nblock,z)
        print*,' Finished Reading : '
        close(10)
      else
        inquire(file='grid.x3d',exist=statex)
        if(.not.statex)then
          print*,' No grid file, stopping '
          stop
        endif
        print*,' READING FORMATTED GRID FILE'
        open(unit=20,file='grid.x3d')
        read(20,*)bsize,nblock
        ni=bsize+3
        allocate(x(ni,ni,ni,nblock)
     &          ,y(ni,ni,ni,nblock)
     &          ,z(ni,ni,ni,nblock)
     &       ,attr(ni,ni,ni,nblock))
        x=0.d0;y=0.d0;z=0.d0;attr=0
        print*,bsize,nblock
        do n=1,nblock
        do k=2,bsize+2
        do j=2,bsize+2
        do i=2,bsize+2
          read(20,*)attr(i,j,k,n)  
     &            ,x(i,j,k,n)
     &            ,y(i,j,k,n)
     &            ,z(i,j,k,n)
        enddo;enddo;enddo;enddo
      endif

c-------store old topology
      xt=x;yt=y;zt=z;attrt=attr

c-------randomize topology
      call random_seed()
      do n=1,nblock
        ncheck=.false.
        do while(ncheck.eq..false.)
        ncheck=.true.
        call random_number(r1)
        val=int(nblock*r1)+1
        do nn=1,n-1
          if(val.eq.number(nn))ncheck=.false.
        enddo
        enddo
        number(n)=val
      enddo

      do n=1,nblock
        print*,' n, n-new : ',n,number(n)
        x(:,:,:,n)=xt(:,:,:,number(n))
        y(:,:,:,n)=yt(:,:,:,number(n))
        z(:,:,:,n)=zt(:,:,:,number(n))
        attr(:,:,:,n)=attrt(:,:,:,number(n))
      enddo

      print*,' bef '
      call UnfGridWrite(20,ni,nblock,x,y,z,attr)
      print*,' aft '

      stop
   30 format('')
   31 format('VARIABLES = ',15(A20,','))
   32 format('ZONE I=',i2,', J=',i2,', K=',i2,' F=BLOCK')
      end

      subroutine ReadBlockReal(unit,block,nblock,x)
      implicit none
      integer unit,nblock,block,i,n
      real(kind=8),dimension(block,nblock)::x
      real(kind=8),dimension(block)       ::xblock
        do n=1,nblock
          read(unit)xblock
          do i=1,block
            x(i,n)=xblock(i)
          end do
        end do
      end subroutine ReadBlockReal

      subroutine ReadBlockInteger(unit,block,nblock,x)
      implicit none
      integer unit,nblock,block,i,n
      integer,dimension(block,nblock)::x
      integer,dimension(block)       ::xblock
        do n=1,nblock
          read(unit)xblock
          do i=1,block
            x(i,n)=xblock(i)
          end do
        end do
      end subroutine ReadBlockInteger

      subroutine UnfGridWrite(unitnr,ni,nblock,x,y,z,attr)
C=======================================================================
C     routine for writing unformatted x3d file
C     remember that the grid must positions in the following region 
C
C        x(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C        y(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C        z(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C     attr(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C
C     the remaining spaces are intended for ghost cells
C=======================================================================
      implicit none
      integer unitnr,ni,nblock
      real(kind=8),dimension(ni,ni,ni,nblock)::x,y,z
      integer,dimension(ni,ni,ni,nblock)::attr

C     Code to be used to add ghost cells before call UnfGridWrite
c     do n=1,nblock
c     do k=2,ni-1
c     do j=2,ni-1
c     do i=2,ni-1
c       xg   (i,j,k,n)=x   (i-1,j-1,k-1,n)
c       yg   (i,j,k,n)=y   (i-1,j-1,k-1,n)
c       zg   (i,j,k,n)=z   (i-1,j-1,k-1,n)
c       attrg(i,j,k,n)=attr(i-1,j-1,k-1,n)
c     enddo;enddo;enddo
c     enddo

      print*,' unitnr ',unitnr
      open(unit=unitnr,file='grid.X3DUNF',form='unformatted')
      write(unitnr)ni-3
      write(unitnr)nblock
      print*,' Writing Attributes '
      call WriteBlockInteger(unitnr,ni**3,nblock,attr)
      print*,' Writing x '
      call WriteBlockReal   (unitnr,ni**3,nblock,x   )
      print*,' Writing y '
      call WriteBlockReal   (unitnr,ni**3,nblock,y   )
      print*,' Writing z '
      call WriteBlockReal   (unitnr,ni**3,nblock,z   )
      print*,' Finished writing '
      close(unitnr)
      return
      end

      subroutine WriteBlockReal(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      real(kind=8),dimension(block,nblock)::x
      real(kind=8),dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine WriteBlockReal

      subroutine WriteBlockInteger(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      integer,dimension(block,nblock)::x
      integer,dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine WriteBlockInteger
