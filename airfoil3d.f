      program Airfoil3D
C-----------------------------------------------------------------------
C     Grid generator for 3D airfoil grid, based on sweeping and 
C     projecting a square 2D slize (grid.x2d) (x,y) onto a 
C     1-block Hypgrid2D  grid (grid.DAT)
C     Author: Niels N. Sørensen 
C     Date: April 2020
C-----------------------------------------------------------------------
      implicit none
      integer::i,j,k,n,l,f,ii,nn
      integer::ni,nis,njs,nbs,nbj,adum
      integer::nre,nrw,nrs,nrn
      real(kind=8)::y1,yn,sval,len,lsmax,lsmin,zsmin,zsmax
      real(kind=8),dimension(:),allocatable::xs,ys,s,xp,yp
      real(kind=8),dimension(:,:),allocatable::x2d,y2d
      real(kind=8),dimension(:,:,:),allocatable::zs,ls
      integer,dimension(:,:,:),allocatable::attrs
      real(kind=8),dimension(:,:),allocatable::zt,lt
      integer,dimension(:,:),allocatable::attrt
      real(kind=8),dimension(:,:,:,:),allocatable::x3s,y3s,z3s
     &                                            ,x3d,y3d,z3d
      integer,dimension(:,:,:,:),allocatable::attr3d,attr3s
      real(kind=8),dimension(2)::v1,v2

c-------read single block HypGrid2D grid
      open(unit=10,file='grid.DAT')
      read(10,*)nis,njs
      allocate(x2d(nis,njs),y2d(nis,njs))
      do j=1,njs
      do i=1,nis
        read(10,*)adum,x2d(i,j),y2d(i,j)
      enddo;enddo
      close(10)

C-------read grid slize. Must have base of ls=0
      open(unit=10,file='slice.x2d')
      read(10,*)ni,nbs
      ni=ni+1
      print*,' ni : ',ni
      allocate(zs(ni,ni,nbs),ls(ni,ni,nbs),attrs(ni,ni,nbs))
      do n=1,nbs
      do k=1,ni
      do j=1,ni
        read(10,*)attrs(j,k,n),zs(j,k,n),ls(j,k,n)
      enddo;enddo
      enddo
      close(10)
      zsmax=maxval(zs);zsmin=minval(zs)
      lsmax=maxval(ls);lsmin=minval(ls)
c-------set boundary conditions
      attrs=1
      nre=0;nrw=0;nrs=0;nrn=0
      do n=1,nbs
      do f=1,4;j=1+mod(f+1,2)*(ni-1);k=j
        do l=1,ni
          select case(f)
            case(1:2);k=l
            case(3:4);j=l
          end select
c---------set bc in decending order
        if(abs(zs(j,k,n)-zsmin).lt.1e-12)then
          attrs(j,k,n)=501
          nrw=nrw+1
        endif
        if(abs(zs(j,k,n)-zsmax).lt.1e-12)then
          attrs(j,k,n)=501
          nre=nre+1
        endif
        if(abs(ls(j,k,n)-lsmax).lt.1e-12)then
          attrs(j,k,n)=210
          nrn=nrn+1
        endif
        if(abs(ls(j,k,n)-lsmin).lt.1e-12)then
          attrs(j,k,n)=101
          nrs=nrs+1
        endif
      enddo;enddo
      enddo

      print*,' West  bc : ',nrw,zsmin
      print*,' East  bc : ',nre,zsmax
      print*,' South bc : ',nrs,lsmin
      print*,' North bc : ',nrn,lsmax

C-------check that grid orintation is right-hand 
      allocate(zt(ni,ni),lt(ni,ni),attrt(ni,ni))
      do n=1,nbs
        v1(1)=zs(ni,1,n)-zs(1,1,n) 
        v1(2)=ls(ni,1,n)-ls(1,1,n) 
        v2(1)=zs(1,ni,n)-zs(1,1,n) 
        v2(2)=ls(1,ni,n)-ls(1,1,n) 
        len=v1(1)*v2(2)-v2(1)*v1(2)
        if(len.gt.0.d0)then
c---------transpose block
c         print*,' negative block : ',n,len
          zt(:,:)=zs(:,:,n)
          lt(:,:)=ls(:,:,n)
          attrt(:,:)=attrs(:,:,n)
          do k=1,ni
          do j=1,ni
            zs(ni+1-j,k,n)=zt(j,k)
            ls(ni+1-j,k,n)=lt(j,k)
            attrs(ni+1-j,k,n)=attrt(j,k)
          enddo;enddo
        endif
      enddo

      do n=1,nbs
        v1(1)=zs(2,1,n)-zs(1,1,n) 
        v1(2)=ls(2,1,n)-ls(1,1,n) 
        v2(1)=zs(1,2,n)-zs(1,1,n) 
        v2(2)=ls(1,2,n)-ls(1,1,n) 
        len=v1(1)*v2(2)-v2(1)*v1(2)
        if(len.gt.0.d0)then
          print*,' negative block : ',n,len
          stop
        endif
      enddo

c--------check that 2D grid and slice can be combined
      if(mod(nis-1,ni-1).ne.0)then
        print*,' grid.DAT do not fit with block size ni '
        stop
      else
        nbj=(nis-1)/(ni-1)
      endif

C-------Generate 3D grid single block in j-direction
      allocate(x3s(nis,ni,ni,nbs),y3s(nis,ni,ni,nbs),z3s(nis,ni,ni,nbs))
      allocate(attr3s(nis,ni,ni,nbs))
      allocate(xp(njs),yp(njs),s(njs),xs(njs),ys(njs))
      do i=1,nis
        print*,' Treating i = ',i
c--------compute curvelength
        s(1)=0.d0
        do j=2,njs
          s(j)=s(j-1)+sqrt((x2d(i,j)-x2d(i,j-1))**2
     &                    +(y2d(i,j)-y2d(i,j-1))**2)
        enddo
        xp(:)=x2d(i,:)
        yp(:)=y2d(i,:)
c---------enforced 2d slice attributes
        do n=1,nbs
        do k=1,ni
        do j=1,ni
          attr3s(i,j,k,n)=attrs(j,k,n)
        enddo;enddo
        enddo
c---------determine x and y by spline interpolation
        y1=1d32;yn=1d32
        call spline(s,xp,njs,y1,yn,xs)
        call spline(s,yp,njs,y1,yn,ys)
        do n=1,nbs
        do k=1,ni
        do j=1,ni
          call splint(s,xp,xs,njs,ls(j,k,n),sval)
          x3s(i,j,k,n)=sval
        enddo;enddo
        enddo
        do n=1,nbs
        do k=1,ni
        do j=1,ni
          call splint(s,yp,ys,njs,ls(j,k,n),sval)
          y3s(i,j,k,n)=sval
        enddo;enddo
        enddo
        do n=1,nbs
        do k=1,ni
        do j=1,ni
          z3s(i,j,k,n)=zs(j,k,n)
        enddo;enddo
        enddo
      enddo

C-------recut grid to cubic blocks
      allocate(x3d(ni+2,ni+2,ni+2,nbs*nbj)
     &        ,y3d(ni+2,ni+2,ni+2,nbs*nbj)
     &        ,z3d(ni+2,ni+2,ni+2,nbs*nbj)
     &     ,attr3d(ni+2,ni+2,ni+2,nbs*nbj))
     
      x3d=0.d0;y3d=0.d0;z3d=0.d0;attr3d=1

      do nn=1,nbj
        do n=1,nbs
        do k=1,ni 
        do j=1,ni
        do i=1,ni;ii=(nn-1)*(ni-1)+i
          attr3d(i+1,j+1,k+1,n+(nn-1)*nbs)=attr3s(ii,j,k,n)
        enddo;enddo;enddo;enddo
        do n=1,nbs
        do k=1,ni 
        do j=1,ni
        do i=1,ni;ii=(nn-1)*(ni-1)+i
          x3d(i+1,j+1,k+1,n+(nn-1)*nbs)=x3s(ii,j,k,n)
        enddo;enddo;enddo;enddo
        do n=1,nbs
        do k=1,ni 
        do j=1,ni
        do i=1,ni;ii=(nn-1)*(ni-1)+i
          y3d(i+1,j+1,k+1,n+(nn-1)*nbs)=y3s(ii,j,k,n)
        enddo;enddo;enddo;enddo
        do n=1,nbs
        do k=1,ni 
        do j=1,ni
        do i=1,ni;ii=(nn-1)*(ni-1)+i
          z3d(i+1,j+1,k+1,n+(nn-1)*nbs)=z3s(ii,j,k,n)
        enddo;enddo;enddo;enddo
      enddo

c-------Write 3D grid to file
      call UnfGridWrite(20,ni+2,nbs*nbj,x3d,y3d,z3d,attr3d)

      stop
      end
   
     
c     $Rev::             $: Revision of last commit
c     $Author::          $: Author of last commit
c     $Date::            $: Date of last commit
      subroutine spline(x,y,n,yp1,ypn,y2)
c----------------------------------------------------------------------
c     Given arrays x and y of length n containing a tabulated function,
c     i.e y(i)=f(x(i)), with x(1)<x(2)<...<x(n), and given values yp1
c     and ypn for the first derivative of the interpolating function at
c     points 1 and n, respectively, this routine returns an array y2 of 
c     lenght n wich contains the second derivatives of the interpolating 
c     function at the tabulated points x(i). Uf yp1 and/or ypn are equal 
c     to 1.d30 or larger, the routine is signalled to the corresponding
c     boundary condition for a natural spline, with zero second derivative
c     on that boundary
c
c     Numerical Recipes
c----------------------------------------------------------------------
      implicit none
c     integer nmax
c     parameter(nmax=1000)
      real*8 x(n),y(n),y2(n),u(n)
      real*8 qn,p,un,sig,yp1,ypn
      integer i,n,k

      if (yp1.gt..99e30) then
        y2(1)=0.d0
        u(1)=0.d0
      else
        y2(1)=-0.5d0
        u(1)=(3./(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
      endif
      do 11 i=2,n-1
        sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
        p=sig*y2(i-1)+2.d0
        y2(i)=(sig-1.d0)/p
        u(i)=(6.*((y(i+1)-y(i))/(x(i+1)-x(i))-(y(i)-y(i-1))
     *      /(x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*u(i-1))/p
11    continue
      if (ypn.gt..99e30) then
        qn=0.
        un=0.
      else
        qn=0.5d0
        un=(3.d0/(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
      endif
      y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.d0)
      do 12 k=n-1,1,-1
        y2(k)=y2(k)*y2(k+1)+u(k)
12    continue
      return
      end

      subroutine splint(xa,ya,y2a,n,x,y)
c----------------------------------------------------------------------
c     Given the arrays xa and ya of length n, wich tabulate a function
c     (with the xa's in order), and given the array y2a, wich is the 
c     output from spline above, and given a value of x, this routine 
c     returns a cubic-spline interpolated value y. 
c
c     Numerical Recipes
c----------------------------------------------------------------------
      implicit none
      integer n
      real*8 xa(n),ya(n),y2a(n),x,y
      real*8 a,b,h
      integer klo,khi,k

      klo=1
      khi=n
1     if (khi-klo.gt.1) then
        k=(khi+klo)/2
        if(xa(k).gt.x)then
          khi=k
        else
          klo=k
        endif
      goto 1
      endif
      h=xa(khi)-xa(klo)
      if (h.eq.0.) pause 'bad xa input.'
      a=(xa(khi)-x)/h
      b=(x-xa(klo))/h
      y=a*ya(klo)+b*ya(khi)+
     *      ((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.d0
      return
      end



      subroutine UnfGridWrite(unitnr,ni,nblock,x,y,z,attr)
C=======================================================================
C     routine for writing unformatted x3d file
C     remember that the grid must positions in the following region 
C
C        x(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C        y(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C        z(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C     attr(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C
C     the remaining spaces are intended for ghost cells
C=======================================================================
      implicit none
      integer unitnr,ni,nblock
      real(kind=8),dimension(ni,ni,ni,nblock)::x,y,z
      integer,dimension(ni,ni,ni,nblock)::attr

C     Code to be used to add ghost cells before call UnfGridWrite
c     do n=1,nblock
c     do k=2,ni-1
c     do j=2,ni-1
c     do i=2,ni-1
c       xg   (i,j,k,n)=x   (i-1,j-1,k-1,n)
c       yg   (i,j,k,n)=y   (i-1,j-1,k-1,n)
c       zg   (i,j,k,n)=z   (i-1,j-1,k-1,n)
c       attrg(i,j,k,n)=attr(i-1,j-1,k-1,n)
c     enddo;enddo;enddo
c     enddo

      print*,' unitnr ',unitnr
      open(unit=unitnr,file='grid.X3DUNF',form='unformatted')
      write(unitnr)ni-3
      write(unitnr)nblock
      print*,' Writing Attributes '
      call WriteBlockInteger(unitnr,ni**3,nblock,attr)
      print*,' Writing x '
      call WriteBlockReal   (unitnr,ni**3,nblock,x   )
      print*,' Writing y '
      call WriteBlockReal   (unitnr,ni**3,nblock,y   )
      print*,' Writing z '
      call WriteBlockReal   (unitnr,ni**3,nblock,z   )
      print*,' Finished writing '
      close(unitnr)
      return
      end

      subroutine WriteBlockReal(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      real(kind=8),dimension(block,nblock)::x
      real(kind=8),dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine WriteBlockReal

      subroutine WriteBlockInteger(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      integer,dimension(block,nblock)::x
      integer,dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine WriteBlockInteger
