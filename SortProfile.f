      program SortProfile
      implicit none
      character(len=64)::arg
      logical::closed=.false.,uchord=.false.,horizontal=.false.
     &        ,sort=.true.
      real(kind=8),dimension(:,:),allocatable::var,vart
      real(kind=8)::maxlen,len,aoa,x0,y0,pi,varmin,cploc,cpval
      real(kind=8),dimension(2)::n1,n2,a
      real(kind=8)::dx,dy,fpx,fpy,mpz,fvx,fvy,mvz,apx,apy,anx,any
      real(kind=8)::dfpx,dfpy,dfvx,dfvy
      real(kind=8),dimension(1)::xt,yt,zt
      real(kind=8)::xr=0.25,yr=0.0
      real(kind=8)::factor=1.d0
      real(kind=8),dimension(:),allocatable::xi,yi,zi,xo,yo,zo
      integer ni,i,j,n,imaxlen,icploc,nin
      integer::npoints=-1,nstart=-1
C-----------------------------------------------------------------------
C     The program assumes that the files holdes the following information
C     x,y,z
C-----------------------------------------------------------------------

c-----read scale factor from command line
      do i=1,iargc(),2
        call getarg(i,arg)
     
        select case(arg(1:2))
c---------vertical scaling factor
        case('-f')
          call getarg(i+1,arg)
          read(arg,*)factor
c-------close trailing edge
        case('-t')
          call getarg(i+1,arg)
          if(arg(1:6).eq.'closed')closed=.true.
c-------respline profile to have a given number of points
        case('-p')
          call getarg(i+1,arg)
          read(arg,*)npoints
c--------specify starting point for sort
        case('-s')
          call getarg(i+1,arg)
          read(arg,*)nstart
c--------scale to unit chord
        case('-u')
          call getarg(i+1,arg)
          if(arg(1:4).eq.'true')uchord=.true.
c--------do not sort
        case('-n')
          call getarg(i+1,arg)
          if(arg(1:5).eq.'false')sort=.false.
c--------make airfoil horizontal
        case('-h')
          call getarg(i+1,arg)
          if(arg(1:4).eq.'true')horizontal=.true.
        end select
      enddo
      print*,' sort profile ',sort
      print*,' close trail. ',closed
      print*,' vert. factor ',factor
      print*,' nr points    ',npoints
      print*,' idx   start  ',nstart
      print*,' unit choord  ',uchord
      print*,' horizontal   ',horizontal

      pi=atan(1.d0)*4

c--------preliminary read to determine the number of points
      ni=0
      allocate(var(3,1))
      open(unit=10,file='prof.dat')
      do
        read(10,*,end=666)(var(n,1),n=1,3)
        ni=ni+1
      enddo
  666 close(10)
      if(allocated(var))deallocate(var)  
      open(unit=10,file='prof.dat')
      if(allocated(vart))print*,' already allocated '
      allocate(vart(3,ni))
      vart=0.d0
c-------read file, removing non-unique points
      nin=0
      do i=1,ni
        read(10,*)(vart(n,i-nin),n=1,3)
        do j=1,i-nin-1
          if(((vart(1,i-nin)-vart(1,j))**2+(vart(2,i-nin)-vart(2,j))**2)
     &    .lt.1.d-12)then
             nin=nin+1
             print*,' Point : ',i,' not unique '
             exit
          endif
        enddo
      enddo
      print*,' Found ',nin,' non-unique points !!! '
      ni=ni-nin
      allocate(var(3,ni))
      var(:,1:ni)=vart(:,1:ni)
      deallocate(vart)
      call computeangle(ni,3,var,x0,y0,maxlen,aoa)
c-------rotate profile to make it horizontal
      n1(1)= cos(aoa)
      n1(2)= sin(aoa)
      n2(1)=-sin(aoa)
      n2(2)= cos(aoa)
      do i=1,ni
        a(1)=var(1,i)-x0
        a(2)=var(2,i)-y0
        var(1,i)=n1(1)*a(1)+n1(2)*a(2)
        var(2,i)=n2(1)*a(1)+n2(2)*a(2)
      enddo
c-------scale airfoil to chord = 1
      do i=1,ni
        var(1,i)=var(1,i)/maxlen
        var(2,i)=var(2,i)/maxlen*factor
        var(3,i)=var(3,i)/maxlen
      enddo

c------sort airfoil data
       if(sort)then
         call pointsort(ni,nstart,var(1,:),var(2,:),var(3,:))
         print*,' finished sorting'
         call reorient(ni,var(1,:),var(2,:),var(3,:))
       endif

      do i=1,ni
        var(1,i)=var(1,i)*maxlen
        var(2,i)=var(2,i)*maxlen/factor
        var(3,i)=var(3,i)*maxlen
      enddo
c-------rotate profile back to original position
      if(.not.horizontal)then
        n1(1)= cos(-aoa)
        n1(2)= sin(-aoa)
        n2(1)=-sin(-aoa)
        n2(2)= cos(-aoa)
        do i=1,ni
          a(1)=var(1,i)
          a(2)=var(2,i)
          var(1,i)=n1(1)*a(1)+n1(2)*a(2)+x0
          var(2,i)=n2(1)*a(1)+n2(2)*a(2)+y0
        enddo
      endif
c-------scaled airfoil chord to one
      if(uchord)then
        xt(1)=var(1,1)
        yt(1)=var(2,1)
        zt(1)=var(3,1)
        var(1,:)=(var(1,:)-xt(1))/maxlen+1
        var(2,:)=(var(2,:)-yt(1))/maxlen
        var(3,:)=(var(3,:)-zt(1))/maxlen
      endif

      if(closed)then
        ni=ni+1
        allocate(vart(3,ni))
        vart=0.d0
        vart(:,1:ni-1)=var(:,1:ni-1)
        vart(:,ni)=var(:,1)
        deallocate(var)
        allocate(var(3,ni))
        var(:,1:ni)=vart(:,1:ni)
        deallocate(vart)
      endif
      if(npoints.gt.0)then
        allocate(xi(ni),yi(ni),zi(ni))
        allocate(xo(npoints),yo(npoints),zo(npoints))
        xi(1:ni)=var(1,1:ni)
        yi(1:ni)=var(2,1:ni)
        zi(1:ni)=var(3,1:ni)
        call SplineCurve(ni,npoints,xi,yi,zi,xo,yo,zo)
        deallocate(xi,yi,zi,var)
        ni=npoints
        allocate(var(3,ni))
        var(1,1:ni)=xo(1:ni)
        var(2,1:ni)=yo(1:ni)
        var(3,1:ni)=zo(1:ni)
        deallocate(xo,yo,zo)
      endif

      open(unit=12,file='prof.new')
c     write(12,20)'# CHORD        : ',maxlen
c     write(12,20)'# AOA          : ',aoa*180/pi
c     write(12,20)'# STAG. POINT  : ',cploc
      do i=1,ni
        write(12,'(3f24.12)')(var(n,i),n=1,3)
      enddo

      stop
   20 format(a12,f12.6)
   21 format('# ',3(f12.6))
      end
   
      subroutine minfunc(x1,x2,x3,y1,y2,y3,minloc,minval)
      implicit none
      real(kind=8)x1,x2,x3,y1,y2,y3,minloc,minval
      real(kind=8)xt1,xt2,xt3,a1,b1,c1
      
      xt1=0.d0;xt2=(x2-x1)/(x3-x1);xt3=1.d0
      c1=y1
      b1=(y2+c1*(xt2**2-1)-y3*xt2**2)/(xt2-xt2**2)
      a1=y3-c1-b1
      minloc=-.5*b1/a1
      minval=a1*minloc**2+b1*minloc+c1
      minloc=minloc*(x3-x1)+x1
      print*,'minloc ',minloc
      return
      end


      subroutine ComputeAngle(ni,nvar,var,xmin,ymin,maxlen,aoa)
      implicit none
      integer ni,nvar,i,imaxlen
      real(kind=8),dimension(nvar,ni)::var
      real(kind=8)::maxlen,aoa,len,x1,y1,x2,y2,xmean,ymean,xmin,ymin

c-------find geometrical angel of attack
      maxlen=0.d0
      xmean=sum(var(1,1:ni))/ni
      ymean=sum(var(2,1:ni))/ni
      do i=1,ni
        len=sqrt((var(1,i)-xmean)**2+(var(2,i)-ymean)**2)
        if(len.gt.maxlen)then
          maxlen=len
          x1=var(1,i)
          y1=var(2,i)
        endif
      enddo
      maxlen=0.d0
      do i=1,ni
        len=sqrt((var(1,i)-x1)**2+(var(2,i)-y1)**2)
        if(len.gt.maxlen)then
          maxlen=len
          x2=var(1,i)
          y2=var(2,i)
        endif
      enddo
      maxlen=sqrt((x2-x1)**2+(y2-y1)**2)
      if(x2.gt.x1)then
        aoa=atan((y2-y1)/(x2-x1))
        xmin=x1;ymin=y1
      else
        aoa=atan((y1-y2)/(x1-x2))
        xmin=x2;ymin=y2
      endif
      
 
      write(20,*)x1,y1,x2,y2,xmean,ymean
      return
      end


      subroutine pointsort(ni,nstart,x,y,z)
      implicit none
      real(kind=8),dimension(ni)::x,y,z,xt,yt
      real(kind=8),dimension(2)::v1,v2
      real(kind=8)::xs,ys,len,l1,l2,atest,atestmax,lminn,factor
      real(kind=8),dimension(5)::lmin
      integer,dimension(ni)::slist
      logical,dimension(ni)::pcheck
      integer::ni,i,j,k,sorted,illast,nstart
      integer,dimension(5)::ilast
      integer,dimension(1)::iloc
      integer itest

      factor=2.d0

      open(unit=10,file='unsorted.dat')
      do i=1,ni
        write(10,*)x(i),y(i),z(i)
      enddo
      close(10)

      pcheck=.true.
      open(unit=10,file='sorted.dat')
      slist=0
c------first point
c------  store the position of fist point 
      if(nstart.lt.0)then
        iloc=maxloc(x)
        ilast(1)=iloc(1)
      else
        ilast(1)=nstart
      endif
c------- store the index of last sorted point in list over sorted points
      slist(1)=ilast(1)
      xs=x(ilast(1));ys=y(ilast(1))
      print*,' first point : ',ilast(1)
c------- flag that the first point do not need to be evaluated any more
      pcheck(ilast(1))=.false.
      write(10,*)xs,ys
c------second point, is only based on distance to neighbour points
      lmin(1)=1000d0
      do i=1,ni
        if(pcheck(i))then
          len=sqrt((x(i)-xs)**2
     &            +(y(i)-ys)**2)
          if(len.lt.lmin(1))then
              lmin(1)=len
              ilast(1)=i
          endif
        endif
      enddo
c------- store the index of last sorted point in list over sorted points
      slist(2)=ilast(1)
c------- flag that second first point do not need to be evaluated any more
      pcheck(slist(2))=.false.
      print*,' second point : ',ilast(1)
      xs=x(ilast(1))
      ys=y(ilast(1))
      write(10,*)xs,ys
      do j=3,ni
        lmin=1.d0;ilast=-1
c---------compute tangent vector and length based on the two last sorted points
        v2(1)=x(slist(j-1))-x(slist(j-2))
        v2(2)=y(slist(j-1))-y(slist(j-2))
        l2=sqrt(v2(1)**2+v2(2)**2)
c--------- find the five nearest points
        do i=1,ni
          if(pcheck(i))then
            len=sqrt((x(i)-xs)**2
     &              +(y(i)-ys)**2)
             if(len.lt.lmin(1))then
               do k=5,2,-1
                 lmin(k)=lmin(k-1);ilast(k)=ilast(k-1)
               enddo
               lmin(1)=len;ilast(1)=i
             elseif(len.lt.lmin(2))then
               do k=5,3,-1
                 lmin(k)=lmin(k-1);ilast(k)=ilast(k-1)
               enddo
               lmin(2)=len;ilast(2)=i
             elseif(len.lt.lmin(3))then
               do k=5,4,-1
                 lmin(k)=lmin(k-1);ilast(k)=ilast(k-1)
               enddo
               lmin(3)=len;ilast(3)=i
             elseif(len.lt.lmin(4))then
               lmin(5)=lmin(4);ilast(5)=ilast(4)
               lmin(4)=len;ilast(4)=i
             elseif(len.lt.lmin(5))then
               lmin(5)=len;ilast(5)=i
             endif
           endif
         enddo
c----------write the nearest points to file
         open(unit=11,file='nearest.dat')
         do k=1,5
           if(ilast(k).gt.0)write(11,*)x(ilast(k)),y(ilast(k))
         enddo  
         close(11)
c----------check the angle to the nearest 5 points
         atestmax=-1.d0;lminn=1.d0
         do k=1,5
           if(ilast(k).gt.0)then
c------------ compute the vector from last sorted point to near point
             v1(1)=x(ilast(k))-xs
             v1(2)=y(ilast(k))-ys
             l1=sqrt(v1(1)**2+v1(2)**2)
             atest=(v1(1)*v2(1)+v1(2)*v2(2))
     &            /(l1*l2)
c-------------- if the vector to the nearest point is less than ~12deg cos(12deg) 
             if(k.eq.1.and.atest.gt..98d0)then
               illast=ilast(k)
               goto 100
             endif
c--------------else check a blend between proximity and angle
c-------------- if the tangent is slightly closer to curve tangent
c-------------- and the distance is not more than 5% longer than the previous min distance 
             if(atest.gt.atestmax*1.02.and.l1.lt.1.05*lminn)then
c            if(atest.gt.atestmax*1.01.and.l1.lt.1.05*lminn)then
               atestmax=atest;lminn=l1
               illast=ilast(k)
             endif
           endif
         enddo
  100   slist(j)=illast
        pcheck(slist(j))=.false.
        xs=x(slist(j))
        ys=y(slist(j))
        open(unit=10,file='sorted.dat',status='old')
        do i=1,j
        write(10,*)x(slist(i)),y(slist(i))
        enddo
        close(10)
      enddo 
      ilast=slist(1)
      close(10)
      
      do j=1,ni
        xt(j)=x(slist(j))
        yt(j)=y(slist(j))
      enddo
      x=xt;y=yt
      do j=1,ni
        xt(j)=z(slist(j))
      enddo
      z=xt
      return 
      end

      subroutine reorient(ni,x,y,z)
      implicit none
      integer ni,i,imax,ii
      real(kind=8),dimension(ni)::x,y,z,xt,yt,zt
      real(kind=8)dx,ym,area,xmax
     
c------determine orientation of line by computing integral
      area=0.d0
      do i=2,ni-1
        dx=x(i+1)-x(i-1) 
        ym=.5*(y(i+1)+y(i)) 
        area=area+dx*ym
      enddo
      write(*,*)' area ',area/abs(area)
c------make orientation clockwise
      if(area.lt.0.d0)then
      do i=1,ni
        xt(i)=x(ni+1-i)
        yt(i)=y(ni+1-i)
        zt(i)=z(ni+1-i)
      enddo
      x=xt;y=yt;z=zt
      endif
c-------make first point trailing edge point
      return
      xmax=-10
      do i=1,ni
        if(x(i).ge.xmax)then
          imax=i
          xmax=x(i)
        endif
      enddo
      ii=0
      do i=imax,ni
        ii=ii+1
        xt(ii)=x(i)
        yt(ii)=y(i)
        zt(ii)=z(i)
      enddo
      do i=1,imax-1
        ii=ii+1
        xt(ii)=x(i)
        yt(ii)=y(i)
        zt(ii)=z(i)
      enddo
      x=xt;y=yt;z=zt
      return
      end


      subroutine SplineDistribution(ndist,x,y,s,ys)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      implicit none
      integer ndist,i
      real(kind=8),dimension(ndist)::x,y,s,ys
      real(kind=8)::dx,dy,y1,yn,xi1,xi2,xiold
      logical::monotonic

c-----spline distribution function
      s(1)=0.d0
      do i=2,ndist
c-------calculate first derivative
        dx=x(i)-x(i-1)
        dy=y(i)-y(i-1)
c-------calculate curve length
        s(i)=s(i-1)+abs(dx)
      enddo
c-----normalize curve length
      do i=1,ndist
        s(i)=s(i)/s(ndist)
      enddo

c-----call spline (natural cubic spline)
      y1=1.d32
      yn=1.d32
      call spline(s,y,ndist,y1,yn,ys)

c-----write spline curve
      open(unit=10,file='dist.DAT')
      monotonic=.true.
      xiold=-1.d0
      do i=1,100
        xi1=real(i-1)/real(99)
        call splint(s,y,ys,ndist,xi1,xi2)
        if(xi2.le.xiold)monotonic=.false.
        xiold=xi2
        write(10,*)xi1,xi2
      enddo
      if(.not.monotonic)
     &  print*,' Error ! distribution function is  '
     &        ,' not monotonically increasing !!!! '
      close(10)
      return
      end

      subroutine SplineCurve(nin,nout,xin,yin,zin,xo,yo,zo)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      implicit none
      integer nin,nout,i
      real(kind=8),dimension(nin)::xin,yin,zin,s,cs
      real(kind=8),dimension(nout)::xo,yo,zo
      real(kind=8)::y1,yn,xi,val

c-----spline profile data
      s(1)=0.d0
      do i=1,nin
        s(i)=real(i-1)/real(nin-1)
      enddo
c-----spline representation of surface (natural cubic spline)
      y1=1.d32
      yn=1.d32
      call spline(s,xin,nin,y1,yn,cs)
      do i=1,nout
        xi=real(i-1)/real(nout-1)
        call splint(s,xin,cs,nin,xi,xo(i))
      enddo
      call spline(s,yin,nin,y1,yn,cs)
      do i=1,nout
        xi=real(i-1)/real(nout-1)
        call splint(s,yin,cs,nin,xi,yo(i))
      enddo
      do i=1,nout
        xi=real(i-1)/real(nout-1)
        call splint(s,zin,cs,nin,xi,zo(i))
      enddo

      return
      end

      subroutine spline(x,y,n,yp1,ypn,y2)
c----------------------------------------------------------------------
c     Given arrays x and y of length n containing a tabulated function,
c     i.e y(i)=f(x(i)), with x(1)<x(2)<...<x(n), and given values yp1
c     and ypn for the first derivative of the interpolating function at
c     points 1 and n, respectively, this routine returns an array y2 of 
c     lenght n wich contains the second derivatives of the interpolating 
c     function at the tabulated points x(i). Uf yp1 and/or ypn are equal 
c     to 1.d30 or larger, the routine is signalled to the corresponding
c     boundary condition for a natural spline, with zero second derivative
c     on that boundary
c
c     Numerical Recipes
c----------------------------------------------------------------------
      implicit none
      integer nmax
      parameter(nmax=1000)
      real*8 x(n),y(n),y2(n),u(nmax)
      real*8 qn,p,un,sig,yp1,ypn
      integer i,n,k

      if (yp1.gt..99e30) then
        y2(1)=0.
        u(1)=0.
      else
        y2(1)=-0.5
        u(1)=(3./(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
      endif
      do 11 i=2,n-1
        sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
        p=sig*y2(i-1)+2.
        y2(i)=(sig-1.)/p
        u(i)=(6.*((y(i+1)-y(i))/(x(i+1)-x(i))-(y(i)-y(i-1))
     *      /(x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*u(i-1))/p
11    continue
      if (ypn.gt..99e30) then
        qn=0.
        un=0.
      else
        qn=0.5
        un=(3./(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
      endif
      y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.)
      do 12 k=n-1,1,-1
        y2(k)=y2(k)*y2(k+1)+u(k)
12    continue
      return
      end

      subroutine splint(xa,ya,y2a,n,x,y)
c----------------------------------------------------------------------
c     Given the arrays xa and ya of length n, wich tabulate a function
c     (with the xa's in order), and given the array y2a, wich is the 
c     output from spline above, and given a value of x, this routine 
c     returns a cubic-spline interpolated value y. 
c
c     Numerical Recipes
c----------------------------------------------------------------------
      implicit none
      integer n
      real*8 xa(n),ya(n),y2a(n),x,y
      real*8 a,b,h
      integer klo,khi,k

      klo=1
      khi=n
1     if (khi-klo.gt.1) then
        k=(khi+klo)/2
        if(xa(k).gt.x)then
          khi=k
        else
          klo=k
        endif
      goto 1
      endif
      h=xa(khi)-xa(klo)
c     if (h.eq.0.) pause 'bad xa input.'
      if (h.eq.0.)then
        print*,'bad xa input.'
        stop
      endif
      a=(xa(khi)-x)/h
      b=(x-xa(klo))/h
      y=a*ya(klo)+b*ya(khi)+
     *      ((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.
      return
      end
