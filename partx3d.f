c     $Rev:: 8           $: Revision of last commit
c     $Author:: nsqr     $: Author of last commit
c     $Date:: 2011-08-02#$: Date of last commit
      program MeshReadWrite
c-----------------------------------------------------------------------
c     A small piece of code to read a unformatted or formatted grid
c     and write it as unformatted grid.
c     Fill in you own operations on the grid between the read
c     and write and have fun
c     Author Niels N. S�rensen
c-----------------------------------------------------------------------
      implicit none
      logical::statex=.false.
      integer::ni
      integer,parameter::idp8=8,ncomp=4
      real(kind=8),dimension(:,:,:,:),allocatable::x,y,z
      integer,dimension(:,:,:,:),allocatable::attr
      real(kind=8)::dummy
      character*11 fnamd,fnamh
      character*128 fieldname(ncomp)
      integer::idummy
      integer::i,j,k,n,nc,nvar,fileform
      integer::bsize,nblock,b1,iblock
      integer::dim,nbm,nbi,nis,io,jo,ko,ii,jj,kk,istart,jstart,kstart,nn
      real(kind=8),dimension(:,:,:,:),allocatable::xs,ys,zs
      integer,dimension(:,:,:,:),allocatable::attrs

      inquire(file='grid.x3dunf',exist=statex)
      if(statex)then
        print*,' READING UNFORMATTED GRID FILE'
        open(unit=20,file='grid.x3dunf',form='unformatted')
        read(20)bsize
        read(20)nblock
        print*,bsize,nblock
        ni=bsize+3
        allocate(x(ni,ni,ni,nblock)
     &          ,y(ni,ni,ni,nblock)
     &          ,z(ni,ni,ni,nblock)
     &       ,attr(ni,ni,ni,nblock))
        x=0.d0;y=0.d0;z=0.d0;attr=0
        print*,' Reading Attributs : '
        call ReadBlockInteger(20,ni**3,nblock,attr)
        print*,' Reading x-coord : '
        call ReadBlockReal   (20,ni**3,nblock,x)
        print*,' Reading y-coord : '
        call ReadBlockReal   (20,ni**3,nblock,y)
        print*,' Reading z-coord : '
        call ReadBlockReal   (20,ni**3,nblock,z)
        print*,' Finished Reading : '
        close(10)
      else
        inquire(file='grid.x3d',exist=statex)
        if(.not.statex)then
          print*,' No grid file, stopping '
          stop
        endif
        print*,' READING FORMATTED GRID FILE'
        open(unit=20,file='grid.x3d')
        read(20,*)bsize,nblock
        ni=bsize+3
        allocate(x(ni,ni,ni,nblock)
     &          ,y(ni,ni,ni,nblock)
     &          ,z(ni,ni,ni,nblock)
     &       ,attr(ni,ni,ni,nblock))
        x=0.d0;y=0.d0;z=0.d0;attr=0
        print*,bsize,nblock
        do n=1,nblock
        do k=2,bsize+2
        do j=2,bsize+2
        do i=2,bsize+2
          read(20,*)attr(i,j,k,n)  
     &            ,x(i,j,k,n)
     &            ,y(i,j,k,n)
     &            ,z(i,j,k,n)
        enddo;enddo;enddo;enddo
      endif

c-----------------------------------------------------------------------
c     place your operation on the grid here !!!
c-----------------------------------------------------------------------
      print*,' Give new block size (cells) '
      read*,dim
      print*,'*******************************************************'
      print*,'size of multidomaine blocks         : ',dim

      nis=dim+3
      nbi=(ni-1)/dim
      nbm=nbi**3*nblock
      allocate(xs(nis,nis,nis,nbm)
     &        ,ys(nis,nis,nis,nbm)
     &        ,zs(nis,nis,nis,nbm)
     &        ,attrs(nis,nis,nis,nbm))
      print*,'*******************************************************'
      print*,'size of multidomaine blocks         : ',dim
      print*,'total number of blocks              : ',nbm 
      print*,'*******************************************************'
      
      nn=0
      do n=1,nblock
      do kk=1,nbi;kstart=(kk-1)*dim
      do jj=1,nbi;jstart=(jj-1)*dim
      do ii=1,nbi;istart=(ii-1)*dim
        nn=nn+1 
        print*,' storing block : ',nn
        do k=2,dim+2;ko=kstart+k
        do j=2,dim+2;jo=jstart+j
        do i=2,dim+2;io=istart+i
          xs(i,j,k,nn)=x(io,jo,ko,n)
          ys(i,j,k,nn)=y(io,jo,ko,n)
          zs(i,j,k,nn)=z(io,jo,ko,n)
          attrs(i,j,k,nn)=attr(io,jo,ko,n)
        enddo;enddo;enddo
      enddo;enddo;enddo;enddo

c-------write unformatted grid -----------------------------------------
      call UnfGridWrite(20,nis,nbm,xs,ys,zs,attrs)

      stop
   30 format('')
   31 format('VARIABLES = ',15(A20,','))
   32 format('ZONE I=',i2,', J=',i2,', K=',i2,' F=BLOCK')
      end

      subroutine ReadBlockReal(unit,block,nblock,x)
      implicit none
      integer unit,nblock,block,i,n
      real(kind=8),dimension(block,nblock)::x
      real(kind=8),dimension(block)       ::xblock
        do n=1,nblock
          read(unit)xblock
          do i=1,block
            x(i,n)=xblock(i)
          end do
        end do
      end subroutine ReadBlockReal

      subroutine ReadBlockInteger(unit,block,nblock,x)
      implicit none
      integer unit,nblock,block,i,n
      integer,dimension(block,nblock)::x
      integer,dimension(block)       ::xblock
        do n=1,nblock
          read(unit)xblock
          do i=1,block
            x(i,n)=xblock(i)
          end do
        end do
      end subroutine ReadBlockInteger

      subroutine UnfGridWrite(unitnr,ni,nblock,x,y,z,attr)
C=======================================================================
C     routine for writing unformatted x3d file
C     remember that the grid must positions in the following region 
C
C        x(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C        y(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C        z(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C     attr(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C
C     the remaining spaces are intended for ghost cells
C=======================================================================
      implicit none
      integer unitnr,ni,nblock
      real(kind=8),dimension(ni,ni,ni,nblock)::x,y,z
      integer,dimension(ni,ni,ni,nblock)::attr

      print*,' unitnr ',unitnr
      open(unit=unitnr,file='grid.X3DUNF',form='unformatted')
      write(unitnr)ni-3
      write(unitnr)nblock
      print*,' Writing Attributes '
      call WriteBlockInteger(unitnr,ni**3,nblock,attr)
      print*,' Writing x '
      call WriteBlockReal   (unitnr,ni**3,nblock,x   )
      print*,' Writing y '
      call WriteBlockReal   (unitnr,ni**3,nblock,y   )
      print*,' Writing z '
      call WriteBlockReal   (unitnr,ni**3,nblock,z   )
      print*,' Finished writing '
      close(unitnr)
      return
      end

      subroutine WriteBlockReal(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      real(kind=8),dimension(block,nblock)::x
      real(kind=8),dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine WriteBlockReal

      subroutine WriteBlockInteger(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      integer,dimension(block,nblock)::x
      integer,dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine WriteBlockInteger
