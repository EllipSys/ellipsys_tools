      program CatX3D
c-----------------------------------------------------------------------
C     program for concattenation of two x3d meshes
C     The meshes should be of same block size (bsize)
C     reading grid-1.x3dunf + grid-2.x3dunf => grid.x3dunf
c-----------------------------------------------------------------------
      implicit none
      logical::statex=.false.
      integer::ni,bsize,nblock,nblock1,nblock2,i,j,k,n
      integer,parameter::idp4=4,idp8=8
      real(kind=8),dimension(:,:,:,:),allocatable::x
      real(kind=8),dimension(:,:,:,:),allocatable::x1,y1,z1
     &                                            ,x2,y2,z2
      integer,dimension(:,:,:,:),allocatable::attr,attr1,attr2


c-------Read Grid-1
      inquire(file='grid-1.x3dunf',exist=statex)
      if(statex)then
        print*,' READING UNFORMATTED GRID FILE'
        open(unit=20,file='grid-1.x3dunf',form='unformatted')
        read(20)bsize
        read(20)nblock1
        print*,' Mesh-1 , bsize, nblock : ',bsize,nblock1
        ni=bsize+3
        allocate(x1(ni,ni,ni,nblock1)
     &          ,y1(ni,ni,ni,nblock1)
     &          ,z1(ni,ni,ni,nblock1)
     &       ,attr1(ni,ni,ni,nblock1))
        call ReadGrid(1,20,ni,nblock1,x1,y1,z1,attr1)
      else
        inquire(file='grid-1.x3d',exist=statex)
        if(.not.statex)then
          print*,' No grid file, stopping '
          stop
        endif
        print*,' READING FORMATTED GRID FILE'
        open(unit=20,file='grid-1.x3d')
        read(20,*)bsize,nblock1
        ni=bsize+3
        allocate(x1(ni,ni,ni,nblock1)
     &          ,y1(ni,ni,ni,nblock1)
     &          ,z1(ni,ni,ni,nblock1)
     &       ,attr1(ni,ni,ni,nblock1))
        call ReadGrid(2,20,ni,nblock1,x1,y1,z1,attr1)
      endif

c-------Read Grid-2
      inquire(file='grid-2.x3dunf',exist=statex)
      if(statex)then
        print*,' READING UNFORMATTED GRID FILE'
        open(unit=20,file='grid-2.x3dunf',form='unformatted')
        read(20)bsize
        read(20)nblock2
        print*,' Mesh-2 , bsize, nblock : ',bsize,nblock2
        if(bsize+3.ne.ni)then
          print*,' Block sizes do no agree !!!! '
          stop
        endif
        allocate(x2(ni,ni,ni,nblock2)
     &          ,y2(ni,ni,ni,nblock2)
     &          ,z2(ni,ni,ni,nblock2)
     &       ,attr2(ni,ni,ni,nblock2))
        call ReadGrid(1,20,ni,nblock2,x2,y2,z2,attr2)
      else
        inquire(file='grid-2.x3d',exist=statex)
        if(.not.statex)then
          print*,' No grid file, stopping '
          stop
        endif
        print*,' READING FORMATTED GRID FILE'
        open(unit=20,file='grid-2.x3d')
        read(20,*)bsize,nblock2
        if(bsize+3.ne.ni)then
          print*,' Block sizes do no agree !!!! '
          stop
        endif
        allocate(x2(ni,ni,ni,nblock2)
     &          ,y2(ni,ni,ni,nblock2)
     &          ,z2(ni,ni,ni,nblock2)
     &       ,attr2(ni,ni,ni,nblock2))
        call ReadGrid(2,20,ni,nblock2,x2,y2,z2,attr2)
      endif
      do n=1,nblock2
        if(attr2(ni/2,ni/2,2,n).eq.101)print*,' wall ',n
      enddo
      close(20)

      nblock=nblock1+nblock2
      open(unit=20,file='grid.x3dunf',form='unformatted')
      print*,' ni ',ni
      print*,' nblock ',nblock
      write(20)ni-3
      write(20)nblock
      allocate(attr(ni,ni,ni,nblock))
      attr(:,:,:,1:nblock1)       =attr1(:,:,:,:)
      attr(:,:,:,nblock1+1:nblock)=attr2(:,:,:,:)
      call WriteBlockInteger(20,ni**3,nblock,attr)
      deallocate(attr,attr1,attr2)
      allocate(x(ni,ni,ni,nblock))
      x(:,:,:,1:nblock1)       =x1(:,:,:,:)
      x(:,:,:,nblock1+1:nblock)=x2(:,:,:,:)
      call WriteBlockReal   (20,ni**3,nblock,x  )
      x(:,:,:,1:nblock1)       =y1(:,:,:,:)
      x(:,:,:,nblock1+1:nblock)=y2(:,:,:,:)
      call WriteBlockReal   (20,ni**3,nblock,x  )
      x(:,:,:,1:nblock1)       =z1(:,:,:,:)
      x(:,:,:,nblock1+1:nblock)=z2(:,:,:,:)
      call WriteBlockReal   (20,ni**3,nblock,x  )
      close(20)

      stop
   30 format('')
   31 format('VARIABLES = ',15(A20,','))
   32 format('ZONE I=',i2,', J=',i2,', K=',i2,' F=BLOCK')
      end

      subroutine ReadBlockReal(unit,block,nblock,x)
      implicit none
      integer unit,nblock,block,i,n
      real(kind=8),dimension(block,nblock)::x
      real(kind=8),dimension(block)       ::xblock
        do n=1,nblock
          read(unit)xblock
          do i=1,block
            x(i,n)=xblock(i)
          end do
        end do
      end subroutine ReadBlockReal

      subroutine ReadBlockInteger(unit,block,nblock,x)
      implicit none
      integer unit,nblock,block,i,n
      integer,dimension(block,nblock)::x
      integer,dimension(block)       ::xblock
        do n=1,nblock
          read(unit)xblock
          do i=1,block
            x(i,n)=xblock(i)
          end do
        end do
      end subroutine ReadBlockInteger

      subroutine ReadGrid(status,unit,ni,nblock,x,y,z,attr)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      integer::ni,nblock,i,j,k,n,unit,status
      real(kind=8),dimension(ni,ni,ni,nblock)::x,y,z
      integer,dimension(ni,ni,ni,nblock)::attr


      if(status.eq.1)then
        x=0;y=0;z=0;attr=0
        print*,' Reading Attributs : '
        call ReadBlockInteger(unit,ni**3,nblock,attr)
        print*,' Reading x-coord : '
        call ReadBlockReal   (unit,ni**3,nblock,x)
        print*,' Reading y-coord : '
        call ReadBlockReal   (unit,ni**3,nblock,y)
        print*,' Reading z-coord : '
        call ReadBlockReal   (unit,ni**3,nblock,z)
        print*,' Finished Reading : '
        close(10)
      else
        print*,' READING FORMATTED GRID FILE'
        x=0;y=0;z=0;attr=0
        do n=1,nblock
        do k=2,ni-1
        do j=2,ni-1
        do i=2,ni-1
          read(unit,*)attr(i,j,k,n)
     &                  ,x(i,j,k,n)
     &                  ,y(i,j,k,n)
     &                  ,z(i,j,k,n)
        enddo;enddo;enddo;enddo
      endif
      return
      end


      subroutine WriteBlockReal(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      real(kind=8),dimension(block,nblock)::x
      real(kind=8),dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine WriteBlockReal

      subroutine WriteBlockInteger(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      integer,dimension(block,nblock)::x
      integer,dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine WriteBlockInteger
