      program detect_transition

      implicit none
      real(kind=8),dimension(:),allocatable::x,y,cp,cf,gamma
      real(kind=8)::xmin,xtup,ytup,xtlw,xstag,ystag,ytlw,dvar,factor
     &             ,value,cpmin,len
      integer ni,i,imin
      character(len=200)::dummychar

c-------read cp,cf,gamma distribution
      open(unit=10,file='grid.pr')
      read(10,*)dummychar
      read(10,*)dummychar
      read(10,*)dummychar
      read(10,*)dummychar
      ni=0
      do 
        read(10,*,end=111)dummychar
        ni=ni+1
      enddo
  111 continue
      close(10)
      open(unit=10,file='grid.pr')
      read(10,*)dummychar
      read(10,*)dummychar
      read(10,*)dummychar
      read(10,*)dummychar
      allocate(x(ni),y(ni),cp(ni),cf(ni),gamma(ni))
      do i=1,ni
        read(10,*)x(i),y(i),cp(i),cf(i),dvar,dvar,gamma(i)
      enddo
      close(10)

c------locate leading edge
      imin=1
      xmin=1000.d0
      do i=1,ni
        if(x(i).lt.xmin)then
          xmin=x(i)
          imin=i
        endif
      enddo
c     print*,' xmin ',xmin
c     print*,' imin ',imin

c------stagnation point
      imin=1
      cpmin=1000.d0
      do i=1,ni
        if(cp(i).lt.cpmin.and.cp(i).gt.-1.005d0)then
          cpmin=cp(i)
          imin=i
        endif
      enddo
      xstag=x(imin);ystag=y(imin)

c     value=.001
      value=.025
c------find suction side transition point
      xtup=x(1)
      ytup=y(1)
      len=0.d0
      do i=imin+1,ni
        len=len+sqrt((x(i)-x(i-1))**2+(y(i)-y(i-1))**2)
c       write(*,'(i6,3f20.6)'),i,len,gamma(i),x(i)
        if(gamma(i).gt.value.and.gamma(i-1).lt.value
     &          .and.len.gt.0.0002d0)then
c         xtup=x(i)
c         ytup=y(i)
          factor=abs((gamma(i)-value)/(gamma(i)-gamma(i-1)+1.d-30))
          xtup=x(i)*(1-factor)+x(i-1)*factor
          ytup=y(i)*(1-factor)+y(i-1)*factor
          exit
        endif  
      enddo
c     print*,' pressure side '
c------find pressure side transition point
      xtlw=x(1)
      ytlw=y(1)
      len=0.d0
      do i=imin,1,-1
        len=len+sqrt((x(i)-x(i+1))**2+(y(i)-y(i+1))**2)
        if(gamma(i).gt.value.and.gamma(i+1).lt.value
     &          .and.len.gt.0.0002d0)then
c       write(*,'(i6,3f20.6)'),i,len,gamma(i),gamma(i+1)
c         xtlw=x(i)
c         ytlw=y(i)
          factor=(gamma(i)-value)/(gamma(i)-gamma(i+1))
          xtlw=x(i)*(1-factor)+x(i+1)*factor
          ytlw=y(i)*(1-factor)+y(i+1)*factor
          exit
        endif  
      enddo
     
      write(*,'(4e14.3)')xtup,xtlw,xstag,ystag
c     print*,' suction  side transtiion : ',xtup
c     print*,' pressure side transtiion : ',xtlw
      open(unit=10,file='tran.dat')
      write(10,*)xtup,ytup
      write(10,*)xtlw,ytlw
      write(10,*)xstag,ystag
      close(10)
     
      stop
      end

    

      
      
