      program SMesh2Fieldview3D
C-----------------------------------------------------------------------
C     Program for performing rotation manipulation of a 3D surface x2d 
c     file and computing the resulting quatenion. The resulting grid
c     will be written to a plot3d file. 
C-----------------------------------------------------------------------
      implicit none
      real(kind=8),dimension(:,:,:),allocatable::x,y,z
      integer,dimension(:,:,:),allocatable::attr
      integer::i,j,n,bsize,ni,nblock
      real(kind=8)::alpha_azimuth,alpha_cone,pi,alpha,vx,vy,vz,len
      real(kind=8),dimension(4)::p,q,rp,rt
      logical::statex

      pi=4.d0*atan(1.d0)

      q=0.d0;q(1)=1.d0
      do  i=1,3
        print*,' Give rotation angle [0:360] and axis vector : '
        read(*,*)alpha,vx,vy,vz
        if(alpha.lt.0.d0)exit
        len=1.d0/sqrt(vx**2+vy**2+vz**2)
        vx=vx*len;vy=vy*len;vz=vz*len
        p=0.d0
        p(1)=cos(alpha*.5d0*pi/180.d0)
        p(2)=sin(alpha*.5d0*pi/180.d0)*vx
        p(3)=sin(alpha*.5d0*pi/180.d0)*vy
        p(4)=sin(alpha*.5d0*pi/180.d0)*vz

        call quatmult(p,q,rt)
        q=rt
      enddo
      print*,' Resulting rotation Quartenion : '
     &        ,q(1),q(2),q(3),q(4)

      inquire(file='grid.x2d',exist=statex)
      if(statex)then
        open(unit=20,file='grid.x2d')
        read(20,*) bsize,nblock
        ni=bsize+1
        allocate(x(ni,ni,nblock),y(ni,ni,nblock),z(ni,ni,nblock)
     &          ,attr(ni,ni,nblock))

        do n=1,nblock
          do j=1,bsize+1
          do i=1,bsize+1
            read(20,*)attr(i,j,n),x(i,j,n),y(i,j,n),z(i,j,n)
          enddo;enddo
        enddo
        close(20)

        p=-q;p(1)=-p(1)
        do n=1,nblock
          do j=1,bsize+1
          do i=1,bsize+1
            rp(1)=0.d0
            rp(2)=x(i,j,n)
            rp(3)=y(i,j,n)
            rp(4)=z(i,j,n)
            call quatmult(rp,p ,rt)
            call quatmult(q ,rt,rp)
            x(i,j,n)=rp(2)
            y(i,j,n)=rp(3)
            z(i,j,n)=rp(4)
          enddo;enddo
        enddo
        call plotfieldview(bsize,nblock,x,y,z,attr)
      endif

      stop
      end

      subroutine plotfieldview(bsize,nb,x,y,z,attr)
c----------------------------------------------------------------------
c
c----------------------------------------------------------------------
      implicit none
      integer,parameter::idp4=8
      integer::bsize,nb,i,j,k,n
      real(kind=8),dimension(bsize+1,bsize+1,nb)::x,y,z,var
      integer,dimension(bsize+1,bsize+1,nb)::attr


c-----make FieldView plot3d unformatted grid file
      print*,'writing grid.xyz ...'
      open(unit=4,file='grid.xyz',form='unformatted')
      write(4)nb
      write(4)(bsize+1,bsize+1,1,n=1,nb)
      do n=1,nb
      write(4)(((real(x(i,j,n),idp4),i=1,bsize+1),j=1,bsize+1),k=1,1),
     &        (((real(y(i,j,n),idp4),i=1,bsize+1),j=1,bsize+1),k=1,1),
     &        (((real(z(i,j,n),idp4),i=1,bsize+1),j=1,bsize+1),k=1,1) 
      enddo
      close(4)

      open(unit=4,file='grid.xyz.fvbnd',form='formatted')
      write(4,*)'FVBND 1 4'
      write(4,*)' wall    '
      write(4,*)' inlet   '
      write(4,*)' outlet  '
      write(4,*)' cyclic  '
      write(4,*)' BOUNDARIES  '
      do n=1,nb
        write(4,'(8i6,a3,i6)')1,n,1,bsize+1,1,bsize+1,1,1,'F',-1
      enddo
      close(4)


c-----------------------------------------------------------------------
      return
      end



      subroutine quatmult(p,q,r)
c-----------------------------------------------------------------------
c     multiplication of two quatenions
c     q=(s,v1,v2,v3)^T
c-----------------------------------------------------------------------
      implicit none
      real(kind=8),dimension(4)::p,q,r

      r(1)=p(1)*q(1)-(p(2)*q(2)+p(3)*q(3)+p(4)*q(4))
      r(2)=p(1)*q(2)+q(1)*p(2)+p(3)*q(4)-p(4)*q(3)
      r(3)=p(1)*q(3)+q(1)*p(3)+p(4)*q(2)-p(2)*q(4)
      r(4)=p(1)*q(4)+q(1)*p(4)+p(2)*q(3)-p(3)*q(2)

      return
      end
