      module params
      integer,parameter::idp4=4,idp=8
      integer::bsize,nblock,ni
      integer,dimension(:,:,:,:),allocatable::attr,attrcf
      end

      program RunTimePlot
c-----------------------------------------------------------------------
c     Program to postprocess unformatted transient data files from     c 
c     EllipSys3D.                                                      c
c     Output file format: Plot3D                                       c
c     Author: Frederik Zahle                                           c
c     Date:   28.03.2008                                               c
c-----------------------------------------------------------------------
      use params
c-----------------------------------------------------------------------
      implicit none
      real(kind=idp4),dimension(:,:,:,:),allocatable::x,y,z,u,v,w,p
      real(kind=idp4),dimension(:,:,:,:),allocatable::iblank
      real(kind=idp4),dimension(:,:,:,:),allocatable::te,omega,vis 
      integer::i,j,k,n,nstep,nplot,npl,nvar,norphan
      real(kind=idp)::time
      character(len=256) filename,filename1,filename2,filename3
      integer,parameter::reduce=1

      call SYSTEM("ls -1 *.PLT > filenames")
      call SYSTEM("wc -l filenames > numfiles")
      open(10,file='filenames',status='unknown')
      open(20,file='numfiles',status='unknown')
      read(20,*)nplot


      print*,'Number of plot files to process:',nplot

      filename(1:13)='grid00000.PLT'

      do npl=1,nplot
      read(10,*)filename
      open(unit=1,file=filename,form='unformatted')

      read(1) bsize
      read(1) nblock 
      read(1) nstep
      read(1) time
      read(1) nvar
      ni=bsize+3
      allocate(x(ni,ni,ni,nblock)
     &        ,y(ni,ni,ni,nblock)
     &        ,z(ni,ni,ni,nblock)
     &        ,u(ni,ni,ni,nblock)
     &        ,v(ni,ni,ni,nblock)
     &        ,w(ni,ni,ni,nblock)
     &        ,p(ni,ni,ni,nblock)
     &        ,vis(ni,ni,ni,nblock)
     &        ,te(ni,ni,ni,nblock)
     &        ,omega(ni,ni,ni,nblock)
     &        ,iblank(ni,ni,ni,nblock)
     &        ,attr(ni,ni,ni,nblock)
     &        ,attrcf(ni,ni,6,nblock))
      call ReadReal(1,ni**3,x)
      call ReadReal(1,ni**3,y)
      call ReadReal(1,ni**3,z)
      call ReadReal(1,ni**3,u)
      call ReadReal(1,ni**3,v)
      call ReadReal(1,ni**3,w)
      call ReadReal(1,ni**3,p)
      call ReadReal(1,ni**3,iblank)
      call ReadInteger(1,ni**3,attr)
      if(nvar.gt.5)then
         call ReadReal(1,ni**3,vis)
         call ReadReal(1,ni**3,te)
         call ReadReal(1,ni**3,omega)
      endif

      do n=1,nblock
      do k=1,bsize+2
      do j=1,bsize+2
      do i=1,bsize+2
         if(attr(i,j,k,n).gt.250.and.
     &      attr(i,j,k,n).lt.260.or.attr(i,j,k,n).eq.2000)
     &      iblank(i,j,k,n)=1.d0
      enddo;enddo;enddo
      enddo

c---- Write FieldView plot3d unformatted grid file ---------------------
      filename1(1:13)='grid00000.xyz'
      write(filename1(5:9),'(i5.5)')npl

      filename2(1:11)='grid00000.f'
      write(filename2(5:9),'(i5.5)')npl

      filename3(1:13)='grid00000.nam'
      write(filename3(5:9),'(i5.5)')npl

      open(unit=2,file=filename1(1:13),form='unformatted')
      write(2)nblock
      write(2)((bsize+1)/reduce+1
     &        ,(bsize+1)/reduce+1
     &        ,(bsize+1)/reduce+1,n=1,nblock)
      print*,'writing file ',filename1(1:13),' ...'
      do n=1,nblock
         write(2)
     &    (((real(x(i,j,k,n),idp4)
     &      ,i=1,bsize+2,reduce)
     &      ,j=1,bsize+2,reduce)
     &      ,k=1,bsize+2,reduce),
     &    (((real(y(i,j,k,n),idp4)
     &      ,i=1,bsize+2,reduce)
     &      ,j=1,bsize+2,reduce)
     &      ,k=1,bsize+2,reduce),
     &    (((real(z(i,j,k,n),idp4)
     &      ,i=1,bsize+2,reduce)
     &      ,j=1,bsize+2,reduce)
     &      ,k=1,bsize+2,reduce),
     &    (((int(iblank(i,j,k,n))
     &      ,i=1,bsize+2,reduce)
     &      ,j=1,bsize+2,reduce)
     &      ,k=1,bsize+2,reduce)
      enddo
      close(2)
c---- write grid.f file ------------------------------------------------
      open(unit=3,file=filename2(1:11),form='unformatted')
      rewind(3)
      if(nvar.eq.5)then
      write(3)nblock
      write(3)((bsize+1)/reduce+1
     &        ,(bsize+1)/reduce+1
     &        ,(bsize+1)/reduce+1,nvar,n=1,nblock)
      do n=1,nblock
      write(3)
     &   (((real(u(i,j,k,n),idp4)
     &     ,i=1,bsize+2,reduce)
     &     ,j=1,bsize+2,reduce)
     &     ,k=1,bsize+2,reduce),
     &   (((real(v(i,j,k,n),idp4)
     &     ,i=1,bsize+2,reduce)
     &     ,j=1,bsize+2,reduce)
     &     ,k=1,bsize+2,reduce),
     &   (((real(w(i,j,k,n),idp4)
     &     ,i=1,bsize+2,reduce)
     &     ,j=1,bsize+2,reduce)
     &     ,k=1,bsize+2,reduce),
     &   (((real(p(i,j,k,n),idp4)
     &     ,i=1,bsize+2,reduce)
     &     ,j=1,bsize+2,reduce)
     &     ,k=1,bsize+2,reduce),
     &   (((real(attr(i,j,k,n),idp4)
     &     ,i=1,bsize+2,reduce)
     &     ,j=1,bsize+2,reduce)
     &     ,k=1,bsize+2,reduce),
     &   (((int(iblank(i,j,k,n))
     &     ,i=1,bsize+2,reduce)
     &     ,j=1,bsize+2,reduce)
     &     ,k=1,bsize+2,reduce)
      enddo
      else
      write(3)nblock
      write(3)((bsize+1)/reduce+1
     &        ,(bsize+1)/reduce+1
     &        ,(bsize+1)/reduce+1,nvar,n=1,nblock)
      do n=1,nblock
      write(3)
     &   (((real(u(i,j,k,n),idp4)
     &     ,i=1,bsize+2,reduce)
     &     ,j=1,bsize+2,reduce)
     &     ,k=1,bsize+2,reduce),
     &   (((real(v(i,j,k,n),idp4)
     &     ,i=1,bsize+2,reduce)
     &     ,j=1,bsize+2,reduce)
     &     ,k=1,bsize+2,reduce),
     &   (((real(w(i,j,k,n),idp4)
     &     ,i=1,bsize+2,reduce)
     &     ,j=1,bsize+2,reduce)
     &     ,k=1,bsize+2,reduce),
     &   (((real(p(i,j,k,n),idp4)
     &     ,i=1,bsize+2,reduce)
     &     ,j=1,bsize+2,reduce)
     &     ,k=1,bsize+2,reduce),
     &   (((real(te(i,j,k,n),idp4)
     &     ,i=1,bsize+2,reduce)
     &     ,j=1,bsize+2,reduce)
     &     ,k=1,bsize+2,reduce),
     &   (((real(omega(i,j,k,n),idp4)
     &     ,i=1,bsize+2,reduce)
     &     ,j=1,bsize+2,reduce) 
     &     ,k=1,bsize+2,reduce),
     &   (((real(vis(i,j,k,n),idp4)
     &     ,i=1,bsize+2,reduce)
     &     ,j=1,bsize+2,reduce)
     &     ,k=1,bsize+2,reduce),
     &   (((real(attr(i,j,k,n),idp4)
     &     ,i=1,bsize+2,reduce)
     &     ,j=1,bsize+2,reduce)
     &     ,k=1,bsize+2,reduce),
     &   (((int(iblank(i,j,k,n))
     &     ,i=1,bsize+2,reduce)
     &     ,j=1,bsize+2,reduce)
     &     ,k=1,bsize+2,reduce)
      enddo
      endif
      close(3)

c---- write grid.nam file ------------------------------------------------
      open(unit=4,file=filename3(1:13))
      rewind(4)
      if(nvar.eq.5)then
         write(4,*)'u-velocity ; velocity'
         write(4,*)'v-velocity'
         write(4,*)'w-velocity'
         write(4,*)'pressure'
         write(4,*)'attr'
      else
         write(4,*)'u-velocity ; velocity'
         write(4,*)'v-velocity'
         write(4,*)'w-velocity'
         write(4,*)'pressure'
         write(4,*)'te'
         write(4,*)'omega'
         write(4,*)'vis'
         write(4,*)'attr'
      endif
      close(4)

c---- write grid.xyz.bnd file ------------------------------------------
      call ExtractWallBoundaryFile(reduce,npl)
      norphan=0
      print*,'writing grid.fvp ...'
      filename1(1:13)='grid00000.fvp'
      write(filename1(5:9),'(i5.5)')npl
      open(unit=24,file=filename1(1:13),form='formatted')
      write(24,400)'FVPARTICLES 2 1'
      write(24,*)'Tag names'
      write(24,*)0
      write(24,*)'Variable Names'
      write(24,*) 1
      write(24,*) 'Block Group'
c     write(24,*) 'Myprocnum'
 400  format(A15)
      do n=1,nblock
      do k=1,bsize+2
      do j=1,bsize+2
      do i=1,bsize+2
         if(attr(i,j,k,n).eq.2000.or.attr(i,j,k,n).eq.1000)then
         norphan=norphan+1
         write(24,*)2
         write(24,100)x(i,j,k,n),y(i,j,k,n),z(i,j,k,n),
     &                attr(i,j,k,n)
         write(24,100)x(i,j,k,n),y(i,j,k,n),z(i,j,k,n),
     &                attr(i,j,k,n)
         endif
      enddo;enddo;enddo
      enddo
      print*,'Number of orphans',norphan
  100 format(3(f18.10),2(i7))
      close(24)


      deallocate(x,y,z,u,v,w,p,vis,te,omega,iblank,attr,attrcf)
      enddo


      call SYSTEM("rm filenames numfiles")

c-----------------------------------------------------------------------
      end

      subroutine ReadReal(unit,block,x)
      use params
      implicit none
      integer unit,i,n,np,dn,count,block
      real(kind=idp4),dimension(block*nblock)::x
      real(kind=idp4),dimension(:),allocatable::xblock
C-----------------------------------------------------------------------
      allocate(xblock(block))
C--------------------------------- Read ROOTs own blocks ---------------
         do n=1,nblock;dn=(n-1)*block
            read(unit)xblock
            do i=1,block;x(i+dn)=xblock(i);end do
         end do
      end
      subroutine ReadInteger(unit,block,x)
      use params
      implicit none
      integer unit,i,n,np,dn,count,block
      integer,dimension(block*nblock)::x
      integer,dimension(:),allocatable::xblock

C-----------------------------------------------------------------------
      allocate(xblock(block))
C--------------------------------- Read ROOTs own blocks ---------------
         do n=1,nblock;dn=(n-1)*block
            read(unit)xblock
            do i=1,block;x(i+dn)=xblock(i);end do
         end do
      end

      subroutine ExtractWallBoundaryFile(reduce,npl)
c-----------------------------------------------------------------------      
      use params
c-----------------------------------------------------------------------      
      implicit none
      character(len=13)zonename
      character(len=256) filename1
      integer lmin,lmax,mmin,mmax,n,nn,l,m,i,j,k,reduce,b1,b2
      integer natr,imin,imax,jmin,jmax,kmin,kmax
      integer attrmin,attrmax
      integer nwallblock,f,i0,npl
c-----------------------------------------------------------------------      
      i0=0
      b1=bsize+1;b2=bsize+2
c     print*,'allocating attrcf',bsize,nblock
c---- cell face attributes ---------------------------------------------
c     allocate(attrcf(b2+1,b2+1,6,nblock))
      do n=1,nblock
c---- face 1-2 ---------------------------------------------------------
      do f=1,2;i=2+mod(f+1,2)*(bsize)
      do k=2,bsize+2; do j=2,bsize+2
         attrcf(j,k,f,n)=attr(i,j,k,n)
      enddo;enddo
      enddo
c---- face 3-4 ---------------------------------------------------------      
      do f=3,4;j=2+mod(f+1,2)*(bsize)
      do k=2,bsize+2; do i=2,bsize+2
         attrcf(k,i,f,n)=attr(i,j,k,n)
      enddo;enddo
      enddo
c---- face 5-6 ---------------------------------------------------------      
      do f=5,6;k=2+mod(f+1,2)*(bsize)
      do j=2,bsize+2; do i=2,bsize+2
         attrcf(i,j,f,n)=attr(i,j,k,n)
      enddo;enddo
      enddo
c-----------------------------------------------------------------------
      enddo

      filename1(1:19)='grid00000.xyz.fvbnd'
      write(filename1(5:9),'(i5.5)')npl
      open(unit=17, file=filename1(1:19),FORM='formatted')
      write(17,*)'FVBND 1 4'
      write(17,*)' wall'
      write(17,*)' inlet'
      write(17,*)' outlet'
      write(17,*)' cyclic'
      write(17,*)' symmetry'
c-----------------------------------------------------------------------
      write(17,*)'BOUNDARIES'
c--------------------------------------------- boundary regions -------
      natr=5
      nwallblock=0
      do i=1,natr
      select case(i)
       case(1) 
         attrmin=101;attrmax=200
       case(2) 
         attrmin=201;attrmax=300
       case(3)  
         attrmin=401;attrmax=500
       case(4) 
         attrmin=501;attrmax=600
       case(5) 
         attrmin=601;attrmax=700
      end select 
      ACTIVE_BLOCKS: do n=1,nblock
          FACE: do f=1,6
c------------------ write segment to file ------------------------------
          SEGMENT_START: do mmin=2,bsize+2,reduce
                         m=max(3,min(mmin,bsize+1))
                         do lmin=2,bsize+2,reduce
                         l=max(3,min(lmin,bsize+1))
c            print*,'segment test',attrcf(l,m,f,n),attrmin,l,m,f,n
          if(attrcf(l,m,f,n).ge.attrmin.and.
     &       attrcf(l,m,f,n).le.attrmax     )then
c            print*,'segment start',attrmin,lmin,mmin,f,n
             exit SEGMENT_START
        endif
      end do; end do SEGMENT_START
c-----------------------------------------------------------------------
      if(lmin.gt.bsize+1.or.mmin.gt.bsize+1) cycle FACE
c-----------------------------------------------------------------------
      SEGMENT_END: do mmax=bsize+2,mmin,-reduce
                    m=max(3,min(mmax,bsize+1))
                   do lmax=bsize+2,lmin,-reduce
                    l=max(3,min(lmax,bsize+1))
        if(attrcf(l,m,f,n).ge.attrmin.and.
     o     attrcf(l,m,f,n).le.attrmax     )then
c            print*,'segment end',attrmin,lmax,mmax,f,n
           exit SEGMENT_END
        endif
      end do; end do SEGMENT_END
c-----------------------------------------------------------------------
      if(lmax.le.lmin.or.mmax.le.mmin) cycle FACE
c-----------------------------------------------------------------------
      lmax=(lmax-1)/reduce+i0
      mmin=(mmin-1)/reduce+i0
      mmax=(mmax-1)/reduce+i0
      if(lmin.gt.1)then
         if(attrcf(lmin-1,mmin,f,n).lt.attrmin)lmin=lmin-1
      endif
      if(mmin.gt.1)then
         if(attrcf(lmin,mmin-1,f,n).lt.attrmin)mmin=mmin-1
      endif
      select case(f)
      case(1)
       imin=1;imax=1
       jmin=lmin;jmax=lmax
       kmin=mmin;kmax=mmax
      case(2)
       imin=bsize/reduce+1;imax=bsize/reduce+1
       jmin=lmin;jmax=lmax
       kmin=mmin;kmax=mmax
      case(3)
       imin=mmin;imax=mmax
       jmin=1;jmax=1
       kmin=lmin;kmax=lmax
      case(4)
       imin=mmin;imax=mmax
       jmin=bsize/reduce+1;jmax=bsize/reduce+1
       kmin=lmin;kmax=lmax
      case(5)
       imin=lmin;imax=lmax
       jmin=mmin;jmax=mmax
       kmin=1;kmax=1
      case(6)
       imin=lmin;imax=lmax
       jmin=mmin;jmax=mmax
       kmin=bsize/reduce+1;kmax=bsize/reduce+1
      end select 
         nn=n
         write(17,'(8i5,1a4,i4)')i,n,
     &                     imin,imax,jmin,jmax,kmin,kmax
     &                         ,'F',(-1)**f
c-----------------------------------------------------------------------
      nwallblock=nwallblock+1
      end do FACE
      end do ACTIVE_BLOCKS
c-----------------------------------------------------------------------
      end do
      close(17)
c----------------------------------------------------------------------
      return
      end

