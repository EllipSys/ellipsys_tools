      program X3D2x3d
C-----------------------------------------------------------------------
C     program reading the output from basis3d (X3D) and writing an
C     unformatted (x3dunf) file with only the finest grid level
C-----------------------------------------------------------------------
      implicit none
      logical::statex=.false.
      integer::ni
      integer,parameter::idp4=4,idp8=8,ncomp=4
      real(kind=8),dimension(:,:,:,:),allocatable::x,y,z
      integer,dimension(:,:,:,:),allocatable::attr
      real(kind=8)::dummy
      character*11 fnamd,fnamh
      character*128 fieldname(ncomp)
      integer::idummy
      integer::i,j,k,n,nc,nvar,fileform
      integer::bsize,nblock,b1,iblock,yesno

      inquire(file='grid.X3D',exist=statex)
      if(statex)then
        print*,' READING UNFORMATTED GRID FILE'
        open(unit=20,file='grid.X3D',form='unformatted')
        read(20)bsize
        read(20)nblock
        print*,bsize,nblock
        ni=bsize+3
        allocate(x(ni,ni,ni,nblock)
     &          ,y(ni,ni,ni,nblock)
     &          ,z(ni,ni,ni,nblock))
        allocate(attr(ni,ni,ni,nblock))
        x=0.d0;y=0.d0;z=0.d0
        print*,' Reading Attributs : '
        call ReadBlockInteger(20,ni**3,nblock,attr)
        print*,' Reading x-coord : '
        call ReadBlockReal   (20,ni**3,nblock,x)
        print*,' Reading y-coord : '
        call ReadBlockReal   (20,ni**3,nblock,y)
        print*,' Reading z-coord : '
        call ReadBlockReal   (20,ni**3,nblock,z)
        print*,' Finished Reading : '
        close(10)
      endif


      print*,' Write unformatted=0 or formatted=1 : '
      read*,yesno
      if(yesno.eq.0)then
c-------x3dunf unformatted file
        print*,' Writing unformatted x3d file : '
        call Unfgridwrite(10,ni,nblock,x,y,z,attr)
      else
c-------x3d formatted file
        print*,' Writing formatted x3d file : '
        open(unit=10,file='grid.x3d',form='formatted')
        write(10,*)ni-3,nblock
        do n=1,nblock;print*,' Writing block : ',n
        do k=2,bsize+2
        do j=2,bsize+2
        do i=2,bsize+2
          write(10,'(i6,3e30.15)')attr(i,j,k,n)
     &                              ,x(i,j,k,n)
     &                              ,y(i,j,k,n)
     &                              ,z(i,j,k,n)
        enddo;enddo;enddo
        enddo
      endif
      close(10)

      stop
   30 format('')
   31 format('VARIABLES = ',15(A20,','))
   32 format('ZONE I=',i2,', J=',i2,', K=',i2,' F=BLOCK')
      end


      subroutine unfgridwrite(unitnr,ni,nblock,x,y,z,attr)
C=======================================================================
C     routine for writing unformatted x3d file
C     remember that the grid must positions in the following region
C
C        x(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C        y(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C        z(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C     attr(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C
C     the remaining spaces are intended for ghost cells
C=======================================================================
      implicit none
      integer  unitnr,ni,nblock
      real(kind=8),dimension(ni,ni,ni,nblock)::x,y,z
      integer,dimension(ni,ni,ni,nblock)::attr

      open(unit=unitnr,file='grid.x3dunf',form='unformatted')
      write(unitnr)ni-3
      write(unitnr)nblock
      print*, ' bsize, nblock ',ni-3,nblock
      call Writeblockinteger(unitnr,ni**3,nblock,attr)
      call Writeblockreal   (unitnr,ni**3,nblock,x   )
      call Writeblockreal   (unitnr,ni**3,nblock,y   )
      call Writeblockreal   (unitnr,ni**3,nblock,z   )

      close(unitnr)
      return
      end

      subroutine writeblockreal(unit,block,nblock,x)
C=======================================================================
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      real(kind=8),dimension(block,nblock)::x
      real(kind=8),dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine writeblockreal

      subroutine writeblockinteger(unit,block,nblock,x)
C=======================================================================
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      integer,dimension(block,nblock)::x
      integer,dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine writeblockinteger

      subroutine ReadBlockReal(unit,block,nblock,x)
C=======================================================================
C=======================================================================
      implicit none
      integer unit,nblock,block,i,n
      real(kind=8),dimension(block,nblock)::x
      real(kind=8),dimension(block)       ::xblock
        do n=1,nblock
          read(unit)xblock
          do i=1,block
            x(i,n)=xblock(i)
          end do
        end do
      end subroutine ReadBlockReal

      subroutine ReadBlockInteger(unit,block,nblock,x)
C=======================================================================
C=======================================================================
      implicit none
      integer unit,nblock,block,i,n
      integer,dimension(block,nblock)::x
      integer,dimension(block)       ::xblock
        do n=1,nblock
          read(unit)xblock
          do i=1,block
            x(i,n)=xblock(i)
          end do
        end do
      end subroutine ReadBlockInteger
