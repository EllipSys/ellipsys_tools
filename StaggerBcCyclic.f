c     $Rev:: 8           $: Revision of last commit
c     $Author:: nsqr     $: Author of last commit
c     $Date:: 2011-08-02#$: Date of last commit
      program MeshReadWrite
c----------------------------------------------------------------------- 
c     A small code for changing the cyclic attributs in order
c     to have a transverse shift of inlet to outlet for cyclic bc's
c     
c     The code assumes that we have main flow direction in the I-direction
c
c     the cyclic bc in the I-direction to be changed must be set to 501
c
c     And the remaining cyclic bc's must be 598, 599 or 600
c
c     Author Niels N. Soerensen
c-----------------------------------------------------------------------
      implicit none
      logical::statex=.false.
      real(kind=8),dimension(:,:,:,:),allocatable::x,y,z
      integer,dimension(:,:,:,:),allocatable::attr
      integer::i,j,k,n,ii,jj,kk,nbi,nbj,nbk,att,attrval
      integer::ni,bsize,nblock,modus
      integer yesno

      inquire(file='org.x3dunf',exist=statex)
      if(statex)then
        print*,' READING UNFORMATTED GRID FILE'
        open(unit=20,file='org.x3dunf',form='unformatted')
        read(20)bsize
        read(20)nblock
        print*,bsize,nblock
        ni=bsize+3
        allocate(x(ni,ni,ni,nblock)
     &          ,y(ni,ni,ni,nblock)
     &          ,z(ni,ni,ni,nblock)
     &       ,attr(ni,ni,ni,nblock))
        x=0.d0;y=0.d0;z=0.d0;attr=0
        print*,' Reading Attributs : '
        call ReadBlockInteger(20,ni**3,nblock,attr)
        print*,' Reading x-coord : '
        call ReadBlockReal   (20,ni**3,nblock,x)
        print*,' Reading y-coord : '
        call ReadBlockReal   (20,ni**3,nblock,y)
        print*,' Reading z-coord : '
        call ReadBlockReal   (20,ni**3,nblock,z)
        print*,' Finished Reading : '
        close(10)
      else
        inquire(file='org.x3d',exist=statex)
        if(.not.statex)then
          print*,' No grid file, stopping '
          stop
        endif
        print*,' READING FORMATTED GRID FILE'
        open(unit=20,file='org.x3d')
        read(20,*)bsize,nblock
        ni=bsize+3
        allocate(x(ni,ni,ni,nblock)
     &          ,y(ni,ni,ni,nblock)
     &          ,z(ni,ni,ni,nblock)
     &       ,attr(ni,ni,ni,nblock))
        x=0.d0;y=0.d0;z=0.d0;attr=0
        print*,bsize,nblock
        do n=1,nblock
        do k=2,bsize+2
        do j=2,bsize+2
        do i=2,bsize+2
          read(20,*)attr(i,j,k,n)  
     &            ,x(i,j,k,n)
     &            ,y(i,j,k,n)
     &            ,z(i,j,k,n)
        enddo;enddo;enddo;enddo
      endif

   
      print*,' Give nr block in I-, J- and K- direction. '
      read(*,*)nbi,nbj,nbk
      print*,' Wall at J-const=1, K-const=2 '
      read(*,*)modus
   

c-----------------------------------------------------------------------
c     Boundary conditions processing
c-----------------------------------------------------------------------
     
c-----'Inlet' boundary (501)
      do kk=1,nbk
      do jj=1,nbj
         n=1+(jj-1)*nbi+(kk-1)*(nbi*nbj)
         if(attr(2,bsize/2+1,bsize/2+1,n).eq.501)then
c---------- Wall at J=const
           if(modus.eq.1)then
             att=nbk+1-kk
c---------- Wall at K=const
           else
             att=nbj+1-jj
           endif
           print*,' Shift block inlet at : ',n,att
           do k=2,bsize+2
           do j=2,bsize+2
             if(attr(2,j,k,n).eq.501)attr(2,j,k,n)=500+att
           enddo;enddo
         endif
      enddo;enddo
c-----'Outflow' boundary (501)
      do kk=1,nbk
      do jj=1,nbj
c---------- Wall at J=const
         n=nbi+(jj-1)*nbi+(kk-1)*(nbi*nbj)
         if(attr(bsize+2,bsize/2+1,bsize/2+1,n).eq.501)then
c---------- Wall at J=const
           if(modus.eq.1)then
             att=nbk-kk;if(att.eq.0)att=nbk
c---------- Wall at K=const
           else
             att=nbj-jj;if(att.eq.0)att=nbj
           endif
           print*,' Shift block outlet at : ',n,att
           do k=2,bsize+2
           do j=2,bsize+2
             if(attr(bsize+2,j,k,n).eq.501)attr(bsize+2,j,k,n)=500+att
           enddo;enddo
         endif
      enddo;enddo

c-------write unformatted grid -----------------------------------------
  100 call UnfGridWrite(20,ni,nblock,x,y,z,attr)

      stop
   30 format('')
   31 format('VARIABLES = ',15(A20,','))
   32 format('ZONE I=',i2,', J=',i2,', K=',i2,' F=BLOCK')
      end

      subroutine ReadBlockReal(unit,block,nblock,x)
      implicit none
      integer unit,nblock,block,i,n
      real(kind=8),dimension(block,nblock)::x
      real(kind=8),dimension(block)       ::xblock
        do n=1,nblock
          read(unit)xblock
          do i=1,block
            x(i,n)=xblock(i)
          end do
        end do
      end subroutine ReadBlockReal

      subroutine ReadBlockInteger(unit,block,nblock,x)
      implicit none
      integer unit,nblock,block,i,n
      integer,dimension(block,nblock)::x
      integer,dimension(block)       ::xblock
        do n=1,nblock
          read(unit)xblock
          do i=1,block
            x(i,n)=xblock(i)
          end do
        end do
      end subroutine ReadBlockInteger


      subroutine UnfGridWrite(unitnr,ni,nblock,x,y,z,attr)
C=======================================================================
C     routine for writing unformatted x3d file
C     remember that the grid must positions in the following region 
C
C        x(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C        y(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C        z(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C     attr(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C
C     the remaining spaces are intended for ghost cells
C=======================================================================
      implicit none
      integer unitnr,ni,nblock
      real(kind=8),dimension(ni,ni,ni,nblock)::x,y,z
      integer,dimension(ni,ni,ni,nblock)::attr

      print*,' unitnr ',unitnr
      open(unit=unitnr,file='grid.X3DUNF',form='unformatted')
      write(unitnr)ni-3
      write(unitnr)nblock
      print*,' Writing Attributes '
      call WriteBlockInteger(unitnr,ni**3,nblock,attr)
      print*,' Writing x '
      call WriteBlockReal   (unitnr,ni**3,nblock,x   )
      print*,' Writing y '
      call WriteBlockReal   (unitnr,ni**3,nblock,y   )
      print*,' Writing z '
      call WriteBlockReal   (unitnr,ni**3,nblock,z   )
      print*,' Finished writing '
      close(unitnr)
      return
      end

      subroutine WriteBlockReal(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      real(kind=8),dimension(block,nblock)::x
      real(kind=8),dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine WriteBlockReal

      subroutine WriteBlockInteger(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      integer,dimension(block,nblock)::x
      integer,dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine WriteBlockInteger
