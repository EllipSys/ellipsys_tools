      program X3D2PLOT3D
      implicit none
      integer bsize,nblock
      integer i,j,k,n
      real(kind=8),dimension(:,:,:),allocatable::x,y,z
      integer,dimension(:,:,:),allocatable::attr
      integer,parameter::idp=8,reduce=1

      open(unit=10,file='grid.x2d')
c-----read grid
      print*,' READING GRID '
      read(10,*)bsize,nblock
      print*,bsize,nblock
      allocate(x(bsize+2,bsize+2,nblock)
     &        ,y(bsize+2,bsize+2,nblock)
     &        ,z(bsize+2,bsize+2,nblock))
      allocate(attr(bsize+2,bsize+2,nblock))
      do n=1,nblock
      do j=2,bsize+2
      do i=2,bsize+2
        read(10,*)attr(i,j,n)
     &          ,x(i,j,n)
     &          ,y(i,j,n)
     &          ,z(i,j,n)
      enddo;enddo;enddo
      close(10)

c-------Write Plot3D file
      open(unit=10,file='grid.xyz',form='formatted')
      write(10,*)nblock
      write(10,*)(bsize/reduce+1
     &           ,bsize/reduce+1
     &           ,1,n=1,nblock)
      do n=1,nblock
        write(10,*)
     &     (((real(x(i,j,n),idp)
     &      ,i=2,bsize+2,reduce)
     &      ,j=2,bsize+2,reduce)
     &      ,k=1,1),
     &     (((real(y(i,j,n),idp)
     &      ,i=2,bsize+2,reduce)
     &      ,j=2,bsize+2,reduce)
     &      ,k=1,1),
     &     (((real(z(i,j,n),idp)
     &      ,i=2,bsize+2,reduce)
     &      ,j=2,bsize+2,reduce)
     &      ,k=1,1)
      enddo
      close(10)

      stop
      end
