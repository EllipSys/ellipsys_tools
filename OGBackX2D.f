      module control
        integer::domaintype=1
        integer::bctype=2
        integer::nk=0
        real(kind=8)::len
        integer::nrcylmax=5,nrboxmax=5,nrcyl=0,nrbox=0
        real(kind=8),dimension(3,5)::cyl
        real(kind=8),dimension(6,5)::box
        integer::nrdist=0
        real(kind=8),allocatable,dimension(:,:)::distinp 
        real(kind=8)::xtranslate=0.d0,ytranslate=0.d0,ztranslate=0.d0
        real(kind=8),dimension(3)::rotangle=0.d0
      end module


C Example input file
C
C # Domain type
C domain-type box
C # BC type
C bc-type inlet / bc-type overset / bc-type symmetry
C # Distribution function in k-direction
C nk 193
C # Number of mesh points in k-direction
C domain-length  2000 
C # Cylindrical rotor cut-out
C rotor-cyl -25 25 41
C rotor-box -25 25 -27 27 -30 30
C # Distribution function nr points (s/smax delta idx)
C k-distribution 4 0.0000 -1 1 0.4875  0.76 65 0.5125  0.76 129 1.0000 -1 193
C # Translate the domain
C   translate xt yt zt
C # Rotate domain around the y-axis for for yaw computations (right hand rule)
C y-rotation -30 
C stop

      program OGBackX2D
C-----------------------------------------------------------------------
C     A program for generating a cylindrical extrusion in the Z-direction 
C     based on a X2D planar grid. 
C-----------------------------------------------------------------------
      use control
      implicit none
      real(kind=8),dimension(:,:,:),allocatable::x2d,y2d
      integer,dimension(:,:,:),allocatable::attr2d
      real(kind=8),dimension(:,:,:,:),allocatable::x,y,z
      integer,dimension(:,:,:,:),allocatable::attr
      real(kind=8),dimension(:,:,:,:),allocatable::xb,yb,zb
      real(kind=8),dimension(:,:,:,:),allocatable::xc,yc,zc
      integer,dimension(:,:,:,:),allocatable::attrb,attrc
      integer,dimension(:),allocatable::active
      real(kind=8),allocatable,dimension(:)::fs
      integer errchk
      real(kind=8)::pi,alpha   
      real(kind=8)::xcent,ycent,zcent,rcent,rmax,radius,xt,yt
      real(kind=8)::xmin,ymin,zmin,xmax,ymax,zmax,rmin,dx,dy,dz
      real(kind=8)::theta
      integer ni,nb,i,j,k,n,m
      integer dim,nib,nbk,nbm,kk,nn,nih,factor,attr1,attr2,attr3

      pi=4.d0*atan(1.d0)

c-------read input file
      call cmdint
      print*,' finished reading input '

c-------distribution function
      if(nrdist.eq.0)then
        nrdist=4
        allocate(fs(nk))
        fs=0.d0
        do k=1,nk
          fs(k)=real(k-1)/real(nk-1)
         enddo
      else
        allocate(fs(nk))
        distinp(:,2)=distinp(:,2)/len
        call distfunc(nrdist,distinp,1,nk,errchk,fs)
      endif

c--------allocate 2d vertices
      open(unit=10,file='grid.x2d')
      read(10,*)ni,nb
      ni=ni+1
      nih=ni/2+1
      allocate(x2d(ni,ni,nb),y2d(ni,ni,nb))
      allocate(attr2d(ni,ni,nb))
      do n=1,nb
      do j=1,ni
      do i=1,ni
        read(10,*)attr2d(i,j,n),x2d(i,j,n),y2d(i,j,n)
      enddo;enddo
      enddo
      close(10)
      attr2d=201

c--------allocate 3d vertices
      allocate(x(ni,ni,nk,nb),y(ni,ni,nk,nb),z(ni,ni,nk,nb)
     &     ,attr(ni,ni,nk,nb))
    
      rmax=0.d0
      do n=1,nb
      do k=1,nk
      do j=1,ni
      do i=1,ni
        x(i,j,k,n)=x2d(i,j,n)
        y(i,j,k,n)=y2d(i,j,n)
        z(i,j,k,n)=-.5*len+len*fs(k)
        radius=sqrt(x(i,j,k,n)**2+y(i,j,k,n)**2)
        if(radius.gt.rmax)rmax=radius
      enddo;enddo;enddo
      enddo
    
C-----------------------------------------------------------------------
c      Set inlet boundary conditions on un-cut mesh (Circular)
C-----------------------------------------------------------------------
      if(domaintype.eq.1) then
      attr=1
      if(bctype.eq.1.or.bctype.eq.3)then
        attr1=201
        attr2=401
        attr3=201
      else
        attr1=251
        attr2=251
        attr3=251
      endif
      attr(:,:,nk,:)=attr2
      attr(:,:,1 ,:)=attr1
c--------set outer cylinder to inlet
      do n=1,nb
c-------west wall
        radius=sqrt(x(1,nih,1,n)**2+y(1,nih,1,n)**2)
        if(radius.gt.0.995*rmax)then
          attr(1 ,:,:,n)=attr1
        endif
c-------east wall
        radius=sqrt(x(ni,nih,1,n)**2+y(ni,nih,1,n)**2)
        if(radius.gt.0.995*rmax)then
          attr(ni,:,:,n)=attr1
        endif
c-------south wall
        radius=sqrt(x(nih,1,1,n)**2+y(nih,1,1,n)**2)
        if(radius.gt.0.995*rmax)then
          attr(: ,1 ,:,n)=attr1
        endif
c-------north wall
        radius=sqrt(x(nih,ni,1,n)**2+y(nih,ni,1,n)**2)
        if(radius.gt.0.995*rmax)then
          attr(: ,ni,:,n)=attr1
        endif
      enddo
      endif


C-----------------------------------------------------------------------
C     Set inlet boundary conditions on un-cut mesh (Box type)
C-----------------------------------------------------------------------
      if(domaintype.eq.2)then
      attr=1
      if(bctype.eq.1)then
        attr1=201
        attr2=401
        attr3=201
      elseif(bctype.eq.2)then
        attr1=251
        attr2=251
        attr3=251
      elseif(bctype.eq.3)then
        attr1=201
        attr2=401
        attr3=601
      endif
      attr(:,:,nk,:)=attr2
      attr(:,:,1 ,:)=attr1
      xmax=maxval(x2d(:,:,:));xmin=minval(x2d(:,:,:));dx=xmax-xmin
      ymax=maxval(y2d(:,:,:));ymin=minval(y2d(:,:,:));dy=ymax-ymin
      print*,'xmin,xmax,dx',xmin,xmax,dx*.001
      print*,'ymin,ymax,dy',ymin,ymax,dy*.001
c--------set outer box to inlet
      do n=1,nb
c-------west wall
        xt=x(1 ,nih,1,n);yt=y(1  ,nih,1,n)
        if(xt.gt.xmax-0.001*dx.or.xt.lt.xmin+0.001*dx.or.
     &     yt.gt.ymax-0.001*dy.or.yt.lt.ymin+0.001*dy)then
          attr(1 ,:,:,n)=attr3
        endif
c-------east wall
        xt=x(ni,nih,1,n);yt=y(ni,nih,1,n)
        if(xt.gt.xmax-0.001*dx.or.xt.lt.xmin+0.001*dx.or.
     &     yt.gt.ymax-0.001*dy.or.yt.lt.ymin+0.001*dy)then
          attr(ni,:,:,n)=attr3
        endif
c-------south wall
        xt=x(nih,1 ,1,n);yt=y(nih,1 ,1,n)
        if(xt.gt.xmax-0.001*dx.or.xt.lt.xmin+0.001*dx.or.
     &     yt.gt.ymax-0.001*dy.or.yt.lt.ymin+0.001*dy)then
          attr(: ,1 ,:,n)=attr3
        endif
c-------north wall
        xt=x(nih,ni,1,n);yt=y(nih,ni,1,n)
        if(xt.gt.xmax-0.001*dx.or.xt.lt.xmin+0.001*dx.or.
     &     yt.gt.ymax-0.001*dy.or.yt.lt.ymin+0.001*dy)then
          attr(: ,ni,:,n)=attr3
        endif
      enddo
      if(bctype.eq.3)then
        attr(:,:,1 ,:)=attr1
        attr(:,:,nk,:)=attr2
      endif
      endif

C-------subdivide k direction
      print*,' Spanwise dimension : ',nk-1
      print*,' Assuming spanwise block size : ',ni-1
      dim=ni-1
      nbk=nk/dim
      print*,' Number of blocks in k-direction : ',nbk
      nbm=nb*nbk
      nib=ni+2

      allocate(xb(nib,nib,nib,nbm)
     &        ,yb(nib,nib,nib,nbm)
     &        ,zb(nib,nib,nib,nbm)
     &     ,attrb(nib,nib,nib,nbm))

      xb=0.d0;yb=0.d0;zb=0.d0;attrb=0.d0
      
      do kk=1,nbk
      do n=1,nb
      do k=2,ni+1
      do j=2,ni+1
      do i=2,ni+1
          nn=n+(kk-1)*nb
          xb(i,j,k,nn)=x(i-1,j-1,k-1+(kk-1)*(ni-1),n)
          yb(i,j,k,nn)=y(i-1,j-1,k-1+(kk-1)*(ni-1),n)
          zb(i,j,k,nn)=z(i-1,j-1,k-1+(kk-1)*(ni-1),n)
       attrb(i,j,k,nn)=
     &              attr(i-1,j-1,k-1+(kk-1)*(ni-1),n)
      enddo;enddo;enddo
      enddo
      enddo
c-------translate the domain in x,y,z
      if(xtranslate.ne.0.d0)xb=xb+xtranslate
      if(ytranslate.ne.0.d0)yb=yb+ytranslate
      if(ztranslate.ne.0.d0)zb=zb+ztranslate

c------- remove blocks that overlap cylinder rotor regions
      nn=0
      allocate(active(nbm));active=1
      do m=1,nrcyl
      do n=1,nbm
        rcent=sqrt(xb(nih,nih,nih,n)**2+yb(nih,nih,nih,n)**2)
        if(zb(nih,nih,nih,n).gt.cyl(1,m).and.
     &     zb(nih,nih,nih,n).lt.cyl(2,m).and.
     &     rcent.lt.cyl(3,m))then
           active(n)=-1
        else
           nn=nn+1
        endif
      enddo
      enddo
c------- remove blocks that overlap box rotor regions
      do m=1,nrbox
      do n=1,nbm
        xcent=xb(nih,nih,nih,n)
        ycent=yb(nih,nih,nih,n)
        zcent=zb(nih,nih,nih,n)
        if(xcent.gt.box(1,m).and.xcent.lt.box(2,m).and.
     &     ycent.gt.box(3,m).and.ycent.lt.box(4,m).and.
     &     zcent.gt.box(5,m).and.zcent.lt.box(6,m))then
           active(n)=-1
           print*,' removing box block : ',n
        else
           nn=nn+1
        endif
      enddo
      enddo
      if(nrcyl.eq.0.and.nrbox.eq.0)nn=nbm

c------relocate the blocks to remove rotor regions
      allocate(xc(nib,nib,nib,nn)
     &        ,yc(nib,nib,nib,nn)
     &        ,zc(nib,nib,nib,nn)
     &     ,attrc(nib,nib,nib,nn))
c-------shift blocks
      nn=0
      do n=1,nbm
        if(active(n).eq.1)then
        nn=nn+1
        xc   (:,:,:,nn)=xb   (:,:,:,n)
        yc   (:,:,:,nn)=yb   (:,:,:,n)
        zc   (:,:,:,nn)=zb   (:,:,:,n)
        attrc(:,:,:,nn)=attrb(:,:,:,n)
        endif
      enddo
      deallocate(xb,yb,zb,attrb)
      nbm=nn

c-------set overset bc's on the inner cylinder holes
      do m=1,nrcyl
      zmin=cyl(1,m)
      zmax=cyl(2,m)
      rmin=cyl(3,m)
      dz=(zmax-zmin)*.01
      do n=1,nbm
c-------west wall
        radius=sqrt(xc(2  ,nih,nih,n)**2+yc(2  ,nih,nih,n)**2)
        if(zc(2   ,nih,nih,n).gt.zmin-dz.and.
     &     zc(2   ,nih,nih,n).lt.zmax+dz.and.
     &     radius.lt.1.1*rmin)then
          attrc(2   ,:   ,:   ,n)=251
        endif
c-------east wall
        radius=sqrt(xc(ni+1,nih,nih,n)**2+yc(ni+1,nih,nih,n)**2)
        if(zc(ni+1,nih,nih,n).gt.zmin-dz.and.
     &     zc(ni+1,nih,nih,n).lt.zmax-dz.and.
     &     radius.lt.1.1*rmin)then
          attrc(ni+1,:   ,:   ,n)=251
        endif
c-------south wall
        radius=sqrt(xc(nih,2   ,nih,n)**2+yc(nih,2   ,nih,n)**2)
        if(zc(nih,2   ,nih,n).gt.zmin-dz.and.
     &     zc(nih,2   ,nih,n).lt.zmax+dz.and.
     &     radius.lt.1.1*rmin)then
          attrc(:   ,2   ,:   ,n)=251
        endif
c-------north wall
        radius=sqrt(xc(nih,ni+1,nih,n)**2+yc(nih,ni+1,nih,n)**2)
        if(zc(nih,ni+1,nih,n).gt.zmin-dz.and.
     &     zc(nih,ni+1,nih,n).lt.zmax+dz.and.
     &     radius.lt.1.1*rmin)then
          attrc(:   ,ni+1,:   ,n)=251
        endif
c-------bottom wall
        radius=sqrt(xc(nih,nih,2   ,n)**2+yc(nih,nih,2   ,n)**2)
        if(zc(nih,nih,2   ,n).gt.zmin-dz.and.
     &     zc(nih,nih,2   ,n).lt.zmax+dz.and.
     &     radius.lt.1.1*rmin)then
          attrc(:   ,:   ,2   ,n)=251
        endif
c-------north wall
        radius=sqrt(xc(nih,nih,ni+1,n)**2+yc(nih,nih,ni+1,n)**2)
        if(zc(nih,nih,ni+1,n).gt.zmin-dz.and.
     &     zc(nih,nih,ni+1,n).lt.zmax+dz.and.
     &     radius.lt.1.1*rmin)then
          attrc(:   ,:   ,ni+1,n)=251
        endif
      enddo
      enddo

c-------set overset bc's on the inner box holes
      do m=1,nrbox
      xmin=box(1,m)
      xmax=box(2,m)
      ymin=box(3,m)
      ymax=box(4,m)
      zmin=box(5,m)
      zmax=box(6,m)
      dx=(xmax-xmin)*.01
      dy=(ymax-ymin)*.01
      dz=(zmax-zmin)*.01
      do n=1,nbm
c-------west wall
        if(xc(2   ,nih,nih,n).gt.xmin-dx.and.
     &     xc(2   ,nih,nih,n).lt.xmax+dx.and.
     &     yc(2   ,nih,nih,n).gt.ymin-dy.and.
     &     yc(2   ,nih,nih,n).lt.ymax+dy.and.
     &     zc(2   ,nih,nih,n).gt.zmin-dz.and.
     &     zc(2   ,nih,nih,n).lt.zmax+dz)then
          attrc(2   ,:   ,:   ,n)=251
        endif
c-------east wall
        if(xc(ni+1,nih,nih,n).gt.xmin-dx.and.
     &     xc(ni+1,nih,nih,n).lt.xmax+dx.and.
     &     yc(ni+1,nih,nih,n).gt.ymin-dy.and.
     &     yc(ni+1,nih,nih,n).lt.ymax+dy.and.
     &     zc(ni+1,nih,nih,n).gt.zmin-dz.and.
     &     zc(ni+1,nih,nih,n).lt.zmax+dz)then
          attrc(ni+1,:   ,:   ,n)=251
        endif
c-------south wall
        if(xc(nih,2   ,nih,n).gt.xmin-dx.and.
     &     xc(nih,2   ,nih,n).lt.xmax+dx.and.
     &     yc(nih,2   ,nih,n).gt.ymin-dy.and.
     &     yc(nih,2   ,nih,n).lt.ymax+dy.and.
     &     zc(nih,2   ,nih,n).gt.zmin-dz.and.
     &     zc(nih,2   ,nih,n).lt.zmax+dz)then
          attrc(:  ,2  ,:   ,n)=251
        endif
c-------north wall
        if(xc(nih,ni+1,nih,n).gt.xmin-dx.and.
     &     xc(nih,ni+1,nih,n).lt.xmax+dx.and.
     &     yc(nih,ni+1,nih,n).gt.ymin-dy.and.
     &     yc(nih,ni+1,nih,n).lt.ymax+dy.and.
     &     zc(nih,ni+1,nih,n).gt.zmin-dz.and.
     &     zc(nih,ni+1,nih,n).lt.zmax+dz)then
          attrc(:  ,ni+1,:   ,n)=251
        endif
c-------bottom wall
        if(xc(nih,nih,2   ,n).gt.xmin-dx.and.
     &     xc(nih,nih,2   ,n).lt.xmax+dx.and.
     &     yc(nih,nih,2   ,n).gt.ymin-dy.and.
     &     yc(nih,nih,2   ,n).lt.ymax+dy.and.
     &     zc(nih,nih,2   ,n).gt.zmin-dz.and.
     &     zc(nih,nih,2   ,n).lt.zmax+dz)then
          attrc(:  ,:  ,2 ,n)=251
        endif
c-------top wall
        if(xc(nih,nih,ni+1,n).gt.xmin-dx.and.
     &     xc(nih,nih,ni+1,n).lt.xmax+dx.and.
     &     yc(nih,nih,ni+1,n).gt.ymin-dy.and.
     &     yc(nih,nih,ni+1,n).lt.ymax+dy.and.
     &     zc(nih,nih,ni+1,n).gt.zmin-dz.and.
     &     zc(nih,nih,ni+1,n).lt.zmax+dz)then
          attrc(:  ,:  ,ni+1,n)=251
        endif
      enddo
      enddo

c-------rotate domain around the y-axis (for yaw simulations)
      if(rotangle(1).ne.0.d0)then
      rotangle(1)=rotangle(1)*atan(1.d0)/45.d0
      do n=1,nbm
      do k=2,ni+1
      do j=2,ni+1
      do i=2,ni+1
        theta=atan2(xc(i,j,k,n),zc(i,j,k,n))+rotangle(1)
        radius=sqrt(xc(i,j,k,n)**2+zc(i,j,k,n)**2)
        xc(i,j,k,n)= radius*sin(theta)
        zc(i,j,k,n)= radius*cos(theta)
      enddo;enddo;enddo
      enddo
      endif

      call Unfgridwrite(10,nib,nbm,xc,yc,zc,attrc)
      stop
  666 write(*,*) ' error: too few data in input file (grid.dat)'
      end


      subroutine unfgridwrite(unitnr,ni,nblock,x,y,z,attr)
C=======================================================================
C     routine for writing unformatted x3d file
C     remember that the grid must positions in the following region 
C
C        x(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C        y(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C        z(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C     attr(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C
C     the remaining spaces are intended for ghost cells
C=======================================================================
      implicit none
      integer  unitnr,ni,nblock
      real(kind=8),dimension(ni,ni,ni,nblock)::x,y,z 
      integer,dimension(ni,ni,ni,nblock)::attr

      open(unit=unitnr,file='grid.x3dunf',form='unformatted')
      write(unitnr)ni-3
      write(unitnr)nblock
      print*, ' bsize, nblock ',ni-3,nblock
      call Writeblockinteger(unitnr,ni**3,nblock,attr)
      call Writeblockreal   (unitnr,ni**3,nblock,x   )
      call Writeblockreal   (unitnr,ni**3,nblock,y   )
      call Writeblockreal   (unitnr,ni**3,nblock,z   )

      close(unitnr)
      return
      end

      subroutine distfunc(nn,dinp,ival,ndist,errchk,fdist)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      implicit none
      integer::nn,ndist
      real(kind=8),dimension(ndist)::fdist,dy
      real(kind=8),dimension(nn,3)::dinp
      real(kind=8) s0,s1,d0,d1
      real(kind=8) len,delta1,delta2,error
      integer n0,n1,ns,nf
      integer i,j,ival,n,errchk,err


      fdist(1)=0.d0
      s0=dinp(1,1);d0=dinp(1,2);n0=int(dinp(1,3))
      do i=2,nn
        s1=dinp(i,1);d1=dinp(i,2);n1=int(dinp(i,3))
c---------if no size given take size from last segment
        if(i.gt.2.and.d0.lt.0.d0)d0=(fdist(n0)-fdist(n0-1))
        len=s1-s0
        delta1=d0
        delta2=d1
        err=1
        if(ival.eq.1)call tanhdist(delta1,delta2,len,n0,n1,err,dy(n0))
        if(ival.eq.2)call sinhdist(delta1,delta2,len,n0,n1,err,dy(n0))
        if(errchk.eq.1.and.err.eq.-1)then
          delta1=dy(n0+1)-dy(n0)
          delta2=dy(n1)-dy(n1-1)
          write(*,'(2x,a35)')' convergence problem in distfunc : '
          write(*,'(2x,a10,i5,i5,i5)')'section : ',i,n0,n1
          error=abs(d0-delta1)/d0*100;if(d0.lt.0.d0)error=0.d0
          write(*,'(2x,a24,f20.4)')'start err in percent  : ',error
          error=abs(d1-delta2)/d1*100;if(d1.lt.0.d0)error=0.d0
          write(*,'(2x,a24,f20.4)')'end   err in percent  : ',error
        endif
        do j=n0+1,n1
        fdist(j)=fdist(n0)+dy(j)
        enddo
        s0=s1;d0=d1;n0=n1
      enddo
      return
      do i=9,ndist,16
        fdist(i)=.5*(fdist(i-8)+fdist(i+8))
      enddo
      do i=5,ndist,8
        fdist(i)=.5*(fdist(i-4)+fdist(i+4))
      enddo
      do i=3,ndist,4
        fdist(i)=.5*(fdist(i-2)+fdist(i+2))
      enddo
      do i=2,ndist,2
        fdist(i)=.5*(fdist(i-1)+fdist(i+1))
      enddo
      do n=1,12
        do i=2,ndist-1
          fdist(i)=.25d0*(fdist(i-1)+2*fdist(i)+fdist(i+1))
        enddo
      enddo
      return
   21 format('#',i6)
      end

      subroutine tanhdist(delta1,delta2,len,i1,i2,err,fdist)
************************************************************************
*     TANDIST
*     program for computing the hyperbolic tangent distribution
*     delta1 : grid spacing at i=i1 (first cell)
*     delta2 : grid spacing at i=i2 (last  cell)
*     len    : lenght of curve
*     i1     : indices of first vertex
*     i2     : indices of last vertex
*     fdist  : compute distribution function
*
*
************************************************************************
*     Author  : niels n. sorensen
*     Date    :
************************************************************************
      implicit none
      integer i1,i2,ni,i,err
      real(kind=8)::delta1,delta2,len
      real(kind=8)::delta,b,a
      real(kind=8)::fdist(i2-i1+1)
      real(kind=8)::transsinh,transtanh,ftmp
      external transsinh,transtanh

      if(i2.eq.i1)return

c------- Normalize delta1 and delta2 with lenght
      delta1=delta1/len
      delta2=delta2/len
c------- Compute number of points
      ni=i2-i1


c------- Assure that b > 1
      if(delta1.le.0.d0.and.1/delta2.lt.ni)then
        delta1=1/(ni**2*delta2*1.02)
      elseif(delta2.le.0d0.and.1/delta1.lt.ni)then
        delta2=1/(ni**2*delta1*1.02)
      endif

c------- Spacing at both end specified
      if(delta1.gt.0.d0.and.delta2.gt.0.d0)then
        a=sqrt(delta2)/sqrt(delta1)
        b=1.d0/(ni*sqrt(delta1*delta2))
        if(b.ge.1.d0)then
          delta=transsinh(b,err)
          do i=1,ni+1
            ftmp=.5*(1+tanh(delta*(real(i-1)/real(ni)-0.5))
     &          /tanh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        else
          delta=transtanh(b,err)
          do i=1,ni+1
            ftmp=.5*(1+sinh(delta*(real(i-1)/real(ni)-0.5))
     &          /sinh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        endif
c------- Spacing only specified at xi=1
      else if(delta1.gt.0.d0)then
        b=1.d0/(ni*delta1)
        delta=transsinh(b,err)
        do i=1,ni+1
          fdist(i)=1.d0+tanh(.5d0*delta*(real(i-1)/real(ni)-1.d0))
     &            /tanh(.5d0*delta)
        enddo
c------- Spacing only specified at xi=ni
      else if(delta2.gt.0.d0)then
        b=1.d0/(ni*delta2)
        delta=transsinh(b,err)
        do i=1,ni+1
          fdist(i)=tanh(.5*delta*real(i-1)/real(ni))/tanh(.5*delta)
        enddo
c------- Error no spacing is given
      else
        print*,' error from tandist, no cell hight is given '
      endif
c------- Compute len*fdist
      do i=1,ni+1
        fdist(i)=fdist(i)*len
      enddo
      return
      end

      subroutine sinhdist(delta1,delta2,len,i1,i2,err,fdist)
************************************************************************
*     SINHDIST
*     program for computing the hyperbolic sine distribution
*     delta1 : grid spacing at i=i1 (first cell)
*     delta2 : grid spacing at i=i2 (last  cell)
*     len    : lenght of curve
*     i1     : indices of first vertex
*     i2     : indices of last vertex
*     fdist  : compute distribution function
*
*
************************************************************************
*     Author  : niels n. sorensen
*     Date    :
************************************************************************
      implicit none
      integer i1,i2,ni,i,err
      real(kind=8)::delta1,delta2,len
      real(kind=8)::delta,b,a
      real(kind=8)::fdist(*)
      real(kind=8)::transsinh,transtanh,ftmp
      external transsinh,transtanh

      if(i2.eq.i1)return

c------- Normalize delta1 and delta2 with lenght
      delta1=delta1/len
      delta2=delta2/len
c------- Compute number of points
      ni=i2-i1

c------- Assure that b > 1
      if(delta1.le.0.d0.and.1/delta2.lt.ni)then
        delta1=1/(ni**2*delta2*1.02)
      elseif(delta2.le.0d0.and.1/delta1.lt.ni)then
        delta2=1/(ni**2*delta1*1.02)
      endif

c------- Spacing at both end specified
      if(delta1.gt.0.d0.and.delta2.gt.0.d0)then
        a=sqrt(delta2)/sqrt(delta1)
        b=1.d0/(ni*sqrt(delta1*delta2))
        if(b.ge.1.d0)then
          delta=transsinh(b,err)
          do i=1,ni+1
            ftmp=.5*(1+tanh(delta*(real(i-1)/real(ni)-0.5))
     &          /tanh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        else
          delta=transtanh(b,err)
          do i=1,ni+1
            ftmp=.5*(1+sinh(delta*(real(i-1)/real(ni)-0.5))
     &          /sinh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        endif
c------- Spacing only specified at xi=1
      else if(delta1.gt.0.d0)then
        b=1.d0/(ni*delta1)
        delta=transsinh(b,err)
        do i=1,ni+1
          fdist(i)=sinh(delta*real(i-1)/real(ni))/sinh(delta)
        enddo
c------- Spacing only specified at xi=ni
      else if(delta2.gt.0.d0)then
        b=1.d0/(ni*delta2)
        delta=transsinh(b,err)
        do i=1,ni+1
          fdist(i)=1-sinh(delta*(1-real(i-1)/real(ni)))/sinh(delta)
        enddo
c------- Error no spacing is given
      else
        print*,' error from tandist, no cell hight is given '
      endif
c------- Compute len*fdist
      do i=1,ni+1
        fdist(i)=fdist(i)*len
      enddo
      return
      end

      double precision function transsinh(b,err)
************************************************************************
*     routine for solving the transendental equation
*
*     b=sinh(delta)/delta
*
*     using Newtonraphson
************************************************************************
*     Author  : niels n. sorensen
*     Date    :
************************************************************************
      implicit none
      integer::err
      real(kind=8)::delta,b
      real(kind=8)::delta_old,f,df,rlim
      parameter(rlim=1.d-1)
      integer::n,nmax=250

c------- Preforme newton iteration to obtain delta
      delta=1.d0
      delta_old=delta
      do n=1,nmax
c--------- Compute f(delta)
c        f =b-sinh(delta)/delta
c        f =b*delta-sinh(delta)
         f =delta/(sinh(delta)+1.d-60)-1.d0/b
c--------- Compute d/d(delta)(f(delta))
c        df=(sinh(delta)-delta*cosh(delta))/delta**2
c        df=b-cosh(delta)
         df=(sinh(delta)-delta*cosh(delta))/sinh(delta)**2
c--------- update delta
        delta=delta-.5*f/df
c       delta=delta-f/df
c       write(*,'(4e16.4)')f,df,f/df,delta
c--------- check convergence criterium
        if(n.eq.nmax.and.abs((delta-delta_old)/delta_old).gt.rlim)then
          err=-1
c         print*,' convergence problem in transinh dist. function !!! '
c         print*,' residual ',(delta-delta_old)/delta_old
        endif
c--------- update old value
        delta_old=delta
      enddo
      transsinh=delta
      return
      end

      double precision function transtanh(b,err)
************************************************************************
*     routine for solving the transendental equation
*
*     b=tanh(delta/2)/(delta/2)
*
*     using Newtonraphson
************************************************************************
*     Author  : niels n. sorensen
*     Date    :
************************************************************************
      implicit none
      integer::err
      real(kind=8)::delta,b
      real(kind=8)::delta_old,f,df,rlim
      parameter(rlim=1.d-1)
      integer::n,nmax=250

      if(b.gt.1.0d0)then
        print*,' error form sinhdist: can not be gridded '
c       stop
      endif
c------- Preforme newton iteration to obtain delta
      delta=1.d0
      delta_old=delta
      do n=1,nmax
c--------- Compute f(delta)
        f =delta/(tanh(.5*delta)+1.d-30)-2./B
c--------- Compute d/d(delta)(f(delta))
        df=(tanh(.5*delta)-delta*(1-tanh(.5*delta)**2))
     &    /(tanh(.5*delta)**2+1.d-30)
c--------- update delta
        delta=delta-.5*f/df
c       delta=delta-f/df
c       write(*,'(i3,1x,3e16.4)')n,delta,f,df
c--------- check convergence criterium
        if(n.eq.nmax.and.abs((delta-delta_old)/delta_old).gt.rlim)then
          err=-1
c         print*,' convergence problem in trantanh dist. function !!! '
c         print*,' residual ',(delta-delta_old)/delta_old
        endif
c--------- update old value
        delta_old=delta
      enddo
      transtanh=delta
      return
      end

      subroutine writeblockreal(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      real(kind=8),dimension(block,nblock)::x
      real(kind=8),dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine writeblockreal

      subroutine writeblockinteger(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      integer,dimension(block,nblock)::x
      integer,dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine writeblockinteger

      subroutine cmdint
c======================================================================
c
c======================================================================
      implicit none
      character(len=1) ctemp
      integer::i
      integer,parameter::mword=40
      integer::nrchar(mword),nrword=0
      character(len=256)inputline
      character(len=40),dimension(mword)::words
      logical::lrecogn=.false.
      open(unit=9,file='gcf.dat')

      do
      read(9,'(a256)',end=100)inputline
      call cmdpart(inputline,mword,nrchar,nrword,words)
C------------------------------------- skip blank lines and comments ---
      if(nrword.le.1) then
        cycle
C---------------------------------- skip rest of file if stop appears --
      elseif(words(1)(1:4).eq.'stop'.or.words(1)(1:4).eq.'STOP') then
      exit
c----------------------------- command inputLine interpreter -----------
      else
        call Ogcmd(mword,nrword,nrchar,words,lrecogn)
        if(.not.lrecogn)then
          print*,' keyword not recognized ! '
          stop
        endif
      endif
      enddo
  100 return
      end

      subroutine ogcmd(mword,nrword,nrchar,words,lrecogn)
c======================================================================
c
c======================================================================
      use control
      implicit none
      integer::i
      integer::mword
      integer::nrchar(mword),nrword
      character(len=40),dimension(mword)::words
      logical::lrecogn

        select case(words(1)(1:nrchar(1)))
C------------------------------ keyword not recognized -----------------
        case default
        lrecogn=.false.
c------------------------------ outer domain shape ---------------------
        case('domain-type')
          lrecogn=.true.
          if(words(2).eq.'cylinder')domaintype=1
          if(words(2).eq.'box')domaintype=2
c------------------------------ outer domain shape ---------------------
        case('bc-type')
          lrecogn=.true.
          if(words(2).eq.'inlet')bctype=1
          if(words(2).eq.'overset')bctype=2
          if(words(2).eq.'symmetry')bctype=3
c------------------------------ number of mesh point in ksi and eta ----
        case('nk')
          lrecogn=.true.
          if(nrword.ge.2)read(words(2),*)nk
c------------------------------ domain size ----------------------------
        case('domain-length')
          lrecogn=.true.
          if(nrword.ge.2)read(words(2),*)len
c------------------------------ cylinder rotor domain-------------------
        case('rotor-cyl')
          nrcyl=nrcyl+1
          if(nrcyl.gt.nrcylmax)then
            print*,' To many rotor-cyl domains, stopping '
            stop
          endif
          lrecogn=.true.
          if(nrword.ge.2)read(words(2),*)cyl(1,nrcyl)
          if(nrword.ge.3)read(words(3),*)cyl(2,nrcyl)
          if(nrword.ge.4)read(words(4),*)cyl(3,nrcyl)
c------------------------------ box rotor domain-------------------------
        case('rotor-box')
          nrbox=nrbox+1
          if(nrbox.gt.nrboxmax)then
            print*,' To many rotor-box domains, stopping '
            stop
          endif
          lrecogn=.true.
          if(nrword.ge.2)read(words(2),*)box(1,nrbox)
          if(nrword.ge.3)read(words(3),*)box(2,nrbox)
          if(nrword.ge.4)read(words(4),*)box(3,nrbox)
          if(nrword.ge.5)read(words(5),*)box(4,nrbox)
          if(nrword.ge.6)read(words(6),*)box(5,nrbox)
          if(nrword.ge.7)read(words(7),*)box(6,nrbox)
c------------  surface distribution
c-----   with the format  number of points np followed by np triplets of (l/lmax delta n/np)
        case('k-distribution')
          lrecogn=.true.
          read(words(2),*)nrdist
          allocate(DistInp(nrdist,3))
          do i=1,nrdist
            read(words(3+(i-1)*3),*)DistInp(i,1)
            read(words(4+(i-1)*3),*)DistInp(i,2)
            read(words(5+(i-1)*3),*)DistInp(i,3)
          enddo
c------------  domain translation
        case('translate')
          lrecogn=.true.
          read(words(2),*)xtranslate
          read(words(3),*)ytranslate
          read(words(4),*)ztranslate
c------------  rotate the domain around the y-axis (for yaw simulations)
        case('y-rotation')
          lrecogn=.true.
          read(words(2),*)rotangle(1)
        end select
      return
      end


      subroutine cmdpart(InputLine,mword,nrchar,nrword,words)
c-----------------------------------------------------------------------
c     routine parting command InputLine into words
c----------------------------------------------------------------------
c
c     Author: Jess A. Michelsen and Niels N. Soerensen
c     last revision :march 3, 1998
c-----------------------------------------------------------------------
      implicit none
      integer mword,nrchar(mword),nrword,i
      integer,parameter::mchar=256
      character(len=40),dimension(mword):: words
      character(len=40)::blank
      character(len=256) InputLine
      integer CurrentPosition,RelativeBlankPosition,BlankPosition
C--------------------------------------------- Reset word counter ------
      nrword=0
      CurrentPosition=1
C--------------------------------------------- Skip if a comment line --
      if(index(InputLine(1:mchar),'#').gt.0) return
C---------------------------------------- find distance to next blank --
   20 RelativeBlankPosition=index(InputLine(CurrentPosition:mchar),' ')
      if(CurrentPosition.eq.mchar.or.RelativeBlankPosition.eq.0) return
C---------------------------------------- found the end of a word ------
      if(RelativeBlankPosition.gt.1.and.RelativeBlankPosition.le.40)then
        BlankPosition=CurrentPosition-1+RelativeBlankPosition
        nrword=nrword+1
        if(nrword.gt.mword)return
        words(nrword)=InputLine(CurrentPosition:BlankPosition)
        nrchar(nrword)=RelativeBlankPosition-1
      endif
c-----------------------------------------------------------------------
      CurrentPosition=CurrentPosition+RelativeBlankPosition
      goto 20
c-----------------------------------------------------------------------
      end
