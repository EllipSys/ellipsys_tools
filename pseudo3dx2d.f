      program pseudo3dx2d
      implicit none
      real(kind=8),dimension(:,:,:),allocatable::x2d,z2d
      integer,dimension(:,:,:),allocatable::attr2d
      real(kind=8),dimension(:,:,:,:),allocatable::x,y,z
      integer,dimension(:,:,:,:),allocatable::attr
      real(kind=8),dimension(:,:,:,:),allocatable::xb,yb,zb
      integer,dimension(:,:,:,:),allocatable::attrb
      real(kind=8)::pi,alpha,len
      integer ni,nj,nb,i,j,k,n
      integer dim,nib,nbj,nbm,jj,nn,factor

      pi=4.d0*atan(1.d0)

      print*,' Give spanwise length : '
      read*,len

c--------allocate 2d vertices
      open(unit=10,file='grid.x2d')
      read(10,*)ni,nb
      ni=ni+1
      allocate(x2d(ni,ni,nb),z2d(ni,ni,nb))
      allocate(attr2d(ni,ni,nb))
      do n=1,nb
      do k=1,ni
      do i=1,ni
        read(10,*)attr2d(i,k,n),x2d(i,k,n),z2d(i,k,n)
      enddo;enddo
      enddo
      close(10)

c--------allocate 3d vertices
      print*,' Give number of vertices in j-direction : '
      read*,nj
      allocate(x(ni,nj,ni,nb),y(ni,nj,ni,nb),z(ni,nj,ni,nb)
     &     ,attr(ni,nj,ni,nb))

c--------compute 3d vertice location
      do n=1,nb
      do k=1,ni
      do j=1,nj
      do i=1,ni
        x(i,j,k,n)=x2d(i,k,n)
        y(i,j,k,n)=len*real(j-1)/real(nj-1)
        z(i,j,k,n)=z2d(i,k,n)
        attr(i,j,k,n)=attr2d(i,k,n)
      enddo;enddo;enddo
      enddo

      j=1
      do n=1,nb
      do k=1,ni
      do i=1,ni
        if(attr(i,j,k,n).ne.1)cycle
        attr(i,nj,k,n)=601
        attr(i, 1,k,n)=601
      enddo;enddo;enddo

C-------subdivide j direction
      print*,' Spanwise dimension : ',nj-1
      print*,' Assuming spanwise block size : ',ni-1
      dim=ni-1
      nbj=nj/dim
      print*,' Number of blocks in j-direction : ',nbj
      nbm=nb*nbj
      nib=ni+2

      allocate(xb(nib,nib,nib,nbm)
     &        ,yb(nib,nib,nib,nbm)
     &        ,zb(nib,nib,nib,nbm)
     &     ,attrb(nib,nib,nib,nbm))

      xb=0.d0;yb=0.d0;zb=0.d0;attrb=0.d0
      
      do jj=1,nbj
      do n=1,nb
      do k=2,ni+1
      do j=2,ni+1
      do i=2,ni+1
          nn=n+(jj-1)*nb
          xb(i,j,k,nn)=x(i-1,j-1+(jj-1)*(ni-1),k-1,n)
          yb(i,j,k,nn)=y(i-1,j-1+(jj-1)*(ni-1),k-1,n)
          zb(i,j,k,nn)=z(i-1,j-1+(jj-1)*(ni-1),k-1,n)
       attrb(i,j,k,nn)=
     &              attr(i-1,j-1+(jj-1)*(ni-1),k-1,n)
      enddo;enddo;enddo
      enddo
      enddo


      call UnfGridWrite(10,nib,nbm,xb,yb,zb,attrb)
 
      stop
  666 write(*,*) ' error: too few data in input file (grid.dat)'
      end


      subroutine UnfGridWrite(unitnr,ni,nblock,x,y,z,attr)
C=======================================================================
C     routine for writing unformatted x3d file
C     remember that the grid must positions in the following region 
C
C        x(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C        y(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C        z(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C     attr(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C
C     the remaining spaces are intended for ghost cells
C=======================================================================
      implicit none
      integer  unitnr,ni,nblock
      real(kind=8),dimension(ni,ni,ni,nblock)::x,y,z 
      integer,dimension(ni,ni,ni,nblock)::attr

C     Code to be used to add ghost cells before call UnfGridWrite
c     do n=1,nblock
c     do k=2,ni-1
c     do j=2,ni-1
c     do i=2,ni-1
c       xg   (i,j,k,n)=x   (i-1,j-1,k-1,n)
c       yg   (i,j,k,n)=y   (i-1,j-1,k-1,n)
c       zg   (i,j,k,n)=z   (i-1,j-1,k-1,n)
c       attrg(i,j,k,n)=attr(i-1,j-1,k-1,n)
c     enddo;enddo;enddo
c     enddo

      open(unit=unitnr,file='grid.x3dunf',form='unformatted')
      write(unitnr)ni-3
      write(unitnr)nblock
      print*, ' bsize, nblock ',ni-3,nblock
      call WriteBlockInteger(unitnr,ni**3,nblock,attr)
      call WriteBlockReal   (unitnr,ni**3,nblock,x   )
      call WriteBlockReal   (unitnr,ni**3,nblock,y   )
      call WriteBlockReal   (unitnr,ni**3,nblock,z   )

      close(unitnr)
      return
      end

      subroutine WriteBlockReal(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      real(kind=8),dimension(block,nblock)::x
      real(kind=8),dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine WriteBlockReal

      subroutine WriteBlockInteger(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      integer,dimension(block,nblock)::x
      integer,dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine WriteBlockInteger

