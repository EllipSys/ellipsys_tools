      module params
C========================================= Real precision definition ===
      integer,parameter::idp=kind(1d0)
C============================================= filenames ===============
      character(len=128) project      ! project, 1'st part of filenames
      integer namelen                 ! length of project name
C============================================= main parameters =========
      integer bsizes(5),bsize         ! bsize      of MG levels
      integer nis(5),ni               ! ni         of MG levels
      integer MGlev                   ! # of MG levels in application
      integer:: MGcoarse=5            ! # of MG levels in inner loop
      integer nblock                  ! # of blocks in application
      integer ncomp                   ! # of BC components
      integer:: InterpolationOrder=4  ! interpolation to cell faces
C============================================= frame of reference ======
      logical:: Polar       = .false.  ! F: cartesian, T: polar
      logical:: PolarMetrics= .false.  ! F: cartesian, T: polar
C============================================= parallel parameters =====
      integer:: nprocs     = 1        ! # of processors
      integer:: MyProcNum  = 1        ! present processors number
      integer:: BPPglobal             ! # of blocks per processor (max)
      integer:: BPP                   ! # of blocks on present processor
C=======================================================================
      integer,dimension(:),allocatable::BPPs ! # of blocks on processors
C=======================================================================
      end module

      module restart
C-----------------------------------------------------------------------
c     restart module
C-----------------------------------------------------------------------
      use params
      implicit none
      integer::ivar,ivarb
      real(kind=idp)::rvar,rvarb
      logical::lvar,lvarb
      character(len=20)::cvar,cvarb
      integer          ,dimension(:),allocatable::iarray,iarrayb
      real(kind=idp)     ,dimension(:),allocatable::rarray,rarrayb
      logical          ,dimension(:),allocatable::larray,larrayb
      character(len=20),dimension(:),allocatable::carray,carrayb
C-----------------------------------------------------------------------
      end module

      program readrst
C=======================================================================
C     A small utility for reading the new restart file
C     
C     Author: Niels N. Sorensen
C=======================================================================
      use restart
      implicit none
      character(len=20)::Label,Type,Action,Labelb,Typeb,Actionb
      integer::size,sizeb
      logical Recognized,MoreFields
      integer::unita=10,unitb=20

c-------read top of grid.arst
      open(unit=unita,file='grid.arst',form='unformatted')
      read(unita)Label(1:20);print*,' Label ',label(1:20)
      read(unita)Label(1:20);print*,' Label ',label(1:20)
c-------read top of grid.brst
      open(unit=unitb,file='grid.brst',form='unformatted')
      read(unitb)Label(1:20);print*,' Label ',label(1:20)
      read(unitb)Label(1:20);print*,' Label ',label(1:20)

      if(label.ne.'New Restart Version ')then
        print*,' Not New Restart Format '
        print*,' Can not be read '
      endif

      do 
c-------read headder
      read(unita)Label(1:20)
      read(unita)Type(1:20)
      read(unita)Action(1:20)
      read(unita)size
      print*,' A ',Label,Type,Action,size

      read(unitb)Labelb(1:20)
      read(unitb)Typeb(1:20)
      read(unitb)Actionb(1:20)
      read(unitb)sizeb
      print*,' B ',Label,Type,Action,size

      if(Label(1:20).ne.Labelb(1:20).or.
     &   Type(1:20) .ne.Typeb(1:20).or.
c    &   Action(1:20).ne.Actionb(1:20).or
     &   size.ne.sizeb)then
        print*,' Contents not identical : '
        print*,Label,Type,Action,size
        stop
      endif


      select case(Action(1:len_trim(Action)))
c-------Bcast
        case('BCAST')
        select case(Type(1:len_trim(type)))
        case('CHARACTER')
          if(size.eq.1)then
            call ReadBcastScalarCharacter(unita,20,CVAR ,MoreFields)
            call ReadBcastScalarCharacter(unitb,20,CVARB,MoreFields)
            if(.not.(CVAR.eq.CVARB))then
              print*,' Fields not equal '
              stop
            endif
          else
            if(allocated(CARRAY))deallocate(CARRAY,CARRAYB)
            allocate(CARRAY(size),CARRAYB(size))
            call ReadBcastArrayCharacterUNF(unita,20,size,CARRAY)
            call ReadBcastArrayCharacterUNF(unitb,20,size,CARRAYB)
            if(.not.all(CARRAY.eq.CARRAYB))then
              print*,' Charaxter Array not equal '
              stop
            endif
          endif
        case('INTEGER')
          if(size.eq.1)then
            call ReadBcastScalarInteger(unita,IVAR)
            call ReadBcastScalarInteger(unitb,IVARB)
            if(.not.(IVAR.eq.IVARB))then
              print*,' Integer not Equal ',IVAR,IVARB
              stop
            endif
          else
            if(allocated(IARRAY))deallocate(IARRAY,IARRAYB)
            allocate(IARRAY(size),IARRAYB(size))
            call ReadBcastArrayInteger (unita,size,IARRAY)
            call ReadBcastArrayInteger (unitb,size,IARRAYB)
            if(sum(IARRAY).ne.sum(IARRAYB))then
              print*,' Integer Array not Equal '
              stop
            endif
          endif
        case('REAL')
          if(size.eq.1)then
            call ReadBcastScalarReal   (unita,RVAR)
            call ReadBcastScalarReal   (unitb,RVARB)
            if(.not.RVAR.eq.RVARB)then
              print*,'Real Equal ',RVAR,RVARB
              stop
            endif
          else
            if(allocated(RARRAY))deallocate(RARRAY,RARRAYB)
            allocate(RARRAY(size),RARRAYB(size))
            call ReadBcastArrayReal   (unita,size,RARRAY)
            call ReadBcastArrayReal   (unitb,size,RARRAYB)
            if(sum(RARRAY).ne.sum(RARRAYB))then
              print*,' Not Equal '
              stop
            endif
          endif
        case('LOGICAL')
          if(size.eq.1)then
            call ReadBcastScalarLogical(unita,LVAR)
            call ReadBcastScalarLogical(unitb,LVARB)
            if(LVAR.neqv.LVARB)then
              print*,' Not Equal '
              stop
            endif
          else
            if(allocated(LARRAY))deallocate(LARRAY,LARRAYB)
            allocate(LARRAY(size),LARRAYB(size))
            call ReadBcastArrayLogical (unita,LARRAY)
            call ReadBcastArrayLogical (unitb,LARRAYB)
            if(sum(IARRAY).ne.sum(IARRAYB))then
              print*,' Not Equal '
              stop
            endif
          endif
        end select
      case('SCATTER')
c-------Scatter
c-------Scatter
        select case(Type(1:len_trim(type)))
        case('INTEGER')
          if(allocated(IARRAY))deallocate(IARRAY,IARRAYB)
          allocate(IARRAY(size*BPP),IARRAYB(size*BPP))
          call ReadScatterInteger(unita,size,IARRAY)
          call ReadScatterInteger(unitb,size,IARRAYB)
          if(sum(IARRAY)-sum(IARRAYB).ne.0.d0)then
            print*,' Not Equal '
            print*,IARRAY
            print*,IARRAYB
            stop
          endif
        case('REAL')
          if(allocated(RARRAY))deallocate(RARRAY,RARRAYB)
          allocate(RARRAY(size*BPP),RARRAYB(size*BPP))
          call ReadScatterReal(unita,size,RARRAY)
          call ReadScatterReal(unitb,size,RARRAYB)
          if(sum(RARRAY)-sum(RARRAYB).ne.0.d0)then
            print*,' sum array a : ',sum(RARRAY)
            print*,' sum array b : ',sum(RARRAYB)
            print*,' sum array   : ',sum(RARRAYB)-sum(RARRAY)
            print*,' Not Equal '
            stop
          endif
        case('LOGICAL')
          if(allocated(LARRAY))deallocate(LARRAY,LARRAYB)
          allocate(LARRAY(size*BPP),LARRAYB(size*BPP))
          call ReadScatterLogical(unita,size,LARRAY)
          call ReadScatterLogical(unitb,size,LARRAYB)
          if(.not.all(CARRAY.eq.CARRAYB))then
            print*,' Not Equal '
            stop
          endif
        end select
      case('STOP')
        close(unita)
        close(unitb)
        exit
      end select

      select case(label)
        case('nblock              ')
          BPP=IVAR
        case('bsize               ')
          bsize=IVAR
C----------Insert action here

C----------
      end select
      enddo

c-------deallocate work arrays
      if(allocated(IARRAY))deallocate(IARRAY)
      if(allocated(RARRAY))deallocate(RARRAY)
      if(allocated(LARRAY))deallocate(LARRAY)
      if(allocated(CARRAY))deallocate(CARRAY)


      print*,' #### FINISHED READING ########'
      print*,' NBLOCK  : ',BPP
      print*,' BSIZE   : ',bsize
      stop
      end

c>    @ingroup platform
      subroutine ReadScatterReal(unit,block,x)
      use params
      implicit none
      integer unit,i,n,dn,block
      real(kind=idp),dimension(block*BPP)::x
      real(kind=idp),dimension(:),allocatable::xblock
C-----------------------------------------------------------------------
          allocate(xblock(block))
C--------------------------------- Read ROOTs own blocks ---------------
            do n=1,BPP;dn=(n-1)*block
              read(unit)xblock
              do i=1,block;x(i+dn)=xblock(i);end do
            end do
C-----------------------------------------------------------------------
          deallocate(xblock)
C-----------------------------------------------------------------------
      end subroutine ReadScatterReal

c>    @ingroup platform
      subroutine ReadScatterInteger(unit,block,x)
      use params
      implicit none
      integer unit,i,n,dn,block
      integer,dimension(block*BPP)::x
      integer,dimension(:),allocatable::xblock
C-----------------------------------------------------------------------
          allocate(xblock(block))
C--------------------------------- Read ROOTs own blocks ---------------
            do n=1,BPP;dn=(n-1)*block
              read(unit)xblock
              do i=1,block;x(i+dn)=xblock(i);end do
            end do
C-----------------------------------------------------------------------
          deallocate(xblock)
C-----------------------------------------------------------------------
      end subroutine ReadScatterInteger

c>    @ingroup platform
      subroutine ReadScatterLogical(unit,block,x)
      use params
      implicit none
      integer unit,i,n,dn,block
      logical,dimension(block*BPP)::x
      logical,dimension(:),allocatable::xblock
C-----------------------------------------------------------------------
          allocate(xblock(block))
C--------------------------------- Read ROOTs own blocks ---------------
            do n=1,BPP;dn=(n-1)*block
              read(unit)xblock
              do i=1,block;x(i+dn)=xblock(i);end do
            end do
C-----------------------------------------------------------------------
          deallocate(xblock)
C-----------------------------------------------------------------------
      end subroutine ReadScatterLogical

      subroutine ReadBcastScalarCharacter(unit,length,x,lexist)
      use params
      implicit none
      integer unit,length
      logical lexist
      character(len=length) x
C-----------------------------------------------------------------------
      if(MyProcNum.eq.1) then
        lexist=.false.
        read(unit,end=1)x
        lexist=.true.
    1   continue
      end if
C-----------------------------------------------------------------------
      if(lexist)print*,'Restart label ',x
C-----------------------------------------------------------------------
      end

c>    @ingroup platform
      subroutine ReadBcastScalarReal(unit,x)
      use params
      implicit none
      integer unit
      real(kind=idp) x
C-----------------------------------------------------------------------
      read(unit)x
C-----------------------------------------------------------------------
      end

c>    @ingroup platform
      subroutine ReadBcastScalarInteger(unit,x)
      use params
      implicit none
      integer unit
      integer x
C-----------------------------------------------------------------------
      read(unit)x
C-----------------------------------------------------------------------
      end

c>    @ingroup platform
      subroutine ReadBcastScalarLogical(unit,x)
      use params
      implicit none
      integer unit
      logical x
C-----------------------------------------------------------------------
      read(unit)x
C-----------------------------------------------------------------------
      end

c>    @ingroup platform
      subroutine BcastScalarLogical(x)
      implicit none
      logical x
      end

c>    @ingroup platform
      subroutine ReadBcastArrayCharacter(unit,count,x)
      use params
      implicit none
      integer unit,count,i
      character(len=2048) x(count)
C-----------------------------------------------------------------------
      do i=1,count
      read(unit,'(a2048)')x(i)
      end do
C-----------------------------------------------------------------------
      end

c>    @ingroup platform
      subroutine ReadBcastArrayCharacterUNF(unit,length,count,x)
      use params
      implicit none
      integer unit,length,count
      character(len=length) x(count)
C-----------------------------------------------------------------------
      read(unit)x
C-----------------------------------------------------------------------
      end

c>    @ingroup platform
      subroutine ReadBcastArrayReal(unit,count,x)
      use params
      implicit none
      integer unit,count
      real(kind=idp) x(count)
C-----------------------------------------------------------------------
      read(unit)x
C-----------------------------------------------------------------------
      end

c>    @ingroup platform
      subroutine ReadBcastArrayInteger(unit,count,x)
      use params
      implicit none
      integer unit,count
      integer x(count)
C-----------------------------------------------------------------------
      read(unit)x
C-----------------------------------------------------------------------
      end

c>    @ingroup platform
      subroutine ReadBcastArrayLogical(unit,count,x)
      use params
      implicit none
      integer unit,count
      logical x(count)
C-----------------------------------------------------------------------
      read(unit)x
C-----------------------------------------------------------------------
      end
