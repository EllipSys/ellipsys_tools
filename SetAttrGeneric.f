c     $Rev:: 8           $: Revision of last commit
c     $Author:: nsqr     $: Author of last commit
c     $Date:: 2011-08-02#$: Date of last commit
      program MeshReadWrite
c-----------------------------------------------------------------------
c     A small piece of code for setting the bc's for a X3D mesh
c     based on the coordinats of the cell face center and attribute value
c
c     For a generic mesh set all attr=101 
c     2) Run Basis3d which resets all internal to attr=1
c     3) Run X3D2x3d which extracts the finest level from the X3D file
c     4) Run SetAttr selecting external faces based on 101 attr
c
c     bc.dat file most hold information of the following type
c     coordinate gt/lt  value  AttrSelect BC-Attr
c
c     x   gt  10  101 201
c     or
c     y   lt  -12 101 401
c
c     Alternatively use the box command
c       
c     box x_min y_min z_min x_max y_max z_max AttSelect BC-Attr
c
c     Cylinder
c
c     xcyl radius  xmin xmax att_select att_set
c
c     ycyl radius  ymin ymax att_select att_set
c
c     zcyl radius  zmin zmax att_select att_set
c
c     Author Niels N. Soerensen
c-----------------------------------------------------------------------
      implicit none
      logical::statex=.false.
      integer::ni
      integer,parameter::idp8=8,ncomp=4
      real(kind=8),dimension(:,:,:,:),allocatable::x,y,z,r
      integer,dimension(:,:,:,:),allocatable::attr
      real(kind=8)::dummy
      character*11 fnamd,fnamh
      character*128 fieldname(ncomp)
      integer::idummy
      integer::i,j,k,n,nc,nvar,fileform
      integer::bsize,nblock,b1,iblock
      integer yesno
      real(kind=8)::clim
      real(kind=8)::xmin,ymin,zmin,xmax,ymax,zmax
      integer::aset,aselect
      integer,parameter::mword=40
      integer nrchar(mword),nrword
      character(len=256)InputLine
      character(len=40),dimension(mword)::words
      logical::lrecogn=.false.

      inquire(file='grid.x3dunf',exist=statex)
      if(statex)then
        print*,' READING UNFORMATTED GRID FILE'
        open(unit=20,file='grid.x3dunf',form='unformatted')
        read(20)bsize
        read(20)nblock
        print*,bsize,nblock
        ni=bsize+3
        allocate(x(ni,ni,ni,nblock)
     &          ,y(ni,ni,ni,nblock)
     &          ,z(ni,ni,ni,nblock)
     &          ,r(ni,ni,ni,nblock)
     &       ,attr(ni,ni,ni,nblock))
        x=0.d0;y=0.d0;z=0.d0;attr=0
        print*,' Reading Attributs : '
        call ReadBlockInteger(20,ni**3,nblock,attr)
        print*,' Reading x-coord : '
        call ReadBlockReal   (20,ni**3,nblock,x)
        print*,' Reading y-coord : '
        call ReadBlockReal   (20,ni**3,nblock,y)
        print*,' Reading z-coord : '
        call ReadBlockReal   (20,ni**3,nblock,z)
        print*,' Finished Reading : '
        close(10)
      else
        inquire(file='grid.x3d',exist=statex)
        if(.not.statex)then
          print*,' No grid file, stopping '
          stop
        endif
        print*,' READING FORMATTED GRID FILE'
        open(unit=20,file='grid.x3d')
        read(20,*)bsize,nblock
        ni=bsize+3
        allocate(x(ni,ni,ni,nblock)
     &          ,y(ni,ni,ni,nblock)
     &          ,z(ni,ni,ni,nblock)
     &          ,r(ni,ni,ni,nblock)
     &       ,attr(ni,ni,ni,nblock))
        x=0.d0;y=0.d0;z=0.d0;r=0.d0;attr=0
        print*,bsize,nblock
        do n=1,nblock
        do k=2,bsize+2
        do j=2,bsize+2
        do i=2,bsize+2
          read(20,*)attr(i,j,k,n)  
     &            ,x(i,j,k,n)
     &            ,y(i,j,k,n)
     &            ,z(i,j,k,n)
        enddo;enddo;enddo;enddo
      endif
c-------compute radius
      do n=1,nblock
      do k=2,bsize+2
      do j=2,bsize+2
      do i=2,bsize+2
        r(i,j,k,n)=sqrt(x(i,j,k,n)**2
     &                 +y(i,j,k,n)**2
     &                 +z(i,j,k,n)**2)
      enddo;enddo;enddo;enddo

c-----------------------------------------------------------------------
c     Boundary conditions processing
c-----------------------------------------------------------------------
      print*,' Processing BC file '
      open(unit=10,file='bc.dat')
      do
      read(10,'(a256)',end=100)InputLine
      call cmdpart(InputLine,mword,nrchar,nrword,words)
C------------------------------------- Skip blank lines and comments ---
      if(nrword.le.1) then
        cycle
C---------------------------------- Skip rest of file if stop appears --
      elseif(words(1)(1:4).eq.'stop'.or.words(1)(1:4).eq.'STOP') then
      exit
c----------------------------- command InputLine interpreter -----------
      else
        lrecogn=.false.
        select case(words(1)(1:nrchar(1)))
C------------------------------ Keyword not recognized -----------------
        case default
        lrecogn=.false.
C------------------------------ determine meshtype ---------------------
        case('x')
          if(nrword.ge.5)then
            read(words(3),*)clim
            read(words(4),*)aselect
            read(words(5),*)aset
            select case(words(2)(1:nrchar(2)))
              case('gt')
                call setBC(ni,nblock,bsize, 1,clim,aselect,aset,x,attr)
              case('lt')
                call setBC(ni,nblock,bsize,-1,clim,aselect,aset,x,attr)
            end select
          endif
        case('y')
          if(nrword.ge.5)then
            read(words(3),*)clim
            read(words(4),*)aselect
            read(words(5),*)aset
            select case(words(2)(1:nrchar(2)))
              case('gt')
                call setBC(ni,nblock,bsize, 1,clim,aselect,aset,y,attr)
              case('lt')
                call setBC(ni,nblock,bsize,-1,clim,aselect,aset,y,attr)
            end select
          endif
        case('z')
          if(nrword.ge.5)then
            read(words(3),*)clim
            read(words(4),*)aselect
            read(words(5),*)aset
            select case(words(2)(1:nrchar(2)))
              case('gt')
                call setBC(ni,nblock,bsize, 1,clim,aselect,aset,z,attr)
              case('lt')
                call setBC(ni,nblock,bsize,-1,clim,aselect,aset,z,attr)
            end select
          endif
        case('r')
          if(nrword.ge.5)then
            read(words(3),*)clim
            read(words(4),*)aselect
            read(words(5),*)aset
            select case(words(2)(1:nrchar(2)))
              case('gt')
                call setBC(ni,nblock,bsize, 1,clim,aselect,aset,r,attr)
              case('lt')
                call setBC(ni,nblock,bsize,-1,clim,aselect,aset,r,attr)
            end select
          endif
        case('box')
          if(nrword.ge.9)then
            read(words(2),*)xmin
            read(words(3),*)ymin
            read(words(4),*)zmin
            read(words(5),*)xmax
            read(words(6),*)ymax
            read(words(7),*)zmax
            read(words(8),*)aselect
            read(words(9),*)aset
            call setBCBox(ni,nblock,bsize,aselect,aset
     &                                   ,xmin,ymin,zmin
     &                                   ,xmax,ymax,zmax
     &                                   ,x   ,y   ,z   ,attr)
          endif
        case('xcyl')
          read(words(2),*)clim
          read(words(3),*)xmin
          read(words(4),*)xmax
          read(words(5),*)aselect
          read(words(6),*)aset
            call setBCCYL(ni,nblock,bsize,aselect,aset
     &                                   ,clim,xmin,xmax
     &                                   ,y   ,z   ,x   ,attr)
        case('ycyl')
          read(words(2),*)clim
          read(words(3),*)ymin
          read(words(4),*)ymax
          read(words(5),*)aselect
          read(words(6),*)aset
            call setBCCYL(ni,nblock,bsize,aselect,aset
     &                                   ,clim,ymin,ymax
     &                                   ,x   ,z   ,y   ,attr)
        case('zcyl')
          read(words(2),*)clim
          read(words(3),*)zmin
          read(words(4),*)zmax
          read(words(5),*)aselect
          read(words(6),*)aset
            call setBCCYL(ni,nblock,bsize,aselect,aset
     &                                   ,clim,zmin,zmax
     &                                   ,x   ,y   ,z   ,attr)
        end select
      endif
      enddo

c-------write unformatted grid -----------------------------------------
  100 call UnfGridWrite(20,ni,nblock,x,y,z,attr)

      stop
   30 format('')
   31 format('VARIABLES = ',15(A20,','))
   32 format('ZONE I=',i2,', J=',i2,', K=',i2,' F=BLOCK')
      end

      subroutine ReadBlockReal(unit,block,nblock,x)
      implicit none
      integer unit,nblock,block,i,n
      real(kind=8),dimension(block,nblock)::x
      real(kind=8),dimension(block)       ::xblock
        do n=1,nblock
          read(unit)xblock
          do i=1,block
            x(i,n)=xblock(i)
          end do
        end do
      end subroutine ReadBlockReal

      subroutine ReadBlockInteger(unit,block,nblock,x)
      implicit none
      integer unit,nblock,block,i,n
      integer,dimension(block,nblock)::x
      integer,dimension(block)       ::xblock
        do n=1,nblock
          read(unit)xblock
          do i=1,block
            x(i,n)=xblock(i)
          end do
        end do
      end subroutine ReadBlockInteger


      subroutine setBC(ni,nblock,bsize,modus,coordval,aselect,attset
     &                ,coord,attr)
C=======================================================================
      implicit none
      integer::ni,nblock,bsize,modus,aselect,attset
      real(kind=8)::coordval
      real(kind=8),dimension(ni,ni,ni,nblock)::coord
      integer,dimension(ni,ni,ni,nblock)::attr

      call setBCface(ni,nblock,bsize,modus,coordval,aselect,attset
     &                ,coord,attr)
c     call setBCcell(ni,nblock,bsize,modus,coordval,aselect,attset
c    &                ,coord,attr)

      return
      end

      subroutine setBCface(ni,nblock,bsize,modus,coordval,aselect,attset
     &                ,coord,attr)
C=======================================================================
c     routine for setting boundary conditions based on attribute and 
c     coordinate value
c     the routine checks all boundary faces of all blocks
c     if attr is equal to aselect
c     and coord is larger than coordval
c     the attribute is changed to attset
c     if modus is -1 it checks for less than coordval instead
C=======================================================================
      implicit none
      integer::ni,nblock,bsize,modus,attset,aselect
      integer::i,j,k,n,kmin,cells
      real(kind=8)::coordval
      real(kind=8),dimension(ni,ni,ni,nblock)::coord
      integer,dimension(ni,ni,ni,nblock)::attr

      do n=1,nblock
c---------west
        if(attr(2      ,bsize/2,bsize/2,n).eq.aselect)then
        if(modus*coord(2,bsize/2,bsize/2,n).gt.modus*coordval)then
          attr(2,2:bsize+2,2:bsize+2,n)=attset
          print*,' block : ',n,' face : ',1,' attr : ',attset
        endif
        endif
c---------east
        if(attr(bsize+2,bsize/2,bsize/2,n).eq.aselect)then
        if(modus*coord(bsize+2,bsize/2,bsize/2,n)
     &                          .gt.modus*coordval)then
          attr(bsize+2,2:bsize+2,2:bsize+2,n)=attset
          print*,' block : ',n,' face : ',2,' attr : ',attset
        endif
        endif
c---------south
        if(attr(bsize/2,2,bsize/2,n).eq.aselect)then
        if(modus*coord(bsize/2,2,bsize/2,n)
     &                          .gt.modus*coordval)then
          attr(2:bsize+2,2,2:bsize+2,n)=attset
          print*,' block : ',n,' face : ',3,' attr : ',attset
        endif
        endif
c---------north
        if(attr(bsize/2,bsize+2,bsize/2,n).eq.aselect)then
        if(modus*coord(bsize/2,bsize+2,bsize/2,n)
     &                          .gt.modus*coordval)then
          attr(2:bsize+2,bsize+2,2:bsize+2,n)=attset
          print*,' block : ',n,' face : ',4,' attr : ',attset
        endif
        endif
c---------bottom
        if(attr(bsize/2,bsize/2,2,n).eq.aselect)then
        if(modus*coord(bsize/2,bsize/2,2,n)
     &                          .gt.modus*coordval)then
          attr(2:bsize+2,2:bsize+2,2,n)=attset
          print*,' block : ',n,' face : ',5,' attr : ',attset
        endif
        endif
c---------top
        if(attr(bsize/2,bsize/2,bsize+2,n).eq.aselect)then
        if(modus*coord(bsize/2,bsize/2,bsize+2,n)
     &                          .gt.modus*coordval)then
          attr(2:bsize+2,2:bsize+2,bsize+2,n)=attset
          print*,' block : ',n,' face : ',6,' attr : ',attset
        endif
        endif
      enddo

      return
      end

      subroutine setBCBox(ni,nblock,bsize,aselect,attset
     &                   ,xmin,ymin,zmin
     &                   ,xmax,ymax,zmax
     &                   ,x   ,y   ,z   ,attr)
C=======================================================================
      implicit none
      integer::ni,nblock,bsize,modus,attset,aselect
      real(kind=8)::xmin,ymin,zmin,xmax,ymax,zmax,xf,yf,zf
      real(kind=8),dimension(ni,ni,ni,nblock)::x,y,z
      integer,dimension(ni,ni,ni,nblock)::attr
      integer i,j,k,f,n,bsizeh,attrf

      bsizeh=bsize/2
      do n=1,nblock
c---------west
        xf=x(2,bsizeh,bsizeh,n)
        yf=y(2,bsizeh,bsizeh,n)
        zf=z(2,bsizeh,bsizeh,n)
        attrf=attr(2,bsizeh,bsizeh,n)
        if((xf.gt.xmin.and.xf.lt.xmax).and.
     &     (yf.gt.ymin.and.yf.lt.ymax).and.
     &     (zf.gt.zmin.and.zf.lt.zmax).and.
     &      attrf.eq.aselect)then
          attr(2,2:bsize+2,2:bsize+2,n)=attset
          print*,' block : ',n,' face : ',1,' attr : ',attset
        endif
c---------east
        xf=x(bsize+2,bsizeh,bsizeh,n)
        yf=y(bsize+2,bsizeh,bsizeh,n)
        zf=z(bsize+2,bsizeh,bsizeh,n)
        attrf=attr(bsize+2,bsizeh,bsizeh,n)
        if((xf.gt.xmin.and.xf.lt.xmax).and.
     &     (yf.gt.ymin.and.yf.lt.ymax).and.
     &     (zf.gt.zmin.and.zf.lt.zmax).and.
     &      attrf.eq.aselect)then
          attr(bsize+2,2:bsize+2,2:bsize+2,n)=attset
          print*,' block : ',n,' face : ',2,' attr : ',attset
        endif
c---------south
        xf=x(bsizeh,2,bsizeh,n)
        yf=y(bsizeh,2,bsizeh,n)
        zf=z(bsizeh,2,bsizeh,n)
        attrf=attr(bsizeh,2,bsizeh,n)
        if((xf.gt.xmin.and.xf.lt.xmax).and.
     &     (yf.gt.ymin.and.yf.lt.ymax).and.
     &     (zf.gt.zmin.and.zf.lt.zmax).and.
     &      attrf.eq.aselect)then
          attr(2:bsize+2,2,2:bsize+2,n)=attset
          print*,' block : ',n,' face : ',3,' attr : ',attset
        endif
c---------north
        xf=x(bsizeh,bsize+2,bsizeh,n)
        yf=y(bsizeh,bsize+2,bsizeh,n)
        zf=z(bsizeh,bsize+2,bsizeh,n)
        attrf=attr(bsizeh,bsize+2,bsizeh,n)
        if((xf.gt.xmin.and.xf.lt.xmax).and.
     &     (yf.gt.ymin.and.yf.lt.ymax).and.
     &     (zf.gt.zmin.and.zf.lt.zmax).and.
     &      attrf.eq.aselect)then
          attr(2:bsize+2,bsize+2,2:bsize+2,n)=attset
          print*,' block : ',n,' face : ',4,' attr : ',attset
        endif
c---------bottom
        xf=x(bsizeh,bsizeh,2,n)
        yf=y(bsizeh,bsizeh,2,n)
        zf=z(bsizeh,bsizeh,2,n)
        attrf=attr(bsizeh,bsizeh,2,n)
        if((xf.gt.xmin.and.xf.lt.xmax).and.
     &     (yf.gt.ymin.and.yf.lt.ymax).and.
     &     (zf.gt.zmin.and.zf.lt.zmax).and.
     &      attrf.eq.aselect)then
          attr(2:bsize+2,2:bsize+2,2,n)=attset
          print*,' block : ',n,' face : ',5,' attr : ',attset
        endif
c---------top
        xf=x(bsizeh,bsizeh,bsize+2,n)
        yf=y(bsizeh,bsizeh,bsize+2,n)
        zf=z(bsizeh,bsizeh,bsize+2,n)
        attrf=attr(bsizeh,bsizeh,bsize+2,n)
        if((xf.gt.xmin.and.xf.lt.xmax).and.
     &     (yf.gt.ymin.and.yf.lt.ymax).and.
     &     (zf.gt.zmin.and.zf.lt.zmax).and.
     &      attrf.eq.aselect)then
          attr(2:bsize+2,2:bsize+2,bsize+2,n)=attset
          print*,' block : ',n,' face : ',6,' attr : ',attset
        endif
      enddo
      return
      end

      subroutine setBCCYL(ni,nblock,bsize,aselect,attset
     &                   ,rmax,zmin,zmax
     &                   ,x   ,y   ,z   ,attr)
C=======================================================================
C      
C=======================================================================
      implicit none
      integer::ni,nblock,bsize,modus,attset,aselect
      real(kind=8)::rmax,zmin,zmax,xf,yf,zf,radius
      real(kind=8),dimension(ni,ni,ni,nblock)::x,y,z
      integer,dimension(ni,ni,ni,nblock)::attr
      integer i,j,k,f,n,bsizeh,attrf

      bsizeh=bsize/2
      do n=1,nblock
c---------west
        xf=x(2,bsizeh,bsizeh,n)
        yf=y(2,bsizeh,bsizeh,n)
        zf=z(2,bsizeh,bsizeh,n)
        attrf=attr(2,bsizeh,bsizeh,n)
        radius=sqrt(xf**2+yf**2)
        if((radius.lt.rmax).and.
     &     (zf.gt.zmin.and.zf.lt.zmax).and.
     &      attrf.eq.aselect)then
          attr(2,2:bsize+2,2:bsize+2,n)=attset
          print*,' block : ',n,' face : ',1,' attr : ',attset
        endif
c---------east
        xf=x(bsize+2,bsizeh,bsizeh,n)
        yf=y(bsize+2,bsizeh,bsizeh,n)
        zf=z(bsize+2,bsizeh,bsizeh,n)
        attrf=attr(bsize+2,bsizeh,bsizeh,n)
        radius=sqrt(xf**2+yf**2)
        if((radius.lt.rmax).and.
     &     (zf.gt.zmin.and.zf.lt.zmax).and.
     &      attrf.eq.aselect)then
          attr(bsize+2,2:bsize+2,2:bsize+2,n)=attset
          print*,' block : ',n,' face : ',2,' attr : ',attset
        endif
c---------south
        xf=x(bsizeh,2,bsizeh,n)
        yf=y(bsizeh,2,bsizeh,n)
        zf=z(bsizeh,2,bsizeh,n)
        attrf=attr(bsizeh,2,bsizeh,n)
        radius=sqrt(xf**2+yf**2)
        if((radius.lt.rmax).and.
     &     (zf.gt.zmin.and.zf.lt.zmax).and.
     &      attrf.eq.aselect)then
          attr(2:bsize+2,2,2:bsize+2,n)=attset
          print*,' block : ',n,' face : ',3,' attr : ',attset
        endif
c---------north
        xf=x(bsizeh,bsize+2,bsizeh,n)
        yf=y(bsizeh,bsize+2,bsizeh,n)
        zf=z(bsizeh,bsize+2,bsizeh,n)
        attrf=attr(bsizeh,bsize+2,bsizeh,n)
        radius=sqrt(xf**2+yf**2)
        if((radius.lt.rmax).and.
     &     (zf.gt.zmin.and.zf.lt.zmax).and.
     &      attrf.eq.aselect)then
          attr(2:bsize+2,bsize+2,2:bsize+2,n)=attset
          print*,' block : ',n,' face : ',4,' attr : ',attset
        endif
c---------bottom
        xf=x(bsizeh,bsizeh,2,n)
        yf=y(bsizeh,bsizeh,2,n)
        zf=z(bsizeh,bsizeh,2,n)
        attrf=attr(bsizeh,bsizeh,2,n)
        radius=sqrt(xf**2+yf**2)
        if((radius.lt.rmax).and.
     &     (zf.gt.zmin.and.zf.lt.zmax).and.
     &      attrf.eq.aselect)then
          attr(2:bsize+2,2:bsize+2,2,n)=attset
          print*,' block : ',n,' face : ',5,' attr : ',attset
        endif
c---------top
        xf=x(bsizeh,bsizeh,bsize+2,n)
        yf=y(bsizeh,bsizeh,bsize+2,n)
        zf=z(bsizeh,bsizeh,bsize+2,n)
        attrf=attr(bsizeh,bsizeh,bsize+2,n)
        radius=sqrt(xf**2+yf**2)
        if((radius.lt.rmax).and.
     &     (zf.gt.zmin.and.zf.lt.zmax).and.
     &      attrf.eq.aselect)then
          attr(2:bsize+2,2:bsize+2,bsize+2,n)=attset
          print*,' block : ',n,' face : ',6,' attr : ',attset
        endif
      enddo
      return
      end

      subroutine tanhdist(delta1,delta2,len,i1,i2,fdist)
************************************************************************
*     TANDIST
*     program for computing the hyperbolic tangent distribution
*     delta1 : grid spacing at i=i1 (first cell)
*     delta2 : grid spacing at i=i2 (last  cell)
*     len    : lenght of curve
*     i1     : indices of first vertex
*     i2     : indices of last vertex
*     fdist  : computed distribution function
*
*
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      integer i1,i2,ni,i
      real(kind=8)::delta1,delta2,len
      real(kind=8)::delta,b,a
      real(kind=8)::fdist(i2-i1+1)
      real(kind=8)::transsinh,transtanh,ftmp
      external transsinh,transtanh

      if(i2.eq.i1)return

c------- Normalize delta1 and delta2 with lenght
      delta1=delta1/len
      delta2=delta2/len
c------- Compute number of points
      ni=i2-i1

c------- Assure that B > 1
      if(delta1.le.0.d0.and.1/delta2.lt.ni)then
        delta1=1/(ni**2*delta2*1.02)
      elseif(delta2.le.0d0.and.1/delta1.lt.ni)then
        delta2=1/(ni**2*delta1*1.02)
      endif

c------- Spacing at both end specified
      if(delta1.gt.0.d0.and.delta2.gt.0.d0)then
        a=sqrt(delta2)/sqrt(delta1)
        b=1.d0/(ni*sqrt(delta1*delta2))
        if(b.ge.1.d0)then
          delta=transsinh(b)
          do i=1,ni+1
            ftmp=.5*(1+tanh(delta*(real(i-1)/real(ni)-0.5))
     &          /tanh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        else
          delta=transtanh(b)
          do i=1,ni+1
            ftmp=.5*(1+sinh(delta*(real(i-1)/real(ni)-0.5))
     &          /sinh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        endif
c------- Spacing only specified at xi=1
      else if(delta1.gt.0.d0)then
        b=1.d0/(ni*delta1)
        delta=transsinh(b)
        do i=1,ni+1
          fdist(i)=1.d0+tanh(.5d0*delta*(real(i-1)/real(ni)-1.d0))
     &            /tanh(.5d0*delta)
        enddo
c------- Spacing only specified at xi=ni
      else if(delta2.gt.0.d0)then
        b=1.d0/(ni*delta2)
        delta=transsinh(b)
        do i=1,ni+1
          fdist(i)=tanh(.5*delta*real(i-1)/real(ni))/tanh(.5*delta)
        enddo
c------- Error no spacing is given
      else
        print*,' Error from tandist, no cell hight is given '
      endif
c------- Compute len*fdist
      do i=1,ni+1
        fdist(i)=fdist(i)*len
      enddo
      return
      end

      subroutine sinhdist(delta1,delta2,len,i1,i2,fdist)
************************************************************************
*     SINHDIST
*     program for computing the hyperbolic sine distribution
*     delta1 : grid spacing at i=i1 (first cell)
*     delta2 : grid spacing at i=i2 (last  cell)
*     len    : lenght of curve
*     i1     : indices of first vertex
*     i2     : indices of last vertex
*     fdist  : compute distribution function
*
*
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      real(kind=8)::delta1,delta2,len
      real(kind=8)::delta,b,a
      real(kind=8)::fdist(*)
      real(kind=8)::transsinh,transtanh,ftmp
      external transsinh,transtanh
      integer i1,i2,ni,i

      if(i2.eq.i1)return

c------- Normalize delta1 and delta2 with lenght
      delta1=delta1/len
      delta2=delta2/len
c------- Compute number of points
      ni=i2-i1

c------- Assure that B > 1
      if(delta1.le.0.d0.and.1/delta2.lt.ni)then
        delta1=1/(ni**2*delta2*1.02)
      elseif(delta2.le.0d0.and.1/delta1.lt.ni)then
        delta2=1/(ni**2*delta1*1.02)
      endif

c------- Spacing at both end specified
      if(delta1.gt.0.d0.and.delta2.gt.0.d0)then
        a=sqrt(delta2)/sqrt(delta1)
        b=1.d0/(ni*sqrt(delta1*delta2))
        if(b.ge.1.d0)then
          delta=transsinh(b)
          do i=1,ni+1
            ftmp=.5*(1+tanh(delta*(real(i-1)/real(ni)-0.5))
     &          /tanh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        else
          delta=transtanh(b)
          do i=1,ni+1
            ftmp=.5*(1+sinh(delta*(real(i-1)/real(ni)-0.5))
     &          /sinh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
          enddo
        endif
c------- Spacing only specified at xi=1
      else if(delta1.gt.0.d0)then
        b=1.d0/(ni*delta1)
        delta=transsinh(b)
        do i=1,ni+1
          fdist(i)=sinh(delta*real(i-1)/real(ni))/sinh(delta)
        enddo
c------- Spacing only specified at xi=ni
      else if(delta2.gt.0.d0)then
        b=1.d0/(ni*delta2)
        delta=transsinh(b)
        do i=1,ni+1
          fdist(i)=1-sinh(delta*(1-real(i-1)/real(ni)))/sinh(delta)
        enddo
c------- Error no spacing is given
      else
        print*,' Error from tandist, no cell hight is given '
      endif
c------- Compute len*fdist
      do i=1,ni+1
        fdist(i)=fdist(i)*len
      enddo
      return
      end

      double precision function transsinh(b)
************************************************************************
*     routine for solving the transendental equation
*
*     b=sinh(delta)/delta
*
*     using NewtonRaphson
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      real(kind=8)::delta,b
      real(kind=8)::delta_old,f,df,rlim
      parameter(rlim=1.d-9)
      integer n

c------- Preforme Newton iteration to obtain delta
      delta=1.d0
      delta_old=delta
      do n=1,25
c--------- Compute f(delta)
c        f =b-sinh(delta)/delta
c        f =b*delta-sinh(delta)
         f =delta/sinh(delta)-1/b
c--------- Compute d/d(delta)(f(delta))
c        df=(sinh(delta)-delta*cosh(delta))/delta**2
c        df=b-cosh(delta)
         df=(sinh(delta)-delta*cosh(delta))/sinh(delta)**2
c--------- update delta
        delta=delta-f/df
c       write(*,'(4e16.4)')f,df,f/df,delta
c--------- check convergence criterium
c       if(abs((delta-delta_old)/delta_old).lt.rlim)goto 200
c--------- update old value
c       delta_old=delta
      enddo
      transsinh=delta
      return
      end

      double precision function transtanh(b)
************************************************************************
*     routine for solving the transendental equation
*
*     b=tanh(delta/2)/(delta/2)
*
*     using NewtonRaphson
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      real(kind=8)::delta,b
      real(kind=8)::delta_old,f,df,rlim
      parameter(rlim=1.d-6)
      integer n

      if(b.gt.1.0d0)then
        print*,' Error form sinhdist: Can not be gridded '
c       stop
      endif
c------- Preforme Newton iteration to obtain delta
      delta=1.d0
      delta_old=delta
      do n=1,25
c--------- Compute f(delta)
        f =delta/(tanh(.5*delta)+1.d-30)-2/B
c--------- Compute d/d(delta)(f(delta))
        df=(tanh(.5*delta)-delta*(1-tanh(.5*delta)**2))
     &    /(tanh(.5*delta)**2+1.d-30)
c--------- update delta
        delta=delta-f/df
c       write(*,'(i3,1x,3e16.4)')n,delta,f,df
c--------- check convergence criterium
c       if(abs((delta-delta_old)/delta_old).lt.rlim)goto 200
c--------- update old value
c       delta_old=delta
      enddo
      transtanh=delta
      return
      end

      subroutine cmdpart(InputLine,mword,nrchar,nrword,words)
c-----------------------------------------------------------------------
c     routine parting command InputLine into words
c----------------------------------------------------------------------
c
c     Author: Jess A. Michelsen and Niels N. Soerensen
c     last revision :march 3, 1998
c-----------------------------------------------------------------------
      implicit none
      integer mword,nrchar(mword),nrword,i
      integer,parameter::mchar=256
      character(len=40),dimension(mword):: words
      character(len=40)::blank
      character(len=256) InputLine
      integer CurrentPosition,RelativeBlankPosition,BlankPosition
C--------------------------------------------- Reset word counter ------
      nrword=0
      CurrentPosition=1
C--------------------------------------------- Skip if a comment line --
      if(index(InputLine(1:mchar),'#').gt.0) return
C---------------------------------------- find distance to next blank --
   20 RelativeBlankPosition=index(InputLine(CurrentPosition:mchar),' ')
      if(CurrentPosition.eq.mchar.or.RelativeBlankPosition.eq.0) return
C---------------------------------------- found the end of a word ------
      if(RelativeBlankPosition.gt.1.and.RelativeBlankPosition.le.40)then
        BlankPosition=CurrentPosition-1+RelativeBlankPosition
        nrword=nrword+1
        if(nrword.gt.mword)return
        words(nrword)=InputLine(CurrentPosition:BlankPosition)
        nrchar(nrword)=RelativeBlankPosition-1
      endif
c-----------------------------------------------------------------------
      CurrentPosition=CurrentPosition+RelativeBlankPosition
      goto 20
c-----------------------------------------------------------------------
      end

      subroutine UnfGridWrite(unitnr,ni,nblock,x,y,z,attr)
C=======================================================================
C     routine for writing unformatted x3d file
C     remember that the grid must positions in the following region 
C
C        x(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C        y(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C        z(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C     attr(2:bsize+2,2:bsize+2,2:bsize+2,1:nblock)
C
C     the remaining spaces are intended for ghost cells
C=======================================================================
      implicit none
      integer unitnr,ni,nblock
      real(kind=8),dimension(ni,ni,ni,nblock)::x,y,z
      integer,dimension(ni,ni,ni,nblock)::attr

      print*,' unitnr ',unitnr
      open(unit=unitnr,file='grid.X3DUNF',form='unformatted')
      write(unitnr)ni-3
      write(unitnr)nblock
      print*,' Writing Attributes '
      call WriteBlockInteger(unitnr,ni**3,nblock,attr)
      print*,' Writing x '
      call WriteBlockReal   (unitnr,ni**3,nblock,x   )
      print*,' Writing y '
      call WriteBlockReal   (unitnr,ni**3,nblock,y   )
      print*,' Writing z '
      call WriteBlockReal   (unitnr,ni**3,nblock,z   )
      print*,' Finished writing '
      close(unitnr)
      return
      end

      subroutine WriteBlockReal(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      real(kind=8),dimension(block,nblock)::x
      real(kind=8),dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine WriteBlockReal

      subroutine WriteBlockInteger(unit,block,nblock,x)
C=======================================================================
      implicit none
      integer unit,block,nblock,n
      integer,dimension(block,nblock)::x
      integer,dimension(block)       ::xblock
        do n=1,nblock
          xblock(:)=x(:,n)
          write(unit)xblock
        end do
      end subroutine WriteBlockInteger
