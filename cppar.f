      program ComputeParra
C-----------------------------------------------------------------------
C     program for computing possible parallelizations options for a 
C     given number of blocks
C-----------------------------------------------------------------------
      implicit none
      integer NCORE,NBLOCK,NCN,BPP,NPROC,NNODES,lcores,a,c,full
      real(kind=8)::rest

      print*,' Give number of blocks : '
      read*,nblock
      print*,' Give number of cores per node : '
      read*,NCN

      print*,'##############################################'
      print*,' nodes, cores, b/core, b/core-last, cores-last '

      do ncore=1,nblock
        a=int(real(nblock)/real(ncore))
        rest=real(nblock)/real(ncore)-a
        if(rest.eq.0.d0)then
          nnodes=ceiling(real(ncore)/real(ncn))
          lcores=ncore-(nnodes-1)*ncn
          full=ncore-int(ncore/ncn)*ncn
          if(full.eq.0)then
            print*,'F1',nnodes,ncore,a,a,lcores
          else
            print*,'N1',nnodes,ncore,a,a,lcores
          endif
        else
          a=ceiling(real(nblock)/real(ncore))
          rest=nblock-a*(ncore-1)
c         print*,' rest ',rest,a
          if(rest.gt.0.d0)then
            nnodes=ceiling(real(ncore)/real(ncn))
            lcores=ncore-(nnodes-1)*ncn
            full=ncore-int(ncore/ncn)*ncn
            if(full.eq.0)then
              print*,'F2',nnodes,ncore,a,int(rest),lcores
            else
              print*,'N2',nnodes,ncore,a,int(rest),lcores
            endif
          endif
        endif
      enddo
      print*,'##############################################'
      do ncore=2,nblock
      enddo


      stop
      end  
