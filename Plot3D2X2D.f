      program Plot3D2X2D
      implicit none
      integer nblock,nim,njm
      integer,dimension(:),allocatable::ni,nj
      integer i,j,k,n,jmax,jmin,jinc,nk
      integer,dimension(:,:,:,:),allocatable::attr
      real(kind=8),dimension(:,:,:,:),allocatable::x,y,z
      integer,dimension(:),allocatable::transpose
      integer::dim,nbi,nbj,nbm,ii,jj,io,jo,istart,jstart,yesno
      real(kind=8)::rest
   
c-------Read Plot3D file
      open(unit=10,file='grid.plot3d')
      read(10,*)nblock
      allocate(ni(nblock),nj(nblock))
      read(10,*)(ni(n),nj(n),nk,n=1,nblock)
      nim=maxval(ni(:));njm=maxval(nj(:))
      print*,nim,njm
      allocate(x(nim,njm,nk,nblock)
     &        ,y(nim,njm,nk,nblock)
     &        ,z(nim,njm,nk,nblock),attr(nim,njm,nk,nblock))
      do n=1,nblock
        print*,' block nr. ', n
        read(10,*)
     &  (((x(i,j,k,n),i=1,ni(n)),j=1,nj(n)),k=1,nk),
     &  (((y(i,j,k,n),i=1,ni(n)),j=1,nj(n)),k=1,nk),
     &  (((z(i,j,k,n),i=1,ni(n)),j=1,nj(n)),k=1,nk)
      enddo
      close(10)

      attr=1

c-----find max size of multigrid blocks
      print*,'*******************************************************'
      print*,'Give block size  '
      read(*,*)dim
      nbm=0
      do n=1,nblock
        print*,n,ni(n),nj(n)
        nbi=(ni(n)-1)/real(dim)
        nbj=(nj(n)-1)/real(dim)
        print*,nbi,nbj
        nbm=nbm+nbi*nbj
      enddo
      print*,'*******************************************************'
      print*,'size of multidomaine blocks         : ',dim
      print*,'total number of blocks              : ',nbm
      print*,'*******************************************************'

      open(unit=20,file='grid.x2d')
      write(20,*) dim,nbm
      do n=1,nblock
        nbi=(ni(n)-1)/dim
        nbj=(nj(n)-1)/dim
        do k=1,nk
        do ii=1,nbi;istart=(ii-1)*dim
        do jj=1,nbj;jstart=(jj-1)*dim
        do j=1,dim+1;jo=jstart+j
        do i=1,dim+1;io=istart+i
          write(20,'(i6,3e30.20)')attr(io,jo,k,n),x(io,jo,k,n)
     &             ,y(io,jo,k,n),z(io,jo,k,n)
        enddo;enddo
      enddo;enddo
      enddo;enddo
      close(20)
                                                                                                           
      stop
      end
