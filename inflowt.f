      program inflowturb
#=======================================================================
#     A small program for computing the decay of tke from the inlet
#     assuming zero velocity shear
#=======================================================================
      implicit none
      integer i
      logical statex
      character(len=10)::adum
      real(kind=8)::dist,vel,omega,ti,kinlet,k,ktmp,x

      inquire(file='inflow.dat',exist=statex)
      if(statex)then
        open(unit=10,file='inflow.dat')
        read(10,*)adum,dist
        read(10,*)adum,vel
        read(10,*)adum,omega
        read(10,*)adum,ti
        read(10,*)adum,kinlet
      else
        print*,' No inflow '
        open(unit=10,file='inflow.dat')
        write(10,*)'# Distance from inlet [m] '
        write(10,*)'# Inflow Velocity (Vel)    [m/s] '
        write(10,*)'# Specific Dissipation Rate (omega) [1/s] '
        write(10,*)'# Turbulence Intensity (100sqrt(2/3*k)/Vel)'
        write(10,*)'# Inflow Turbulence (k)  [m**2 s**-2]'
        stop
      endif
      close(10)
      print*,''
      print*,''
      print*,''
      print*,'###################################################'
      print*,' Inlet Values'
      print*,' Distance [m]          : ',dist
      print*,' Velocity [m/s]        : ',vel
      print*,' Omega    [1/s]        : ',omega
      print*,' Ti       in %         : ',ti
      print*,' K        [m**2s**-2]  : ',kinlet
  
      k=(ti*vel/100.d0)**2*1.5d0
      ktmp=k*(1+omega*.09d0*dist/vel)**.92d0
      print*,'###################################################'
      print*,' Inlet values to obtain the specified ti value '
      print*,' Inlet k-value : ',ktmp   ,' [m**2 s**-2] '
      print*,'###################################################'
      ktmp=kinlet*(1+omega*.09d0*dist/vel)**-.92d0
      print*,' Resulting value based on the specified k value '
      print*,' Resulting k-value : ',ktmp,' [m**2 s**-2] '
      print*,' Resulting Ti-value : ',sqrt(ktmp*2.d0/3.d0)/vel*100
     &      ,' in % '
      print*,'###################################################'
      print*,''
      print*,''

      k=(ti*vel/100.d0)**2*1.5d0*(1+omega*.09*dist/vel)**.92
      do i=1,100
        x=dist*real(i)/real(100)
        ktmp=k*(1+omega*.09d0*x/vel)**-.92d0
        write(20,*)x,ktmp
      enddo
      stop
      end
