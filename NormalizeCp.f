      program NormalizeCp
      implicit none
      character(len=64)::arg
      logical::scale=.true.,rotate=.true.,printout=.true.
      real(kind=8),dimension(:,:),allocatable::var,vart
      real(kind=8)::maxlen,len,aoa,x0,y0,pi,varmin,cploc,cpval
      real(kind=8),dimension(2)::n1,n2,a
      real(kind=8)::dx,dy,fpx,fpy,mpz,fvx,fvy,mvz,apx,apy,anx,any
      real(kind=8)::dfpx,dfpy,dfvx,dfvy
      real(kind=8)::xr=0.25,yr=0.0
      integer ni,i,j,n,imaxlen,icploc,nin
C-----------------------------------------------------------------------
C     The program assumes that the files holdes the following information
C     x,y,cp,cf
C-----------------------------------------------------------------------

      do i=1,iargc(),1
        call getarg(i,arg)

        select case(arg(1:2))
c-----------switch of rescaling airfoil chord
          case('-s');scale=.false.
          print*,' Scale switched off '
c-----------switch of rotation to horizontal
          case('-r');rotate=.false.
c-----------switch of print out
          case('-p');printout=.false.
        end select
      enddo
       

      pi=atan(1.d0)*4

c--------preliminary read to determine the number of points
      ni=0
      allocate(var(4,1))
      open(unit=10,file='prof.dat')
      do
        read(10,*,end=666)(var(n,1),n=1,4)
        ni=ni+1
      enddo
  666 deallocate(var)  
      close(10)

      open(unit=10,file='prof.dat')
      allocate(vart(4,ni))
      vart=0.d0
c-------read pressure file, removing non-unique points
      nin=0
      do i=1,ni
        read(10,*,end=666)(vart(n,i-nin),n=1,4)
        do j=1,i-nin-1
          if(((vart(1,i-nin)-vart(1,j))**2+(vart(2,i-nin)-vart(2,j))**2)
     &    .lt.1.d-12)then
             nin=nin+1
          endif
        enddo
      enddo
      close(10)
      if(.not.printout)print*,' Found ',nin,' non-unique points !!! '
      ni=ni-nin
      allocate(var(4,ni))
      var(:,1:ni)=vart(:,1:ni)
      deallocate(vart)

      call computeangle(ni,4,var,x0,y0,maxlen,aoa)
c-------rotate profile to make it horizontal
      n1(1)= cos(aoa)
      n1(2)= sin(aoa)
      n2(1)=-sin(aoa)
      n2(2)= cos(aoa)
      do i=1,ni
        a(1)=var(1,i)-x0
        a(2)=var(2,i)-y0
        var(1,i)=n1(1)*a(1)+n1(2)*a(2)
        var(2,i)=n2(1)*a(1)+n2(2)*a(2)
      enddo
c-------scale airfoil to chord = 1
      if(scale)then
        do i=1,ni
          var(1,i)=var(1,i)/maxlen
          var(2,i)=var(2,i)/maxlen
        enddo
      endif
      if(.not.rotate)then
c-------rotate profile to make it horizontal
        n1(1)= cos(-aoa)
        n1(2)= sin(-aoa)
        n2(1)=-sin(-aoa)
        n2(2)= cos(-aoa)
        do i=1,ni
          a(1)=var(1,i);a(2)=var(2,i)
          var(1,i)=n1(1)*a(1)+n1(2)*a(2)+x0
          var(2,i)=n2(1)*a(1)+n2(2)*a(2)+y0
        enddo
      endif

c------sort airfoil data
      call pointsort(ni,var(1,:),var(2,:),var(3,:),var(4,:))
      call reorder(ni,var(1,:),var(2,:),var(3,:),var(4,:))

c-------determine stagnation point location
 10   varmin=1.d30
c-------find minimum pressure point
      do i=1,ni
        if(var(3,i).lt.varmin)then
          varmin=var(3,i)
          icploc=i
        endif
      enddo
      call minfunc(var(1,icploc+1),var(1,icploc),var(1,icploc-1) 
     &            ,var(3,icploc+1),var(3,icploc),var(3,icploc-1)
     &            ,cploc,cpval)

c------integrate forces
      fpx=0.d0;fpy=0.d0;fvx=0.d0;fvy=0.d0;mpz=0.d0;mvz=0.d0
      apx=0.d0;apy=0.d0;anx=0.d0;any=0.d0
      do i=1,ni-1
        dx=(var(1,i+1)-var(1,i))
        dy=(var(2,i+1)-var(2,i))
        dfpx=-dy*.5*(var(3,i+1)+var(3,i))
        dfpy= dx*.5*(var(3,i+1)+var(3,i))
        dfvx= dx*.5*(var(4,i+1)+var(4,i))
        dfvy= dy*.5*(var(4,i+1)+var(4,i))
        fpx=fpx+dfpx
        fpy=fpy+dfpy
        fvx=fvx+dfvx
        fvy=fvy+dfvy
        mpz=mpz-.5*(var(1,i+1)+var(1,i)-2*xr)*dfpy
     &         +.5*(var(2,i+1)+var(2,i)-2*yr)*dfpx
        mvz=mvz+.5*(var(1,i+1)+var(1,i)-2*xr)*dfvy
     &         +.5*(var(2,i+1)+var(2,i)-2*yr)*dfvx
c       mpz=mpz+.25*(var(1,i+1)+var(1,i)-2*xr)*dx*(var(3,i+1)+var(3,i))
c    &         +.25*(var(2,i+1)+var(2,i)-2*yr)*dy*(var(3,i+1)+var(3,i))    
c       mvz=mvz+.25*(var(1,i+1)+var(1,i)-2*xr)*dy*(var(4,i+1)+var(4,i))
c    &         -.25*(var(2,i+1)+var(2,i)-2*yr)*dx*(var(4,i+1)+var(4,i))    
        apx=apx+.5*(dx+abs(dx))
        apy=apy+.5*(dy+abs(dy))
        anx=anx+.5*(dx-abs(dx))
        any=any+.5*(dy-abs(dy))
      enddo
      dx=(var(1,1)-var(1,ni))
      dy=(var(2,1)-var(2,ni))
      dfpx=-dy*.5*(var(3,1)+var(3,ni))
      dfpy= dx*.5*(var(3,1)+var(3,ni))
      dfvx= dx*.5*(var(4,1)+var(4,ni))
      dfvy= dy*.5*(var(4,1)+var(4,ni))
      fpx=fpx+dfpx
      fpy=fpy+dfpy
      fvx=fvx+dfvx
      mpz=mpz-.25*(var(1,1)+var(1,ni))*dx*(var(3,1)+var(3,ni))
     &       +.25*(var(2,1)+var(2,ni))*dy*(var(3,1)+var(3,ni))    
      mvz=mvz+.25*(var(1,1)+var(1,ni))*dy*(var(4,1)+var(4,ni))
     &       +.25*(var(2,1)+var(2,ni))*dx*(var(4,1)+var(4,ni))    

      open(unit=12,file='prof.new')
      write(12,20)'# CHORD        : ',maxlen
      write(12,20)'# AOA          : ',aoa*180/pi
      write(12,20)'# STAG. POINT  : ',cploc
      write(12,20)'# FPX          : ',fpx
      write(12,20)'# FPY          : ',fpy
      write(12,20)'# MPZ          : ',mpz
      write(12,20)'# FVX          : ',fvx
      write(12,20)'# FVY          : ',fvy
      write(12,20)'# MVZ          : ',mvz
      write(12,20)'# APX          : ',apy
      write(12,20)'# APY          : ',apx
      write(12,20)'# ANX          : ',anx
      write(12,20)'# ANY          : ',any
      do i=1,ni
        write(12,'(4f16.6)')(var(n,i),n=1,4)
      enddo

      if(printout)then
        open(unit=13,file='min.dat')
        do i=1,-1,-1
          write(13,*)var(1,icploc+i),var(3,icploc+i)
        enddo
        write(13,*)cploc,cpval
      endif

      stop
   20 format(a12,f12.6)
   21 format('# ',3(f12.6))
      end
   
      subroutine minfunc(x1,x2,x3,y1,y2,y3,minloc,minval)
      implicit none
      real(kind=8)x1,x2,x3,y1,y2,y3,minloc,minval
      real(kind=8)xt1,xt2,xt3,a1,b1,c1
      
      xt1=0.d0;xt2=(x2-x1)/(x3-x1);xt3=1.d0
      c1=y1
      b1=(y2+c1*(xt2**2-1)-y3*xt2**2)/(xt2-xt2**2)
      a1=y3-c1-b1
      minloc=-.5*b1/a1
      minval=a1*minloc**2+b1*minloc+c1
      minloc=minloc*(x3-x1)+x1
      return
      end


      subroutine ComputeAngle(ni,nvar,var,xmin,ymin,maxlen,aoa)
      implicit none
      integer ni,nvar,i,imaxlen
      real(kind=8),dimension(nvar,ni)::var
      real(kind=8)::maxlen,aoa,len,x1,y1,x2,y2,xmean,ymean,xmin,ymin

c-------find geometrical angel of attack
      maxlen=0.d0
      xmean=sum(var(1,1:ni))/ni
      ymean=sum(var(2,1:ni))/ni
      do i=1,ni
        len=sqrt((var(1,i)-xmean)**2+(var(2,i)-ymean)**2)
        if(len.gt.maxlen)then
          maxlen=len
          x1=var(1,i)
          y1=var(2,i)
        endif
      enddo
      maxlen=0.d0
      do i=1,ni
        len=sqrt((var(1,i)-x1)**2+(var(2,i)-y1)**2)
        if(len.gt.maxlen)then
          maxlen=len
          x2=var(1,i)
          y2=var(2,i)
        endif
      enddo
      maxlen=sqrt((x2-x1)**2+(y2-y1)**2)
      if(x2.gt.x1)then
        aoa=atan((y2-y1)/(x2-x1))
        xmin=x1;ymin=y1
      else
        aoa=atan((y1-y2)/(x1-x2))
        xmin=x2;ymin=y2
      endif
      
 
      write(20,*)x1,y1,x2,y2,xmean,ymean
      return
      end


      subroutine pointsort(ni,x,y,cp,cf)
      implicit none
      real(kind=8),dimension(ni)::x,y,xt,yt,cp,cf
      real(kind=8),dimension(2)::v1,v2
      real(kind=8)::xs,ys,len,l1,l2,atest,atestmax,lminn,factor
      real(kind=8),dimension(5)::lmin
      integer,dimension(ni)::slist
      logical,dimension(ni)::pcheck
      integer::ni,i,j,k,sorted,illast
      integer,dimension(5)::ilast
      integer,dimension(1)::iloc
      integer itest

      factor=2.d0

      open(unit=10,file='unsorted.dat')
      do i=1,ni
        write(10,*)x(i),y(i)
      enddo
      close(10)

      pcheck=.true.
      open(unit=10,file='sorted.dat')
      slist=0
c------first point
c     print*,' Give start point for sorting : '
c     read(*,*)ilast(1)
      ilast(1)= 1
      if(ilast(1).lt.0)then
        iloc=maxloc(x)
        ilast(1)=iloc(1)
      endif
c------- store the index of last sorted point in list over sorted points
      slist(1)=ilast(1)
      xs=x(ilast(1));ys=y(ilast(1))
c     print*,' first point : ',ilast(1)
c------- flag that the first point do not need to be evaluated any more
      pcheck(ilast(1))=.false.
      write(10,*)xs,ys
c------second point, is only based on distance to neighbour points
      lmin(1)=.25d0
      do i=1,ni
        if(pcheck(i))then
          len=sqrt((x(i)-xs)**2
     &            +(y(i)-ys)**2)
          if(len.lt.lmin(1))then
              lmin(1)=len
              ilast(1)=i
          endif
        endif
      enddo
c------- store the index of last sorted point in list over sorted points
      slist(2)=ilast(1)
c     pcheck(ilast)=.false.
c------- flag that second first point do not need to be evaluated any more
      pcheck(slist(2))=.false.
c     print*,' second point : ',ilast(1)
      xs=x(ilast(1))
      ys=y(ilast(1))
      write(10,*)xs,ys
c-------remaining point
      do j=3,ni
        lmin=1.d0;ilast=-1
c---------compute tangent vector and length based on the two last sorted points
        v2(1)=x(slist(j-1))-x(slist(j-2))
        v2(2)=y(slist(j-1))-y(slist(j-2))
        l2=sqrt(v2(1)**2+v2(2)**2)
c--------- find the five nearest points
        do i=1,ni
          if(pcheck(i))then
            len=sqrt((x(i)-xs)**2
     &              +(y(i)-ys)**2)
             if(len.lt.lmin(1))then
               do k=5,2,-1
                 lmin(k)=lmin(k-1);ilast(k)=ilast(k-1)
               enddo
               lmin(1)=len;ilast(1)=i
             elseif(len.lt.lmin(2))then
               do k=5,3,-1
                 lmin(k)=lmin(k-1);ilast(k)=ilast(k-1)
               enddo
               lmin(2)=len;ilast(2)=i
             elseif(len.lt.lmin(3))then
               do k=5,4,-1
                 lmin(k)=lmin(k-1);ilast(k)=ilast(k-1)
               enddo
               lmin(3)=len;ilast(3)=i
             elseif(len.lt.lmin(4))then
               lmin(5)=lmin(4);ilast(5)=ilast(4)
               lmin(4)=len;ilast(4)=i
             elseif(len.lt.lmin(5))then
               lmin(5)=len;ilast(5)=i
             endif
           endif
         enddo
c----------write the nearest points to file
         open(unit=11,file='nearest.dat')
         do k=1,4
           if(ilast(k).gt.0)write(11,*)x(ilast(k)),y(ilast(k))
         enddo  
         close(11)
c----------check the angle to the nearest 5 points
         atestmax=-1.d0;lminn=1.d0
         do k=1,5
           if(ilast(k).gt.0)then
c------------ compute the vector from last sorted point to near point
             v1(1)=x(ilast(k))-xs
             v1(2)=y(ilast(k))-ys
             l1=sqrt(v1(1)**2+v1(2)**2)
             atest=(v1(1)*v2(1)+v1(2)*v2(2))
     &            /(l1*l2)
c-------------- if the vector to the nearest point is less than ~12deg cos(12deg) 
             if(k.eq.1.and.atest.gt..98d0)then
               illast=ilast(k)
               goto 100
             endif
c--------------else check a blend between proximity and angle
c-------------- if the tangent is slightly closer to curve tangent
c-------------- and the distance is not more than 5% longer than the previous min distance 
             if(atest.gt.atestmax*1.02.and.l1.lt.1.05*lminn)then
c            if(atest.gt.atestmax*1.01.and.l1.lt.1.05*lminn)then
               atestmax=atest;lminn=l1
               illast=ilast(k)
             endif
           endif
         enddo
  100   slist(j)=illast
        pcheck(slist(j))=.false.
        xs=x(slist(j))
        ys=y(slist(j))
        open(unit=10,file='sorted.dat',status='old')
        do i=1,j
        write(10,*)x(slist(i)),y(slist(i))
        enddo
        close(10)
        open(unit=10,file='sorted.dat',status='old')
c       print*,' give next '
c       read*
      enddo 
      ilast=slist(1)
      close(10)
      
      do j=1,ni
        xt(j)=x(slist(j))
        yt(j)=y(slist(j))
      enddo
      x=xt;y=yt
      do j=1,ni
        xt(j)=cp(slist(j))
        yt(j)=cf(slist(j))
      enddo
      cp=xt;cf=yt
      return 
      end

      subroutine reorder(ni,x,y,cp,cf)
      implicit none
      integer ni,i,imax,ii
      real(kind=8),dimension(ni)::x,y,cp,cf,xt,yt,cpt,cft
      real(kind=8)dx,ym,area,xmax
     
c------determine orientation of line by computing integral
      area=0.d0
      do i=2,ni-1
        dx=x(i+1)-x(i-1) 
        ym=.5*(y(i+1)+y(i)) 
        area=area+dx*ym
      enddo
c     write(*,*)' area ',area/abs(area)
c------make orientation clockwise
      if(area.lt.0.d0)then
      do i=1,ni
        xt(i)=x(ni+1-i)
        yt(i)=y(ni+1-i)
        cpt(i)=cp(ni+1-i)
        cft(i)=cf(ni+1-i)
      enddo
      x=xt;y=yt;cp=cpt;cf=cft
      endif
c-------make first point trailing edge point
      xmax=-10
      do i=1,ni
        if(x(i).ge.xmax)then
          imax=i
          xmax=x(i)
        endif
      enddo
c     print*,imax,xmax
      ii=0
      do i=imax,ni
        ii=ii+1
        xt(ii)=x(i)
        yt(ii)=y(i)
        cpt(ii)=cp(i)
        cft(ii)=cf(i)
      enddo
      do i=1,imax-1
        ii=ii+1
        xt(ii)=x(i)
        yt(ii)=y(i)
        cpt(ii)=cp(i)
        cft(ii)=cf(i)
      enddo
      x=xt;y=yt;cp=cpt;cf=cft
      return
      end

