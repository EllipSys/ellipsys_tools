      program curvature
      implicit none
      real(kind=8),dimension(:),allocatable::x,y
      real(kind=8) ds,ds1,ds2,curv,dist,a
      real(kind=8) dxds,dyds,d2xds2,d2yds2,clength
      logical::statex
      character adum
      integer ni,i

c-------open surf data
      inquire(file='prof.new',exist=statex)
      if(statex)then
        open(unit=10,file='prof.new')
      else
        open(unit=10,file='prof.dat')
      endif
      read(10,*)adum,ni
      allocate(x(ni),y(ni))
      do i=1,ni
          read(10,*)x(i),y(i)
      enddo

c-------compute curve lenght
      clength=0.d0
      do i=2,ni
        ds1=sqrt((x(i  )-x(i-1))**2+(y(i  )-y(i-1))**2)
        clength=clength+ds1
      enddo
c-------compute curvature
      open(unit=11,file='curv.dat')
      dist=0.d0
      do i=2,ni-1
        ds1=sqrt((x(i  )-x(i-1))**2+(y(i  )-y(i-1))**2)
        ds2=sqrt((x(i+1)-x(i  ))**2+(y(i+1)-y(i  ))**2)
        ds =.5*sqrt(ds1**2+ds2**2)
        dxds=.5*(x(i+1)-x(i-1))/ds
        dyds=.5*(y(i+1)-y(i-1))/ds
        d2xds2=(x(i+1)-2*x(i)+x(i-1))/ds**2
        d2yds2=(y(i+1)-2*y(i)+y(i-1))/ds**2
        dist=dist+ds1
        curv=(d2xds2*dyds-dxds*d2yds2)/(dxds**2+dyds**2)**1.5
        write(11,'(5(e25.8))')x(i),y(i),dist/clength,.5*curv,ds1
      enddo
      close(11)
      
      stop
      end

    
