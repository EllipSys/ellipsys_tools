      program SMesh2Fieldview3D
C-----------------------------------------------------------------------
C     program reading a 3D surface x2d file and writing a plot3d file
C-----------------------------------------------------------------------
      implicit none
      real(kind=8),dimension(:,:,:),allocatable::x,y,z
      integer,dimension(:,:,:),allocatable::attr
      integer::i,j,n,bsize,ni,nblock

      open(unit=20,file='grid.x2d')
      read(20,*) bsize,nblock
      ni=bsize+1
      allocate(x(ni,ni,nblock),y(ni,ni,nblock),z(ni,ni,nblock)
     &        ,attr(ni,ni,nblock))

      do n=1,nblock
      do j=1,bsize+1
      do i=1,bsize+1
      read(20,*)attr(i,j,n),x(i,j,n),y(i,j,n),z(i,j,n)
      enddo;enddo
      enddo
      close(20)

     
      call plotfieldview(bsize,nblock,x,y,z,attr)

      end

      subroutine plotfieldview(bsize,nb,x,y,z,attr)
c----------------------------------------------------------------------
c
c----------------------------------------------------------------------
      implicit none
      integer,parameter::idp4=8
      integer::bsize,nb,i,j,k,n
      real(kind=8),dimension(bsize+1,bsize+1,nb)::x,y,z,var
      integer,dimension(bsize+1,bsize+1,nb)::attr


c-----make FieldView plot3d unformatted grid file
      print*,'writing grid.xyz ...'
      open(unit=4,file='grid.xyz',form='unformatted')
      write(4)nb
      write(4)(bsize+1,bsize+1,1,n=1,nb)
      do n=1,nb
      write(4)(((real(x(i,j,n),idp4),i=1,bsize+1),j=1,bsize+1),k=1,1),
     &        (((real(y(i,j,n),idp4),i=1,bsize+1),j=1,bsize+1),k=1,1),
     &        (((real(z(i,j,n),idp4),i=1,bsize+1),j=1,bsize+1),k=1,1) 
      enddo
      close(4)

      open(unit=4,file='grid.xyz.fvbnd',form='formatted')
      write(4,*)'FVBND 1 4'
      write(4,*)' wall    '
      write(4,*)' inlet   '
      write(4,*)' outlet  '
      write(4,*)' cyclic  '
      write(4,*)' BOUNDARIES  '
      do n=1,nb
        write(4,'(8i6,a3,i6)')1,n,1,bsize+1,1,bsize+1,1,1,'F',-1
      enddo
      close(4)


c-----------------------------------------------------------------------
      return
      end
