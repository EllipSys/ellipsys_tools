      program DAT2x2d
      implicit none
      real(kind=8),dimension(:,:),allocatable::x,y
      integer,dimension(:,:),allocatable::attr
      real(kind=8),dimension(:,:,:),allocatable::xb,yb
      integer,dimension(:,:,:),allocatable::attrb
      integer ni,nj,i,j,n,yesno
      integer::istart,jstart,io,jo,ii,jj,dim,nb,nbi,nbj,nbm
      real(kind=8)::rest
                                                                                                             
c--------allocate 2d vertices
      open(unit=10,file='grid.DAT')
      read(10,*)ni,nj
      allocate(x(ni,nj),y(ni,nj),attr(ni,nj))
      do j=1,nj
      do i=1,ni
        read(10,*)attr(i,j),x(i,j),y(i,j)
      enddo;enddo

c------respecify wall attributes
c     attr(112:240,1)=104
c     attr(11:90,1)=104

c-----find max size of multigrid blocks
      dim=int(min(real(ni),real(nj)))
      dim=dim-1

  100 rest=mod((ni-1),dim)
     &    +mod((nj-1),dim)
      if(rest.eq.0)goto 200
      dim=(dim-1)/2+1
      goto 100
  200 continue
      print*,'*******************************************************'
      print*,'size of multidomaine blocks         : ',dim
      print*,'want to change this yes = 1, no = 2   '
      read(*,*) yesno
      if(yesno.eq.1)then
        print*,'give new dimension  '
        read(*,*)dim
      endif
      nbi=(ni-1)/dim
      nbj=(nj-1)/dim
      nbm=nbi*nbj
      print*,'*******************************************************'
      print*,'size of multidomaine blocks         : ',dim
      print*,'number of block in xi direction     : ',nbi
      print*,'number of block in eta direction    : ',nbj
      print*,'total number of blocks              : ',nbm
      print*,'*******************************************************'
                                                                                                             
c-----store grid in multiblock format
      allocate(xb(dim+1,dim+1,nbm),yb(dim+1,dim+1,nbm)
     &         ,attrb(dim+1,dim+1,nbm))
      nb=0
      do ii=1,nbi;istart=(ii-1)*dim
      do jj=1,nbj;jstart=(jj-1)*dim
        nb=nb+1
        do j=1,dim+1;jo=jstart+j
        do i=1,dim+1;io=istart+i
          xb(i,j,nb)=x(io,jo)
          yb(i,j,nb)=y(io,jo)
          attrb(i,j,nb)=attr(io,jo)
        enddo;enddo
      enddo;enddo

      open(unit=20,file='grid.X2D')
      write(20,*) dim,nbm
      do n=1,nbm
      do j=1,dim+1
      do i=1,dim+1
      write(20,*)attrb(i,j,n),xb(i,j,n),yb(i,j,n)
      enddo;enddo
      enddo
      close(20)
      stop
      end
