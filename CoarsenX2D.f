      program CoarsenX2D
C-----------------------------------------------------------------------
C     program for coarsning an X2D surface file, eg. before generation
C     of hyperbolic mesh with HypGrid3D
C     Author: Niels N. Soerensen
C-----------------------------------------------------------------------
      implicit none
      integer i,j,n,ni,nb,factor
      real(kind=8),dimension(:,:,:),allocatable::x,y,z
      integer,dimension(:,:,:),allocatable::attr
      integer DimTest,dim
      external Dimtest

      open(unit=10,file='org.x2d')
      dim=DimTest(10)
      print*,' Dimension ',dim
      read(10,*)ni,nb
      ni=ni+1
      allocate(x(ni,ni,nb),y(ni,ni,nb),z(ni,ni,nb),attr(ni,ni,nb))
      x=0;y=0;z=0;attr=0
c---------2D Surface grid -------------------------------
      if(dim.eq.3)then
        do n=1,nb
        do j=1,ni
        do i=1,ni
          read(10,*)attr(i,j,n),x(i,j,n),y(i,j,n),z(i,j,n)
        enddo;enddo
        enddo
      else
c---------2D grid -------------------------------------
        do n=1,nb
        do j=1,ni
        do i=1,ni
          read(10,*)attr(i,j,n),x(i,j,n),y(i,j,n)
        enddo;enddo
        enddo
      endif
      close(10)
 
      print*,' Give reduction factor : '
      read*,factor
      open(unit=10,file='grid.x2d')
      write(10,*)(ni-1)/factor,nb
      if(dim.eq.3)then
        do n=1,nb
        do j=1,ni,factor
        do i=1,ni,factor
          write(10,'(i6,3(e30.18))')attr(i,j,n),x(i,j,n),y(i,j,n)
     &                                                  ,z(i,j,n)
        enddo;enddo
        enddo
c---------2D grid -------------------------------------
      else
        do n=1,nb
        do j=1,ni,factor
        do i=1,ni,factor
          write(10,'(i6,3(e30.18))')attr(i,j,n),x(i,j,n),y(i,j,n)
        enddo;enddo
        enddo
      endif
      close(10)
      stop
      end

      integer function dimtest(iunit)
      implicit none
      integer i,ilast,dim,iunit
      character(len=128) string
C--------------------------------------- Read first coordinate post----- 
      read(iunit,'(a128)')string
      read(iunit,'(a128)')string
      rewind(iunit)
C-----------------------------------------------------------------------
      ilast=0
C-----------------------------------------------------------------------
       do dim=0,3
C-------------------- Find start/end of post ---------------------------
        do i=ilast+1,128;ilast=i;if(string(i:i).ne.' ') exit;end do
        do i=ilast+1,128;ilast=i;if(string(i:i).eq.' ') exit;end do
C-------------------- Update post counter ------------------------------
        if(ilast.eq.128) exit
        dimtest=dim
C-----------------------------------------------------------------------
       end do
C-----------------------------------------------------------------------
      end
