      Program grd2plot3d
c-----------------------------------------------------------------------
c     transform grd file to plot3d and x2d  format
c
c        -p true/false: force  periodic domain by copying column and rows 
c        -s nbuf      : edge smoothing for nbuf vertices
c        -d dim       : x2d block size
c
c     e.g. grd2plot3d -s 30 -d 256
c
c-----------------------------------------------------------------------
      implicit none
      character(len=14)::id
      character(len=64)::arg
      logical statex
      integer::i,j,k,n,ni,nj,nbuf=0
      integer,parameter::idp=8
      character(len=128) filename
      real(kind=8)::xlo,ylo,zlo,xhi,yhi,zhi,dx,dy,factor
      integer::nbi,nbj,nbm,ii,jj,io,jo,istart,jstart,ifinish,jfinish
     &         ,nb,dim=0
      logical::lperiodic=.false.
      real(kind=8),dimension(:,:),allocatable::x,y,z
      integer,dimension(:,:),allocatable::attr


      do i=1,iargc(),2
        call getarg(i,arg)

        select case(arg(1:2))
c---------force periodic
        case('-p')
          call getarg(i+1,arg)
          if(arg(1:4).eq.'true')lperiodic=.true.
c---------edge smoothing 
        case('-s')
          call getarg(i+1,arg)
          read(arg,*)nbuf
c---------x2d block size
        case('-d')
          call getarg(i+1,arg)
          read(arg,*)dim
        end select
      enddo

      filename(1:4)='surf'
      filename(5:8)='.grd'
      inquire(file=filename(1:8),exist=statex)
      if(.not.statex)stop

      open(unit=10,file=filename(1:8))
c-----read input from surface file
      read(10,21) id
c-----read number of grid lines in x and y
      read(10,*) ni,nj
c-----read min and max value in x
      read(10,*) xlo,xhi
c-----read min and max value in y
      read(10,*) ylo,yhi
c-----read min and max value in z
      read(10,*) zlo,zhi
                                                                                                                   
      if(lperiodic)then
c-------make space for additional row and column
        ni=ni+1;nj=nj+1
        allocate(x(ni,nj),y(ni,nj),z(ni,nj))
        read(10,*)((z(i,j),i=1,ni-1),j=1,nj-1)
        dx=(xhi-xlo)/real(ni-2)
        dy=(yhi-ylo)/real(nj-2)
        print*, ' dx , dy ',dx,dy
      else
        allocate(x(ni,nj),y(ni,nj),z(ni,nj))
        read(10,*)((z(i,j),i=1,ni-1),j=1,nj-1)
        dx=(xhi-xlo)/real(ni-1)
        dy=(yhi-ylo)/real(nj-1)
        print*, ' dx , dy ',dx,dy
      endif
      close(10)

      print*,' xlo, xhi, xcent ',xlo,xhi,.5*(xlo+xhi),ni
      print*,' ylo, yhi, ycent ',ylo,yhi,.5*(ylo+yhi),nj

      do j=1,nj
      do i=1,ni
        x(i,j)=xlo+dx*real(i-1)
        y(i,j)=ylo+dy*real(j-1)
      enddo;enddo

c-----make a copy the first i-line to the last i-line (periodicity)
      do j=1,nj-1
        z(ni,j)=z(1,j)
      enddo
c-----make a copy of the first j-line to the last j-line (periodicity)
      do i=1,ni
        z(i,nj)=z(i,1)
      enddo

      if(nbuf.gt.0)then
c       nbuf=30
c-----smooth closed to the j=nj line
        jstart=nj-nbuf
      do j=jstart,nj
        factor=tanh(4*((j-jstart)/real(nj-jstart))**2)
        z(:,j)=z(:,j)*(1-factor)+factor*z(:,nj)
      enddo
c-----smooth closed to the j=1 line
      jfinish=nbuf+1
      do j=1,jfinish
        factor=tanh(4*((j-1)/real(jfinish-1))**2)
        z(:,j)=z(:,j)*factor+z(:,1)*(1-factor)
      enddo
c-----smooth closed to the i=ni line
      istart=ni-nbuf
      do i=istart,ni
        factor=tanh(4*((i-istart)/real(ni-istart))**2)
        z(i,:)=z(i,:)*(1-factor)+factor*z(ni,:)
      enddo
c-----smooth closed to the i=1 line
      ifinish=nbuf+1
      do i=1,ifinish
        factor=tanh(4*((i-1)/real(ifinish-1))**2)
        z(i,:)=z(i,:)*factor+z(1,:)*(1-factor)
      enddo
      endif

        
c-------Write Plot3D file
      open(unit=10,file='grid.xyz',form='formatted')
      write(10,*)1
      write(10,*)ni,nj,1,1
      write(10,*)
     &     (((real(x(i,j),idp)
     &      ,i=1,ni)
     &      ,j=1,nj)
     &      ,k=1,1),
     &     (((real(y(i,j),idp)
     &      ,i=1,ni)
     &      ,j=1,nj)
     &      ,k=1,1),
     &     (((real(z(i,j),idp)
     &      ,i=1,ni)
     &      ,j=1,nj)
     &      ,k=1,1)

c-----store grid in multiblock format
      if(dim.eq.0)then
        print*,' Give block size '
        read*,dim
      endif

      nbi=(ni-1)/dim
      nbj=(nj-1)/dim
      nbm=nbi*nbj
      print*,'*******************************************************'
      print*,'size of multidomaine blocks         : ',dim
      print*,'number of block in xi direction     : ',nbi
      print*,'number of block in eta direction    : ',nbj
      print*,'total number of blocks              : ',nbm
      print*,'*******************************************************'

c-----attributs
      allocate(attr(ni,nj))
      attr=1
      attr(1 ,: )=101
      attr(ni,: )=101
      attr(: ,1 )=102
      attr(: ,nj)=102
      attr(1 ,1 )=111
      attr(ni,1 )=111
      attr(1 ,nj)=111
      attr(ni,nj)=111

      open(unit=10,file='grid.x2d')
      write(10,*)dim,nbm
      do ii=1,nbi;istart=(ii-1)*dim
      do jj=1,nbj;jstart=(jj-1)*dim
        do j=1,dim+1;jo=jstart+j
        do i=1,dim+1;io=istart+i
          write(10,*)attr(io,jo),x(io,jo),y(io,jo),z(io,jo)
        enddo;enddo
      enddo;enddo
      close(10)

      open(unit=10,file='surf-out.grd')
c-----read input from surface file
      write(10,21) id
c-----read number of grid lines in x and y
      write(10,*) ni-1,nj-1
c-----read min and max value in x
      write(10,*) xlo,xlo+(ni-1)*dx
c-----read min and max value in y
      write(10,*) ylo,ylo+(nj-1)*dy
c-----read min and max value in z
      write(10,*) zlo,zhi
   
c-----write height
      write(10,*)((z(i,j),i=1,ni-1),j=1,nj-1)
      close(10)

      stop
   21 format(a14)
      end

