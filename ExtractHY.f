      program ExtractHY 
c-----------------------------------------------------------------------
c     program for extracting force history at specific instances
c     the user must give the first and last iteration and the increment 
c     the user must give the number of slices in the spanwise direction
c
c-----------------------------------------------------------------------
      implicit none
      real(kind=8),dimension(:,:),allocatable::slices
      integer:: nstep,nstepstart,nstepfinish,nstepinc,it,l,nn
      integer:: nslice,Nplanes=0
      character(len=128) filename,ofilename
      character(len=1) adum

      print*,'Input filename:'
      read*, filename

      open(unit=10,file=filename)
      open(unit=11,file='mean1HY.dat')

      print*,' Give first, last iteration and increment : '
      read*,nstepstart,nstepfinish,nstepinc

      print*,' Input number of slices : '
      read*,Nplanes
      allocate(slices(4,Nplanes))

c-------read header of file
      read(10,*)adum
      read(10,*)adum
      read(10,*)adum

c-------read spanwise force history
  111 read(10,*,end=666)nstep,((slices(l,nn),l=1,4),nn=1,Nplanes)

      if(nstep.ge.nstepstart.and.nstep.le.nstepfinish.
     &   and.mod(nstep,nstepinc).eq.0)then
        print*,' nstep out ',nstep
        close(11)
        ofilename(1:5)='step.'
        write(ofilename(6:12),'(i7.7)')nstep
        open(unit=11,file=ofilename(1:12))
        do nn=1,Nplanes
          write(11,333)slices(1,nn),slices(2,nn),slices(3,nn)
     &                ,slices(4,nn)
        enddo
      endif
      goto 111
  666 print*,'nstep,Fxm,Fym,Fzm'


      stop
  333 format(1x,4(f30.12))
      end
