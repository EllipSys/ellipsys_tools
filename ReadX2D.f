      program ReadX2D
      implicit none
      integer ni,nblock,msize,msize2
      logical lexist
     
      inquire(file='grid.X2D',exist=lexist) 
      if(lexist)then
        open(unit=10,file='grid.X2D',form='unformatted')
        print*,' Reading grid.X2D : '
        read(10)ni
        read(10)nblock
        goto 100
      endif
      inquire(file='dummy.X2D',exist=lexist) 
      if(lexist)then
        open(unit=10,file='dummy.X2D',form='unformatted')
        print*,' Reading dummy.X2D : '
        read(10)ni
        read(10)nblock
        goto 100
      endif
      inquire(file='grid.x2d',exist=lexist) 
      if(lexist)then
        open(unit=10,file='grid.x2d',form='formatted')
        print*,' Reading grid.x2d    : '
        read(10,*)ni,nblock
        goto 100
      endif
      inquire(file='dummy.x2d',exist=lexist) 
      if(lexist)then
        open(unit=10,file='dummy.x2d   ',form='formatted')
        print*,' Reading dummy.x2d    : '
        read(10,*)ni,nblock
        goto 100
      endif

  100 msize=nblock*ni**2
      msize2=nblock*(ni+3)**2

      print*,' Number of blocks : ',nblock
      print*,' Size of blocks   : ',ni
      print*,' Mesh       size        : ',msize
      print*,' Mesh Ghost size        : ',msize2
      stop
      end
